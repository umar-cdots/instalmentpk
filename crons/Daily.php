<?php
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-09-24 18:21:29
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-25 19:15:25
 */
define('BASEPATH', '../');
define('BASEURL', 'https://demo.instalment.pk/');
define('ENVIRONMENT', 'development');

require_once '../vendor/autoload.php';
require_once '../application/config/database.php';

use Application\Models\Users as User;
use Application\Models\SubscriptionHistory as Subscription;
use Application\Models\SubscriptionPackages as Package;
use Application\Models\ProductOffers as Offer;

class Daily
{
	private static $emailTemplates;

	public static function packageExpirationCheck(Subscription $subscription)
	{
		$response 	= array('is_error' => , 'message' => "", 'data' => []);
		switch($subscription->daysLeft())
		{
			case 0:
				$subscription->expireMe();
				$subscription->vendor->freezeOffers();
				$response['is_error'] = self::notifyUserAboutExpiry($subscription, $subscription->vendor->user);
				
				break;
			case 1:
				$response['is_error']	= self::notifyUserAboutExpiry($subscription, $subscription->vendor->user, 1);
				$response['message']	= sprintf("Expiring in %d Day(s).", $subscription->daysLeft());
				$response['data']		= $subscription->vendor->toArray();
				break;
			case 5:
				$response['is_error']	= self::notifyUserAboutExpiry($subscription, $subscription->vendor->user, 5);
				$response['message']	= sprintf("Expiring in %d Day(s).", $subscription->daysLeft());
				$response['data']		= $subscription->vendor->toArray();
				break;
			default:
				$response['is_error']	= false;
				$response['message']	= sprintf("Subscription Expired.", $subscription->daysLeft());
				$response['data']		= $subscription->vendor->toArray();
				break;
		}
	}

	public static function notifyUserAboutExpiry(Subscription $subscription, User $user, $days = 0)
	{
		if($days > 0)
		{
			if($days > 1)
			{
				$mail 	= new Mailer();
				$mail->addAddress($user->email_address, $user->fullName());
				$mail->isHTML(true);                                  // Set email format to HTML

				$email_template= self::getTemplate('upcoming_subscription_expiry');

				if($email_template)
				{
					$mail->Subject = sprintf($email_template['subject'], $subscription->subscriptionPackage->package_title);
					$mail->Body    = sprintf($email_template['body'], $days, date('F'), date('d'), date('Y'));
					$mail->AltBody = strip_tags($mail->Body);
					return $mail->send() ? true : false;
				}
				return false;
			}
			else
			{
				$mail 	= new Mailer();
				$mail->addAddress($user->email_address, $user->fullName());
				$mail->isHTML(true);                                  // Set email format to HTML

				$email_template = self::getTemplate('tomorrow_subscription_expiry');

				if($email_template)
				{
					$mail->Subject = sprintf($email_template['subject'], $subscription->subscriptionPackage->package_title);
					$mail->Body    = sprintf($email_template['body'], date('F'), date('d'), date('Y'));
					$mail->AltBody = strip_tags($mail->Body);
					return $mail->send() ? true : false;
				}
				return false;
			}
		}
		else
		{
			$mail 	= new Mailer();
			$mail->addAddress($user->email_address, $user->fullName());
			$mail->isHTML(true);                                  // Set email format to HTML

			$email_template = self::getTemplate('tomorrow_subscription_expiry');

			if($email_template)
			{
				$mail->Subject = sprintf($email_template['subject'], $subscription->subscriptionPackage->package_title);
				$mail->Body    = sprintf($email_template['body'], BASEURL . "vendors/packages");
				$mail->AltBody = strip_tags($mail->Body);
				return $mail->send() ? true : false;
			}
			return false;
		}
		//Send Email to $user->email_address

		//Send SMS to $user->vendor->business_phone && $user->vendor->business_phone2
	}

	public static function getTemplate($item)
	{
		$path 	= BASEPATH . 'application' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'email_templates.php';
		if(is_file($path))
		{
			if(count(self::$emailTemplates) > 0)
			{
				return self::$emailTemplates[$item] ?? false;
			}
			else
			{
				$config 	= [];
				require $path;
				if(isset($config['email_templates']))
					self::$emailTemplates 	= $config['email_templates'];
				return self::$emailTemplates[$item] ?? false;
			}
		}
		return false;
	}
}

$response 		= array();
$subscriptions 	= Subscription::where('subscription_status', 'subscribed')->get();

foreach($subscriptions as $subscription)
{
	$response[$subscription->vendor->id]	= array('is_error' => , 'message' => "", 'data' => [])
	$response[$subscription->vendor->id] 	= Daily::packageExpirationCheck($subscription);
}
