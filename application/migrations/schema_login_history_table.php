<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-10 15:44:22
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-05-10 15:52:06
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_login_history_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('login_history', function(Blueprint $table){
			$table->increments('id');
			$table->integer('user_id'); //Foriegn Key from Customers Table
			$table->datetime('login');
			$table->string('ip_address');
			$table->string('user_agent');
			$table->datetime('last_activity');
			$table->datetime('logout');
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('login_history');
	}
}