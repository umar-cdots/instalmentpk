<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_product_to_category_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('product_to_category', function(Blueprint $table){
			$table->increments('id');
			$table->integer('product_id');
			$table->integer('category_id');
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('product_to_category');
	}
}