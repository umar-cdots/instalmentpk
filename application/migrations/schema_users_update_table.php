<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_users_update_table extends CI_Migration
{
	public static function up()
	{
		Schema::table('users', function(Blueprint $table){
			$table->integer('id');
		});
	}

	public static function down()
	{
		Schema::table('users', function(Blueprint $table){
			$table->dropColumn([]);
		});
	}
}