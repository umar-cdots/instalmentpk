<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_users_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('users', function(Blueprint $table){
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email_address');
			$table->string('password');
			$table->string('profile_image_path');
			$table->date('dob');
			$table->integer('vendor_id')->nullable(); //Foriegn Key from Vendors Table 
			$table->integer('customer_id')->nullable(); //Foriegn Key from Customers Table 
			$table->string('email_token')->nullable();
			$table->datetime('email_token_generated')->nullable();
			$table->string('sms_code')->nullable();
			$table->datetime('sms_code_generated')->nullable();
			$table->integer('access_level_bits')->default(0);
			$table->enum('user_type', ['super_admin', 'admin', 'verification_officer', 'vendor', 'customer']);
			$table->enum('status', ['active', 'deactive', 'email_verification_pending', 'sms_verification_pending']);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('users');
	}
}