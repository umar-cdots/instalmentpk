<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-10 15:58:19
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-06-28 18:20:25
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Migration_Migration1 extends CI_Migration
{
	public function up()
	{
		Schema_customers_table::up();
		Schema_guarantors_table::up();
		Schema_login_history_table::up();
		Schema_orders_table::up();
		Schema_phone_verifications_table::up();
		Schema_physical_verifications_table::up();
		Schema_product_categories_table::up();
		Schema_product_images_table::up();
		Schema_product_offers_table::up();
		Schema_product_to_category_table::up();
		Schema_products_table::up();
		Schema_subscription_history_table::up();
		Schema_subscription_packages_table::up();
		Schema_users_table::up();
		Schema_vendors_table::up();
	}

	public function down()
	{
		Schema_customers_table::down();
		Schema_guarantors_table::down();
		Schema_login_history_table::down();
		Schema_orders_table::down();
		Schema_phone_verifications_table::down();
		Schema_physical_verifications_table::down();
		Schema_product_categories_verifications_table::down();
		Schema_product_images_table::down();
		Schema_product_offers_table::down();
		Schema_product_to_category_table::down();
		Schema_products_table::down();
		Schema_subscription_history_table::down();
		Schema_subscription_packages_table::down();
		Schema_users_table::down();
		Schema_vendors_table::down();
	}
}