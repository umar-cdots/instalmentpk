<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_product_categories_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('product_categories', function(Blueprint $table){
			$table->increments('id');
			$table->string('category_title');
			$table->integer('parent_id')->nullable();
			$table->string('icon_class');
			$table->integer('sort')->nullable();
			$table->boolean('show_at_homepage')->default(0);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('product_categories');
	}
}