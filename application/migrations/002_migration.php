<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-10 15:58:19
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-06-28 18:11:56
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint; 
use Illuminate\Database\Migrations\Migration;

class Migration_Migration extends CI_Migration
{
	public function up()
	{
		Schema_customers_new_columns_table::up();
	}

	public function down()
	{
		Schema_customers_new_columns_table::down();
	}
}