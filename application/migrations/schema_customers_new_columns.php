<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-06-05 15:25:29
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-06-28 18:18:41
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_customers_new_columns_table extends CI_Migration
{
	public static function up()
	{
		Schema::table('customers', function(Blueprint $table){
			$table->string('designation');
			$table->string('organization');
			$table->string('city');
			$table->string('postal_code');
		});
	}

	public static function down()
	{
		Schema::dropColumn(['designation', 'organization', 'city', 'postal_code']);
	}
}