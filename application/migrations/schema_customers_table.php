<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_customers_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('customers', function(Blueprint $table){
			$table->increments('id');
			$table->string('primary_phone');
			$table->string('secondary_phone')->nullable();
			$table->string('work_phone')->nullable();
			$table->text('current_address');
			$table->text('permanent_address');
			$table->text('work_address');
			$table->string('cnic_no');
			$table->string('cnic_front_img_path')->nullable();
			$table->string('cnic_back_img_path')->nullable();
			$table->float('customer_lat');
			$table->float('customer_lon');
			$table->enum('marital_status', ['single', 'married', 'divorced', 'widow'])->nullable();
			$table->enum('verification_status', ['phone_verified', 'physical_verified', 'verified', 'unverified']);
			$table->enum('status', ['active', 'deactive', 'blacklisted']);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('customers');
	}
}