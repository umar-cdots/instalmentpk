<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_subscription_packages_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('subscription_packages', function(Blueprint $table){
			$table->increments('id');
			$table->string('package_title');
			$table->text('description');
			$table->float('package_charges'); //Amount in Ruppees
			$table->integer('duration'); //Package duration in Days
			$table->boolean('is_commission');
			$table->float('commision_percentage')->nullable();
			$table->float('fixed_commision_amount')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('subscription_packages');
	}
}