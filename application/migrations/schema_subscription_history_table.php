<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_subscription_history_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('subscription_history', function(Blueprint $table){
			$table->increments('id');
			$table->integer('vendor_id'); //Foriegn Key from Vendors Table
			$table->integer('subscription_package_id'); //Foriegn Key from Subscription Packages Table
			$table->date('subscription_date');
			$table->date('subscription_exp_date');
			$table->enum('subscription_status', ['subscribed', 'payment_due', 'expired', 'renewed']);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('subscription_history');
	}
}