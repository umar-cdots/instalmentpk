<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_orders_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('orders', function(Blueprint $table){
			$table->increments('id');
			$table->integer('customer_id'); //Foreign Key from Customers Table
			$table->integer('vendor_product_offer_id'); //Foreign Key from Vendors Product Offers Table
			$table->text('customer_comment');
			$table->text('vendor_comment');
			$table->text('admin_comment');
			$table->enum('status', ['pending', 'phone_verified', 'physical_verified', 'approved', 'completed']);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('orders');
	}
}