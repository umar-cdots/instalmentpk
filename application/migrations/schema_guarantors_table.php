<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_guarantors_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('guarantors', function(Blueprint $table){
			$table->increments('id');
			$table->integer('customer_id'); //Foriegn Key from Customers Table
			$table->integer('order_id'); //Foriegn Key from Orders Table
			$table->string('first_name');
			$table->string('last_name');
			$table->date('dob');
			$table->string('primary_phone');
			$table->string('secondary_phone');
			$table->text('current_address');
			$table->text('permanent_address');
			$table->text('work_address');
			$table->string('cnic_front');
			$table->string('cnic_back');
			$table->string('relationship');
			$table->enum('marital_status', ['single', 'married', 'divorced', 'widowed']);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('guarantors');
	}
}