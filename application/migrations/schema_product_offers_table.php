<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_product_offers_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('product_offers', function(Blueprint $table){
			$table->increments('id');
			$table->integer('vendor_id'); //Foreign Key from Vendors Table
			$table->integer('product_id'); //Foreign Key from Products Table
			$table->float('total_price_on_cash');
			$table->float('total_price_on_installment');
			$table->float('total_rent_on_installments');
			$table->integer('total_installments');
			$table->integer('down_payment');
			$table->float('amount_per_month');
			$table->text('offer_notes');
			$table->enum('status', ['active', 'pending', 'expired']);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('product_offers');
	}
}