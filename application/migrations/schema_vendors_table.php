<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_vendors_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('vendors', function(Blueprint $table){
			$table->increments('id');
			$table->string('owner_first_name');
			$table->string('owner_last_name');
			$table->string('business_name');
			$table->string('business_email');
			$table->text('business_address');
			$table->string('business_phone');
			$table->string('business_phone2');
			$table->float('vendor_lat');
			$table->float('vendor_lon');
			$table->integer('current_subscription_id'); //Foreign Key from Subscription History Table
			$table->enum('status', ['active', 'deactive']);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('vendors');
	}
}