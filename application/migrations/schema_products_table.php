<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_products_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('products', function(Blueprint $table){
			$table->increments('id');
			$table->integer('category_id'); //Foreign Key from Categories Table
			$table->string('title');
			$table->text('description');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('products');
	}
}