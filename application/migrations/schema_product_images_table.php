<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_product_images_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('product_images', function(Blueprint $table){
			$table->increments('id');
			$table->integer('product_id'); //Foriegn Key from Products Table
			$table->string('image_path');
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('product_images');
	}
}