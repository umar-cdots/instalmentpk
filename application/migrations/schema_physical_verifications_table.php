<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class Schema_physical_verifications_table extends CI_Migration
{
	public static function up()
	{
		Schema::create('physical_verifications', function(Blueprint $table){
			$table->increments('id');
			$table->integer('user_id'); //Foreign Key from Users Table of A User of Type Verification Officer
			$table->integer('order_id'); //Foreign Key from Orders Table
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public static function down()
	{
		Schema::drop('physical_verifications');
	}
}