<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-04 18:06:29
 * @Last Modified by:   Dell
 * @Last Modified time: 2018-08-03 01:18:28
 */

function show_401()
{
	die("<h1>Unauthorized Access</h1><br><a href='".(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url())."'>Go Back</a>");
}
function show_500()
{
	die("<h1>Something Went Wrong.</h1>");
}

function encrypt_me($string = '')
{
	$CI 	= CI_Controller::get_instance();
	return $CI->encryption->encrypt($string);
}

function decrypt_me($string = '')
{
	$CI 	= CI_Controller::get_instance();
	return $CI->encryption->decrypt($string);
}

// function formSubmitTrigger()
// {
// 	$values 		= array();
// 	foreach($_REQUEST as $k => $v)
// 	{
// 		$values[$k]	= $v;
// 	}
// 	$CI 			= &get_instance();
// 	$CI->load->library('session');
// 	$CI->session->set_flashdata('formOldValues', json_encode($values));
// }

// formSubmitTrigger();

// function old($name = '')
// {	
// 	$CI 		= &get_instance();
// 	$CI->load->library('session');
// 	$values 	= json_decode($CI->session->flashdata('formOldValues'), true);
// 	return $values[$name] ?? "";
// }

function is_authless($module, $controller = '', $method = '')
{
	$module_authless 		= false;
	$controller_authless 	= false;
	$method_authless 		= false;

	$CI 		= CI_Controller::get_instance();
	$CI->config->load('authless_paths', TRUE);
	$authless 	= $CI->config->item('authless_paths')['authless'];

	if(!empty($controller))
	{
		if(!empty($method))
		{
			if(in_array($module, array_column($authless, 'module')))
				$module_authless 		= true;
			if(in_array($controller, array_column($authless, 'controller')))
				$controller_authless 	= true;
			if(in_array($method, array_column($authless, 'method')))
				$method_authless 		= true;
		}
		else
		{
			$module_authless 			= true;
			if(in_array($module, array_column($authless, 'module')))
				$module_authless 		= true;
			if(in_array($controller, array_column($authless, 'controller')))
				$controller_authless 	= true;
		}
	}
	else
	{
		$controller_authless 	= true;
		$method_authless 		= true;
		if(in_array($module, array_column($authless, 'module')))
			$module_authless 	= true;
	}

	return ($module_authless && $controller_authless && $method_authless);
}

function getLocality($addresses)
{
	$types 		= array_column($addresses, 'types');
	foreach($types as $i => $type)
	{
		if(in_array('locality', $type))
			return $addresses[$i];
	}
}

function errorBag()
{
	if(!isset(get_instance()->form_validation))
		return '';
	
	get_instance()->form_validation->set_error_delimiters('<li>', '</li>');
	$form_errors = validation_errors();

	if(!empty($form_errors))
	{
		get_instance()->session->set_flashdata('errorBag', $form_errors);
		return $form_errors;
	}

	$flash_errors 	= get_instance()->session->flashdata('errorBag');

	if(empty($flash_errors))
		return '';

	return $flash_errors;
}

function imageToBase64($image)
{
	$base_string 	= "";

	if($image instanceof Application\Models\ProductImages || $image instanceof Application\Models\Uploads)
	{
		$base_string 	= "data:{$image->file_type};base64," . base64_encode(file_get_contents($image->full_path));
	}
	else
	{
		$base_string 	= 'data:image/jpg;base64,' . base64_encode(file_get_contents(_MEDIA_UPLOAD_PATH_ . "no-preview.jpg"));	
	}

	return $base_string;
}

function generateUniqueFilename($name)
{
	$ext 		= pathinfo($name, PATHINFO_EXTENSION);
	$temp_name 	= md5($name . time() . uniqid());
	$full_path 	= _MEDIA_UPLOAD_PATH_ . $temp_name . '.' . $ext;

	if(is_file($full_path))
		generateUniqueFilename($name);

	return $temp_name;
}

function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K') 
{
	$theta 	= $lon1 - $lon2;
	$dist 	= sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist 	= acos($dist);
	$dist 	= rad2deg($dist);
	$miles 	= $dist * 60 * 1.1515;
	$unit 	= strtoupper($unit);

	if ($unit == 'K') 
	{
  		return ($miles * 1.609344);
	} 
	else if ($unit == 'N') 
	{
  		return ($miles * 0.8684);
	} 
	return $miles;
}

function slugify($text)
{
	// replace non letter or digits by -
	$text = preg_replace('~[^\pL\d]+~u', '-', $text);
	// transliterate
	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
	// remove unwanted characters
	$text = preg_replace('~[^-\w]+~', '', $text);
	// trim
	$text = trim($text, '-');
	// remove duplicate -
	$text = preg_replace('~-+~', '-', $text);
	// lowercase
	$text = strtolower($text);

	if (empty($text)) 
	{
		return time().uniqid();
	}
	return $text;
}

$emailTemplates = array();
function getEmailTemplate($item)
{
	$path 	= BASEPATH . '../application' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'email_templates.php';
	if(is_file($path))
	{
		global $emailTemplates;
		if(is_array($emailTemplates) && count($emailTemplates) > 0)
		{
			return $emailTemplates[$item] ?? false;
		}
		else
		{
			$config 	= [];
			require $path;
			if(isset($config['email_templates']))
				$emailTemplates 	= $config['email_templates'];
			return $emailTemplates[$item] ?? false;
		}
	}
	return false;
}