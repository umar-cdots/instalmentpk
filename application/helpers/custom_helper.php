<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-04 18:06:29
 * @Last Modified by:   Dell
 * @Last Modified time: 2018-08-03 01:18:28
 */

function show_401()
{
    die("<h1>Unauthorized Access</h1><br><a href='" . (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url()) . "'>Go Back</a>");
}

function show_500()
{
    die("<h1>Something Went Wrong.</h1>");
}

function encrypt_me($string = '')
{
    $CI = CI_Controller::get_instance();
    return $CI->encryption->encrypt($string);
}

function decrypt_me($string = '')
{
    $CI = CI_Controller::get_instance();
    return $CI->encryption->decrypt($string);
}

// function formSubmitTrigger()
// {
// 	$values 		= array();
// 	foreach($_REQUEST as $k => $v)
// 	{
// 		$values[$k]	= $v;
// 	}
// 	$CI 			= &get_instance();
// 	$CI->load->library('session');
// 	$CI->session->set_flashdata('formOldValues', json_encode($values));
// }

// formSubmitTrigger();

// function old($name = '')
// {	
// 	$CI 		= &get_instance();
// 	$CI->load->library('session');
// 	$values 	= json_decode($CI->session->flashdata('formOldValues'), true);
// 	return $values[$name] ?? "";
// }

function is_authless($module, $controller = '', $method = '')
{
    $module_authless = false;
    $controller_authless = false;
    $method_authless = false;

    $CI = CI_Controller::get_instance();
    $CI->config->load('authless_paths', TRUE);
    $authless = $CI->config->item('authless_paths')['authless'];

    if (!empty($controller)) {
        if (!empty($method)) {
            if (in_array($module, array_column($authless, 'module')))
                $module_authless = true;
            if (in_array($controller, array_column($authless, 'controller')))
                $controller_authless = true;
            if (in_array($method, array_column($authless, 'method')))
                $method_authless = true;
        } else {
            $module_authless = true;
            if (in_array($module, array_column($authless, 'module')))
                $module_authless = true;
            if (in_array($controller, array_column($authless, 'controller')))
                $controller_authless = true;
        }
    } else {
        $controller_authless = true;
        $method_authless = true;
        if (in_array($module, array_column($authless, 'module')))
            $module_authless = true;
    }

    return ($module_authless && $controller_authless && $method_authless);
}

function getLocality($addresses)
{
    $types = array_column($addresses, 'types');
    foreach ($types as $i => $type) {
        if (in_array('locality', $type))
            return $addresses[$i];
    }
}

function getAdministratorLocality($addresses)
{
    $types = array_column($addresses, 'types');
    foreach ($types as $i => $type) {
        if (in_array('administrative_area_level_2', $type))
            return $addresses[$i];
    }
}

function errorBag()
{
    if (!isset(get_instance()->form_validation))
        return '';

    get_instance()->form_validation->set_error_delimiters('<li>', '</li>');
    $form_errors = validation_errors();

    if (!empty($form_errors)) {
        get_instance()->session->set_flashdata('errorBag', $form_errors);
        return $form_errors;
    }

    $flash_errors = get_instance()->session->flashdata('errorBag');

    if (empty($flash_errors))
        return '';

    return $flash_errors;
}

function imageToBase64($image)
{
    $base_string = "";

    if ($image instanceof Application\Models\ProductImages || $image instanceof Application\Models\Uploads) {
        $base_string = "data:{$image->file_type};base64," . base64_encode(file_get_contents($image->full_path));
    } else {
        $base_string = 'data:image/jpg;base64,' . base64_encode(file_get_contents(_MEDIA_UPLOAD_PATH_ . "no-preview.jpg"));
    }

    return $base_string;
}

function generateUniqueFilename($name)
{
    $ext = pathinfo($name, PATHINFO_EXTENSION);
    $temp_name = md5($name . time() . uniqid());
    $full_path = _MEDIA_UPLOAD_PATH_ . $temp_name . '.' . $ext;

    if (is_file($full_path))
        generateUniqueFilename($name);

    return $temp_name;
}

function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K')
{
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == 'K') {
        return ($miles * 1.609344);
    } else if ($unit == 'N') {
        return ($miles * 0.8684);
    }
    return $miles;
}

function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    // trim
    $text = trim($text, '-');
    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);
    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return time() . uniqid();
    }
    return $text;
}

$emailTemplates = array();
function getEmailTemplate($item)
{
    $path = BASEPATH . '../application' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'email_templates.php';
    if (is_file($path)) {
        global $emailTemplates;
        if (is_array($emailTemplates) && count($emailTemplates) > 0) {
            return $emailTemplates[$item] ?? false;
        } else {
            $config = [];
            require $path;
            if (isset($config['email_templates']))
                $emailTemplates = $config['email_templates'];
            return $emailTemplates[$item] ?? false;
        }
    }
    return false;
}

function totalOffersByVendor($vendor_id)
{
    return Application\Models\ProductOffers::where('vendor_id', $vendor_id)->count();
}

function totalOffersByProduct($vendor_id, $product_id)
{
    return Application\Models\ProductOffers::where(['vendor_id' => $vendor_id, 'product_id' => $product_id])->count();
}

function sendSMS($destinationNumber, $messageContent)
{
    try {


        ini_set('soap.wsdl_cache_enabled', 0);
        ini_set('soap.wsdl_cache_ttl', 0);

        $opts = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'ciphers' => 'RC4-SHA',
                'allow_self_signed' => true
            ),
            'http' => array(
                'user_agent' => 'PHPSoapClient'
            ),
            'https' => array(
                'user_agent' => 'PHPSoapClient'
            ),

        );

        $context = stream_context_create($opts);

        $wsdlUrl = 'http://cbs.zong.com.pk/reachcwsv2/corporatesms.svc?wsdl';


        $soapClientOptions = array(
            'trace' => 1,
            'exceptions' => 1,
            'stream_context' => $context,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'encoding' => 'UTF-8',
            'verify_peer' => false,
            'verifyhost' => false,

            /*   'location'       => $wsdlUrl,*/

        );

        $credentials = array(
            'loginId' => '923124742135',
            'loginPassword' => 'Zong@123'
        );
        libxml_disable_entity_loader(false); //adding this worked for me
        $client = new SoapClient($wsdlUrl, $soapClientOptions);

        /*$client = new SoapClient(null, array('location' => $wsdlUrl,
            'uri'      => $wsdlUrl,
            'stream_context' => stream_context_create($opts)));*/



        $smsParameters = array(
            'obj_QuickSMS' => array(
                'loginId' => '923124742135',
                'loginPassword' => 'Zong@123',
                'Destination' => $destinationNumber,
                'Mask' => 'Instalment',
                'Message' => $messageContent,
                'Unicode' => '0',
                'ShortCodePrefered' => 'n',
            )
        );



        /*$summaryParameters = array(
            'obj_GetAccountSummary' => $credentials
        );*/


        /*   $result = $client->GetAccountSummary($summaryParameters);*/

        $result = $client->QuickSMS($smsParameters);

        echo '<pre>';
        print_r($result);
        echo '</pre>';
        exit;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function getVendor($id)
{
    return \Application\Models\Vendors::find($id);
}

function isCategoryPage()
{
    $CI = get_instance();
    $CI->load->helper('url');

    if(strpos(current_url(), "/category/"))
        return true;
    return false;
}

function getCategorySlugBySegment()
{
    $CI = get_instance();
    // $CI->load->helper('uri');

    return $CI->uri->segment(2);
}

function getCategoryName($slug)
{
    return Application\Models\ProductCategories::where('slug', getCategorySlugBySegment())->first()->category_title;
}

function getCategoryIdBySlug()
{
    return Application\Models\ProductCategories::where('slug', getCategorySlugBySegment())->first()->id;
}

function isBrandPage()
{
    $CI = get_instance();
    $CI->load->helper('url');

    if(strpos(current_url(), "/brand/"))
        return true;
    return false;
}

function getBrandSlugBySegment()
{
    $CI = get_instance();
    // $CI->load->helper('uri');

    return $CI->uri->segment(2);
}

function getBrandIdBySlug()
{
    return Application\Models\Brands::where('slug', getBrandSlugBySegment())->first()->id;
}

function getCurrentUrl()
{
    get_instance()->load->helper('url');
    $currentURL = current_url();
    //print_r($currentURL);
}

function getCurrentRequestURI()
{
    return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}

function getAvailableKeyFeatureValues($key_feature_id)
{
    return Application\Models\ProductsMeta::where('key_feature_id', $key_feature_id)->groupBy(['key_feature_id', 'meta_value'])->get();
}