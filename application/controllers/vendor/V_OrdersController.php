<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Nadeem
 * @Date:   2018-09-19
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-24 11:51:54
 */
 
class V_OrdersController extends VendorController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Orders", 'is_active' => true, 'url' => site_url(_VENDOR_ROUTE_PREFIX_ . '/orders')];
		$this->productImage 		= null;
	}
	
	function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$Orders = Application\Models\Orders::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->where('status', '!=', 'pending')->orderBy('id', 'DESC')->get();	
		
		echo $this->Blade->make('vendor.orders.index', ['breadcrumbs' => $this->Breadcrumbs, 'orders' => $Orders]);
	}
	
	function customlist($types)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$Orders = Application\Models\Orders::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->orderBy('id', 'DESC')->get();	
		$Orders = $Orders->where('verification_type', $types);
		
		echo $this->Blade->make('vendor.orders.index', ['breadcrumbs' => $this->Breadcrumbs, 'orders' => $Orders]);
	}
	
	function show($id)
	{
		
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$Orders = Application\Models\Orders::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->orderBy('id', 'DESC')->get();	
		$Orders = $Orders->find($id);
		
		echo $this->Blade->make('vendor.orders.show', ['breadcrumbs' => $this->Breadcrumbs, 'orders' => $Orders]);
	
	}
	
	function edit($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
		
		$Orders = Application\Models\Orders::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->orderBy('id', 'DESC')->get();	
		$Orders = $Orders->find($id);
		
		if(!is_null($Orders))
		{
			echo $this->Blade->make('vendor.orders.edit', ['breadcrumbs' => $this->Breadcrumbs, 'orders' => $Orders]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	
	public function update($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
		
		//die($id);
		
		$Orders = Application\Models\Orders::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->orderBy('id', 'DESC')->get();	
		$Orders = $Orders->find($id);
		
		if(!is_null($Orders))
		{
			$this->form_validation->set_rules('Status','Status', 'required');
			$this->form_validation->set_rules('vendor_comment','Comment','trim');
			if ($this->form_validation->run() === true)
			{
				//$user_id 						            = $this->input->post('user_id');
				$status						= $this->input->post('Status');
				$vendor_comment				= $this->input->post('vendor_comment');

			    $Orders->status				= $status;
			    $Orders->vendor_comment		= $vendor_comment;

			    $Orders->save();

			    $this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Order Updated Successfully!");
			    redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/orders'));
			    die();
			} else {
				$this->session->set_flashdata('status', "error");
				$this->session->set_flashdata('message', "Something Went Wrong!");
				errorBag();
				redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/orders/edit/' . $id));
				die();	
			}
		} 
		else 
		{
			show_404();
			die();
		}
	}
	
	function phone_verification_form()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
		echo $this->Blade->make('vendor.orders.phone_verification_form', ['breadcrumbs' => $this->Breadcrumbs]);
	}
	
	public function phone_verification_add($types)
	{
		
		//var_dump($_POST);
		$Orders = Application\Models\Orders::where('phoneverifications_id', LoginInfo::getInstance()->getUserTypeID())->orderBy('id', 'DESC')->get();	
		$Orders = $Orders->where('verification_type', $types);
		//var_dump($Orders);
		
		if(count($_POST) > 0)
		{
			foreach($_POST as $keys => $valus)
			{
				$order_ID = 1;
				insert_phone($order_ID,$keys,$valus);
			}
		}
	}
	
	
	function physical_verification_form()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
		
		echo $this->Blade->make('vendor.orders.physical_verification_form', ['breadcrumbs' => $this->Breadcrumbs]);
	}


	public function verificationInfo($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(true);

		$order = Application\Models\Orders::find($id);
		
		if(!is_null($order) && $order->vendor_id == LoginInfo::getInstance()->getUserTypeID() && $order->status != 'pending')
		{
			echo $this->Blade->make('vendor.orders.verification_info', ['breadcrumbs' => $this->Breadcrumbs, 'order' => $order]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
}