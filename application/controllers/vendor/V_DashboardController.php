<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-05-11 19:48:44
 */

class V_DashboardController extends VendorController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Dashboard", 'is_active' => true, 'url' => site_url('vendor/dashboard')];
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		echo $this->Blade->make('vendor.dashboard.index', ['breadcrumbs' => $this->Breadcrumbs]);
	}
}