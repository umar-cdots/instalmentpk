<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-05-11 19:48:44
 */

class V_DashboardController extends VendorController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Dashboard", 'is_active' => true, 'url' => site_url('vendor/dashboard')];
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$total_orders             = Application\Models\Orders::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->count();	
		$total_comp_orders        = Application\Models\Orders::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->where('status', 'completed')->get()->count();	
		$total_offers             = Application\Models\ProductOffers::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->count();
		$users                    = Application\Models\Users::where('user_type', 'customer')->get();

		$feature_offers             = (int)LoginInfo::getInstance()->getUser()->vendor->feature_count - Application\Models\ProductOffers::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->where('is_featured', true)->count();

		echo $this->Blade->make('vendor.dashboard.index', ['breadcrumbs' => $this->Breadcrumbs, 'total_orders' => $total_orders, 'total_offers' => $total_offers,'total_comp_orders' => $total_comp_orders, 'feature_offers'=> $feature_offers]);
	}
}