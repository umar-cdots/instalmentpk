-<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-09 18:21:43
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-07-24 12:17:20
 */
use Application\Models\Users;
use \Application\Models\SubscriptionPackages;

class V_AuthController extends VendorController
{
	private $logo;

	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Auth", 'is_active' => true, 'url' => site_url(_VENDOR_ROUTE_PREFIX_ . '/login')];
		$this->load->library('form_validation');
		$this->load->helper('form');
	}

	public function login()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		
		if(LoginInfo::getInstance()->isLogin())
		{
			redirect(_VENDOR_ROUTE_PREFIX_ . '/dashboard');
			die;
		}

		echo $this->Blade->make('vendor.auth.login');
	}

	public function tryLogin()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(true);

		$email 						= $this->input->post('email_address');
		$password 					= $this->input->post('password');
		$remember_me 				= $this->input->post('remember_me');

		if($this->User = LoginInfo::getInstance()->attemptLogin($email, $password, $remember_me, _VENDOR_))
		{
			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Login Successful!");

			redirect(_VENDOR_ROUTE_PREFIX_ . '/dashboard');
			die;
		}

		$this->session->set_flashdata('status', "error");
		$this->session->set_flashdata('message', "Invalid Email or Password.");

		redirect(_VENDOR_ROUTE_PREFIX_ . '/login');
		die;
	}

	public function logout()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(LoginInfo::getInstance()->isLogin())
		{
			if(LoginInfo::getInstance()->destroySession())
			{
				$this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Logout Successfully!");
			}
		}
		redirect(_VENDOR_ROUTE_PREFIX_ . '/login');
		die;
	}

	public function register()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(!LoginInfo::getInstance()->isLogin())
		{
			if($this->input->get('package_id') > 0 && in_array($this->input->get('package_id'), SubscriptionPackages::pluck('id')->toArray()))
			{
				echo $this->Blade->make('vendor.auth.register', ['packages' => SubscriptionPackages::pluck('package_title', 'id'), 'package_id' => $this->input->get('package_id')]);
			}
			else
			{
				redirect(_VENDOR_ROUTE_PREFIX_ . '/packages');
				die;
			}
		}
		else
		{
			redirect(_VENDOR_ROUTE_PREFIX_ . '/dashboard');
			die;
		}
	}

	
	public function saveRegisteration()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(!LoginInfo::getInstance()->isLogin())
		{
			if(empty($this->input->get('package_id')) || !in_array($this->input->get('package_id'), SubscriptionPackages::pluck('id')->toArray()))
			{
				redirect(_VENDOR_ROUTE_PREFIX_ . '/packages');
				die;
			}
			$package_id = $this->input->get('package_id');

			$this->form_validation->set_rules('cnic', 'CNIC No', 'trim|required|regex_match[/[0-9]{5}\-[0-9]{7}\-[0-9]{1}/]');
			$this->form_validation->set_rules('designation', 'Designation', 'trim|required');
			$this->form_validation->set_rules('organization','Organization', 'trim|required');
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email|is_unique[users.email_address]');
			$this->form_validation->set_rules('primary_phone', 'Telephone', 'trim|required|regex_match[/((\+92)|(0092))\d{3}\d{7}/]');
			$this->form_validation->set_rules('current_address', 'Address', 'trim|required');
			//$this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
			$this->form_validation->set_rules('operational_area', 'Operational Area', 'trim|required');
			$this->form_validation->set_rules('business_address', 'Business Address', 'trim|required');
			$this->form_validation->set_rules('business_name', 'Business Name', 'trim|required');
			$this->form_validation->set_rules('logo', 'Logo', 'callback_validateLogo');
			//$this->form_validation->set_rules('vendor_description', 'Vendor Description', 'trim|required');
			$this->form_validation->set_rules('city', 'City', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('confirm_password', 'confirm_password', 'required|matches[password]');
			$this->form_validation->set_rules('agreed', 'Terms & Conditions', 'required');
			
			$this->form_validation->set_rules('userLat', 'userLat');
			$this->form_validation->set_rules('userLon', 'userLon');
			
			$this->form_validation->set_rules('description', 'Description');
			$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">x </a><strong>', '</strong></div>');
			
			//var_dump($_POST);
			//die();
			
			if ($this->form_validation->run() === true)
			{
				$cnic 						= $this->input->post('cnic');
				$designation 			    = $this->input->post('designation');
				$organization 				= $this->input->post('organization');
				$fname 				        = $this->input->post('first_name');
				$lname 				        = $this->input->post('last_name');
				$email 				        = $this->input->post('email_address');
				$phone 				        = $this->input->post('primary_phone');
				$address 				    = $this->input->post('current_address');
				$vendor_description 		= $this->input->post('vendor_description');
				$city 				    	= $this->input->post('city');
				$postal_code 				= $this->input->post('postal_code');
				$operational_area 		    = $this->input->post('operational_area');
				$business_address 		    = $this->input->post('business_address');
				$vendor_description 		= $this->input->post('vendor_description');
				$logo 				        = $this->input->post('logo');
				$business_name 				= $this->input->post('business_name');
				$slug 						= slugify($this->input->post('business_name') . uniqid());
				$feature_count 				= _FEARURELIMIT_;

				
				$userLat = $this->input->post('userLat');
				$userLon = $this->input->post('userLon');
				
				$password = encrypt_me($this->input->post('password'));
				
				$vendor = array(
				'owner_first_name' => $fname,
				'owner_last_name' => $lname,
				'business_email' => $email,
				'business_address' => $address,
				'vendor_description' => $vendor_description,
				'city' => $city,
				'designation' => $designation,
				'organization' => $organization,
				'operational_area' => $operational_area,
				'business_address' => $business_address,
				'vendor_description' => $vendor_description,
				'business_name' => $business_name,
				'slug'             => $slug,
				'feature_count'    => $feature_count,
				'vendor_lat' => $userLat,
				'vendor_lon' => $userLon,
				'business_phone' => $phone,
				'created_at' => time()
				);
				
				$this->db->insert('vendors', $vendor);
				$VendorID = $this->db->insert_id();
				
				$data = array(
				'first_name' => $fname,
				'last_name' => $lname,
				'email_address' => $email,
				'password' => $password ,
				'postal_code' => $postal_code ,
				'upload_id' => $this->logo->id ,
				'user_type' => 'vendor',
				'status' => 'email_verification_pending',
				'vendor_id' => $VendorID,
				'access_level_bits' => _VENDOR_,
				'created_at' => time(),
				'cnic'		=> $cnic,
				);
				
				$this->db->insert('users', $data);
				$UserID = $this->db->insert_id();

				/* Code From Packages Controller */
				$user_id 	= $UserID;
				$vendorID 	= $VendorID;
				$packageID 	= $package_id;
				
				$this->db->set('subscription_status', 'expired');
				$this->db->where('vendor_id', $vendorID);
				$this->db->where('subscription_status','subscribed');
				$this->db->update('subscription_history');
				
				$this->db->where('id',$packageID);
				$packageInfo = $this->db->get('subscription_packages');
				$packageInfo = $packageInfo->result_array();
				$packageInfo = $packageInfo[0];
				
				$data = array();
				$data['vendor_id'] = $vendorID;
				$data['subscription_package_id'] = $packageInfo['id'];
				$data['subscription_status'] = 'subscribed';
				$data['subscription_date'] = date('Y-m-d');
				
				$stop_date = date('Y-m-d', strtotime('+'.$packageInfo['duration'].' day'));
				$data['subscription_exp_date'] = $stop_date;		
				$this->db->insert('subscription_history', $data);
				$product_id = $this->db->insert_id();

				$subscription = \Application\Models\SubscriptionHistory::find($product_id);
				$subscription->vendor->unFreezeOffers(); //UN-Freezing Vendor Offers
				
				
				$ledger = new \Application\Models\Ledger();
				$ledger->user_id = $user_id;
				$ledger->product_id = $product_id;
				$ledger->description = $packageInfo['description'];
				$ledger->amount = $packageInfo['package_charges'];
				$ledger->type = 'subscription';
				$ledger->payment_type = 'debit';
				$ledger->save();

				/* End */


				$this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Vendor Registered Successfully!");
				//die('Here');
				LoginInfo::getInstance()->attemptLogin($email, $this->input->post('password'), false, _VENDOR_);
				
				redirect(_VENDOR_ROUTE_PREFIX_ . '/thanks?package_id=' . $package_id);
				die;
			
			} 
			else 
			{
				echo $this->Blade->make('vendor.auth.register', ['packages' => SubscriptionPackages::pluck('package_title', 'id'), 'package_id' => $this->input->get('package_id')]);
			}
			//var_dump($_POST);		
		}
		else
		{
			redirect(_VENDOR_ROUTE_PREFIX_ . '/dashboard');
			die;
		}
	}

	public function profile()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(true);
		echo $this->Blade->make('vendor.auth.profile', ['user' => LoginInfo::getInstance()->getUser()->vendor]);
	}

	public function updateProfile()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(true);

		$user 							= LoginInfo::getInstance()->getUser();
		$vendor 				        = LoginInfo::getInstance()->getUser()->vendor;

		$owner_first_name 			 = $this->input->post('owner_first_name');
        $owner_last_name 			 = $this->input->post('owner_last_name');
        $business_name 				 = $this->input->post('business_name');
        $business_address 			 = $this->input->post('business_address');
        $business_phone 			 = $this->input->post('business_phone');
        $operational_area 			 = $this->input->post('operational_area');
		// $password 					 = $this->input->post('password');
		$new_password 				 = $this->input->post('new_password');
		$confirm_password 			 = $this->input->post('confirm_password');

	
		if(true /*$password == decrypt_me($user->password)*/)
		{
			$vendor->owner_first_name 	    = $owner_first_name;
			$vendor->owner_last_name 	    = $owner_last_name;
			$vendor->business_name 	        = $business_name;
			$vendor->business_address 	    = $business_address;
			$vendor->business_phone 	    = $business_phone;
			$vendor->operational_area 	    = $operational_area;

			$user->first_name 	    = $owner_first_name;
			$user->last_name 	    = $owner_last_name;

			if(!empty($new_password) && $new_password == $confirm_password)
			{
				$user->password = encrypt_me($new_password);
			}

			$user->save();
			$vendor->save();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Profile Updated Successfully!");
		}
		else
		{
			//echo $password , decrypt_me($user->password);
			//die();
			$this->session->set_flashdata('status', "error");
			$this->session->set_flashdata('message', "Invalid Current Password Required.");
		}

		echo $this->Blade->make('vendor.auth.profile', ['user' => LoginInfo::getInstance()->getUser()->vendor]);
	}
	public function forget()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(!LoginInfo::getInstance()->isLogin())
		{
			echo $this->Blade->make('vendor.auth.forget');
		}
	}
	public function Recoverpassword()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(!LoginInfo::getInstance()->isLogin())
		{
			if (!empty($this->input->post('email_address')) && filter_var($this->input->post('email_address'), FILTER_VALIDATE_EMAIL))
			{
			    $email 				        = $this->input->post('email_address');

		        $user = Application\Models\Users::where('email_address', $email)
		        									->where('user_type', 'vendor')
		        										->where('status', 'active')
		        											->first();
				if(!empty($user))
				{ 
				    $email_template = getEmailTemplate('password_request');

					if($email_template)
					{
				        $this->load->library('email');
				        $this->email->initialize($this->config->item('email'));

				        $email_body = $this->Blade->make('email.notification',  ['subject' => $email_template['subject'], 'recepient_name' => $user->fullName(), 'content' => sprintf($email_template['body'], decrypt_me($user->password), base_url('/login'), "click here")]);

				        $this->email->from($this->config->item('email')['smtp_user'], 'InstalmentPK');
				        $this->email->to($user->email_address, $user->fullName());
				        $this->email->subject($email_template['subject']);
				        $this->email->message($email_body);

						if($this->email->send())
						{
							$this->session->set_flashdata('status', "success");
							$this->session->set_flashdata('message', "Please Check Your Email!");
							redirect(_VENDOR_ROUTE_PREFIX_ . '/login');
							die;
						}
						else
						{
							$this->session->set_flashdata('status', "error");
							$this->session->set_flashdata('message', "Something Went Wrong.");
						}
					}
					else
					{
						$this->session->set_flashdata('status', "error");
						$this->session->set_flashdata('message', "Something Went Wrong.");
					}
				}
				else
				{
					$this->session->set_flashdata('status', "error");
					$this->session->set_flashdata('message', "Invalid Email Address.");
				}
			}	
			else
			{
				$this->session->set_flashdata('status', "error");
				$this->session->set_flashdata('message', "Email Address Required." . $this->input->post('email_address'));
			}	
			redirect(_VENDOR_ROUTE_PREFIX_ . '/forget');
			die;

		} 
		else
		{
			redirect(_VENDOR_ROUTE_PREFIX_ . '/dashboard');
			die;
		}
	}

    public function validateLogo()
    {
    	$field_name 	= 'logo';
    	if(isset($_FILES[$field_name]['name']) && !empty($_FILES[$field_name]['name']))
    	{
	    	$config 						= array();
	    	$config['upload_path']          = _MEDIA_UPLOAD_PATH_;
	        $config['allowed_types']        = 'gif|jpg|jpeg|png';
	        $config['file_ext_tolower']     = true;
	        $config['file_name']        	= generateUniqueFilename($_FILES[$field_name]['name']);
	        $config['max_size']             = 5120;
	        // $config['max_width']            = 1024;
	        // $config['max_height']           = 768;

	        $this->load->library('upload', $config);

	        if(!$this->upload->do_upload($field_name))
	        {
	            $this->form_validation->set_message('validateLogo', $this->upload->display_errors());
				return false;
	        }
	        $data 						= $this->upload->data();

	        $logo 						= new Application\Models\Uploads;
		    $logo->file_name			= $data['file_name'];
		    $logo->file_type			= $data['file_type'];
		    $logo->file_path			= $data['file_path'];
		    $logo->full_path			= $data['full_path'];
		    $logo->raw_name				= $data['raw_name'];
		    $logo->orig_name			= $data['orig_name'];
		    $logo->client_name			= $data['client_name'];
		    $logo->file_ext				= $data['file_ext'];
		    $logo->file_size			= $data['file_size'];
		    $logo->image_width			= $data['image_width'];
		    $logo->image_height			= $data['image_height'];
		    $logo->image_type			= $data['image_type'];
		    $logo->save();

	        $this->logo 				= $logo;
	        return true;
    	}
    	elseif($this->input->post('upload_id'))
    	{
    		$upload_id	= (int)$this->input->post('upload_id');
    		$this->logo = Application\Models\Uploads::find($upload_id);

    		if(empty($this->logo))
    		{
    			$this->form_validation->set_message('validateLogo', "Something Went Wrong.");
    			return false;
    		}
    	}

    	// $this->form_validation->set_message('validateLogo', "Seller Logo is Required.");
        // return false;
        return true;
    }	
	
}