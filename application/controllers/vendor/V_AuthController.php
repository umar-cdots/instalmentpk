<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-09 18:21:43
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-07-24 12:17:20
 */
use Application\Models\Users;
use \Application\Models\SubscriptionPackages;

class V_AuthController extends VendorController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Auth", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/login')];
		$this->load->library('form_validation');
		$this->load->helper('form');
	}

	public function login()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		
		if(LoginInfo::getInstance()->isLogin())
		{
			redirect(_VENDOR_ROUTE_PREFIX_ . '/dashboard');
			die;
		}

		echo $this->Blade->make('vendor.auth.login');
	}

	public function tryLogin()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(true);

		$email 						= $this->input->post('email_address');
		$password 					= $this->input->post('password');
		$remember_me 				= $this->input->post('remember_me');

		if($this->User = LoginInfo::getInstance()->attemptLogin($email, $password, $remember_me, _VENDOR_))
		{
			redirect(_VENDOR_ROUTE_PREFIX_ . '/dashboard');
			die;
		}
		
		redirect('vendor/login');
		die;
	}

	public function logout()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(LoginInfo::getInstance()->isLogin())
		{
			if(LoginInfo::getInstance()->destroySession())
			{
				$this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Logout Successfully!");
			}
		}
		redirect(_VENDOR_ROUTE_PREFIX_ . '/login');
		die;
	}

	public function register()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(!LoginInfo::getInstance()->isLogin())
		{
			echo $this->Blade->make('vendor.auth.register', ['packages' => SubscriptionPackages::pluck('package_title', 'id')]);
		}
		else
		{
			redirect(_VENDOR_ROUTE_PREFIX_ . '/dashboard');
			die;
		}
	}

	
	public function saveRegisteration()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(!LoginInfo::getInstance()->isLogin())
		{
			$this->form_validation->set_rules('cnic', 'CNIC No', 'trim|required|regex_match[/[0-9]{5}\-[0-9]{7}\-[0-9]{1}/]');
			$this->form_validation->set_rules('designation', 'Designation', 'trim|required');
			$this->form_validation->set_rules('organization','Organization', 'trim|required');
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email|is_unique[users.email_address]');
			$this->form_validation->set_rules('primary_phone', 'Telephone', 'trim|required');
			$this->form_validation->set_rules('current_address', 'Address', 'trim|required');
			$this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('confirm_password', 'confirm_password', 'required|matches[password]');
			$this->form_validation->set_rules('agreed', 'Terms & Conditions', 'required');
			
			$this->form_validation->set_rules('userLat', 'userLat');
			$this->form_validation->set_rules('userLon', 'userLon');
			
			$this->form_validation->set_rules('description', 'Description');
			$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">x </a><strong>', '</strong></div>');
			
			//var_dump($_POST);
			//die();
			
			if ($this->form_validation->run() === true)
			{
				$cnic 						= $this->input->post('cnic');
				$designation 			    = $this->input->post('designation');
				$organization 				= $this->input->post('organization');
				$fname 				        = $this->input->post('first_name');
				$lname 				        = $this->input->post('last_name');
				$email 				        = $this->input->post('email_address');
				$phone 				        = $this->input->post('primary_phone');
				$address 				    = $this->input->post('current_address');
				$postal_code 				= $this->input->post('postal_code');
				
				$userLat = $this->input->post('userLat');
				$userLon = $this->input->post('userLon');
				
				$password = encrypt_me($this->input->post('password'));
				
				$vendor = array(
				'owner_first_name' => $fname,
				'owner_last_name' => $lname,
				'business_email' => $email,
				'business_address' => $address,
				'vendor_lat' => $userLat,
				'vendor_lon' => $userLon,
				'business_phone' => $phone,
				'created_at' => time()
				);
				
				$this->db->insert('vendors', $vendor);
				$VendorID = $this->db->insert_id();
				
				$data = array(
				'first_name' => $fname,
				'last_name' => $lname,
				'email_address' => $email,
				'password' => $password ,
				'user_type' => 'vendor',
				'status' => 'active',
				'vendor_id' => $VendorID,
				'access_level_bits' => _VENDOR_,
				'created_at' => time()
				);
				
				$this->db->insert('users', $data);
				$UserID = $this->db->insert_id();

				$this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Customer Registered Successfully!");
				
				LoginInfo::getInstance()->attemptLogin($email, $this->input->post('password'), false, _VENDOR_);
				
				redirect(_VENDOR_ROUTE_PREFIX_ . '/packages');
				die;
			
			} else {
				echo $this->Blade->make('vendor.auth.register', ['packages' => SubscriptionPackages::pluck('package_title', 'id')]);
			}
			//var_dump($_POST);		
		}
		else
		{
			redirect(_VENDOR_ROUTE_PREFIX_ . '/dashboard');
			die;
		}
	}

	public function profile()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(true);
		echo $this->Blade->make('vendor.auth.profile', ['user' => LoginInfo::getInstance()->getUser()->vendor]);
	}

	public function updateProfile()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(true);

		$user 							= LoginInfo::getInstance()->getUser();
		$vendor 				        = LoginInfo::getInstance()->getUser()->vendor;

		$owner_first_name 			 = $this->input->post('owner_first_name');
        $owner_last_name 			 = $this->input->post('owner_last_name');
        $business_name 				 = $this->input->post('business_name');
        $business_address 			 = $this->input->post('business_address');
        $business_phone 			 = $this->input->post('business_phone');
        $operational_area 			 = $this->input->post('operational_area');
		$password 					 = $this->input->post('password');
		$new_password 				 = $this->input->post('new_password');
		$confirm_password 			 = $this->input->post('confirm_password');

	
		if($password == decrypt_me($user->password))
		{
			$vendor->owner_first_name 	    = $owner_first_name;
			$vendor->owner_last_name 	    = $owner_last_name;
			$vendor->business_name 	        = $business_name;
			$vendor->business_address 	    = $business_address;
			$vendor->business_phone 	    = $business_phone;
			$vendor->operational_area 	    = $operational_area;

			$user->first_name 	    = $owner_first_name;
			$user->last_name 	    = $owner_last_name;

			if($new_password == $confirm_password)
			{
				$user->password = encrypt_me($new_password);
			}

			$user->save();
			$vendor->save();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Profile Updated Successfully!");
		}
		else
		{
			//echo $password , decrypt_me($user->password);
			//die();
			$this->session->set_flashdata('status', "error");
			$this->session->set_flashdata('message', "Invalid Current Password Required.");
		}

		echo $this->Blade->make('vendor.auth.profile', ['user' => LoginInfo::getInstance()->getUser()->vendor]);
	}
	public function forget()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(!LoginInfo::getInstance()->isLogin())
		{
			echo $this->Blade->make('vendor.auth.forget');
		}
	}
	public function Recoverpassword()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(!LoginInfo::getInstance()->isLogin())
		{
			if (!empty($this->input->post('email_address')) && filter_var($this->input->post('email_address'), FILTER_VALIDATE_EMAIL))
			{
			    $email 				        = $this->input->post('email_address');

		        $user = Application\Models\Users::where('email_address', $email)
		        									->where('user_type', 'vendor')
		        										->where('status', 'active')
		        											->first();
				if(!empty($user))
				{ 
					$mail 			= new Mailer();
					$mail->addAddress($user->email_address, $user->fullName());
					$mail->isHTML(true);

				    $email_template = getEmailTemplate('password_request');

					if($email_template)
					{
						$mail->Subject 	= $email_template['subject'];
						$mail->Body    	= $this->Blade->make('email.notification', ['subject' => $email_template['subject'], 'recepient_name' => $user->fullName(), 'content' => sprintf($email_template['body'], decrypt_me($user->password), base_url(_VENDOR_ROUTE_PREFIX_ . '/login'), "click here")]);
						$mail->AltBody 	= strip_tags($mail->Body);

						if($mail->send())
						{
							$this->session->set_flashdata('status', "success");
							$this->session->set_flashdata('message', "Please Check Your Email!");
							redirect(_VENDOR_ROUTE_PREFIX_ . '/login');
							die;
						}
						else
						{
							$this->session->set_flashdata('status', "error");
							$this->session->set_flashdata('message', "Something Went Wrong.");
						}
					}
					else
					{
						$this->session->set_flashdata('status', "error");
						$this->session->set_flashdata('message', "Something Went Wrong.");
					}
				}
				else
				{
					$this->session->set_flashdata('status', "error");
					$this->session->set_flashdata('message', "Invalid Email Address.");
				}
			}	
			else
			{
				$this->session->set_flashdata('status', "error");
				$this->session->set_flashdata('message', "Email Address Required." . $this->input->post('email_address'));
			}	
			redirect(_VENDOR_ROUTE_PREFIX_ . '/forget');
			die;

		} 
		else
		{
			redirect(_VENDOR_ROUTE_PREFIX_ . '/dashboard');
			die;
		}
	}
	
}