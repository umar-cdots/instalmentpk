<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-08-09 06:30:33
 */

class V_OffersController extends VendorController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Offers", 'is_active' => true, 'url' => site_url(_VENDOR_ROUTE_PREFIX_ . '/offers')];
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->productImage 		= null;
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$offers = Application\Models\ProductOffers::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->latest('id')->get();	

		echo $this->Blade->make('vendor.offers.index', ['breadcrumbs' => $this->Breadcrumbs, 'offers' => $offers]);
	}

	public function create()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Create", 'is_active' => true, 'url' => site_url(_VENDOR_ROUTE_PREFIX_ . '/offers/create')];

    	$products = Application\Models\Products::orderBy('title', 'ASC')->get();

		echo $this->Blade->make('vendor.offers.create', ['breadcrumbs' => $this->Breadcrumbs, 'products' => $products]);
    }

    public function store()
    { 
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

	    //form validation
	    $this->form_validation->set_rules('product_id', 'Product', 'callback_validateProduct');
		$this->form_validation->set_rules('total_installments[]', 'No. of Instalments', 'required');
		$this->form_validation->set_rules('total_price_on_installment[]', 'Total Amount', 'required');
		$this->form_validation->set_rules('down_payment[]', 'Down Payment', 'required');
		$this->form_validation->set_rules('amount_per_month[]','Per Month', 'required');
		$this->form_validation->set_rules('offer_notes','Offer Notes', 'trim');

	   	if ($this->form_validation->run() === true)
		{
			$product_id 				= $this->input->post('product_id');
			$total_installments 		= $this->input->post('total_installments');
			$total_price_on_installment = $this->input->post('total_price_on_installment');
			$down_payment 				= $this->input->post('down_payment');
			$amount_per_month 			= $this->input->post('amount_per_month');
			$offer_notes 				= $this->input->post('offer_notes');
			// $meta_keys 					= $this->input->post('meta_key');
			// $meta_values 				= $this->input->post('meta_value');

			$now 						= Carbon\Carbon::now()->toDateTimeString();
			$vendor_id					= LoginInfo::getInstance()->getUserTypeID();

			$product_metas 				= \Application\Models\Products::find($product_id)->meta;

			$metas 						= array();
	    	// foreach($meta_keys as $i => $key)
	    	foreach($product_metas as $meta)
	    	{
	    		// if(empty($key) || empty($meta_values[$i]))
	    			// continue;
	    		// $metas[]				= ['meta_key' => $key, 'meta_value' => $meta_values[$i], 'created_at' => $now, 'updated_at' => $now];
	    		$metas[]				= ['key_feature_id' => $meta->key_feature_id, 'meta_key' => $meta->meta_key, 'meta_value' => $meta->meta_value];
	    	}

			$offers_array 				= array();

			foreach($total_installments as $i => $instalment)
			{
				$offers_array			= array(
					'vendor_id'						=> $vendor_id,
					'product_id'					=> $product_id,
					'total_installments'			=> (int) $instalment,
					'total_price_on_installment'	=> (float) $total_price_on_installment[$i],
					'down_payment'					=> (float) $down_payment[$i],
					'amount_per_month'				=> (float) $amount_per_month[$i],
					'offer_notes'					=> $offer_notes,
					'slug'							=> slugify(Application\Models\Products::find($product_id)->title .'-'. substr(uniqid(rand(10000,99999).microtime()), 0, 8)),
					'status'						=> "active",
					'created_at'					=> $now,
					'updated_at'					=> $now,
				);
				$offer_id = Application\Models\ProductOffers::insertGetId($offers_array);
				foreach($metas as $i => $meta)
				{
					$metas[$i]['product_offer_id']	= $offer_id;
				}
				Application\Models\OffersMeta::insert($metas);
			}

		    $this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Offer Saved Successfully!");
		    redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/offers'));
		    die();
		}
		$this->session->set_flashdata('status', "error");
		$this->session->set_flashdata('message', "Something Went Wrong!");
        redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/offers/create'));
        die();	
    }
    public function show($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "View", 'is_active' => true, 'url' => site_url(_VENDOR_ROUTE_PREFIX_ . '/offers/show/' . $id)];

		$offer = Application\Models\ProductOffers::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->where('id', $id)->first();

		if(!is_null($offer))
		{
			echo $this->Blade->make('vendor.offers.show', ['breadcrumbs' => $this->Breadcrumbs, 'offer' => $offer]);
		} 
		else 
		{
			show_404();
			die();
		}

    }
    public function edit($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Edit", 'is_active' => true, 'url' => site_url(_VENDOR_ROUTE_PREFIX_ . '/offers/edit/' . $id)];

		$offer 	= Application\Models\ProductOffers::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->where('id', $id)->first();

		if(!is_null($offer))
		{
    		$products 	= Application\Models\Products::orderBy('title', 'ASC')->get();

			echo $this->Blade->make('vendor.offers.edit', ['breadcrumbs' => $this->Breadcrumbs, 'offer' => $offer, 'products' => $products]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	public function update($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$offer 	= Application\Models\ProductOffers::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->where('id', $id)->first();

		if(!is_null($offer))
		{
		    //form validation
		    $this->form_validation->set_rules('product_id', 'Product', 'callback_validateProduct');
			$this->form_validation->set_rules('total_installments', 'No. of Instalments', 'required');
			$this->form_validation->set_rules('total_price_on_installment', 'Total Amount', 'required');
			$this->form_validation->set_rules('down_payment', 'Down Payment', 'required');
			$this->form_validation->set_rules('amount_per_month','Per Month', 'required');
			$this->form_validation->set_rules('offer_notes','Offer Notes', 'trim');

		   	if ($this->form_validation->run() === true)
			{
				$product_id 				= $this->input->post('product_id');
				$total_installments 		= $this->input->post('total_installments');
				$total_price_on_installment = $this->input->post('total_price_on_installment');
				$down_payment 				= $this->input->post('down_payment');
				$amount_per_month 			= $this->input->post('amount_per_month');
				$offer_notes 				= $this->input->post('offer_notes');
				$meta_keys 					= $this->input->post('meta_key');
				$meta_values 				= $this->input->post('meta_value');

				$offer->product_id 					= $product_id;
				$offer->total_installments 			= (int) $total_installments;
				$offer->total_price_on_installment 	= (float) $total_price_on_installment;
				$offer->down_payment 				= (float) $down_payment;
				$offer->amount_per_month 			= (float) $amount_per_month;
				$offer->offer_notes 				= $offer_notes;

			    $metas 								= array();
		    	foreach($meta_keys as $i => $key)
		    	{
		    		if(empty($key) || empty($meta_values[$i]))
		    			continue;
		    		$metas[]						= new Application\Models\OffersMeta(['meta_key' => $key, 'meta_value' => $meta_values[$i]]);
		    	}
		    	$offer->meta()->delete();
		    	$offer->meta()->saveMany($metas);

				$offer->save();

			    $this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Offer Saved Successfully!");
			    redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/offers'));
			    die();
			}
		    $this->session->set_flashdata('status', "error");
			$this->session->set_flashdata('message', "Something Went Wrong!");
	        redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/offers/create'));
	        die();	
    	} 
		else 
		{
			show_404();
			die();
		}
    }

	public function destroy($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$offer 	= Application\Models\ProductOffers::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->where('id', $id)->first();

		if(!is_null($offer))
		{
			$offer->delete();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Offer Removed Successfully!");
			redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/offers'));
			die();

		} 
		else 
		{
			show_404();
			die();
		}
	}	
    
	public function feature($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$offer 	= Application\Models\ProductOffers::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->where('id', $id)->first();

		if(!is_null($offer))
		{
			$feature_offers         = Application\Models\ProductOffers::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->where('is_featured', true)->count();
			
			if($feature_offers < LoginInfo::getInstance()->getUser()->vendor->feature_count)
			{
				$offer->is_featured 	= true;
				$offer->featured_from 	= Carbon\Carbon::now();
				$offer->featured_to 	= Carbon\Carbon::now()->addMonths(1);
				$offer->save();

				$this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Offer Marked as Featured!");
			}
			else
			{
				$this->session->set_flashdata('status', "error");
				$this->session->set_flashdata('message', "You reached your maximum limit of " . LoginInfo::getInstance()->getUser()->vendor->feature_count . " features.");
			}

			$this->load->library('user_agent');
			if ($this->agent->referrer())
			{
				redirect($this->agent->referrer());
				die;
			}

			redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/offers/' . $id));
			die();

		} 
		else 
		{
			show_404();
			die();
		}
	}
    
	public function unFeature($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$offer 	= Application\Models\ProductOffers::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->where('id', $id)->first();

		if(!is_null($offer))
		{
			$offer->is_featured 	= false;
			$offer->featured_from 	= "";
			$offer->featured_to 	= "";
			$offer->save();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Offer Unfeatured Successfully!");

			$this->load->library('user_agent');
			if ($this->agent->referrer())
			{
				redirect($this->agent->referrer());
				die;
			}

			redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/offers/' . $id));
			die();
		} 
		else 
		{
			show_404();
			die();
		}
	}

    public function validateProduct()
    {
    	$product_id 	= (int)$this->input->post("product_id");
    	if(!empty(Application\Models\Products::find($product_id)))
    		return true;

    	$this->form_validation->set_message('validateProduct', 'Product does not Exist.');
    	return false;
    }
}