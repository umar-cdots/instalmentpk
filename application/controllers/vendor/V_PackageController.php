<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-24 19:16:21
 */
use \Application\Models\SubscriptionPackages;
use \Application\Models\Ledger;

class V_PackageController extends VendorController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Packages", 'is_active' => true, 'url' => site_url(_VENDOR_ROUTE_PREFIX_ . '/offers')];
		$this->load->library('form_validation');
		$this->load->helper('form');
	}
	
	function lists()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		// $this->isAuthentic(false);
		
		// if($this->input->get('packageid') > 0)
		// {
		// 	$user_id 	= LoginInfo::getInstance()->getUser()->id;
		// 	$vendorID 	= LoginInfo::getInstance()->getUser()->vendor->id;
		// 	$packageID 	= $this->input->get('packageid');
			
		// 	$this->db->set('subscription_status', 'expired');
		// 	$this->db->where('vendor_id', $vendorID);
		// 	$this->db->where('subscription_status','subscribed');
		// 	$this->db->update('subscription_history');
			
		// 	$this->db->where('id',$packageID);
		// 	$packageInfo = $this->db->get('subscription_packages');
		// 	$packageInfo = $packageInfo->result_array();
		// 	$packageInfo = $packageInfo[0];
			
		// 	$data = array();
		// 	$data['vendor_id'] = $vendorID;
		// 	$data['subscription_package_id'] = $packageInfo['id'];
		// 	$data['subscription_status'] = 'subscribed';
		// 	$data['subscription_date'] = date('Y-m-d');
			
		// 	$stop_date = date('Y-m-d', strtotime('+'.$packageInfo['duration'].' day'));
		// 	$data['subscription_exp_date'] = $stop_date;		
		// 	$this->db->insert('subscription_history', $data);
		// 	$product_id = $this->db->insert_id();

		// 	$subscription = \Application\Models\SubscriptionHistory::find($product_id);
		// 	$subscription->vendor->unFreezeOffers(); //UN-Freezing Vendor Offers
			
			
		// 	$ledger = new Ledger();
		// 	$ledger->user_id = $user_id;
		// 	$ledger->product_id = $product_id;
		// 	$ledger->description = $packageInfo['description'];
		// 	$ledger->amount = $packageInfo['package_charges'];
		// 	$ledger->type = 'subscription';
		// 	$ledger->payment_type = 'debit';
		// 	$ledger->save();
			
		// 	//Ledger::add_ledger($user_id,$product_id, $packageInfo['description'],$packageInfo['package_charges'],'subscription','debit');
			
		// 	redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/thanks/?packageid='.$packageID));
		//     die();
			
		// }
		
		echo $this->Blade->make('vendor.packages.list', ['Packages' => SubscriptionPackages::all()]);
	}
	
	public function thanks()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		echo $this->Blade->make('vendor.packages.thanks', ['Package' => SubscriptionPackages::findOrFail($this->input->get('package_id'))]);
	}
}