<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-10-01 17:41:59
 */

class V_AjaxController extends VendorController
{
	private $response = [];

	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->response 			= array('status' => "", 'message' => "", 'data' => []);
		header('Content-Type: application/json');
	}

	public function __destruct()
	{
		echo json_encode($this->response);
	}

	public function getProductMeta($id)
	{
		$product = Application\Models\Products::find($id);

		if($product->count() > 0)
		{
			$this->response['status'] 	= "success";
			$this->response['message'] 	= "Returning Product's Meta.";
			$this->response['data']		= array(
												'meta' => $product->meta->toArray()
											);
		}
		else
		{
			$this->response['status'] 	= "error";
			$this->response['message'] 	= "Product Does Not Exist.";
		}
	}

	public function unsubscribe($id)
	{
		$subscription = Application\Models\SubscriptionHistory::find($id);

		if($subscription->count() > 0)
		{
			if($subscription->vendor_id == LoginInfo::getInstance()->getUserTypeID())
			{
				if($subscription->subscription_status == "subscribed")
				{
					$subscription->subscription_status 	= "unsubscribed";
					$subscription->save();

					$subscription->vendor->freezeOffers(); //Freezing Vendor Offers

					$this->response['status'] 	= "success";
					$this->response['message'] 	= "Unsubscribed Successfully.";
				}
				else
				{
					$this->response['status'] 	= "error";
					$this->response['message'] 	= "You're already Unsubscribed.";
				}

				
			}
			else
			{
				$this->response['status'] 	= "error";
				$this->response['message'] 	= "Something Went Wrong.";
			}
		}
		else
		{
			$this->response['status'] 	= "error";
			$this->response['message'] 	= "Something Went Wrong.";
		}
	}
}