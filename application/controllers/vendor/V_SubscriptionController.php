<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Nadeem
 * @Date:   2018-09-19
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-24 11:51:54
 */
 
class V_SubscriptionController extends VendorController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Subscriptions", 'is_active' => true, 'url' => site_url(_VENDOR_ROUTE_PREFIX_ . '/subscription')];
		$this->productImage 		= null;
	}
	
	function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$subscription = Application\Models\SubscriptionHistory::where('vendor_id', LoginInfo::getInstance()->getUserTypeID())->orderBy('id', 'DESC')->get();	

		echo $this->Blade->make('vendor.subscription.index', ['breadcrumbs' => $this->Breadcrumbs, 'subscription' => $subscription]);
	}
}