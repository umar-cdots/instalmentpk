<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-09 16:06:07
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-19 15:41:07
 */
use \Application\Models\Users;

abstract class VendorController extends CI_Controller
{
	protected $CurrentController;
	protected $CurrentModule;
	protected $CurrentMethod;

	public function __construct()
	{
		parent::__construct();
		$this->CurrentModule = "vendor";
		LoginInfo::getInstance()->setWhoShouldI(_VENDOR_);
	}

	private function isAuthLess()
	{
		if(is_authless($this->CurrentModule, $this->CurrentController, $this->CurrentMethod))
		{
			return true;
		}
		return false;
	}

	public function isAuthentic($redirect = true)
	{
		if(!$this->isAuthLess())
		{
			if(!LoginInfo::getInstance()->isLogin())
			{
				redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/login'));
				die;
			}
			elseif(!LoginInfo::getInstance()->hasAccess(_VENDOR_) && !$redirect)
			{
				$redirect ? redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/login')) : show_401();	
				die;
			}
			if($this->uri->segment(2) != 'packages' && $this->uri->segment(2) != 'thanks')
			{
				if(LoginInfo::getInstance()->getUser()->vendor->lastSubscription->subscription_status  == 'subscribed')
				{
					return true;
				}
				redirect(base_url(_VENDOR_ROUTE_PREFIX_ . '/packages'));
				die;
			}
			else
			{
				return true;
			}
		}
		else
		{ 
			return true;
		}
	}
}