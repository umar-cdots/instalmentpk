<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-10-04 16:09:24
 */

class BrandsController extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
	}

	public function show($slug)
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$brand 	= Application\Models\Brands::where('slug', $slug)
												->first();
		if(!empty($brand))
		{
			$offers 	= Application\Models\ProductOffers::ambiguityFreeSelection()
																->singleCheapest()
																	->activeOffers()
																		->where('brand_id', $brand->id)
																			->orderBy('product_offers.id', 'DESC')
																				->paginate(8);
																					// ->get();

			echo $this->Blade->make('customer.brand.show', ['brand' => $brand, 'offers' => $offers]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
}