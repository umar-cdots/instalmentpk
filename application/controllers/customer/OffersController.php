<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-10-05 20:12:39
 */
use Application\Models\ProductOffers;
use Application\Models\Orders;

class OffersController extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
	}

	public function show($slug)
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$offer 	= ProductOffers::ambiguityFreeSelection()
									->where('product_offers.slug', $slug)
										->activeOffers()
											->first();

		if(!empty($offer))
		{
			$filtered_offers 	= ProductOffers::where('product_offers.product_id', $offer->product_id)
													->where('product_offers.id', '<>', $offer->id)
														->limit(5)
															->nearestOffers()
																->activeOffers()
																	->get();

			echo $this->Blade->make('customer.offer.show', ['offer' => $offer, 'filtered_offers' => $filtered_offers]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	
	public function showWithFilters($slug)
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$offer 	= ProductOffers::ambiguityFreeSelection()
										->where('product_offers.slug', $slug)
											->activeOffers()
												->first();

		$min_down_payment 		= $this->input->post('min_down_payment');
		$max_down_payment 		= $this->input->post('max_down_payment');
		$max_total_installments = $this->input->post('max_total_installments');
		$min_amount_per_month 	= $this->input->post('min_amount_per_month');
		$max_amount_per_month 	= $this->input->post('max_amount_per_month');
		$sort_by 				= $this->input->post('sort_by');

		if(!empty($offer))
		{
			
			if($this->input->get('grabit') == 'yes')
			{
				$offerDeatils 			= serialize($offer);
				$customerID 			= LoginInfo::getInstance()->getUserTypeID();
				$vendorID 				= $offer->vendor->id;
			 	$offerID 				= $offer->id; 
				$productID 				= $offer->product->id;
				$verificationType 		= $offer->vendor->currentSubscription->subscriptionPackage->verification_type;
				
				$AddOrder = new Orders();
				$AddOrder->customer_id = $customerID;
				$AddOrder->vendor_id = $vendorID;
				$AddOrder->product_id = $productID;
				$AddOrder->product_offer_id = $offerID;
				$AddOrder->product_offer_state = $offerDeatils;
				$AddOrder->customer_comment = $this->input->post('comment');
				$AddOrder->verification_type =$verificationType;
				$AddOrder->status = 'pending';
				$AddOrder->save();
				
				redirect(base_url('/offer/thanks'));
			    die();
			}
			
			$filtered_offers 	= ProductOffers::where('product_offers.product_id', $offer->product_id)
									->where('product_offers.id', '<>', $offer->id)
										->limit(5)
											->nearestOffers()
												->activeOffers();

			if(!empty($min_down_payment) && !empty($max_down_payment))
				$filtered_offers 	= $filtered_offers->whereBetween('down_payment', [$min_down_payment, $max_down_payment]);
			elseif(!empty($min_down_payment) && empty($max_down_payment))
				$filtered_offers 	= $filtered_offers->where('down_payment', '>=', $min_down_payment);
			elseif(empty($min_down_payment) && !empty($max_down_payment))
				$filtered_offers 	= $filtered_offers->where('down_payment', '<=', $max_down_payment);

			if(!empty($max_total_installments))
				$filtered_offers	= $filtered_offers->where('total_installments', '<=', $max_total_installments);

			if(!empty($min_amount_per_month) && !empty($max_amount_per_month))
				$filtered_offers 	= $filtered_offers->whereBetween('amount_per_month', [$min_amount_per_month, $max_amount_per_month]);
			elseif(!empty($min_amount_per_month) && empty($max_amount_per_month))
				$filtered_offers 	= $filtered_offers->where('amount_per_month', '>=', $min_amount_per_month);
			elseif(empty($min_amount_per_month) && !empty($max_amount_per_month))
				$filtered_offers 	= $filtered_offers->where('amount_per_month', '<=', $max_amount_per_month);

			if($sort_by == 'popular')
				$filtered_offers	= $filtered_offers->topOffers();

			echo $this->Blade->make('customer.offer.show', ['offer' => $offer, 'filtered_offers' => $filtered_offers->get(), 'min_amount_per_month' => $min_amount_per_month, 'max_amount_per_month' => $max_amount_per_month, 'max_total_installments' => $max_total_installments, 'min_down_payment' => $min_down_payment, 'max_down_payment' => $max_down_payment, 'sort_by' => $sort_by]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	
	public function thanks()
	{
		echo $this->Blade->make('customer.offer.thanks');
		//echo 'Thanks';
	}

	public function wishList()
	{
		$wishlist 		= $this->session->wishlist ?? [];
		$offers 		= ProductOffers::whereIn('id', $wishlist)
											->get();
		echo $this->Blade->make('customer.offer.wishlist', ['wishlist' => $wishlist, 'offers' => $offers]);
	}
}