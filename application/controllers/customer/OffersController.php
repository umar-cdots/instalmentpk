<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2019-04-11 12:54:22
 */
use Application\Models\ProductOffers;
use Application\Models\Orders;

class OffersController extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
	}

	public function show($slug)
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$offer 	= ProductOffers::ambiguityFreeSelection()
									->where('product_offers.slug', $slug)
										->activeOffers()
										     ->limit(10)
											    ->first();

		if(!empty($offer))
		{
			if($this->input->get('grabit') == 'yes')
			{
				$offerDeatils 			= serialize($offer);
				$customerID 			= LoginInfo::getInstance()->getUserTypeID();
				$vendorID 				= $offer->vendor->id;
			 	$offerID 				= $offer->id; 
				$productID 				= $offer->product->id;
				$verificationType 		= $offer->vendor->currentSubscription->subscriptionPackage->verification_type;
				
				$AddOrder = new Orders();
				$AddOrder->customer_id = $customerID;
				$AddOrder->vendor_id = $vendorID;
				$AddOrder->product_id = $productID;
				$AddOrder->product_offer_id = $offerID;
				$AddOrder->product_offer_state = $offerDeatils;
				$AddOrder->customer_comment = $this->input->post('comment');
				$AddOrder->verification_type =$verificationType;
				$AddOrder->status = 'pending';
				$AddOrder->save();
				
				redirect(base_url('/offer/thanks'));
			    die();
			}

			$other_offers 		= ProductOffers::where(['product_id' => $offer->product_id, 'vendor_id' => $offer->vendor_id])->where('id', '!=', $offer->id)->get();

			$filtered_offers 	= ProductOffers::ambiguityFreeSelection()
													->where('product_offers.product_id', $offer->product_id)
														->where('product_offers.id', '<>', $offer->id)
															->where('product_offers.vendor_id', '<>', $offer->vendor_id)
																->limit(10000)
																	->nearestOffers()
																		->activeOffers()
																		  //->orderBy('total_installments')
																		   ->orderByCheapest()
																			->get();
			$filtered_first 	= [];/*ProductOffers::ambiguityFreeSelection()
													->where('product_offers.product_id', $offer->product_id)
														->where('product_offers.id', '<>', $offer->id)
															->where('product_offers.vendor_id', '<>', $offer->vendor_id)
																->limit(10000)
																	->nearestOffers()
																		->activeOffers()
																		  //->orderBy('total_installments')
																		   ->orderByCheapest()
																			->first();*/

			$max_adv_limit = 100000;//$filtered_offers->count() > 0 ? $filtered_offers->sortByDesc('down_payment')->first()->down_payment : 200000;
			$max_emi_limit = 50000;//$filtered_offers->count() > 0 ? $filtered_offers->sortByDesc('amount_per_month')->first()->amount_per_month : 200000;

			echo $this->Blade->make('customer.offer.show', ['offer' => $offer, 'other_offers' => $other_offers, 'filtered_offers' => $filtered_offers, 'filtered_first' => $filtered_first, 'min_amount_per_month' => 0, 'max_amount_per_month' => 200000, 'min_down_payment' => 0, 'max_down_payment' => 200000, 'max_emi_limit' => $max_emi_limit, 'max_adv_limit' => $max_adv_limit]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	
	public function showWithFilters($slug)
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$offer 	= ProductOffers::ambiguityFreeSelection()
										->where('product_offers.slug', $slug)
											->activeOffers()
												->first();

		$min_down_payment 		= $this->input->post('min_down_payment');
		$max_down_payment 		= $this->input->post('max_down_payment');
		$max_total_installments = $this->input->post('max_total_installments');
		$min_amount_per_month 	= $this->input->post('min_amount_per_month');
		$max_amount_per_month 	= $this->input->post('max_amount_per_month');
		$sort_by 				= $this->input->post('sort_by');

		if(!empty($offer))
		{
			if($this->input->get('grabit') == 'yes')
			{
				$offerDeatils 			= serialize($offer);
				$customerID 			= LoginInfo::getInstance()->getUserTypeID();
				$vendorID 				= $offer->vendor->id;
			 	$offerID 				= $offer->id; 
				$productID 				= $offer->product->id;
				$verificationType 		= $offer->vendor->currentSubscription->subscriptionPackage->verification_type;
				
				$AddOrder = new Orders();
				$AddOrder->customer_id = $customerID;
				$AddOrder->vendor_id = $vendorID;
				$AddOrder->product_id = $productID;
				$AddOrder->product_offer_id = $offerID;
				$AddOrder->product_offer_state = $offerDeatils;
				$AddOrder->customer_comment = $this->input->post('comment');
				$AddOrder->verification_type =$verificationType;
				$AddOrder->status = 'pending';
				$AddOrder->save();
				
				redirect(base_url('/offer/thanks'));
			    die();
			}
			
			$other_offers 		= ProductOffers::where(['product_id' => $offer->product_id, 'vendor_id' => $offer->vendor_id])->where('id', '!=', $offer->id)->get();
			
			$filtered_offers 	= ProductOffers::ambiguityFreeSelection()
									->where('product_offers.product_id', $offer->product_id)
										->where('product_offers.id', '<>', $offer->id)
										->where('product_offers.vendor_id', '<>', $offer->vendor_id)
											->limit(10000)
												->nearestOffers()
													->activeOffers();

			if(!empty($min_down_payment) && !empty($max_down_payment))
				$filtered_offers 	= $filtered_offers->whereBetween('down_payment', [$min_down_payment, $max_down_payment]);
			elseif(!empty($min_down_payment) && empty($max_down_payment))
				$filtered_offers 	= $filtered_offers->where('down_payment', '>=', $min_down_payment);
			elseif(empty($min_down_payment) && !empty($max_down_payment))
				$filtered_offers 	= $filtered_offers->where('down_payment', '<=', $max_down_payment);

			if(!empty($max_total_installments))
				$filtered_offers	= $filtered_offers->where('total_installments', '<=', $max_total_installments);

			if(!empty($min_amount_per_month) && !empty($max_amount_per_month))
				$filtered_offers 	= $filtered_offers->whereBetween('amount_per_month', [$min_amount_per_month, $max_amount_per_month]);
			elseif(!empty($min_amount_per_month) && empty($max_amount_per_month))
				$filtered_offers 	= $filtered_offers->where('amount_per_month', '>=', $min_amount_per_month);
			elseif(empty($min_amount_per_month) && !empty($max_amount_per_month))
				$filtered_offers 	= $filtered_offers->where('amount_per_month', '<=', $max_amount_per_month);

			if($sort_by == 'popular')
				$filtered_offers	= $filtered_offers->topOffers();
			elseif($sort_by == 'latest')
				$filtered_offers	= $filtered_offers->latest('product_offers.id');
			elseif($sort_by == 'lowest')
				$filtered_offers	= $filtered_offers->orderByCheapest();
			else
				$filtered_offers	= $filtered_offers->orderByHighest();

			$filtered_offers 		= $filtered_offers->get();

			$max_adv_limit = 100000;//$filtered_offers->count() > 0 ? $filtered_offers->sortByDesc('down_payment')->first()->down_payment : 200000;
			$max_emi_limit = 50000;//$filtered_offers->count() > 0 ? $filtered_offers->sortByDesc('amount_per_month')->first()->amount_per_month : 200000;

			echo $this->Blade->make('customer.offer.show', ['offer' => $offer, 'other_offers' => $other_offers, 'filtered_offers' => $filtered_offers, 'min_amount_per_month' => $min_amount_per_month, 'max_amount_per_month' => $max_amount_per_month, 'max_total_installments' => $max_total_installments, 'min_down_payment' => $min_down_payment, 'max_down_payment' => $max_down_payment, 'sort_by' => $sort_by, 'max_emi_limit' => $max_emi_limit, 'max_adv_limit' => $max_adv_limit]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	
	public function thanks()
	{
		echo $this->Blade->make('customer.offer.thanks');
		//echo 'Thanks';
	}

	public function wishList()
	{
		$wishlist 		= $this->session->wishlist ?? [];
		$offers 		= ProductOffers::whereIn('id', $wishlist)
											->get();
		echo $this->Blade->make('customer.offer.wishlist', ['wishlist' => $wishlist, 'offers' => $offers]);
	}
}