<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Dell
 * @Last Modified time: 2018-08-02 06:36:20
 */

class HomepageController extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 

		// $featuredOffers 	= Application\Models\ProductOffers::nearCustomer()->where('is_featured', 1)->get();
		// echo "<pre>";print_r($featuredOffers->toArray());die;

// 		$aa = Application\Models\Products::topAndNearProducts();
// 		// $aa = $aa->with(['productOffers', 'orders'])->get();//Application\Models\Products::topAndNearProducts();
// echo "<pre>";print_r($aa->toArray());
// die;
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
        $data['bodyClass'] = "HomePage";
		echo $this->Blade->make('customer.index',$data);
	}

	public function sendVendorSMS()
	{
		sendSMS('923040556099', 'Testing Message');
	}
}