<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-10-05 19:34:25
 */

class ProductsController extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
	}

	public function show($slug)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		
		$product 	= Application\Models\Products::where('slug', $slug)
													->first();
		if(!empty($product))
		{
			$offers 	= Application\Models\ProductOffers::ambiguityFreeSelection()
																	->nearestOffers()
																	->activeOffers()
																		->where('product_offers.product_id', $product->id)
																		  //->orderBy('total_installments')
																			->orderByCheapest()
																				->get();
			$max_adv_limit = $offers->count() > 0 ? $offers->sortByDesc('down_payment')->first()->down_payment : 200000;
			$max_emi_limit = $offers->count() > 0 ? $offers->sortByDesc('amount_per_month')->first()->amount_per_month : 200000;																

			echo $this->Blade->make('customer.product.show', ['product' => $product, 'offers' => $offers ,  'min_amount_per_month' => 0, 'max_amount_per_month' => 200000, 'min_down_payment' => 0, 'max_down_payment' => 200000,  'max_emi_limit' => $max_emi_limit, 'max_adv_limit' => $max_adv_limit]);
		} 
		else 
		{
			show_404();
			die();
		}
	}

	public function showOffers($slug)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		
		$product_offer 	= Application\Models\ProductOffers::where('slug', $slug)
													->first();
		if(!empty($product_offer))
		{//dd($product_offer->product_id);die;
			$offers 	= Application\Models\ProductOffers::ambiguityFreeSelection()
																	->nearestOffers()
																		->activeOffers()
																			->where(['product_offers.product_id' => $product_offer->product_id, 'product_offers.vendor_id' => $product_offer->vendor_id])
																				//->orderBy('total_installments')
																				  ->orderByCheapest()
																					->get();
			$max_adv_limit = $offers->count() > 0 ? $offers->sortByDesc('down_payment')->first()->down_payment : 200000;
			$max_emi_limit = $offers->count() > 0 ? $offers->sortByDesc('amount_per_month')->first()->amount_per_month : 200000;

			echo $this->Blade->make('customer.product.offers', ['product_offer' => $product_offer, 'offers' => $offers, 'min_amount_per_month' => 0, 'max_amount_per_month' => 200000, 'min_down_payment' => 0, 'max_down_payment' => 200000, 'max_emi_limit' => $max_emi_limit, 'max_adv_limit' => $max_adv_limit]);
		} 
		else 
		{
			show_404();
			die();
		}
	}


	public function showWithFilters($slug)
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$product 	= Application\Models\Products::where('slug', $slug)
													->first();

		$min_down_payment 		= $this->input->post('min_down_payment');
		$max_down_payment 		= $this->input->post('max_down_payment');
		$max_total_installments = $this->input->post('max_total_installments');
		$min_amount_per_month 	= $this->input->post('min_amount_per_month');
		$max_amount_per_month 	= $this->input->post('max_amount_per_month');
		$sort_by 				= $this->input->post('sort_by');

		if(!empty($product))
		{
			$offers 	= Application\Models\ProductOffers::ambiguityFreeSelection()
																->nearestOffers()
																	->activeOffers()
																		->where('product_offers.product_id', $product->id);

			if(!empty($min_down_payment) && !empty($max_down_payment))
				$offers 	= $offers->whereBetween('down_payment', [$min_down_payment, $max_down_payment]);
			elseif(!empty($min_down_payment) && empty($max_down_payment))
				$offers 	= $offers->where('down_payment', '>=', $min_down_payment);
			elseif(empty($min_down_payment) && !empty($max_down_payment))
				$offers 	= $offers->where('down_payment', '<=', $max_down_payment);

			if(!empty($max_total_installments))
				$offers	= $offers->where('total_installments', '<=', $max_total_installments);

			if(!empty($min_amount_per_month) && !empty($max_amount_per_month))
				$offers 	= $offers->whereBetween('amount_per_month', [$min_amount_per_month, $max_amount_per_month]);
			elseif(!empty($min_amount_per_month) && empty($max_amount_per_month))
				$offers 	= $offers->where('amount_per_month', '>=', $min_amount_per_month);
			elseif(empty($min_amount_per_month) && !empty($max_amount_per_month))
				$offers 	= $offers->where('amount_per_month', '<=', $max_amount_per_month);

			if($sort_by == 'popular')
				$offers	= $offers->topOffers();
			elseif($sort_by == 'latest')
				$offers	= $offers->latest('product_offers.id');
			elseif($sort_by == 'lowest')
				$offers	= $offers->orderByCheapest();
			else
				$offers	= $offers->orderByHighest();

			$offers 		= $offers->get();

			$max_adv_limit = $offers->count() > 0 ? $offers->sortByDesc('down_payment')->first()->down_payment : 200000;
			$max_emi_limit = $offers->count() > 0 ? $offers->sortByDesc('amount_per_month')->first()->amount_per_month : 200000;

			echo $this->Blade->make('customer.product.show', ['product' => $product, 'offers' => $offers, 'min_amount_per_month' => $min_amount_per_month, 'max_amount_per_month' => $max_amount_per_month, 'max_total_installments' => $max_total_installments, 'min_down_payment' => $min_down_payment, 'max_down_payment' => $max_down_payment, 'sort_by' => $sort_by, 'max_emi_limit' => $max_emi_limit, 'max_adv_limit' => $max_adv_limit]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
}