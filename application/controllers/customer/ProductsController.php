<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-10-05 19:34:25
 */

class ProductsController extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
	}

	public function show($slug)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		
		$product 	= Application\Models\Products::where('slug', $slug)
													->first();
		if(!empty($product))
		{
			$offers 	= Application\Models\ProductOffers::ambiguityFreeSelection()
																	->activeOffers()
																		->where('product_offers.product_id', $product->id)
																			->orderByCheapest()
																				->get();
			echo $this->Blade->make('customer.product.show', ['product' => $product, 'offers' => $offers]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
}