<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-05-28 11:34:49
 */

class CmsController extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
	}

	public function contact()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		echo $this->Blade->make('customer.cms.contact-us');
	}

	public function about()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		echo $this->Blade->make('customer.cms.about-us');
	}
	public function faq()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		echo $this->Blade->make('customer.cms.faq');
	}
	public function termsCondition()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		echo $this->Blade->make('customer.cms.terms');
	}
	public function privacyPolicy()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		echo $this->Blade->make('customer.cms.privacy');
	}
	public function CorporateInstallments()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		echo $this->Blade->make('customer.cms.corporate');
	}
	public function WhybuyfromUs()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		echo $this->Blade->make('customer.cms.why-buy');
	}
}