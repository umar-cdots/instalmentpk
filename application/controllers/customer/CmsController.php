<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-05-28 11:34:49
 */

class CmsController extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
	}

	public function contact()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		echo $this->Blade->make('customer.cms.contact-us');
	}
	public function request()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		echo $this->Blade->make('customer.cms.product-request');
	}
	public function about()
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$about = Application\Models\CMS::where('page_title', 'about-us')->first();

		echo $this->Blade->make('customer.cms.about-us', ['about' => $about]);
	}
	
	public function faq()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$faq = Application\Models\CMS::where('page_title', 'faq')->first();
		echo $this->Blade->make('customer.cms.faq',['faq' => $faq]);
	}
	public function termsCondition()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$terms_conditions = Application\Models\CMS::where('page_title', 'terms-conditions')->first();
		echo $this->Blade->make('customer.cms.terms', ['terms_conditions' => $terms_conditions]);
	}
	public function privacyPolicy()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$privacy_policy = Application\Models\CMS::where('page_title', 'terms-conditions')->first();

		echo $this->Blade->make('customer.cms.privacy',['privacy_policy' => $privacy_policy]);
	}
	public function CorporateInstallments()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$corporate_installments = Application\Models\CMS::where('page_title', 'corporate-installments')->first();
		echo $this->Blade->make('customer.cms.corporate',['corporate_installments' => $corporate_installments]);
	}
	public function WhybuyfromUs()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$Why_buy_from_us = Application\Models\CMS::where('page_title', 'Why-buy-from-us')->first();
		echo $this->Blade->make('customer.cms.why-buy',['Why_buy_from_us' => $Why_buy_from_us]);
	}
	public function financingIslamic()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$islamic_financing = Application\Models\CMS::where('page_title', 'islamic-financing')->first();
		echo $this->Blade->make('customer.cms.islamic-financing',['islamic_financing' => $islamic_financing]);
	}
	public function contactUsSave()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		// $this->isAuthentic(false);

        $name       = $this->input->post('name');
        $from_email = $this->input->post('email');
        $enquiry    = $this->input->post('enquiry');


        $this->load->library('email');
        $this->email->initialize($this->config->item('email'));

        $email_body = $this->Blade->make('email.notification', ['subject' => "Contact Us", 'recepient_name' => $name, 'content' => $enquiry]);

        $this->email->from($this->config->item('email')['smtp_user'], 'Contact Us');
        $this->email->reply_to($from_email, $name);
        $this->email->to($this->config->item('admin_email'));
        $this->email->subject('Contact Us');
        $this->email->message($email_body);

        //Send mail
        if($this->email->send())
        {
        	die(json_encode(['status' => "success", 'message' => "Congragulation Email Send Successfully."]));
   //      	$this->session->set_flashdata('status', "success");
			// $this->session->set_flashdata('message', "Congragulation Email Send Successfully.");
   //          redirect(base_url('/contact-us'));
        }
        else
        {
        	die(json_encode(['status' => "error", 'message' => "Something Went Wrong. Please Try Again."]));
   //      	$this->session->set_flashdata('status', "error");
			// $this->session->set_flashdata('message', "Something Went Wrong!");
   //          redirect(base_url('/contact-us'));
        }		
	}
	public function requestSave()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		// $this->isAuthentic(false);

        $name       = $this->input->post('name');
        $from_email = $this->input->post('email');
        $product    = $this->input->post('product');
        $description = $this->input->post('description');


        $this->load->library('email');
        $this->email->initialize($this->config->item('email'));

        $email_body = $this->Blade->make('email.request', ['subject' => "Request for product adding", 'recepient_name' => $name, 'content' => $description, 'product' => $product]);

        $this->email->from($this->config->item('email')['smtp_user'], 'Request for product adding');
        $this->email->reply_to($from_email, $name);
        $this->email->to($this->config->item('admin_email'));
        $this->email->subject('Request for product adding');
        $this->email->message($email_body);

        //Send mail
        if($this->email->send())
        {
        	die(json_encode(['status' => "success", 'message' => "Congragulation Email Send Successfully."]));
   //      	$this->session->set_flashdata('status', "success");
			// $this->session->set_flashdata('message', "Congragulations Email Send Successfully.");
   //          redirect(base_url('/contact-us'));
        }
        else
        {
        	die(json_encode(['status' => "error", 'message' => "Something Went Wrong. Please Try Again."]));
   //      	$this->session->set_flashdata('status', "error");
			// $this->session->set_flashdata('message', "Something Went Wrong!");
   //           redirect(base_url('/contact-us'));
        }		
	}
}