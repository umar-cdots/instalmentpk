<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-10-04 16:09:19
 */

class CategoryController extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
	}

	public function show($slug)
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$category 	= Application\Models\ProductCategories::where('slug', $slug)
														->first();

		if(!empty($category))
		{
       		$child_category_ids = $category->getAllChildCategories()->pluck('id')->toArray();

       		$offers 	= Application\Models\ProductOffers::ambiguityFreeSelection()
																->singleCheapest()
																	->activeOffers()
																		->categoryOffers($child_category_ids)
																			->orderBy('product_offers.id', 'DESC');
			$tmp_offers = clone $offers; //Little Hack to show all brands

			if(isset($_GET['brand']) && is_array($_GET['brand']))
       		{
       			$offers 	= $offers->join('brands', 'brands.id', '=', 'products.brand_id')
       									->whereIn('brands.id', array_values($_GET['brand']));
       		}

       		if(isset($_GET['feature']) && is_array($_GET['feature']))
       		{
       			$key_feature_ids = [];
       			$meta_values 	 = [];

       			array_map(function($v) use (&$key_feature_ids, &$meta_values){
       				$tmp = explode(':', $v);
       				if(isset($tmp[0], $tmp[1]))
       				{
       					$key_feature_ids[] = $tmp[0];
       					$meta_values[] = $tmp[1];
       				}
       			}, $_GET['feature']);

       			$offers 	= $offers->join('offers_meta', 'product_offers.id', '=', 'offers_meta.product_offer_id')
       									->whereIn('offers_meta.key_feature_id', $key_feature_ids)
       										->whereIn('offers_meta.meta_value', $meta_values);
       // 			$offers_meta = new Application\Models\OffersMeta;
       // 			$offers_meta = $offers_meta->selectRaw(new \Illuminate\Database\Query\Expression("product_offer_id, count(product_offer_id) as poi_total"));
       // 			$offers_meta = $offers_meta->whereIn('key_feature_id', array_keys($_GET['feature']))
       // 										->whereIn('meta_value', array_values($_GET['feature']));
       // 			$offers_meta = $offers_meta->groupBy('product_offer_id');
   				// $offers_meta = $offers_meta->havingRaw(new \Illuminate\Database\Query\Expression("poi_total >= " . count($_GET['feature'])));

       // 			$offers 	= $offers->whereIn('product_offers.id', $offers_meta->pluck('product_offer_id'));
       		}
// $offers->where('idd', 1)->get();
		   	$offers 		= $offers->paginate(8);
// echo "<pre>";print_r($offers);die;
       		$product_ids 	= $tmp_offers->pluck('product_id')->unique()->toArray();
       		$brand_ids 		= Application\Models\Products::whereIn('id', $product_ids)->pluck('brand_id')->unique()->toArray();
	       	$brands 		= Application\Models\Brands::whereIn('id', $brand_ids)->get();

	       	$key_features 	= Application\Models\KeyFeature::whereIn('product_category_id', $child_category_ids)->get();

	       	// echo "<pre>";
	       	// dd($key_features->first()->products->toArray());
																			       
			echo $this->Blade->make('customer.category.show', ['category' => $category, 'offers' => $offers, 'brands' => $brands, 'key_features' => $key_features]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
}