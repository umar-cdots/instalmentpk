<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-27 12:53:58
 */
use \Application\Models\Users;

abstract class CustomerController extends CI_Controller
{
	protected $CurrentController;
	protected $CurrentModule;
	protected $CurrentMethod;

	public function __construct()
	{
		parent::__construct();
		$this->CurrentModule = "customer";
		LoginInfo::getInstance()->setWhoShouldI(_CUSTOMER_);
		$this->setCustomerLocation();
	}

	private function isAuthLess()
	{
		if(is_authless($this->CurrentModule, $this->CurrentController, $this->CurrentMethod))
		{
			return true;
		}
		return false;
	}

	private function setCustomerLocation()
	{
		$user_location 	= $this->session->user_location;
		if(empty($user_location))
		{
			$this->session->set_userdata('user_location_full', "Lahore, Punjab, Pakistan");
			$this->session->set_userdata('user_location', "Lahore");
			$this->session->set_userdata('location_lat', 31.4848634);
			$this->session->set_userdata('location_lon', 74.3830406);
			$this->session->set_userdata('default_location', true);
		}
	}

	public function isAuthentic($redirect = false)
	{
		if(!$this->isAuthLess())
		{
			if(!LoginInfo::getInstance()->isLogin())
			{
				redirect(base_url('login'));
				die;
			}
			elseif(!LoginInfo::getInstance()->hasAccess(_CUSTOMER_) && !$redirect)
			{
				$redirect ? redirect(base_url('login')) : show_401();	
				die;
			}
			return true;
		}
		else
		{ 
			return true;
		}
	}
}
