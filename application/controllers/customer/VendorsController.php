<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-10-04 16:09:19
 */

class VendorsController extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
	}

	public function show($slug)
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$vendor 	= Application\Models\Vendors::activeVendor()
													->where('slug', $slug)
														->first();

		if(!empty($vendor))
		{
			$offers 	= Application\Models\ProductOffers::ambiguityFreeSelection()
																->singleCheapest()
																	->activeOffers()
																		->where('vendor_id', $vendor->id)
																			->orderBy('product_offers.id', 'DESC')
																				->get();
			echo $this->Blade->make('customer.vendor.show', ['vendor' => $vendor, 'offers' => $offers]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
}