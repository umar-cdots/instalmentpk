<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-10-05 18:11:43
 */
use Application\Models\ProductOffers;

class SearchController extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
	}

	public function show($keyword)
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$keyword	= urldecode($keyword);
		$offers 	= ProductOffers::ambiguityFreeSelection()
										->singleCheapest()
											->search($keyword)
												->nearestOffers()
													->activeOffers()
														->get();

		echo $this->Blade->make('customer.search', ['keyword' => $keyword, 'offers' => $offers]);
	}

	public function showWithFilters($keyword)
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$keyword	= urldecode($keyword);

		$min_down_payment 		= $this->input->post('min_down_payment');
		$max_down_payment 		= $this->input->post('max_down_payment');
		$max_total_installments = $this->input->post('max_total_installments');
		$min_amount_per_month 	= $this->input->post('min_amount_per_month');
		$max_amount_per_month 	= $this->input->post('max_amount_per_month');
		$sort_by 				= $this->input->post('sort_by');

		$filtered_offers 		= ProductOffers::singleCheapest()
													->search($keyword)
														->nearestOffers()
															->activeOffers();

		if(!empty($min_down_payment) && !empty($max_down_payment))
			$filtered_offers 	= $filtered_offers->whereBetween('down_payment', [$min_down_payment, $max_down_payment]);
		elseif(!empty($min_down_payment) && empty($max_down_payment))
			$filtered_offers 	= $filtered_offers->where('down_payment', '>=', $min_down_payment);
		elseif(empty($min_down_payment) && !empty($max_down_payment))
			$filtered_offers 	= $filtered_offers->where('down_payment', '<=', $max_down_payment);

		if(!empty($max_total_installments))
			$filtered_offers	= $filtered_offers->where('total_installments', '<=', $max_total_installments);

		if(!empty($min_amount_per_month) && !empty($max_amount_per_month))
			$filtered_offers 	= $filtered_offers->whereBetween('amount_per_month', [$min_amount_per_month, $max_amount_per_month]);
		elseif(!empty($min_amount_per_month) && empty($max_amount_per_month))
			$filtered_offers 	= $filtered_offers->where('amount_per_month', '>=', $min_amount_per_month);
		elseif(empty($min_amount_per_month) && !empty($max_amount_per_month))
			$filtered_offers 	= $filtered_offers->where('amount_per_month', '<=', $max_amount_per_month);

		if($sort_by == 'popular')
			$filtered_offers	= $filtered_offers->topOffers();
		else
			$filtered_offers->ambiguityFreeSelection();

		echo $this->Blade->make('customer.search', ['keyword' => $keyword, 'offers' => $filtered_offers->get(), 'min_amount_per_month' => $min_amount_per_month, 'max_amount_per_month' => $max_amount_per_month, 'max_total_installments' => $max_total_installments, 'min_down_payment' => $min_down_payment, 'max_down_payment' => $max_down_payment, 'sort_by' => $sort_by]);
	}
}