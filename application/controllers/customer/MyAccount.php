<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Nadeem
 * @Date:   2018-09-26 04:00:16
 * @Last Modified by:   Mac
 * @Last Modified time: 2018-09-26 04:00:16
 */


 class MyAccount extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
	}
	
	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
		
		
		echo $this->Blade->make('customer.my-account.index');
	}
	
	public function orders()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
		
		$orders = Application\Models\Orders::where('customer_id', LoginInfo::getInstance()->getUserTypeID())->latest('id')->get();
		
		echo $this->Blade->make('customer.my-account.orders',['orders' => $orders]);
	}
	
	public function edit_account()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);


		$user 							= LoginInfo::getInstance()->getUser();
		$customer 				        = LoginInfo::getInstance()->getUser()->customer;

		if(!empty($this->input->post('first_name')))
		{
			$first_name 			     = $this->input->post('first_name');
	        $last_name 			         = $this->input->post('last_name');
	        $primary_phone 				 = $this->input->post('primary_phone');
	        $current_address 			 = $this->input->post('current_address');
	        $cnic_no 			         = $this->input->post('cnic_no');
			// $password 					 = $this->input->post('password');
			$new_password 				 = $this->input->post('new_password');
			$confirm_password 			 = $this->input->post('confirm_password');

			if(true/*$password == decrypt_me($user->password)*/)
			{
				$customer->primary_phone 	            = $primary_phone;
				$customer->current_address 	            = $current_address;
				$customer->cnic_no 	                    = $cnic_no;

				$user->first_name 	    = $first_name;
				$user->last_name 	    = $last_name;

				if(!empty($new_password) && $new_password == $confirm_password)
				{
					$user->password = encrypt_me($new_password);
				}

				$user->save();
				$customer->save();

				$this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Profile Updated Successfully!");
			}
			else
			{
				$this->session->set_flashdata('status', "error");
				$this->session->set_flashdata('message', "Invalid Current Password Required.");
			}
		}
		
		echo $this->Blade->make('customer.my-account.edit-account',['user' => $user, 'customer'=> $customer]);
	}
}