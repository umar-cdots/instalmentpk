<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-07-05 17:54:03
 */
use Application\Models;
class AuthController extends CustomerController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->load->library('form_validation');
		$this->load->helper('form');
	}

	public function signup()
	{
		$this->CurrentMethod 		= __FUNCTION__;

		if(LoginInfo::getInstance()->isLogin())
		{
			redirect('home');
			die;
		}
		
		echo $this->Blade->make('customer.auth.signup');
	}
	public function login()
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$this->session->set_userdata('redirect_after_login', $_SERVER['HTTP_REFERER'] ?? '/');

		if(LoginInfo::getInstance()->isLogin())
		{
			redirect('home');
			die;
		}
		
		echo $this->Blade->make('customer.auth.login');
	}
	public function register()
	{
		$this->CurrentMethod 		= __FUNCTION__;

		if(LoginInfo::getInstance()->isLogin())
		{
			redirect('home');
			die;
		}

        //form validation
        //$this->form_validation->set_rules('cnic_no', 'CNIC No', 'trim|required|regex_match[/[0-9]{5}\-[0-9]{7}\-[0-9]{1}/]');
		//$this->form_validation->set_rules('designation', 'Designation', 'trim|required');
		//$this->form_validation->set_rules('organization','Organization', 'trim|required');
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email|is_unique[users.email_address]');
		$this->form_validation->set_rules('primary_phone', 'Primary Phone', 'trim|required');
		//$this->form_validation->set_rules('current_address', 'Address', 'trim|required');
		//$this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('confirm_password', 'confirm_password', 'required|matches[password]');
		$this->form_validation->set_rules('agreed', 'Terms & Conditions', 'callback_accept_terms');

		//$this->form_validation->set_rules('description', 'Description');
        $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

    	if ($this->form_validation->run() === true)
        {
			//$cnic 						= $this->input->post('cnic_no');
			//$designation 			    = $this->input->post('designation');
			//$organization 				= $this->input->post('organization');
			$fname 				        = $this->input->post('first_name');
			$lname 				        = $this->input->post('last_name');
			$email 				        = $this->input->post('email_address');
			$phone 				        = $this->input->post('primary_phone');
			//$address 				    = $this->input->post('current_address');
			//$postal_code 				= $this->input->post('postal_code');
			$password 					= encrypt_me($this->input->post('password'));

            $user = new Application\Models\Users;
            $customer = new Application\Models\Customers;

           // $customer->cnic_no = $cnic;
            //$customer->designation = $designation;
            //$customer->organization = $organization;
              $customer->primary_phone = $phone;
           // $customer->current_address = $address;
            //$customer->postal_code = $postal_code;
              $customer->save();

            $user->first_name = $fname;
            $user->last_name = $lname;
            $user->email_address = $email;
            $user->password = $password;

            $user->customer_id			= $customer->id;
            $user->access_level_bits 	= _CUSTOMER_;
            $user->user_type			= "customer";
            $user->save();

	        $this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Customer Registered Successfully!");

			LoginInfo::getInstance()->attemptLogin($email, $this->input->post('password'), false, _CUSTOMER_);

	        redirect(base_url('home'));
	        die();
        }
	    else
	    {
	    	$this->signup();
	    }
	    $this->session->set_flashdata('status', "error");
		$this->session->set_flashdata('message', "Something Went Wrong!");
        redirect(base_url('signup'));
        die();
	}
	public function tryLogin()
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$this->form_validation->set_rules('email_address', 'Email Address', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');

		$email 						= $this->input->post('email_address');
		$password 					= $this->input->post('password');
		$remember_me 				= $this->input->post('remember_me');

		if ($this->form_validation->run() === true)
        {
			if($this->User = LoginInfo::getInstance()->attemptLogin($email, $password, $remember_me, _CUSTOMER_))
			{
				//die('Here');
				$this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Customer login Successfully!");
				$redirect = $this->session->userdata('redirect_after_login');
				if(!empty($redirect) && strpos($redirect, "customer") < 1 )
				{
					$this->session->unset_userdata('redirect_after_login');
					redirect($redirect);
				}
				else
				{
					redirect('home');
				}
				die();
			}
		}
		else
		{
			$this->login();
		}
		redirect('login');
		die();
	}
	public function logout()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(LoginInfo::getInstance()->isLogin())
		{
			if(LoginInfo::getInstance()->destroySession())
			{
				$this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Logout Successfully!");
			}
		}
		redirect('login');
		die;
	}
	public function simpleRedirect()
	{
		redirect('login');
		die;
	}

	//Validations
	function accept_terms() 
	{
	    if (!empty($this->input->post('agreed'))) 
	    	return true;
	    $this->form_validation->set_message('accept_terms', 'Please read and accept our terms and conditions.');
	    return false;
	}
	public function forget()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		echo $this->Blade->make('customer.auth.forget');
	}
	public function Recoverpassword()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(!LoginInfo::getInstance()->isLogin())
		{
			if (!empty($this->input->post('email_address')) && filter_var($this->input->post('email_address'), FILTER_VALIDATE_EMAIL))
			{
			    $email 				        = $this->input->post('email_address');

		        $user = Application\Models\Users::where('email_address', $email)
		        									->where('user_type', 'customer')
		        										->where('status', 'active')
		        											->first();
				if(!empty($user))
				{ 
				    $email_template = getEmailTemplate('password_request');

					if($email_template)
					{
				        $this->load->library('email');
				        $this->email->initialize($this->config->item('email'));

				        $email_body = $this->Blade->make('email.notification',  ['subject' => $email_template['subject'], 'recepient_name' => $user->fullName(), 'content' => sprintf($email_template['body'], decrypt_me($user->password), base_url('/login'), "click here")]);

				        $this->email->from($this->config->item('email')['smtp_user'], 'InstalmentPK');
				        $this->email->to($user->email_address, $user->fullName());
				        $this->email->subject($email_template['subject']);
				        $this->email->message($email_body);

						if($this->email->send())
						{
							$this->session->set_flashdata('status', "success");
							$this->session->set_flashdata('message', "Please Check Your Email!");
							redirect('/login');
							die;
						}
						else
						{
							$this->session->set_flashdata('status', "error");
							$this->session->set_flashdata('message', "Something Went Wrong.");
						}
					}
					else
					{
						$this->session->set_flashdata('status', "error");
						$this->session->set_flashdata('message', "Something Went Wrong.");
					}
				}
				else
				{
					$this->session->set_flashdata('status', "error");
					$this->session->set_flashdata('message', "Invalid Email Address.");
				}
			}	
			else
			{
				$this->session->set_flashdata('status', "error");
				$this->session->set_flashdata('message', "Email Address Required." . $this->input->post('email_address'));
			}	
			redirect('/forget');
			die;

		} 
		else
		{
			redirect('/login');
			die;
		}
	}
}