<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-11-16 12:53:29
 */

class AjaxController extends CustomerController
{
	private $response = [];

	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->response 			= array('status' => "", 'message' => "", 'data' => []);
		header('Content-Type: application/json');
	}

	public function __destruct()
	{
		echo json_encode($this->response);
	}

	public function setLocation()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->form_validation->set_rules('location_title', 'Location', 'trim');
		$this->form_validation->set_rules('location_lat', 'Location Latitude', 'trim|required');
		$this->form_validation->set_rules('location_lon', 'Location Longitude', 'trim|required');

		if ($this->form_validation->run() === true)
        {
			$this->response['status']	= "success";

			$user_location_full = $this->input->post('location_title'); 
			$user_location 		= explode(',', $this->input->post('location_title'));
			array_pop($user_location);
			$user_location 		= trim(end($user_location));
			$location_lat 		= $this->input->post('location_lat');
			$location_lon 		= $this->input->post('location_lon');

			$params 		= array(
								'sensor' 	=> 'false',
								'latlng'	=> $location_lat . ',' . $location_lon,
								'key'		=> _GOOGLE_MAP_KEY_ 
							);

			$content 		= file_get_contents(_GOOGLE_END_POINT_ . http_build_query($params));
			$output 		= json_decode($content);

			if($output->status == 'OK')
			{
				$user_location = getLocality($output->results[0]->address_components)->long_name;
			}
			elseif($output->status == 'ZERO_RESULTS')
			{
				$this->response['status']	= "error";
				$this->response['message']	= "No Result Found.";
			}
			else
			{
				$this->response['status']	= "error";
				$this->response['message']	= "Something Went Wrong. Please Try Again.";
			}

			if($this->response['status'] != "error")
			{
				$this->session->set_userdata('user_location_full', empty($user_location_full) ? $output->results[0]->formatted_address : $user_location_full);
				$this->session->set_userdata('user_location', $user_location);
				$this->session->set_userdata('location_lat', $location_lat);
				$this->session->set_userdata('location_lon', $location_lon);
				$this->session->set_userdata('default_location', false);

				$this->response['message']	= "Location Updated Successfully.";
				$this->response['data']		= array(
													'user_location_full' => empty($user_location_full) ? $output->results[0]->formatted_address : $user_location_full,
													'user_location' => $user_location,
													'location_lat'	=> $this->input->post('location_lat'),
													'location_lon'	=> $this->input->post('location_lon'),
													''
												);
			}
		}
		else
		{
			$this->response['status']	= "error";
			$this->response['message']	= "Validation Erros.";
			$this->response['data']		= explode('|', validation_errors('', '|'));
		}
	}

	public function searchLocations()
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$this->form_validation->set_data(['query' => $this->input->get('query')]);
		$this->form_validation->set_rules('query', 'Location', 'trim|required');

		if ($this->form_validation->run() === true)
        {
        	$query 				= urlencode($this->input->get('query'));

        	$params 			= array(
									'sensor' 	=> 'false',
									'address'	=> $query,
									'components'=> 'country:PK',
									'key'		=> _GOOGLE_MAP_KEY_ 
								);

			$content 		= file_get_contents(_GOOGLE_END_POINT_ . http_build_query($params));
			$output 		= json_decode($content);

			if($output->status == 'OK')
			{
				$results 	= array();
				$single 	= array(
								'address' 	=> "",
								'lat'		=> "",
								'lng'		=> ""
							);
				foreach($output->results as $result)
				{
					$single['address']	= $result->formatted_address;
					$single['lat']		= $result->geometry->location->lat;
					$single['lng']		= $result->geometry->location->lng;

					$results[]			= $single;
				}

				$this->response['status']	= "success";
				$this->response['message']	= "Returning Location(s).";
				$this->response['data']		= $results;
			}
			elseif($output->status == 'ZERO_RESULTS')
			{
				$this->response['status']	= "error";
				$this->response['message']	= "No Result Found.";
			}
			else
			{
				$this->response['status']	= "error";
				$this->response['message']	= "Something Went Wrong. Please Try Again.";
			}
        }
        else
        {
        	$this->response['status']	= "errorr";
			$this->response['message']	= "Validation Erros.";
			$this->response['data']		= array_values(explode('|', validation_errors('', '|')));
        }
	}

	function updateWishlist()
	{
		$this->form_validation->set_rules('product_offer_id', 'Offer', 'required|numeric|callback_offerExist');
		$this->form_validation->set_rules('action', 'Action', 'required|callback_validateWishAction');
		if ($this->form_validation->run() === true)
        {
        	$wishlist 	= $this->session->wishlist ?? [];
        	$offer_id 	= $this->input->post('product_offer_id');
        	$action 	= strtolower($this->input->post('action'));

        	if($action == "add")
        	{
        		if(in_array($offer_id, $wishlist))
        		{
        			$this->response['status']	= "error";
					$this->response['message']	= "Offer Already in Wishlist.";
        		}
        		else
        		{
	        		array_push($wishlist, $offer_id);

		        	$this->response['status']	= "success";
					$this->response['message']	= "Offer Added to Wishlist.";
					$this->response['data']		= array_values($wishlist);
				}

				$this->session->set_userdata('wishlist', array_values($wishlist));
        	}
        	else
        	{
        		if (($key = array_search($offer_id, $wishlist)) !== false) 
        		{
				    unset($wishlist[$key]);

		        	$this->response['status']	= "success";
					$this->response['message']	= "Offer Remove From Wishlist.";
					$this->response['data']		= array_values($wishlist);

					$this->session->set_userdata('wishlist', array_values($wishlist));
				}
				else
				{
					$this->response['status']	= "error";
					$this->response['message']	= "Something Went Wrong.";
				}
        	}
        }
        else
        {
        	$this->response['status']	= "error";
			$this->response['message']	= "Validation Erros.";
			$this->response['data']		= array_values(explode('|', validation_errors('', '|')));
        }
	}

	function offerExist()
	{
		$id = $this->input->post('product_offer_id');
	    $this->db->where('id', $id);
	    $query = $this->db->get('product_offers');
	    if ($query->num_rows() > 0)
	    {
	        return true;
	    }
	    $this->form_validation->set_message('offerExist', 'Offer does not Exist.');
        return false;
	}

	function validateWishAction()
	{
		$action = $this->input->post('action');
		if($action != "add" && $action != "remove")
		{
			$this->form_validation->set_message('validateWishAction', 'Invalid Action.');
        	return false;
		}
		return true;
	}

	function searchSuggestions()
	{
		$this->CurrentMethod 		= __FUNCTION__;

		$this->form_validation->set_data(['keyword' => $this->input->get('keyword')]);
		$this->form_validation->set_rules('keyword', 'Search Text', 'trim|required');

		if ($this->form_validation->run() === true)
        {
        	$keyword 			= urldecode($this->input->get('keyword'));

        	$results 			= Application\Models\Products::search($keyword)
									->get();

			$this->response['status']	= "success";
			$this->response['message']	= "";
			$this->response['data']		= $results;
        }
        else
        {
        	$this->response['status']	= "error";
			$this->response['message']	= "Validation Erros.";
			$this->response['data']		= array_values(explode('|', validation_errors('', '|')));
        }
	}
}