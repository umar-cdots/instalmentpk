<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Nadeem
 * @Date:   2018-09-24 7:05:00
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-11-22 18:41:02
 */

use \Application\Models\Orders;
use \Application\Models\PhoneVerifications;
use \Application\Models\PhysicalVerifications;
use \Application\Models\Guarantors;
use \Carbon\Carbon;

class A_OrdersController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Orders", 'is_active' => true, 'url' => site_url('admin/orders')];
		$this->load->library('form_validation');
		$this->load->helper('form');
	}

	public function index($verification_type = '')
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$vendors 		= Application\Models\Vendors::all();
		$vendor_id		= $this->input->post('vendor_id') ?? '';
		$date_from		= $this->input->post('date_from') ?? '';
		$date_to		= $this->input->post('date_to') ?? '';

		$orders 		= Application\Models\Orders::orderBy('id', 'DESC');

		if(!empty($verification_type))
		{
			$orders 	= $orders->where('verification_type', $verification_type);
		}
		if(!empty($vendor_id))
		{
			$orders 	= $orders->where('vendor_id', $user_id);	
		}
		if(!empty($date_from))
		{
			$orders 	= $orders->where('created_at', '>=', Carbon::parse($date_from)->format('Y-m-d'));
		}
		if(!empty($date_to))
		{
			$orders 	= $orders->where('created_at', '<=', Carbon::parse($date_to)->format('Y-m-d'));
		}
		
		echo $this->Blade->make('admin.orders.index', ['breadcrumbs' => $this->Breadcrumbs, 'orders' => $orders->get(), 'vendors' => $vendors, 'vendor_id' => $vendor_id, 'date_from' => $date_from, 'date_to' => $date_to, 'verification_type' => $verification_type]);
	}	

	public function create()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Create", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/ledger/create')];

    	$categories = Application\Models\ProductCategories::all();
		$vendors = Application\Models\Vendors::all();

		echo $this->Blade->make('admin.ledger.create', ['breadcrumbs' => $this->Breadcrumbs, 'categories' => $categories,'vendors' => $vendors]);
    }

    public function store()
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
		
	    //form validation
	    $this->form_validation->set_rules('user_id', 'Select User', 'required');
		$this->form_validation->set_rules('payment_type','Payment Type', 'required');
		$this->form_validation->set_rules('amount','Amount','trim|required');
		$this->form_validation->set_rules('description','Description','trim');
		// $this->form_validation->set_rules('sort','Sort', 'trim|required');

	   	if ($this->form_validation->run() === true)
		{
			$user_id 						            = $this->input->post('user_id');
			$payment_type 			                    = $this->input->post('payment_type');
			$amount 			                    	= $this->input->post('amount');
			$description 			                    = $this->input->post('description');
			
			
			$ledger = new Ledger();
			$ledger->user_id = $user_id;
			$ledger->product_id = 0;
			$ledger->description = $description;
			$ledger->amount = $amount;
			$ledger->type = 'payment';
			$ledger->payment_type = $payment_type;
			$ledger->save();
			
		    /*
			$categroy = new Application\Models\ProductCategories;

		    $categroy->parent_id 			= $parent_id;
		    $categroy->category_title 		= $category_title;
		    $categroy->icon_class 			= $icon_class;

		    if(!empty($show_at_homepage))
		    	$categroy->show_at_homepage 	= true;

		    $categroy->sort 				= $sort;
		    $categroy->save();

		    $categroy->slug 				= slugify($category_title .'-'. $categroy->id); //Little hack to add primary key into slug
	    	$categroy->save();
			*/

		    $this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Category Saved Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/ledger'));
		    die();
		}
		$this->session->set_flashdata('status', "error");
		$this->session->set_flashdata('message', "Something Went Wrong!");
		errorBag();
        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/ledger/create'));
        die();	
    }
    
	public function show($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "View", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/orders/' . $id)];

		$order = Application\Models\Orders::find($id);

		if(!is_null($order))
		{
			echo $this->Blade->make('admin.orders.show', ['breadcrumbs' => $this->Breadcrumbs, 'order' => $order]);
		} 
		else 
		{
			show_404();
			die();
		}
    }
	
    public function edit($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$orders = Application\Models\Orders::find($id);
		
		if(!is_null($orders))
		{
			echo $this->Blade->make('admin.orders.edit', ['breadcrumbs' => $this->Breadcrumbs, 'orders' => $orders]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	
	public function update($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$orders = Application\Models\Orders::find($id);
		//var_dump($orders);
		
		if(!is_null($orders))
		{
		    //form validation
			//$this->form_validation->set_rules('user_id', 'Select User', 'required');
			$this->form_validation->set_rules('VerificationType','Verification Type', 'required');
			$this->form_validation->set_rules('VerificationStatus','Verification Status','required');
			$this->form_validation->set_rules('admin_comment','Comments','trim');
			// $this->form_validation->set_rules('show_at_homepage','Show in Nav', 'trim');
			// $this->form_validation->set_rules('sort','Sort', 'trim|required');

		   	if ($this->form_validation->run() === true)
			{
				//$user_id 						            = $this->input->post('user_id');
				$verification_type							= $this->input->post('VerificationType');
				$verification_status						= $this->input->post('VerificationStatus');
				$admin_comment								= $this->input->post('admin_comment');

			    $orders->verification_type					= $verification_type;
			    $orders->verification_status				= $verification_status;
			    $orders->admin_comment						= $admin_comment;

			    $orders->save();

			    $this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Category Updated Successfully!");
				if( $this->input->post('VerificationStatus') == 'Yes')
				{
					redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/orders/phone_verification/form/' . $id));
				} else {
			    	redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/orders'));
				}
				
			}
		    $this->session->set_flashdata('status', "error");
			$this->session->set_flashdata('message', "Something Went Wrong!");
			errorBag();
	        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/orders/edit/' . $id));
	        die();	
    	} 
		else 
		{
			show_404();
			die();
		}
    }
	
	public function destroy($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$category 	= Application\Models\ProductCategories::find($id);

		if(!is_null($category))
		{
			$category->delete();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Category Removed Successfully!");
			redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/category'));
			die();

		} 
		else 
		{
			show_404();
			die();
		}
	}

	public function verificationInfo($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(true);

		$order = Application\Models\Orders::find($id);
		
		if(!is_null($order))
		{
			echo $this->Blade->make('admin.orders.verification_info', ['breadcrumbs' => $this->Breadcrumbs, 'order' => $order]);
		} 
		else 
		{
			show_404();
			die();
		}
	}

	public function verifyOrder($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Verification", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/orders/verify/' . $id)];

		$order = Application\Models\Orders::find($id);

		if(!is_null($order))
		{
			$order_info = get_order_meta($order->id, 'all', true);
			$order_info = arrayDecode($order_info);

			if($order->verification_type == 'phone_verification')
			{
				$view_path = 'admin.orders.phone_verification_form';
			}
			elseif($order->verification_type == 'physical_verification')
			{
				$view_path = 'admin.orders.physical_verification_form';
			}
			else
			{
				redirect(site_url(_ADMIN_ROUTE_PREFIX_ . '/orders/' . $order->id));
				die;
			}

			if(!isset($order_info['full_name']))
				$order_info['full_name'] = $order->customer->user->first_name . ' ' . $order->customer->user->last_name;
			if(!isset($order_info['cnic_no']))
				$order_info['cnic_no'] 				= $order->customer->cnic_no;
			if(!isset($order_info['cnic_no']))
				$order_info['mobile_number'] 		= $order->customer->primary_phone;
			if(!isset($order_info['current_address_cnic']))
				$order_info['current_address_cnic'] 	= $order->customer->current_address;
			if(!isset($order_info['permanent_address_cnic']))
				$order_info['permanent_address_cnic'] 	= $order->customer->permanent_address;

			echo $this->Blade->make($view_path, ['breadcrumbs' => $this->Breadcrumbs, 'order' => $order, 'order_info' => $order_info]);
		} 
		else 
		{
			show_404();
			die();
		}
	}

	public function saveOrderVerification($id)
	{ //echo "<pre>";die(print_r($_REQUEST));//$_POST['guarantor']['json']));
		$order = Orders::find($id);
		
		if(!is_null($order))
		{
			if($order->verification_type == 'phone_verification')
			{
				$verification 						= PhoneVerifications::firstOrCreate(['order_id' => $order->id]);
				$verification->user_id 				= $order->customer_id;
				$verification->order_id 			= $order->id;
				$verification->name 				= $this->input->post('ps_name');
				$verification->guardian_name 		= $this->input->post('ps_guardian_name');
				$verification->cnic_no 				= $this->input->post('ps_cnic_no');
				$verification->nominee_name 		= $this->input->post('ps_nominee_name');
				$verification->nominee_guardian_name = $this->input->post('ps_nominee_guardian_name');
				$verification->landline_phone 		= $this->input->post('ps_landline_phone');
				$verification->mobile_phone 		= $this->input->post('ps_mobile_phone');
				$verification->current_address 		= $this->input->post('ps_current_address');
				$verification->permanent_address 	= $this->input->post('ps_permanent_address');
				$verification->professional_type 	= $this->input->post('professional_type');
				$verification->profession_json 		= json_encode($_POST['personal']['json'] ?? NULL);
				$verification->commitment_json 		= json_encode($_POST['commitment'] ?? NULL);
				$verification->profession_commitment 	    = $this->input->post('profession_commitment');
				$verification->income_json   		= json_encode($_POST['income_employment'] ?? NULL);
				$verification->assesment_type 	    = $this->input->post('assesment_type');
				$verification->has_bank_account 	= $this->input->post('ps_has_bank_account');
				$verification->can_provide_cheque 	= $this->input->post('ps_can_provide_cheque');
				$verification->commitment_to_residency = $this->input->post('ps_commitment_to_residency');
				$verification->verification_status	= 'in_progress';

				if($this->input->post('submit') == 'final_save')
				{
					$order->status = 'phone_verified';
					$verification->verification_status	= 'in_progress';
				}
				else
				{
					$order->status = 'pending';
				}
				$order->save();
				$verification->save();

				if($order->guarantors->count() > 0)
					Guarantors::where('order_id', $order->id)->forceDelete();
					// $order->guarantors->destroy();

				if(isset($_POST['total_guarantors']) && is_numeric($_POST['total_guarantors']))
				{
					for($i = 0; $i <= $_POST['total_guarantors']; $i++)
					{
						$guarantor = new Guarantors;
						$guarantor->customer_id         = $order->customer_id;
						$guarantor->order_id 	        = $order->id;
						$guarantor->name 		        = $_POST['guarantor']['name'][$i] ?? '';
						$guarantor->guardian_name 		= $_POST['guarantor']['guardian_name'][$i] ?? '';
						$guarantor->relation_with_applicant = $_POST['guarantor']['relation_with_applicant'][$i] ?? '';
						$guarantor->mobile_phone 	   = $_POST['guarantor']['mobile_phone'][$i] ?? '';
						$guarantor->landline_phone 	   = $_POST['guarantor']['landline_phone'][$i] ?? '';
						$guarantor->current_address    = $_POST['guarantor']['current_address'][$i] ?? '';
						$guarantor->permanent_address  = $_POST['guarantor']['permanent_address'][$i] ?? '';
						$guarantor->cnic_no            = $_POST['guarantor']['cnic_no'][$i] ?? '';
						$guarantor->professional_type  = $_POST['guarantor']['professional_type'][$i] ?? '';
						$guarantor->profession_json    = json_encode($_POST['guarantor']['json'][$i] ?? NULL);
						$guarantor->has_bank_account   = $_POST['guarantor']['has_bank_account'][$i] ?? '';
						$guarantor->can_provide_cheque = $_POST['guarantor']['can_provide_cheque'][$i] ?? '';
						$guarantor->commitment_to_residency = $_POST['guarantor']['commitment_to_residency'][$i] ?? '';
						$guarantor->save();
						// header('Content-Type: application/json');die(json_encode($guarantor));
					}
				}
			}
			elseif($order->verification_type == 'physical_verification')
			{
				$view_path = 'admin.orders.physical_verification_form';
			}
			else
			{
				redirect(site_url(_ADMIN_ROUTE_PREFIX_ . '/orders/verification/' . $order->id));
				die;
			}

			// insert_phone($order->id, 'all', arrayEncode($_POST));
			
			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Verification Updated Successfully!");
			redirect(site_url(_ADMIN_ROUTE_PREFIX_ . '/orders/verification/' . $order->id));
			die;
		}
		else
		{
			show_404();
			die();
		}
	}

   	public function phoneVerification()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(true);

		echo $this->Blade->make('admin.orders.phone');
	}
	
	function phone_verification_form()
	{
		
		$order_id = $this->uri->segment('5');
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
		$orderData = get_order_meta($order_id, 'all',true);
		$orderData = arrayDecode($orderData);
		
		echo $this->Blade->make('admin.orders.phone_verification_form', ['breadcrumbs' => $this->Breadcrumbs,
		 'order_id' => $order_id,
		 'orderdata' => $orderData]);
	}
	
	public function phone_verification_add()
	{
		$order_id = $this->uri->segment('5');
		
		insert_phone($order_id,'all',arrayEncode($_POST));
		
		$this->session->set_flashdata('status', "success");
		$this->session->set_flashdata('message', "Verification Updated Successfully!");
		redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/orders/phone_verification'));
	}
	
	
	function physical_verification_form()
	{
		$order_id = $this->uri->segment('5');
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
		$orderData = get_order_meta($order_id, 'all',true);
		$orderData = arrayDecode($orderData);
		echo $this->Blade->make('admin.orders.physical_verification_form', ['breadcrumbs' => $this->Breadcrumbs]);

	}
	
	public function physical_verification_add($types)
	{
		$order_id = $this->uri->segment('5');
		
		insert_physical($order_id,'all',arrayEncode($_POST));
		
		$this->session->set_flashdata('status', "success");
		$this->session->set_flashdata('message', "Verification Updated Successfully!");
		redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/orders'));
	}
}