<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-07-24 12:37:28
 */

class A_CustomerController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Customers", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/customers')];
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$customers       = Application\Models\Customers::latest('id')->get();

				// $users                        = Application\Models\Users::where('user_type', 'customer')->get();
		echo $this->Blade->make('admin.customers.index', ['breadcrumbs' => $this->Breadcrumbs, 'customers' => $customers/*,'users' => $users*/]);
	}
	public function show($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "View", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/customers/' . $id)];

		$customer = Application\Models\Customers::find($id);

		if(!empty($customer) && $customer->count() > 0)
		{
			if(!$customer->is_admin_view)
			{
				$customer->is_admin_view = true;
				$customer->save();
			}
			echo $this->Blade->make('admin.customers.show', ['breadcrumbs' => $this->Breadcrumbs, 'customer' => $customer]);
		}
		else
		{
			show_404();
		}
	}


	public function edit($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Edit", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/customers/edit/' . $id)];

		$customer 	= Application\Models\Customers::find($id);

		if(!is_null($customer))
		{

			echo $this->Blade->make('admin.customers.edit', ['breadcrumbs' => $this->Breadcrumbs, 'customer' => $customer]);
		} 
		else 
		{
			show_404();
			die();
		}
	}

    public function update($id)
	{

		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Edit", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/customers/edit/' . $id)];

		$customer 	= Application\Models\Customers::find($id);

		if(!is_null($customer))
		{
			//die('Here');
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email');
			$this->form_validation->set_rules('primary_phone', 'Primary Phone', 'trim|required|regex_match[/((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}/]');
			
			if ($this->form_validation->run() === true)
			{
				//die('Here');
			    $fname 				        = $this->input->post('first_name');
			    $lname 				        = $this->input->post('last_name');
			    $email 				        = $this->input->post('email_address');
			    $phone 				        = $this->input->post('primary_phone');
			

				$customer->user->first_name 		   = $fname;
				$customer->user->last_name 			   = $lname;
				$customer->user->email_address 	       = $email;
				$customer->primary_phone 	           = $phone;
				$customer->user->save();
				$customer->save();

				$this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Customer Updated Successfully!");
				redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/customers'));
			    die();
			} 
			else 
			{
				$this->session->set_flashdata('status', "error");
				$this->session->set_flashdata('message', "Something Went Wrong!");
			}
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/customers/edit/' . $id));
		    die();
		}
	}
	public function destroy($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$customer 	= Application\Models\Customers::find($id);

		if(!is_null($customer))
		{
			$customer->user->delete();
			$customer->delete();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Customers Removed Successfully!");
			redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/customer'));
			die();

		} 
		else 
		{
			show_404();
			die();
		}
	}	
}