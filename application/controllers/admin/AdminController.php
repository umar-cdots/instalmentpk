<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-09 16:06:07
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-07-17 18:24:44
 */
use \Application\Models\Users;

abstract class AdminController extends CI_Controller
{
	protected $CurrentController;
	protected $CurrentModule;
	protected $CurrentMethod;

	public function __construct()
	{
		parent::__construct();
		$this->CurrentModule = "admin";
		LoginInfo::getInstance()->setWhoShouldI(_SUPER_ADMIN_ + _ADMIN_ + _VERIFICATION_OFFICER_);
	}

	private function isAuthLess()
	{
		if(is_authless($this->CurrentModule, $this->CurrentController, $this->CurrentMethod))
		{
			return true;
		}
		return false;
	}

	public function isAuthentic($redirect = false)
	{
		if(!$this->isAuthLess())
		{
			if(!LoginInfo::getInstance()->isLogin())
			{
				redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/login'));
				die;
			}
			elseif(!LoginInfo::getInstance()->hasAccess(_VERIFICATION_OFFICER_) && !$redirect)
			{
				$redirect ? redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/login')) : show_401();	
				die;
			}
			return true;
		}
		else
		{ 
			return true;
		}
	}
}