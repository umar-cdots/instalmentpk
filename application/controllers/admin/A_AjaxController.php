<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-11-16 12:54:16
 */
use Spatie\Image\Image;
use Spatie\Image\Manipulations;

class A_AjaxController extends AdminController
{
	private $response = [];

	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->response 			= array('status' => "", 'message' => "", 'data' => []);
		header('Content-Type: application/json');
	}

	public function __destruct()
	{
		echo json_encode($this->response);
	}

	public function unsubscribe($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$subscription = Application\Models\SubscriptionHistory::find($id);

		if($subscription->count() > 0)
		{
			if($subscription->subscription_status == "subscribed")
			{
				$subscription->subscription_status 	= "unsubscribed";
				$subscription->save();

				$subscription->vendor->freezeOffers(); //Freezing Vendor Offers

				$this->response['status'] 	= "success";
				$this->response['message'] 	= "Unsubscribed Successfully.";
			}
			else
			{
				$this->response['status'] 	= "error";
				$this->response['message'] 	= "You're already Unsubscribed.";
			}
		}
		else
		{
			$this->response['status'] 	= "error";
			$this->response['message'] 	= "Something Went Wrong.";
		}
	}

	public function subscribe()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$vendor		= Application\Models\Vendors::find($this->input->post('vendor_id'));
		$package 	= Application\Models\SubscriptionPackages::find($this->input->post('package_id'));

		if($vendor->count() > 0 && $package->count() > 0)
		{
			foreach(Application\Models\SubscriptionHistory::where(['subscription_status' => "subscribed", 'vendor_id' => $vendor->id])->get() as $old_sub)
			{
				$old_sub->subscription_status	= "unsubscribed";
				$old_sub->save();
			}

			$subscription 				= new Application\Models\SubscriptionHistory();
			$subscription->vendor_id	= $vendor->id;
			$subscription->subscription_package_id	= $package->id;
			$subscription->subscription_date		= date('Y-m-d');
			$subscription->subscription_exp_date	= date('Y-m-d', strtotime("+{$package->duration} days"));
			$subscription->subscription_status		= "subscribed";
			$subscription->save();

			$subscription->vendor->unFreezeOffers(); //UN-Freezing Vendor Offers

			if(!empty($this->input->post('add_to_ledger')) && $this->input->post('add_to_ledger') == "add_ledger")
			{
				$ledger 				= new Application\Models\Ledger();
				$ledger->user_id 		= $vendor->user->id;
				$ledger->product_id 	= $subscription->id;
				$ledger->description	= $package->description;
				$ledger->amount 		= $package->package_charges;
				$ledger->type 			= 'subscription';
				$ledger->payment_type 	= 'debit';
				$ledger->save();
			}

			$this->response['status'] 	= "success";
			$this->response['message'] 	= "Subscribed Successfully.";
			$this->response['data']		= $subscription;
		}
		else
		{
			$this->response['status'] 	= "error";
			$this->response['message'] 	= "Something Went Wrong.";
		}
	}

	public function uploadImages()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		if(isset($_FILES['images']))
		{
			$this->load->library('upload');

			$config 						= array();
	    	$config['upload_path']          = _MEDIA_UPLOAD_PATH_;
	        $config['allowed_types']        = 'gif|jpg|jpeg|png';
	        $config['file_ext_tolower']     = true;
	        $config['max_size']             = 5120;

	        $productImages 					= array();

	        $images 						= $_FILES['images'];

        	foreach($images['name'] as $key => $image)
        	{
        		$_FILES['image']['name']	= $images['name'][$key];
		        $_FILES['image']['type']	= $images['type'][$key];
		        $_FILES['image']['tmp_name']= $images['tmp_name'][$key];
		        $_FILES['image']['error']	= $images['error'][$key];
		        $_FILES['image']['size']	= $images['size'][$key];

	        	$config['file_name']        = generateUniqueFilename($image);
	        	$this->upload->initialize($config);

	        	if(!$this->upload->do_upload('image'))
		        {
		            continue;
		        }

		        $data 						= $this->upload->data();

		        $productImage 				= new Application\Models\ProductImages;
			    $productImage->file_name	= $data['file_name'];
			    $productImage->file_type	= $data['file_type'];
			    $productImage->file_path	= $data['file_path'];
			    $productImage->full_path	= $data['full_path'];
			    $productImage->raw_name		= $data['raw_name'];
			    $productImage->orig_name	= $data['orig_name'];
			    $productImage->client_name	= $data['client_name'];
			    $productImage->file_ext		= $data['file_ext'];
			    $productImage->file_size	= $data['file_size'];
			    $productImage->image_width	= $data['image_width'];
			    $productImage->image_height	= $data['image_height'];
			    $productImage->image_type	= $data['image_type'];
			    $productImages[$image]  	= array('object' => base64_encode(serialize($productImage)), 'path' => _MEDIA_UPLOAD_URL_ . "{$productImage->raw_name}310x310{$productImage->file_ext}");

			    Image::load($productImage->full_path)
								->fit(Manipulations::FIT_FILL, 310, 310)
									->background('ffffff')
										->save(_MEDIA_UPLOAD_PATH_ . "{$productImage->raw_name}310x310{$productImage->file_ext}");

			    Image::load($productImage->full_path)
								->fit(Manipulations::FIT_FILL, 190, 120)
									->background('ffffff')
										->save(_MEDIA_UPLOAD_PATH_ . "{$productImage->raw_name}190x120{$productImage->file_ext}");

			    Image::load($productImage->full_path)
								->fit(Manipulations::FIT_FILL, 170, 120)
									->background('ffffff')
										->save(_MEDIA_UPLOAD_PATH_ . "{$productImage->raw_name}170x120{$productImage->file_ext}");

			    Image::load($productImage->full_path)
								->fit(Manipulations::FIT_FILL, 338, 170)
									->background('ffffff')
										->save(_MEDIA_UPLOAD_PATH_ . "{$productImage->raw_name}338x170{$productImage->file_ext}");

			}

			$this->response['status'] 	= "success";
			$this->response['message'] 	= "Images Uploaded Successfully.";
			$this->response['data']		= $productImages;
		}
		else
		{
			$this->response['status'] 	= "error";
			$this->response['message'] 	= "Something Went Wrong.";
		}
	}

	public function getLocation()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->form_validation->set_rules('location_title', 'Location', 'trim');
		$this->form_validation->set_rules('location_lat', 'Location Latitude', 'trim|required');
		$this->form_validation->set_rules('location_lon', 'Location Longitude', 'trim|required');

		if ($this->form_validation->run() === true)
        {
			$this->response['status']	= "success";

			$user_location_full = $this->input->post('location_title'); 
			$user_location 		= explode(',', $this->input->post('location_title'));
			array_pop($user_location);
			$user_location 		= trim(end($user_location));
			$location_lat 		= $this->input->post('location_lat');
			$location_lon 		= $this->input->post('location_lon');

			$params 		= array(
								'sensor' 	=> 'false',
								'latlng'	=> $location_lat . ',' . $location_lon,
								'key'		=> _GOOGLE_MAP_KEY_ 
							);

			$content 		= file_get_contents(_GOOGLE_END_POINT_ . http_build_query($params));
			$output 		= json_decode($content);

			if($output->status == 'OK')
			{
				$user_location = getLocality($output->results[0]->address_components)->long_name;
			}
			elseif($output->status == 'ZERO_RESULTS')
			{
				$this->response['status']	= "error";
				$this->response['message']	= "No Result Found.";
			}
			else
			{
				$this->response['status']	= "error";
				$this->response['message']	= "Something Went Wrong. Please Try Again.";
			}

			if($this->response['status'] != "error")
			{
				$this->response['message']	= "Returning Location Successfully.";
				$this->response['data']		= array(
													'user_location_full' => empty($user_location_full) ? $output->results[0]->formatted_address : $user_location_full,
													'user_location' => $user_location,
													'location_lat'	=> $this->input->post('location_lat'),
													'location_lon'	=> $this->input->post('location_lon'),
													''
												);
			}
		}
		else
		{
			$this->response['status']	= "error";
			$this->response['message']	= "Validation Erros.";
			$this->response['data']		= explode('|', validation_errors('', '|'));
		}
	}
}