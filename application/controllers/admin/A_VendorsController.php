<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-07-24 12:37:28
 */

class A_VendorsController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Vendors", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/vendors')];
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "List", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/seller')];

		$vendors = Application\Models\Vendors::latest('id')->get();	

		echo $this->Blade->make('admin.vendors.index', ['vendors' => $vendors]);
	}

	public function show($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "View", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/seller/' . $id)];

		$vendor = Application\Models\Vendors::find($id);
		if(!empty($vendor) && $vendor->count() > 0)
		{
			if(!$vendor->is_admin_view)
			{
				$vendor->is_admin_view = true;
				$vendor->save();
			}
			echo $this->Blade->make('admin.vendors.show', ['breadcrumbs' => $this->Breadcrumbs, 'vendor' => $vendor ]);
		}
		else
		{
			show_404();
		}
	}


	public function edit($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Edit", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/seller/edit/' . $id)];

		$vendor 	= Application\Models\Vendors::find($id);

		if(!is_null($vendor))
		{
    		// $vendor= Application\Models\Users::where('user_type', 'vendor')->get();

			echo $this->Blade->make('admin.vendors.edit', ['breadcrumbs' => $this->Breadcrumbs, 'vendor' => $vendor]);
		} 
		else 
		{
			show_404();
			die();
		}
	}

    public function update($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Edit", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/seller/edit/' . $id)];

		$vendor 	= Application\Models\Vendors::find($id);

		if(!is_null($vendor))
		{
			$this->form_validation->set_rules('cnic', 'CNIC No', 'trim|required|regex_match[/[0-9]{5}\-[0-9]{7}\-[0-9]{1}/]');
			$this->form_validation->set_rules('designation', 'Designation', 'trim|required');
			$this->form_validation->set_rules('owner_first_name', 'First Name', 'trim|required');
			$this->form_validation->set_rules('owner_last_name','Last Name', 'trim|required');
			$this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
			$this->form_validation->set_rules('operational_area', 'Operational Area', 'trim|required');
			$this->form_validation->set_rules('business_address', 'Business Address', 'trim|required');
			$this->form_validation->set_rules('business_name', 'Business Name', 'trim|required');
			$this->form_validation->set_rules('vendor_description', 'Vendor Description', 'trim|required');
			$this->form_validation->set_rules('city', 'City', 'trim|required');
			
			if ($this->form_validation->run() === true)
			{
				$cnic 						= $this->input->post('cnic');
				$designation 			    = $this->input->post('designation');
				$owner_first_name 			= $this->input->post('owner_first_name');
				$owner_last_name 			= $this->input->post('owner_last_name');
				$organization 				= $this->input->post('organization');
				$postal_code 				= $this->input->post('postal_code');
			    $operational_area 		    = $this->input->post('operational_area');
			    $business_address 		    = $this->input->post('business_address');
				//$address 				    = $this->input->post('current_address');
				$vendor_description 		= $this->input->post('vendor_description');
				$city 				    	= $this->input->post('city');
				$business_name 				= $this->input->post('business_name');
				$slug 						= slugify($this->input->post('business_name') . uniqid());
				$feature_count 				= $this->input->post('feature_count');

				$vendor->user->cnic 		    = $cnic;
				$vendor->owner_first_name 	    = $owner_first_name;
				$vendor->owner_last_name 		= $owner_last_name;
				$vendor->designation 			= $designation;
				$vendor->organization 	        =$organization;
				$vendor->user->postal_code 		= $postal_code;
				$vendor->operational_area 	    = $operational_area;
				$vendor->business_address 		= $business_address;
				$vendor->feature_count 		    = $feature_count;
				$vendor->vendor_description     = $vendor_description;
				$vendor->city 		            = $city;
				$vendor->business_name 		    = $business_name;
				$vendor->user->save();
				$vendor->save();

				$this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Seller Updated Successfully!");
				redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/seller'));
			    die();
			} 
			else 
			{//var_dump(validation_errors());die;
				$this->session->set_flashdata('status', "error");
				$this->session->set_flashdata('message', "Something Went Wrong!");
			}

			    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/seller'));
			    die();
		}
	}
	public function destroy($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$vendors 	= Application\Models\Vendors::find($id);

		if(!is_null($vendors))
		{
			$vendors->user->delete();
			$vendors->delete();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Seller Removed Successfully!");
			redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/seller'));
			die();

		} 
		else 
		{
			show_404();
			die();
		}
	}	
    
}