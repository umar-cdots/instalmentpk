<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-24 12:06:34
 */

class A_KeyFeatureController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Key Features", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/key_feature')];
		$this->load->library('form_validation');
		$this->load->helper('form');
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$key_features = Application\Models\KeyFeature::all();	

		echo $this->Blade->make('admin.key_feature.index', ['breadcrumbs' => $this->Breadcrumbs, 'key_features' => $key_features]);
	}

	public function create()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Create", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/key_feature/create')];

    	$product_categories = Application\Models\ProductCategories::all();

		echo $this->Blade->make('admin.key_feature.create', ['breadcrumbs' => $this->Breadcrumbs, 'product_categories' => $product_categories]);
    }

    public function store()
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

	    //form validation
		$this->form_validation->set_rules('product_category_id','Category', 'callback_validateCategory');
		$this->form_validation->set_rules('title','Title', 'required|trim');

	   	if ($this->form_validation->run() === true)
		{
			$title 			        		= $this->input->post('title');
			$categories 					= (array) $this->input->post('product_category_id');

			foreach($categories as $category)
			{
			    $key_feature						= new Application\Models\KeyFeature;
			    $key_feature->product_category_id 	= $category;
			    $key_feature->title 				= $title;
			    $key_feature->save();
			}

		    $this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Key Feature Saved Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/key_feature'));
		    die();
		}
		$this->session->set_flashdata('status', "error");
		$this->session->set_flashdata('message', "Something Went Wrong!");
		errorBag();
        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/key_feature/create'));
        die();	
    }
    public function show($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "View", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/key_feature/show/' . $id)];

		$key_feature = Application\Models\KeyFeature::find($id);

		if(!is_null($key_feature))
		{
			echo $this->Blade->make('admin.key_feature.show', ['breadcrumbs' => $this->Breadcrumbs, 'key_feature' => $key_feature]);
		} 
		else 
		{
			show_404();
			die();
		}

    }
    public function edit($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$key_feature 	= Application\Models\KeyFeature::find($id);
		if(!is_null($key_feature))
		{

    		$product_categories = Application\Models\ProductCategories::all();
			echo $this->Blade->make('admin.key_feature.edit', ['breadcrumbs' => $this->Breadcrumbs, 'key_feature' => $key_feature, 'product_categories' => $product_categories]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	public function update($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$key_feature 	= Application\Models\KeyFeature::find($id);

		if(!is_null($key_feature))
		{
		    //form validation
			$this->form_validation->set_rules('product_category_id','Category', 'callback_validateCategory');
			$this->form_validation->set_rules('title','Title', 'required|trim');

		   	if ($this->form_validation->run() === true)
			{
				$title 			        		= $this->input->post('title');
				$categories 					= $this->input->post('product_category_id');

			    $key_feature->product_category_id 	= $category;
			    $key_feature->title 				= $title;
			    $key_feature->save();

			    \Application\Models\ProductsMeta::where('key_feature_id', $id)->update('title', $title);
			    \Application\Models\OffersMeta::where('key_feature_id', $id)->update('title', $title);

			    $this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Key Feature Updated Successfully!");
			    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/brand'));
			    die();
			}
		    $this->session->set_flashdata('status', "error");
			$this->session->set_flashdata('message', "Something Went Wrong!");
			errorBag();
	        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/key_feature/edit/' . $id));
	        die();	
    	} 
		else 
		{
			show_404();
			die();
		}
    }
	public function destroy($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$key_feature 	= Application\Models\KeyFeature::find($id);

		if(!is_null($key_feature))
		{
			$key_feature->delete();

			\Application\Models\ProductsMeta::where('key_feature_id', $id)->delete();
		    \Application\Models\OffersMeta::where('key_feature_id', $id)->delete();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Key Feature Removed Successfully!");
			redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/key_feature'));
			die();

		} 
		else 
		{
			show_404();
			die();
		}
	}

	public function validateCategory()
	{
		$product_category_ids 	= (array)$this->input->post("product_category_id");

		$result = Application\Models\ProductCategories::whereIn('id', $product_category_ids)->get();
    	if(!empty($result))
    	{
    		return true;
    	}

    	$this->form_validation->set_message('validateCategory', 'Category does not Exist.');
    	return false;
	}
}