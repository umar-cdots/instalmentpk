<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-24 12:06:28
 */

class A_DashboardController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Dashboard", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/dashboard')];
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		echo $this->Blade->make('admin.dashboard.index', ['breadcrumbs' => $this->Breadcrumbs]);
	}
}