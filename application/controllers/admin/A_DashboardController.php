<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-24 12:06:28
 */
use Application\Models\Orders;
use Application\Models\Brands;
use Application\Models\Products;
use Application\Models\ProductCategories;
use Application\Models\OffersMeta;
use Application\Models\ProductOffers;
use Application\Models\Users;
class A_DashboardController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Dashboard", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/dashboard')];
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$total_orders                 = Orders::count();
		$total_orders_completed       = Orders::where('status', 'completed')->count();
		$total_brands                 = Brands::count();
		$total_products               = Products::count();
		$total_categories             = ProductCategories::count();
		$total_offers                 = ProductOffers::count();
		$total_customers              = Users::where('user_type', 'customer')->count();
		$total_vendors                = Users::where('user_type', 'vendor')->count();
		$new_orders_count                   = Application\Models\Orders::where('is_admin_view', false)->count();

		echo $this->Blade->make('admin.dashboard.index', ['breadcrumbs' => $this->Breadcrumbs, 'total_orders' => $total_orders,'total_brands'=> $total_brands,'total_products'=> $total_products,'total_categories'=>$total_categories,'total_offers'=>$total_offers,'total_customers'=>$total_customers,'total_vendors'=> $total_vendors,'total_orders_completed'=> $total_orders_completed, 'new_orders_count'=> $new_orders_count]);
	}

}