<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-09 18:21:43
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-07-24 12:17:12
 */
use Application\Models\Users;

class A_AuthController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Auth", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/login')];
	}

	public function login()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		
		if(LoginInfo::getInstance()->isLogin())
		{
			redirect(_ADMIN_ROUTE_PREFIX_ . '/dashboard');
			die;
		}

		echo $this->Blade->make('admin.auth.login');
	}

	public function tryLogin()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(true);

		$email 						= $this->input->post('email_address');
		$password 					= $this->input->post('password');
		$remember_me 				= $this->input->post('remember_me');

		if($this->User = LoginInfo::getInstance()->attemptLogin($email, $password, $remember_me, _VERIFICATION_OFFICER_))
		{
			redirect(_ADMIN_ROUTE_PREFIX_ . '/dashboard');
			die;
		}
		redirect(_ADMIN_ROUTE_PREFIX_ . '/login');
		die;
	}

	public function logout()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		if(LoginInfo::getInstance()->isLogin())
		{
			if(LoginInfo::getInstance()->destroySession())
			{
				$this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Logout Successfully!");
			}
		}
		redirect(_ADMIN_ROUTE_PREFIX_ . '/login');
		die;
	}

	public function profile()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(true);

		echo $this->Blade->make('admin.auth.profile', ['user' => LoginInfo::getInstance()->getUser()]);
	}

	public function updateProfile()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(true);

		$user 				        = LoginInfo::getInstance()->getUser();

		$first_name 				= $this->input->post('first_name');
        $last_name 					= $this->input->post('last_name');
		//$password 					= $this->input->post('password');
		$new_password 				= $this->input->post('new_password');
		$confirm_password 			= $this->input->post('confirm_password');

		
		if(true/*$password == decrypt_me($user->password)*/)
		{

			$user->first_name 	    = $first_name;
			$user->last_name 	    = $last_name;

			if(!empty($new_password) && $new_password == $confirm_password)
			{
				$user->password = encrypt_me($new_password);
			}
			$user->save();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Profile Updated Successfully!");
		}
		else
		{
			$this->session->set_flashdata('status', "error");
			$this->session->set_flashdata('message', "Invalid Current Password Required.");
		}

		echo $this->Blade->make('admin.auth.profile', ['user' => LoginInfo::getInstance()->getUser()]);
	}
}