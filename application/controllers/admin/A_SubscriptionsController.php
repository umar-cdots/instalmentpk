<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-24 17:06:51
 */

class A_SubscriptionsController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Subscriptions", 'is_active' => true, 'url' => site_url('admin/category')];
		$this->load->library('form_validation');
		$this->load->helper('form');
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$vendors 		= Application\Models\Vendors::all();
		$vendor_id		= $this->input->post('vendor_id') ?? '';

		if($this->input->post('vendor_id') && !empty($this->input->post('vendor_id')))
		{
			$subscriptions 	= Application\Models\SubscriptionHistory::where('vendor_id', $this->input->post('vendor_id'))->orderBy('id', 'DESC')->get();	
		}
		else
		{
			$subscriptions 	= Application\Models\SubscriptionHistory::orderBy('id', 'DESC')->get();	
		}
		$packages 		= Application\Models\SubscriptionPackages::all();

		echo $this->Blade->make('admin.subscriptions.index', ['breadcrumbs' => $this->Breadcrumbs, 'vendor_id' => $vendor_id, 'vendors' => $vendors, 'subscriptions' => $subscriptions, 'packages' => $packages]);
	}

	public function create()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Create", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/category/create')];

    	$categories = Application\Models\ProductCategories::all();

		echo $this->Blade->make('admin.category.create', ['breadcrumbs' => $this->Breadcrumbs, 'categories' => $categories]);
    }

    public function store()
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

	    //form validation
	    $this->form_validation->set_rules('parent_id', 'Parent Category', 'trim|callback_vaidateCategory');
		$this->form_validation->set_rules('category_title','Category Name', 'trim|required');
		$this->form_validation->set_rules('icon_class','Icon Class', 'trim');
		// $this->form_validation->set_rules('show_at_homepage','Show in Nav', 'trim');
		// $this->form_validation->set_rules('sort','Sort', 'trim|required');

	   	if ($this->form_validation->run() === true)
		{
			$parent_id 						                = $this->input->post('parent_id');
			$category_title 			                    = $this->input->post('category_title');
			$icon_class 			                    	= $this->input->post('icon_class');
			$show_at_homepage 			                    = $this->input->post('show_at_homepage');
			$sort 			                    			= (int)$this->input->post('sort');

		    $categroy = new Application\Models\ProductCategories;

		    $categroy->parent_id 			= $parent_id;
		    $categroy->category_title 		= $category_title;
		    $categroy->icon_class 			= $icon_class;

		    if(!empty($show_at_homepage))
		    	$categroy->show_at_homepage 	= true;

		    $categroy->sort 				= $sort;
		    $categroy->save();

		    $categroy->slug 				= slugify($category_title .'-'. $categroy->id); //Little hack to add primary key into slug
	    	$categroy->save();

		    $this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Category Saved Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/category'));
		    die();
		}
		$this->session->set_flashdata('status', "error");
		$this->session->set_flashdata('message', "Something Went Wrong!");
		errorBag();
        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/category/create'));
        die();	
    }
    public function show($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "View", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/category/show/' . $id)];

		$category = Application\Models\ProductCategories::find($id);

		if(!is_null($category))
		{
			echo $this->Blade->make('admin.category.show', ['breadcrumbs' => $this->Breadcrumbs, 'category' => $category]);
		} 
		else 
		{
			show_404();
			die();
		}

    }
    public function edit($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$categories = Application\Models\ProductCategories::all();
		$category 	= Application\Models\ProductCategories::find($id);
		if(!is_null($category))
		{
			echo $this->Blade->make('admin.category.edit', ['breadcrumbs' => $this->Breadcrumbs, 'categories' => $categories, 'category' => $category]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	public function update($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$categroy 	= Application\Models\ProductCategories::find($id);

		if(!is_null($categroy))
		{
		    //form validation
		    $this->form_validation->set_rules('parent_id', 'Parent Category', 'trim|callback_vaidateCategory', "");
			$this->form_validation->set_rules('category_title','Category Name', 'trim|required');
			$this->form_validation->set_rules('icon_class','Icon Class', 'trim');
			// $this->form_validation->set_rules('show_at_homepage','Show in Nav', 'trim');
			// $this->form_validation->set_rules('sort','Sort', 'trim|required');

		   	if ($this->form_validation->run() === true)
			{
				$parent_id 						                = $this->input->post('parent_id');
				$category_title 			                    = $this->input->post('category_title');
				$icon_class 			                    	= $this->input->post('icon_class');
				$show_at_homepage 			                    = $this->input->post('show_at_homepage');
				$sort 			                    			= (int)$this->input->post('sort');

			    $categroy->parent_id 			= $parent_id;
			    $categroy->category_title 		= $category_title;
			    $categroy->icon_class 			= $icon_class;

			    if(!empty($show_at_homepage))
			    	$categroy->show_at_homepage 	= true;

			    $categroy->sort 				= $sort;
			    $categroy->save();

			    $this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Category Updated Successfully!");
			    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/category'));
			    die();
			}
		    $this->session->set_flashdata('status', "error");
			$this->session->set_flashdata('message', "Something Went Wrong!");
			errorBag();
	        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/category/edit/' . $id));
	        die();	
    	} 
		else 
		{
			show_404();
			die();
		}
    }
	public function destroy($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$category 	= Application\Models\ProductCategories::find($id);

		if(!is_null($category))
		{
			$category->delete();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Category Removed Successfully!");
			redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/category'));
			die();

		} 
		else 
		{
			show_404();
			die();
		}
	}	
}