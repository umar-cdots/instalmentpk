<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-10-01 19:34:17
 */

class A_ProductsController extends AdminController
{
	private $productImage;

	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Products", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/products')];
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->productImage 		= null;
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$products = Application\Models\Products::all();	

		echo $this->Blade->make('admin.products.index', ['breadcrumbs' => $this->Breadcrumbs, 'products' => $products]);
	}

	public function create()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Create", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/products/create')];

    	$categories = Application\Models\ProductCategories::orderBy('category_title', 'ASC')->get();
    	$brands 	= Application\Models\Brands::orderBy('brand_title', 'ASC')->get();

		echo $this->Blade->make('admin.products.create', ['breadcrumbs' => $this->Breadcrumbs, 'categories' => $categories, 'brands' => $brands]);
    }

    public function store()
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

	    //form validation
	    $this->form_validation->set_rules('title', 'Product Title', 'trim|required');
		$this->form_validation->set_rules('brand_id', 'Brand', 'callback_validateBrand');
		$this->form_validation->set_rules('category_id', 'Category', 'callback_validateProductCategories');
		$this->form_validation->set_rules('images[]', 'Images', 'callback_validateImage');
		$this->form_validation->set_rules('description','Description', 'trim|required');

	   	if ($this->form_validation->run() === true)
		{
			$title 						= $this->input->post('title');
			$brand_id 			    	= $this->input->post('brand_id');
			$category_ids 			    = $this->input->post('category_id');
			$description 				= $this->input->post('description');
			$meta_keys 					= $this->input->post('meta_key');
			$meta_values 				= $this->input->post('meta_value');
			$images_obj					= $this->input->post('image_obj');

		    $product = new Application\Models\Products;

		    $product->title 			= $title;
		    $product->brand_id 			= $brand_id;
		    $product->description 		= $description;
		    $product->save();

		    $product->slug 				= slugify($title .'-'. $product->id); //Little hack to add primary key into slug
	    	$metas 						= array();
	    	foreach($meta_keys as $i => $key)
	    	{
	    		if(empty($key) || empty($meta_values[$i]))
	    			continue;

	    		$key_feature 	= \Application\Models\KeyFeature::find($key);
	    		if(empty($key_feature))
	    			continue;

	    		$metas[]				= new Application\Models\ProductsMeta(['key_feature_id' => $key, 'meta_key' => $key_feature->title, 'meta_value' => $meta_values[$i]]);
	    	}
	    	$product->meta()->saveMany($metas);
	    	$product->save();

	    	foreach($images_obj as $image)
	    	{
	    		$temp_obj 				= unserialize(base64_decode($image));
	    		$temp_obj->product_id 	= $product->id;
	    		$temp_obj->save();
	    	}

		    // $this->productImage->product_id	= $product->id;

		    // $product->productImages()->save($this->productImage);

		    $product->productCategories()->attach($category_ids);

		    $this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Product Saved Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/products'));
		    die();
		}
		$this->session->set_flashdata('status', "error");
		$this->session->set_flashdata('message', "Something Went Wrong!");die(validation_errors());
        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/products/create'));
        die();	
    }
    public function show($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "View", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/products/show/' . $id)];

		$product = Application\Models\Products::find($id);

		if(!is_null($product))
		{
			echo $this->Blade->make('admin.products.show', ['breadcrumbs' => $this->Breadcrumbs, 'product' => $product]);
		} 
		else 
		{
			show_404();
			die();
		}

    }
    public function edit($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$product 	= Application\Models\Products::find($id);

		if(!is_null($product))
		{
			$categories = Application\Models\ProductCategories::orderBy('category_title', 'ASC')->get();
    		$brands 	= Application\Models\Brands::orderBy('brand_title', 'ASC')->get();

			echo $this->Blade->make('admin.products.edit', ['breadcrumbs' => $this->Breadcrumbs, 'product' => $product, 'categories' => $categories, 'brands' => $brands]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	public function update($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$product 	= Application\Models\Products::find($id);

		if(!is_null($product))
		{
		    //form validation
		    $this->form_validation->set_rules('title', 'Product Title', 'trim|required');
			$this->form_validation->set_rules('brand_id', 'Brand', 'callback_validateBrand');
			$this->form_validation->set_rules('category_id', 'Category', 'callback_validateProductCategories');
			$this->form_validation->set_rules('images[]', 'Images', 'callback_validateImage');
			$this->form_validation->set_rules('description','Description', 'trim|required');

		   	if ($this->form_validation->run() === true)
			{
				$title 						= $this->input->post('title');
				$brand_id 			    	= $this->input->post('brand_id');
				$category_ids 			    = $this->input->post('category_id');
				$description 				= $this->input->post('description');
				$meta_keys 					= $this->input->post('meta_key');
				$meta_values 				= $this->input->post('meta_value');
				$images_obj					= $this->input->post('image_obj');

			    $product->title 			= $title;
			    $product->brand_id 			= $brand_id;
			    $product->description 		= $description;
			    $metas 						= array();
		    	foreach($meta_keys as $i => $key)
		    	{
		    		if(empty($key) || empty($meta_values[$i]))
		    			continue;

		    		$key_feature 	= \Application\Models\KeyFeature::find($key);
		    		if(empty($key_feature))
		    			continue;

		    		$metas[]				= new Application\Models\ProductsMeta(['key_feature_id' => $key, 'meta_key' => $key_feature->title, 'meta_value' => $meta_values[$i]]);
		    	}
		    	$product->meta()->delete();
		    	$product->meta()->saveMany($metas);
			    $product->save();

		    	foreach($images_obj as $image)
		    	{
		    		$temp_obj 				= unserialize(base64_decode($image));
		    		$temp_obj->product_id 	= $product->id;
		    		$temp_obj->save();
		    	}

			    // $this->productImage->product_id	= $product->id;
			    // $product->productImages()->save($this->productImage);

		    	$product->productCategories()->sync($category_ids);

			    $this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Product Saved Successfully!");
			    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/products'));
			    die();
			}
		    $this->session->set_flashdata('status', "error");
			$this->session->set_flashdata('message', "Something Went Wrong!");
	        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/products/create'));
	        die();	
    	} 
		else 
		{
			show_404();
			die();
		}
    }
	public function destroy($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$product 	= Application\Models\Products::find($id);

		if(!is_null($product))
		{
			$product->productCategories()->detach();
			$product->delete();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Product Removed Successfully!");
			redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/products'));
			die();

		} 
		else 
		{
			show_404();
			die();
		}
	}	

    public function validateProductCategories()
    {
    	$categories 	= $this->input->post("category_id");
    	if(is_null($categories))
    	{
    		$categories = array();
    	}
    	$product_categories = implode(',', $categories);

    	if($product_categories != '')
    		return true;

    	$this->form_validation->set_message('validateProductCategories', 'Category Required.');    	
    	return false;
    }

    public function validateBrand()
    {
    	$brand_id 	= (int)$this->input->post("brand_id");
    	if(!empty(Application\Models\Brands::find($brand_id)))
    		return true;

    	$this->form_validation->set_message('validateBrand', 'Brand does not Exist.');
    	return false;
    }

    public function validateImage()
    {
    	if(!isset($_POST['image_obj']) || count($_POST['image_obj']) == 0)
    	{
    		$this->form_validation->set_message('validateImage', "Image Required.");
    		return false;
    	}
    	return true;

    	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
    	{
	    	$config 						= array();
	    	$config['upload_path']          = _MEDIA_UPLOAD_PATH_;
	        $config['allowed_types']        = 'gif|jpg|jpeg|png';
	        $config['file_ext_tolower']     = true;
	        $config['file_name']        	= generateUniqueFilename($_FILES['image']['name']);
	        $config['max_size']             = 5120;
	        // $config['max_width']            = 1024;
	        // $config['max_height']           = 768;

	        $this->load->library('upload', $config);

	        if(!$this->upload->do_upload('image'))
	        {
	            $this->form_validation->set_message('validateImage', $this->upload->display_errors());
				return false;
	        }
	        $data 						= $this->upload->data();

	        $productImage 				= new Application\Models\ProductImages;
		    $productImage->file_name	= $data['file_name'];
		    $productImage->file_type	= $data['file_type'];
		    $productImage->file_path	= $data['file_path'];
		    $productImage->full_path	= $data['full_path'];
		    $productImage->raw_name		= $data['raw_name'];
		    $productImage->orig_name	= $data['orig_name'];
		    $productImage->client_name	= $data['client_name'];
		    $productImage->file_ext		= $data['file_ext'];
		    $productImage->file_size	= $data['file_size'];
		    $productImage->image_width	= $data['image_width'];
		    $productImage->image_height	= $data['image_height'];
		    $productImage->image_type	= $data['image_type'];

	        $this->productImage 		= $productImage;
    	}
    	elseif($this->input->post('product_images_id'))
    	{
    		$product_images_id	= (int)$this->input->post('product_images_id');
    		$this->productImage = Application\Models\ProductImages::find($product_images_id);

    		if(empty($this->productImage))
    			return false;
    	}

        return true;
    }

    public function showOffers($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "View", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/products/showOffers/' . $id)];

		$product = Application\Models\Products::find($id);
		$offers =  Application\Models\ProductOffers::where('product_id' , $id)->get();	

		if(!is_null($product))
		{
			echo $this->Blade->make('admin.products.show_offers', ['breadcrumbs' => $this->Breadcrumbs, 'product' => $product, 'offers'=>$offers]);
		} 
		else 
		{
			show_404();
			die();
		}

    }
}