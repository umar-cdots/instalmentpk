<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-24 12:06:34
 */

class A_BrandsController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Brand", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/brand')];
		$this->load->library('form_validation');
		$this->load->helper('form');
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$brands = Application\Models\Brands::all();	

		echo $this->Blade->make('admin.brand.index', ['breadcrumbs' => $this->Breadcrumbs, 'brands' => $brands]);
	}

	public function create()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Create", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/brand/create')];

    	$brands = Application\Models\Brands::all();

		echo $this->Blade->make('admin.brand.create', ['breadcrumbs' => $this->Breadcrumbs, 'brands' => $brands]);
    }

    public function store()
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

	    //form validation
		$this->form_validation->set_rules('brand_title','Brand Name', 'trim|required');
		$this->form_validation->set_rules('logo', 'Logo', 'callback_validateLogo');
		$this->form_validation->set_rules('description','Description', 'trim');

	   	if ($this->form_validation->run() === true)
		{
			$brand_title 			        = $this->input->post('brand_title');
			$description 			        = $this->input->post('description');

		    $brand 							= new Application\Models\Brands;

		    $brand->brand_title 			= $brand_title;
		    $brand->upload_id 				= $this->logo->id;
		    $brand->description 			= $description;
		    $brand->save();

		    $brand->slug 					= slugify($brand_title .'-'. $brand->id); //Little hack to add primary key into slug
	    	$brand->save();

		    $this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Brand Saved Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/brand'));
		    die();
		}
		$this->session->set_flashdata('status', "error");
		$this->session->set_flashdata('message', "Something Went Wrong!");
		errorBag();
        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/brand/create'));
        die();	
    }
    public function show($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "View", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/brand/show/' . $id)];

		$brand = Application\Models\Brands::find($id);

		if(!is_null($brand))
		{
			echo $this->Blade->make('admin.brand.show', ['breadcrumbs' => $this->Breadcrumbs, 'brand' => $brand]);
		} 
		else 
		{
			show_404();
			die();
		}

    }
    public function edit($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$brand 	= Application\Models\Brands::find($id);
		if(!is_null($brand))
		{
			echo $this->Blade->make('admin.brand.edit', ['breadcrumbs' => $this->Breadcrumbs, 'brand' => $brand]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	public function update($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$brand 	= Application\Models\Brands::find($id);

		if(!is_null($brand))
		{
		    //form validation
			$this->form_validation->set_rules('brand_title','Brand Name', 'trim|required');
			$this->form_validation->set_rules('logo', 'Logo', 'callback_validateLogo');
			$this->form_validation->set_rules('description','Description', 'trim|required');

		   	if ($this->form_validation->run() === true)
			{
				$brand_title 			    = $this->input->post('brand_title');
				$description 			    = $this->input->post('description');

			    $brand->brand_title 		= $brand_title;
		    	$brand->upload_id 			= $this->logo->id;
			    $brand->description 		= $description;
			    $brand->save();

			    $this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Brand Updated Successfully!");
			    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/brand'));
			    die();
			}
		    $this->session->set_flashdata('status', "error");
			$this->session->set_flashdata('message', "Something Went Wrong!");
			errorBag();
	        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/brand/edit/' . $id));
	        die();	
    	} 
		else 
		{
			show_404();
			die();
		}
    }
	public function destroy($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$brand 	= Application\Models\Brands::find($id);

		if(!is_null($brand))
		{
			$brand->delete();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Brand Removed Successfully!");
			redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/brand'));
			die();

		} 
		else 
		{
			show_404();
			die();
		}
	}

    public function validateLogo()
    {
    	$field_name 	= 'logo';
    	if(isset($_FILES[$field_name]['name']) && !empty($_FILES[$field_name]['name']))
    	{
	    	$config 						= array();
	    	$config['upload_path']          = _MEDIA_UPLOAD_PATH_;
	        $config['allowed_types']        = 'gif|jpg|jpeg|png';
	        $config['file_ext_tolower']     = true;
	        $config['file_name']        	= generateUniqueFilename($_FILES[$field_name]['name']);
	        $config['max_size']             = 5120;
	        // $config['max_width']            = 1024;
	        // $config['max_height']           = 768;

	        $this->load->library('upload', $config);

	        if(!$this->upload->do_upload($field_name))
	        {
	            $this->form_validation->set_message('validateLogo', $this->upload->display_errors());
				return false;
	        }
	        $data 						= $this->upload->data();

	        $logo 						= new Application\Models\Uploads;
		    $logo->file_name			= $data['file_name'];
		    $logo->file_type			= $data['file_type'];
		    $logo->file_path			= $data['file_path'];
		    $logo->full_path			= $data['full_path'];
		    $logo->raw_name				= $data['raw_name'];
		    $logo->orig_name			= $data['orig_name'];
		    $logo->client_name			= $data['client_name'];
		    $logo->file_ext				= $data['file_ext'];
		    $logo->file_size			= $data['file_size'];
		    $logo->image_width			= $data['image_width'];
		    $logo->image_height			= $data['image_height'];
		    $logo->image_type			= $data['image_type'];
		    $logo->save();

	        $this->logo 				= $logo;
	        return true;
    	}
    	elseif($this->input->post('upload_id'))
    	{
    		$upload_id	= (int)$this->input->post('upload_id');
    		$this->logo = Application\Models\Uploads::find($upload_id);

    		if(empty($this->logo))
    		{
    			$this->form_validation->set_message('validateLogo', "Something Went Wrong.");
    			return false;
    		}
    	}

    	$this->form_validation->set_message('validateLogo', "Brand Logo is Required.");
        return false;
    }	
}