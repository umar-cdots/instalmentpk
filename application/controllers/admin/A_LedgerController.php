<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Nadeem
 * @Date:   2018-09-24 7:05:00
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-25 19:20:44
 */

use \Application\Models\Ledger;
use \Carbon\Carbon;

class A_LedgerController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Ledger", 'is_active' => true, 'url' => site_url('admin/ledger')];
		$this->load->library('form_validation');
		$this->load->helper('form');
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$vendors 		= Application\Models\Vendors::all();
		$user_id		= $this->input->post('user_id') ?? '';
		$date_from		= $this->input->post('date_from') ?? '';
		$date_to		= $this->input->post('date_to') ?? '';

		$ledgers 		= Application\Models\Ledger::orderBy('id', 'DESC');

		if(!empty($user_id))
		{
			$ledgers 	= $ledgers->where('user_id', $user_id);	
		}
		if(!empty($date_from))
		{
			$ledgers 	= $ledgers->where('created_at', '>=', Carbon::parse($date_from)->toDateTimeString());
		}
		if(!empty($date_to))
		{
			$ledgers 	= $ledgers->where('created_at', '<=', Carbon::parse($date_to)->format('Y-m-d'));
		}
		
		echo $this->Blade->make('admin.ledger.index', ['breadcrumbs' => $this->Breadcrumbs, 'ledgers' => $ledgers->get(), 'vendors' => $vendors, 'user_id' => $user_id, 'date_from' => $date_from, 'date_to' => $date_to]);
	}

	public function create()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Create", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/ledger/create')];

    	$categories = Application\Models\ProductCategories::all();
		$vendors = Application\Models\Vendors::all();

		echo $this->Blade->make('admin.ledger.create', ['breadcrumbs' => $this->Breadcrumbs, 'categories' => $categories,'vendors' => $vendors]);
    }

    public function store()
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
		
		//var_dump($_POST);
		//die();

	    //form validation
	    $this->form_validation->set_rules('user_id', 'Select User', 'required');
		$this->form_validation->set_rules('payment_type','Payment Type', 'required');
		$this->form_validation->set_rules('amount','Amount','trim|required');
		$this->form_validation->set_rules('description','Description','trim');
		// $this->form_validation->set_rules('sort','Sort', 'trim|required');

	   	if ($this->form_validation->run() === true)
		{
			$user_id 						            = $this->input->post('user_id');
			$payment_type 			                    = $this->input->post('payment_type');
			$amount 			                    	= $this->input->post('amount');
			$description 			                    = $this->input->post('description');
			
			
			$ledger = new Ledger();
			$ledger->user_id = $user_id;
			$ledger->product_id = 0;
			$ledger->description = $description;
			$ledger->amount = $amount;
			$ledger->type = 'payment';
			$ledger->payment_type = $payment_type;
			$ledger->save();
			
		    /*
			$categroy = new Application\Models\ProductCategories;

		    $categroy->parent_id 			= $parent_id;
		    $categroy->category_title 		= $category_title;
		    $categroy->icon_class 			= $icon_class;

		    if(!empty($show_at_homepage))
		    	$categroy->show_at_homepage 	= true;

		    $categroy->sort 				= $sort;
		    $categroy->save();

		    $categroy->slug 				= slugify($category_title .'-'. $categroy->id); //Little hack to add primary key into slug
	    	$categroy->save();
			*/

		    $this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Category Saved Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/ledger'));
		    die();
		}
		$this->session->set_flashdata('status', "error");
		$this->session->set_flashdata('message', "Something Went Wrong!");
		errorBag();
        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/ledger/create'));
        die();	
    }
    
	public function show($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "View", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/ledger/show/' . $id)];

		$ledger = Application\Models\Ledger::find($id);

		if(!is_null($ledger))
		{
			echo $this->Blade->make('admin.ledger.show', ['breadcrumbs' => $this->Breadcrumbs, 'ledger' => $ledger]);
		} 
		else 
		{
			show_404();
			die();
		}

    }
	
    public function edit($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		//$categories = Application\Models\ProductCategories::all();
		//$category 	= Application\Models\ProductCategories::find($id);
		$vendors = Application\Models\Vendors::all();
		$ledger = Application\Models\Ledger::find($id);
		
		if(!is_null($ledger))
		{
			echo $this->Blade->make('admin.ledger.edit', ['breadcrumbs' => $this->Breadcrumbs,'ledger' => $ledger, 'vendors' => $vendors]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	
	public function update($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$ledger = Application\Models\Ledger::find($id);

		if(!is_null($ledger))
		{
		    //form validation
			//$this->form_validation->set_rules('user_id', 'Select User', 'required');
			$this->form_validation->set_rules('payment_type','Payment Type', 'required');
			$this->form_validation->set_rules('amount','Amount','trim|required');
			$this->form_validation->set_rules('description','Description','trim');
			// $this->form_validation->set_rules('show_at_homepage','Show in Nav', 'trim');
			// $this->form_validation->set_rules('sort','Sort', 'trim|required');

		   	if ($this->form_validation->run() === true)
			{
				//$user_id 						            = $this->input->post('user_id');
				$payment_type 			                    = $this->input->post('payment_type');
				$amount 			                    	= $this->input->post('amount');
				$description 			                    = $this->input->post('description');

			    $ledger->payment_type 				= $payment_type;
			    $ledger->amount 		= $amount;
			    $ledger->description 			= $description;

			    $ledger->save();

			    $this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Category Updated Successfully!");
			    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/ledger'));
			    die();
			}
		    $this->session->set_flashdata('status', "error");
			$this->session->set_flashdata('message', "Something Went Wrong!");
			errorBag();
	        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/ledger/edit/' . $id));
	        die();	
    	} 
		else 
		{
			show_404();
			die();
		}
    }
	
	public function destroy($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$category 	= Application\Models\ProductCategories::find($id);

		if(!is_null($category))
		{
			$category->delete();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Category Removed Successfully!");
			redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/category'));
			die();

		} 
		else 
		{
			show_404();
			die();
		}
	}	
    public function vaidateCategory()
    {
    	$category_id	= (int)$this->input->post("parent_id");
    	if(!empty($category_id))
    	{
    		$this->form_validation->set_message('vaidateCategory', 'Invalid Parent Category.');
    		return !empty(Application\Models\ProductCategories::find($category_id)) ? true : false;
    	}
    	return true;
    }
}