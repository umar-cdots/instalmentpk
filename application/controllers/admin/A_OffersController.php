<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-07-24 12:37:28
 */

class A_OffersController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "Offers", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/offers')];
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->productImage 		= null;
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$offers = Application\Models\ProductOffers::latest('id')->get();
		echo $this->Blade->make('admin.offers.index', ['breadcrumbs' => $this->Breadcrumbs, 'offers' => $offers]);
	}

	public function create()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Create", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/offers/create')];

    	$products 	= Application\Models\Products::orderBy('title', 'ASC')->get();
    	$vendors	= Application\Models\Users::where('user_type', 'vendor')->where('vendor_id', '!=', 'null')->get();


		echo $this->Blade->make('admin.offers.create', ['breadcrumbs' => $this->Breadcrumbs, 'products' => $products, 'vendors' => $vendors]);
    }

    public function store()
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

	    //form validation
	    $this->form_validation->set_rules('product_id', 'Product', 'callback_validateProduct');
		$this->form_validation->set_rules('total_installments[]', 'No. of Instalments', 'required');
		$this->form_validation->set_rules('total_price_on_installment[]', 'Total Amount', 'required');
		$this->form_validation->set_rules('down_payment[]', 'Down Payment', 'required');
		$this->form_validation->set_rules('amount_per_month[]','Per Month', 'required');
		$this->form_validation->set_rules('offer_notes','Offer Notes', 'trim');
	    $this->form_validation->set_rules('vendor_id', 'Vendor', 'callback_validateVendor');

	   	if ($this->form_validation->run() === true)
		{
			$product_id 				= $this->input->post('product_id');
			$total_installments 		= $this->input->post('total_installments');
			$total_price_on_installment = $this->input->post('total_price_on_installment');
			$down_payment 				= $this->input->post('down_payment');
			$amount_per_month 			= $this->input->post('amount_per_month');
			$offer_notes 				= $this->input->post('offer_notes');

			$now 						= Carbon\Carbon::now()->toDateTimeString();
			$vendor_id					= $this->input->post('vendor_id');

			$product_metas 				= \Application\Models\Products::find($product_id)->meta;

			$metas 						= array();
	    	// foreach($meta_keys as $i => $key)
	    	foreach($product_metas as $meta)
	    	{
	    		// if(empty($key) || empty($meta_values[$i]))
	    			// continue;
	    		// $metas[]				= ['meta_key' => $key, 'meta_value' => $meta_values[$i], 'created_at' => $now, 'updated_at' => $now];
	    		$metas[]				= ['key_feature_id' => $meta->key_feature_id, 'meta_key' => $meta->meta_key, 'meta_value' => $meta->meta_value];
	    	}

			// $offers_array 				= array();

			foreach($total_installments as $i => $instalment)
			{
				$offers_array			= array(
					'vendor_id'						=> $vendor_id,
					'product_id'					=> $product_id,
					'total_installments'			=> (int) $instalment,
					'total_price_on_installment'	=> (float) $total_price_on_installment[$i],
					'down_payment'					=> (float) $down_payment[$i],
					'amount_per_month'				=> (float) $amount_per_month[$i],
					'offer_notes'					=> $offer_notes,
					'slug'							=> slugify(Application\Models\Products::find($product_id)->title .'-'. substr(uniqid(rand(10000, 99999).microtime()), 0, 8)),
					'status'						=> "active",
					'created_at'					=> $now,
					'updated_at'					=> $now,
				);

				$offer_id = Application\Models\ProductOffers::insertGetId($offers_array);

				foreach($metas as $i => $meta)
				{
					$metas[$i]['product_offer_id']	= $offer_id;
				}
				Application\Models\OffersMeta::insert($metas);
			}

			// Application\Models\ProductOffers::insert($offers_array);

		    $this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Offer Saved Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/offers'));
		    die();
		}
		$this->session->set_flashdata('status', "error");
		$this->session->set_flashdata('message', "Something Went Wrong!");
        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/offers/create'));
        die();	
    }
    public function show($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "View", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/offers/show/' . $id)];

		$offer = Application\Models\ProductOffers::where('id', $id)->first();

		if(!is_null($offer))
		{
			echo $this->Blade->make('admin.offers.show', ['breadcrumbs' => $this->Breadcrumbs, 'offer' => $offer]);
		} 
		else 
		{
			show_404();
			die();
		}

    }
    public function edit($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] = false;
    	$this->Breadcrumbs[] 		= ['title' => "Edit", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/offers/edit/' . $id)];

		$offer 	= Application\Models\ProductOffers::find($id);

		if(!is_null($offer))
		{
    		$products 	= Application\Models\Products::orderBy('title', 'ASC')->get();
    		$vendors	= Application\Models\Users::where('user_type', 'vendor')->get();

			echo $this->Blade->make('admin.offers.edit', ['breadcrumbs' => $this->Breadcrumbs, 'offer' => $offer, 'products' => $products, 'vendors' => $vendors]);
		} 
		else 
		{
			show_404();
			die();
		}
	}
	public function update($id)
    {
    	$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$offer 	= Application\Models\ProductOffers::find($id);

		if(!is_null($offer))
		{
		    //form validation
		    $this->form_validation->set_rules('product_id', 'Product', 'callback_validateProduct');
			$this->form_validation->set_rules('total_installments', 'No. of Instalments', 'required');
			$this->form_validation->set_rules('total_price_on_installment', 'Total Amount', 'required');
			$this->form_validation->set_rules('down_payment', 'Down Payment', 'required');
			$this->form_validation->set_rules('amount_per_month','Per Month', 'required');
			$this->form_validation->set_rules('offer_notes','Offer Notes', 'trim');

		   	if ($this->form_validation->run() === true)
			{
				$product_id 				= $this->input->post('product_id');
				$total_installments 		= $this->input->post('total_installments');
				$total_price_on_installment = $this->input->post('total_price_on_installment');
				$down_payment 				= $this->input->post('down_payment');
				$amount_per_month 			= $this->input->post('amount_per_month');
				$offer_notes 				= $this->input->post('offer_notes');

				$offer->product_id 					= $product_id;
				$offer->total_installments 			= (int) $total_installments;
				$offer->total_price_on_installment 	= (float) $total_price_on_installment;
				$offer->down_payment 				= (float) $down_payment;
				$offer->amount_per_month 			= (float) $amount_per_month;
				$offer->offer_notes 				= $offer_notes;
				$offer->save();

			    $this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Offer Saved Successfully!");
			    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/offers'));
			    die();
			}
		    $this->session->set_flashdata('status', "error");
			$this->session->set_flashdata('message', "Something Went Wrong!");
	        redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/offers/create'));
	        die();	
    	} 
		else 
		{
			show_404();
			die();
		}
    }

	public function destroy($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$offer 	= Application\Models\ProductOffers::find($id);

		if(!is_null($offer))
		{
			$offer->delete();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Offer Removed Successfully!");
			redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/offers'));
			die();

		} 
		else 
		{
			show_404();
			die();
		}
	}	
    
	public function feature($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$offer 	= Application\Models\ProductOffers::find($id);

		if(!is_null($offer))
		{
			$offer->is_featured 	= true;
			$offer->featured_from 	= Carbon\Carbon::now();
			$offer->featured_to 	= Carbon\Carbon::now()->addMonths(1);
			$offer->save();

			$leadger = new Application\Models\Ledger();
			$leadger->user_id               = $offer->vendor->user->id;
			$leadger->product_id            = $offer->product_id;
			$leadger->description           = "Feaatured Product";
			$leadger->amount                = 500;
			$leadger->type                  = "featured";
			$leadger->payment_type          = "debit";
			$leadger->save();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Offer Marked as Featured!");

			$this->load->library('user_agent');
			if ($this->agent->referrer())
			{
				redirect($this->agent->referrer());
				die;
			}

			redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/offers/' . $id));
			die();

		} 
		else 
		{
			show_404();
			die();
		}
	}
    
	public function unFeature($id)
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$offer 	= Application\Models\ProductOffers::find($id);

		if(!is_null($offer))
		{
			$offer->is_featured 	= false;
			$offer->featured_from 	= "";
			$offer->featured_to 	= "";
			$offer->save();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Offer Unfeatured Successfully!");

			$this->load->library('user_agent');
			if ($this->agent->referrer())
			{
				redirect($this->agent->referrer());
				die;
			}

			redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/offers/' . $id));
			die();
		} 
		else 
		{
			show_404();
			die();
		}
	}

    public function validateProduct()
    {
    	$product_id 	= (int)$this->input->post("product_id");
    	if(!empty(Application\Models\Products::find($product_id)))
    		return true;

    	$this->form_validation->set_message('validateProduct', 'Product does not Exist.');
    	return false;
    }

    public function validateVendor()
    {
    	$vendor_id 	= (int)$this->input->post("vendor_id");
    	if(!empty(Application\Models\Users::where('vendor_id', $vendor_id)->where('user_type', 'vendor')->first()))
    		return true;

    	$this->form_validation->set_message('validateVendor', 'Vendor does not Exist.');
    	return false;
    }
}