<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-05-11 19:47:10
 */

class A_CmsController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "CMS", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/cms')];
		$this->load->library('form_validation');
		$this->load->helper('form');
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
		echo $this->Blade->make('admin.cms.about', ['breadcrumbs' => $this->Breadcrumbs]);
	}

	public function about()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] 	= false;
		$this->Breadcrumbs[] 		= ['title' => "About Us", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/cms/about-us')];

		$about = Application\Models\CMS::where('page_title', 'about-us')->first();

		echo $this->Blade->make('admin.cms.about', ['breadcrumbs' => $this->Breadcrumbs, 'about' => $about]);
	}

	public function aboutUsSave()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$about_us 					= $this->input->post('about_us');
		//die($cms);
		if(!empty($about_us))
		{
			// $cms->page_content 		= $about_us;
			// $cms->save();
			$cms 					= Application\Models\CMS::where('page_title', 'about-us')->first();
			if(empty($cms))
				$cms 				= new Application\Models\CMS();

			$cms->page_title 		= 'about-us';
			$cms->page_content 		= $about_us;
			$cms->save();
			// $cms 					= Application\Models\CMS::where('page_title', 'about-us')->firstOrCreate(['page_title' => 'about-us', 'page_content' => $about_us]);

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "About Us Content Updated Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/cms/about-us'));
		    die();
		}
		else 
		{
			show_404();
			die;
		}
	}
	public function faq()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] 	= false;
		$this->Breadcrumbs[] 		= ['title' => "FAQ", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/cms/faq')];

		$faq = Application\Models\CMS::where('page_title', 'faq')->first();

		echo $this->Blade->make('admin.cms.faq', ['breadcrumbs' => $this->Breadcrumbs, 'faq' => $faq]);
	}
	public function faqSave()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$faq 					= $this->input->post('faq');
		//die($cms);
		if(!empty($faq))
		{
			// $cms->page_content 		= $about_us;
			// $cms->save();
			$cms 					= Application\Models\CMS::where('page_title', 'faq')->first();
			if(empty($cms))
				$cms 				= new Application\Models\CMS();
			
			$cms->page_title 		= 'faq';
			$cms->page_content 		= $faq;
			$cms->save();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "FAQ Content Updated Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/cms/faq'));
		    die();
		}
		else 
		{
			show_404();
			die;
		}
	}
	public function terms()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] 	= false;
		$this->Breadcrumbs[] 		= ['title' => "Terms & Conditions", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/cms/terms-conditions')];

		$terms_conditions = Application\Models\CMS::where('page_title', 'terms-conditions')->first();

		echo $this->Blade->make('admin.cms.terms', ['breadcrumbs' => $this->Breadcrumbs, 'terms_conditions' => $terms_conditions]);
	}
	public function termsconditionsSave()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$terms_conditions 					= $this->input->post('terms-conditions');
		if(!empty($terms_conditions))
		{
			$cms 	= Application\Models\CMS::where('page_title', 'terms-conditions')->first();
			if(empty($cms))
		    $cms                   = new Application\Models\CMS();
			
			$cms->page_title 		= 'terms-conditions';
			$cms->page_content 		= $terms_conditions;
			$cms->save();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Terms Conditions Content Updated Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/cms/terms-conditions'));
		    die();
		}
		else 
		{
			show_404();
			die;
		}
	}
	public function policy()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] 	= false;
		$this->Breadcrumbs[] 		= ['title' => "Privacy Policy", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/cms/privacy-policy')];

		$privacy_policy = Application\Models\CMS::where('page_title', 'privacy-policy')->first();

		echo $this->Blade->make('admin.cms.policy', ['breadcrumbs' => $this->Breadcrumbs, 'privacy_policy' => $privacy_policy]);
	}
	public function privacyPolicy()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$privacy_policy 					= $this->input->post('privacy-policy');
		//die($terms_conditions);
		if(!empty($privacy_policy))
		{
			// $cms->page_content 		= $about_us;
			// $cms->save();
			$cms 					= Application\Models\CMS::where('page_title', 'privacy-policy')->first();
			if(empty($cms))
				$cms 				= new Application\Models\CMS();
			
			$cms->page_title 		= 'privacy-policy';
			$cms->page_content 		= $privacy_policy;
			$cms->save();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Privacy Policy Content Updated Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/cms/privacy-policy'));
		    die();
		}
		else 
		{
			show_404();
			die;
		}
	}
	public function corporate()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] 	= false;
		$this->Breadcrumbs[] 		= ['title' => "Corporate Installments", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/cms/corporate-installments')];

		$corporate_installments = Application\Models\CMS::where('page_title', 'corporate-installments')->first();

		echo $this->Blade->make('admin.cms.corporate', ['breadcrumbs' => $this->Breadcrumbs, 'corporate_installments' => $corporate_installments]);
	}
	public function corporateInstallments()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$corporate_installments 					= $this->input->post('corporate-installments');
	if(!empty($corporate_installments))
			{
				// $cms->page_content 		= $about_us;
				// $cms->save();
				$cms 					= Application\Models\CMS::where('page_title', 'corporate-installments')->first();
				if(empty($cms))
					$cms 				= new Application\Models\CMS();
				
				$cms->page_title 		= 'corporate-installments';
				$cms->page_content 		= $corporate_installments;
				$cms->save();

				$this->session->set_flashdata('status', "success");
				$this->session->set_flashdata('message', "Corporate Installments Content Updated Successfully!");
			    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/cms/corporate-installments'));
			    die();
			}
		else 
		{
			show_404();
			die;
		}
	}
	public function buy()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] 	= false;
		$this->Breadcrumbs[] 		= ['title' => "Why buy from Us", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/cms/Why-buy-from-us')];

		$Why_buy_from_us = Application\Models\CMS::where('page_title', 'Why-buy-from-us')->first();

		echo $this->Blade->make('admin.cms.buy', ['breadcrumbs' => $this->Breadcrumbs, 'Why_buy_from_us' => $Why_buy_from_us]);
	}
	public function whyBuy()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$Why_buy_from_us 					= $this->input->post('Why-buy-from-us');
		//die($corporate_installments);
	if(!empty($Why_buy_from_us))
			{
				// $cms->page_content 		= $about_us;
				// $cms->save();
				$cms 					= Application\Models\CMS::where('page_title', 'Why-buy-from-us')->first();
				if(empty($cms))
					$cms 				= new Application\Models\CMS();
				
				$cms->page_title 		= 'Why-buy-from-us';
				$cms->page_content 		= $Why_buy_from_us;
				$cms->save();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Why buy from Us Content Updated Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/cms/Why-buy-from-us'));
		    die();
		}
		else 
		{
			show_404();
			die;
		}
	}
	public function islamicFinancing()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] 	= false;
		$this->Breadcrumbs[] 		= ['title' => "Islamic Financing", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/cms/islamic-financing')];

		$islamic_financing = Application\Models\CMS::where('page_title', 'islamic-financing')->first();

		echo $this->Blade->make('admin.cms.financing', ['breadcrumbs' => $this->Breadcrumbs, 'islamic_financing' => $islamic_financing]);
	}
	public function Financing()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$islamic_financing 					= $this->input->post('islamic-financing');
		//die($corporate_installments);
		if(!empty($islamic_financing))
			{
				// $cms->page_content 		= $about_us;
				// $cms->save();
				$cms 					= Application\Models\CMS::where('page_title', 'islamic-financing')->first();
				if(empty($cms))
					$cms 				= new Application\Models\CMS();
				
				$cms->page_title 		= 'islamic-financing';
				$cms->page_content 		= $islamic_financing;
				$cms->save();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "Why buy from Us Content Updated Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/cms/islamic-financing'));
		    die();
		}
		else 
		{
			show_404();
			die;
		}
	}
	public function contactUsSave()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		// $contact = 
	}
}