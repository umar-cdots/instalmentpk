<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-08 13:04:18
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-05-11 19:47:10
 */

class A_CmsController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->CurrentController 	= __CLASS__; 
		$this->Breadcrumbs[] 		= ['title' => "CMS", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/cms')];
		$this->load->library('form_validation');
		$this->load->helper('form');
	}

	public function index()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
		echo $this->Blade->make('admin.cms.about', ['breadcrumbs' => $this->Breadcrumbs]);
	}

	public function about()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$this->Breadcrumbs[0]['is_active'] 	= false;
		$this->Breadcrumbs[] 		= ['title' => "About Us", 'is_active' => true, 'url' => site_url(_ADMIN_ROUTE_PREFIX_ . '/cms/about-us')];

		$about = Application\Models\CMS::where('page_title', 'about-us')->first()->page_content;

		echo $this->Blade->make('admin.cms.about', ['breadcrumbs' => $this->Breadcrumbs, 'about' => $about]);
	}

	public function aboutUsSave()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);

		$about_us 					= $this->input->post('about_us');
		$cms 						= Application\Models\CMS::where('page_title', 'about-us')->first();
		if(!empty($cms))
		{
			$cms->page_content 		= $about_us;
			$cms->save();

			$this->session->set_flashdata('status', "success");
			$this->session->set_flashdata('message', "About Us Content Updated Successfully!");
		    redirect(base_url(_ADMIN_ROUTE_PREFIX_ . '/cms/about-us'));
		    die();
		}
		else 
		{
			show_404();
			die;
		}
	}
	public function contactUsSave()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->isAuthentic(false);
	}
}