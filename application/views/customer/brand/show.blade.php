@extends('customer.layouts.master')
@section('title', "{$brand->brand_title} - Offers")

{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start --}}
@push('scripts')
    <script src="{{ base_url("assets/js/owl.carousel.min.js")}}"></script> 
@endpush
{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start End --}}

@section('content')
  @include('customer.layouts.featured')
  <div class="offers deals results">
    <div class="container">
      <h2 class="brandTitle">{{ $brand->brand_title }} <span class="totalPro">Total Product : {{ $offers->total() }}</span></h2>
      <div class="lat_left">
        <img style="width: 100%;" src="{{ isset($brand->logo->file_name) && is_file(_MEDIA_UPLOAD_PATH_ . $brand->logo->file_name) ? _MEDIA_UPLOAD_URL_ . $brand->logo->file_name : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $brand->brand_title }}">
        {{-- <h3 style="font-size: 16px;">{{ $brand->brand_title }}</h3> --}}
        {{-- <p>{{ $brand->description }}</p> --}}
      </div>
       <div class="lat_hold">
        <p class="">{{ $brand->description }}</p>
        {{-- <a class="showMore">Read More</a> --}}
        </div>
        <div class="clearfix"></div>
        <div class="dealsBrandPage">
          @foreach($offers as $offer) 
           @continue(empty($offer->vendor))
         <div class="deals_box offer_search_box mrgn">
        <div class="box_img deals_box_img deals_box_img2"> 
          <img class="zoomImageHover" src="{{ isset($offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$offer->product->productImages[0]->raw_name}190x120{$offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $offer->product->title }}" style="width: 189px; height: 117px;">
          <h3><a href="{{ base_url('offer/' . $offer->slug) }}" title="{{ $offer->product->title }}">{{ str_limit($offer->product->title, 20) }}</a></h3>
        </div>
        <div class="box_details deals_box_details">
          <h2><span class="rupeesText">Rs.</span>{{ number_format($offer->amount_per_month) }}<span>/month</span></h2>
          <h4> {{ $offer->vendor->businessName() }} <span>{{ $offer->vendor->business_address }}</span></h4>
          <a href="{{ base_url('offer/' . $offer->slug) }}">View Details</a>
        </div>
      </div> 
      @endforeach
        <div class="clearfix"></div>
      </div>
      {{ $offers->links() }}
    </div>
  </div>
@endsection
@push('scripts')
<script type="text/javascript">
  $(document).ready(function(){
    $('.showMore').on('click', function(){
      $('.lat_hold p').toggleClass("heightPara");
      if($('.lat_hold p').hasClass("heightPara"))
      {
          $('.showMore').text("Read More");
      }
      else
      {
        $('.showMore').text("Read Less");
      }
    });
  });
</script>
@endpush