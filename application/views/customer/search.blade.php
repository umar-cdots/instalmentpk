@extends('customer.layouts.master')
@section('title', (method_exists($offers, 'count') ? $offers->count() : '') . " Result(s) Found - {$keyword}")

@section('content')
<div class="offers deals results" style="padding: 50px 0;">
	<div class="container">
		<h2>{{ method_exists($offers, 'count') ? $offers->count() : 0 }} Results Found</h2>
		<div class="result_box pro_sold_result">
	      	<form action="" method="post">
		        <div class="result_sm_fld">
		          <h3>Advance:</h3>
		          <div id="slider-range"></div>
		            <input type="text" id="amount" readonly style="border:0; color:#1e9fbc; font-weight:bold;">
		            <input type="hidden" name="max_down_payment" value="{{ $max_down_payment ?? '200000' }}">
		            <input type="hidden" name="min_down_payment" value="{{ $min_down_payment ?? '0' }}">
		        </div>
		        <div class="result_sm_fld_hf">
		          <h3>Max Instalments:</h3>
		          <select name="max_total_installments">
		            @foreach(range(24, 1) as $month)
		              <option value="{{ $month }}" {{ isset($max_total_installments) && $max_total_installments == $month ? 'selected' : '' }}>{{ $month }}</option>
		            @endforeach
		          </select>
		        </div>
		        <div class="result_sm_fld">
		          <h3>EMI:</h3>
		          <div id="slider-range2"></div>
		            <input type="text" id="amount2" readonly style="border:0; color:#1e9fbc; font-weight:bold;">
		            <input type="hidden" name="max_amount_per_month" value="{{ $max_amount_per_month ?? '200000' }}">
		            <input type="hidden" name="min_amount_per_month" value="{{ $min_amount_per_month ?? '0' }}">
		        </div>
		        <div class="result_sm_fld_hf">
		          <h3>Sort By:</h3>
		          <select name="sort_by">
		            <option value="latest" {{ isset($sort_by) && $sort_by == 'latest' ? 'selected' : '' }}>Latest</option>
		            <option value="popular" {{ isset($sort_by) && $sort_by == 'popular' ? 'selected' : '' }}>Most Popular</option>
		          </select>
		        </div>
		        <div class="result_sm_fld_hf result_sm_fld_hf2">
		          <button type="submit" name="filter">Apply Search</button>   
		        </div>
		        <div class="clearfix"></div>
		      </form>
	    </div>
	    @foreach($offers as $offer)  
 		  	<div class="deals_box offer_search_box">
		        <div class="box_img deals_box_img deals_box_img2 {{ in_array($offer->id, $wishlist) ? "activepro" : "" }}" data-product-offer-id="{{ $offer->id }}"> 
		          <img src="{{ isset($offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . $offer->product->productImages[0]->file_name : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $offer->product->title }}" style="width: 189px; height: 117px;">
		          <h3>{{ $offer->product->title }}</h3>
		        </div>
		        <div class="box_details deals_box_details">
		          <h2>Rs.{{ number_format($offer->amount_per_month) }}<span>/month</span></h2>
		          <h4><span>By:</span> {{ $offer->vendor->businessName() }}</h4>
		          <a href="{{ base_url('offer/' . $offer->slug) }}">View Details</a>
		        </div>
	      	</div> 
      	@endforeach
    	<div class="clearfix"></div>
  	</div>
</div>
@endsection


@push('styles')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
	$('#slider-range').slider({
      range: true,
      min: 0,
      max: 200000,
      values: [{{ $min_down_payment ?? 0 }}, {{ $max_down_payment ?? 200000 }}],
      slide: function(event, ui) {
        $("#amount").val( "Rs" + ui.values[0] + " - Rs" + ui.values[1]);
        $('input[name="min_down_payment"]').val(ui.values[0]);
        $('input[name="max_down_payment"]').val(ui.values[1]);
      }
    });
    $('#amount').val("Rs" + $('#slider-range').slider('values', 0) + " - Rs" + $('#slider-range').slider("values", 1));
    $( "#slider-range2" ).slider({
      range: true,
      min: 0,
      max: 200000,
      values: [{{ $min_amount_per_month ?? 0 }}, {{ $max_amount_per_month ?? 200000 }}],
      slide: function( event, ui ) {
        $('#amount2').val("Rs" + ui.values[0] + " - Rs" + ui.values[1]);
        $('input[name="min_amount_per_month"]').val(ui.values[0]);
        $('input[name="max_amount_per_month"]').val(ui.values[1]);
      }
    });
    $('#amount2').val("Rs" + $('#slider-range2').slider("values", 0) + " - Rs" + $('#slider-range2').slider('values', 1));
</script>
@endpush