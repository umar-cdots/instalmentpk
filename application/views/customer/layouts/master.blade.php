<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>@yield('title', "Instalments") - Instalment</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="format-detection" content="telephone=no"> 
<link rel="shortcut icon" href="{{ base_url("assets/images/favi.png") }}" type="image/png" sizes="16x16">
<!--<link rel="stylesheet" href="{{ base_url("assets/bower_components/bootstrap/dist/css/bootstrap.min.css") }}"> -->
<link href="{{ base_url("assets/css/all.css")}}" rel="stylesheet">
<link href="{{ base_url("assets/css/style.css")}}" rel="stylesheet">
<link href="{{ base_url("assets/css/magnific-popup.css")}}" rel="stylesheet">
<link href="{{ base_url("assets/css/media.css")}}" rel="stylesheet">
<link href="{{ base_url("assets/css/owl.carousel.min.css")}}" rel="stylesheet">
<link href="{{ base_url("assets/css/toastr.min.css")}}" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
@stack('styles')
</head>
<body>

@include('customer.layouts.loader')
@include('customer.layouts.top')
@include('customer.layouts.header')
@include('customer.layouts.navigation')

@yield('content')

@include('customer.layouts.links')
<footer>
  <div class="container">
    <p>Copyright © 2018 Installment.pk. All Rights Reserved. </p>
  </div>
</footer>
<script src="{{ base_url("assets/bower_components/jquery/dist/jquery.min.js") }}"></script>
<script src="{{ base_url("assets/js/jquery.magnific-popup.min.js") }}"></script>
<!-- Bootstrap Notify -->
<script src="{{ base_url("assets/js/toastr.min.js") }}"></script>
<script src="{{ base_url("assets/js/all.js")}}"></script>

@stack('scripts')
<script type="text/javascript">
    $('.ar_wishlist').click(function(){
        var node         = $(this);
        var target_class = $(node).data('target-class');
        var product_offer_id 	= $(node).data('product-offer-id');
        var action 				= "remove";
        if($(node).hasClass('not_active_product'))
        {
        	action = "add";
        }

        $.ajax({
    		type: "POST",
    		url: "{{ base_url('ajax/update_wishlist') }}",
    		data: "action=" + action + "&product_offer_id=" + product_offer_id,
    		dataType: "json",
    		success: function(response)
    		{
    			if(response.status == "success")
    			{
                    $(node).hide();
                    $('.' + target_class +'[data-product-offer-id='+ product_offer_id +']').show();
                    
    				toastr.success(response.message);
    				$('.options').find('.cart').text(response.data.length);
                    console.log(response.data.length);
    			}
    			else
    			{
    				toastr.error(response.data == "" ? response.message : response.data);
                    $(this).toggleClass('activepro');
    			}
    		},
    		error: function(response)
    		{
    			toastr.error("Something Went Wrong.");
                $(this).toggleClass('activepro');
    		}
    	});
    });
</script>
@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
	<script type="text/javascript">
		toastr.{{ get_instance()->session->flashdata('status') == 'error' ? 'error' : 'success' }}('{{ get_instance()->session->flashdata('message') }}');
	</script>
@endif
</body>
</html>