{{-- More Items to Consider --}}
<h2 class="more_itm">More Items to consider</h2>
@foreach($more_offers as $more_offer)
  <div class="deals_box offer_search_box pro_det_box">
    <div class="box_img deals_box_img deals_box_img2" {{ in_array($more_offer->id, $wishlist) ? "activepro" : "" }}" data-product-offer-id="{{ $more_offer->id }}"> 
      <div class="wishlist_img">
        <img src="{{ base_url('assets/images/clr_shape.png') }}" style="display: {{ in_array($more_offer->id, $wishlist) ? "inline-block" : "none" }}" class="active_product ar_wishlist" data-target-class="not_active_product" data-product-offer-id="{{ $more_offer->id }}">
        <img src="{{ base_url('assets/images/no_clr.png') }}" style="display: {{ in_array($more_offer->id, $wishlist) ? "none" : "inline-block" }}" class="not_active_product ar_wishlist" data-target-class="active_product" data-product-offer-id="{{ $more_offer->id }}">
      </div>
      <img class="zoomImageHover" src="{{ isset($more_offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $more_offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$more_offer->product->productImages[0]->raw_name}190x120{$more_offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $more_offer->product->title }}" style="width: 189px; height: 117px;">
      <h3><a href="{{ base_url('offer/' . $more_offer->slug) }}" title="{{ $more_offer->product->title }}">{{ str_limit($more_offer->product->title, 20) }}</a></h3>
    </div>
    <div class="box_details deals_box_details">
      <h2><span class="rupeesText">Rs.</span>{{ number_format($more_offer->amount_per_month) }}<span>/month</span></h2>
      <h4> {{ $more_offer->vendor->businessName() }} <span>/ {{ $offer->vendor->business_address }}</span></h4>
      <a href="{{ base_url('offer/' . $more_offer->slug) }}">View Details</a> 
    </div>
  </div>
  @endforeach
<div class="clearfix"></div>