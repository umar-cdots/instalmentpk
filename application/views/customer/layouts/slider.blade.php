<div class="slider_section">
  <div class="container">
    <div class="img1"> <img src="{{base_url('assets/images/bg1.png')}}" alt="bg"> </div>
    <div class="img2"> <img src="{{base_url('assets/images/bg2.png')}}" alt="bg"> </div>
    <div class="owl-carousel" id="slider">
      <div class="item">
        <div class="pro_slider"> <img src="{{base_url('assets/images/sliderImg.png')}}"> </div>
      </div>
    </div>
  </div>
</div>