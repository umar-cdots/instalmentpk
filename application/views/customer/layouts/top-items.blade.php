<div class="products top-items">
  <div class="container">
    <div class="w3-bar w3-black">
      <button class="w3-bar-item w3-button active_btn" onclick="openCity('Vendors')">
      <p>Top Vendors</p>
      </button>
      <button class="w3-bar-item w3-button" onclick="openCity('Brands')">
      <p>Top Brands</p>
      </button>
      <button class="w3-bar-item w3-button" onclick="openCity('products')">
      <p>Top Products</p>
      </button>
    </div>
    <div class="pro_main">
      <div id="Vendors" class="w3-container city">
        <div class="owl-carousel owl-carousel2" id="slider4">
          @foreach($topVendors as $vendor)
            <div class="item">
              <div class="offers_box deals_box pro_box">
                <a href="{{ base_url('vendor/' . $vendor->slug) }}">
                  <div class="box_img">
                    <img src="{{ isset($vendor->user->logo->file_name) && is_file(_MEDIA_UPLOAD_PATH_ . $vendor->user->logo->file_name) ? _MEDIA_UPLOAD_URL_ . $vendor->user->logo->file_name : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $vendor->businessName() }}" style="width: 167px; height: 146px;">
                    <h3>{{ $vendor->businessName() }}</h3>
                  </div>
                </a>
              </div>
            </div>
          @endforeach
        </div>
      </div>
      <div id="Brands" class="w3-container city" style="display:none">
        <div class="owl-carousel owl-carousel2" id="slider5">
          @foreach($topBrands as $brand)
            <div class="item">
              <div class="offers_box deals_box pro_box">
                <a href="{{ base_url('brand/' . $brand->slug) }}">
                  <div class="box_img"> 
                    <img src="{{ isset($brand->logo) && is_file(_MEDIA_UPLOAD_PATH_ . $brand->logo->file_name) ? _MEDIA_UPLOAD_URL_ . $brand->logo->file_name : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $brand->brand_title }}" style="width: 167px; height: 146px;">
                    <h3>{{ $brand->brand_title }}</h3>
                  </div>
                </a>
              </div>
            </div>
          @endforeach
        </div>
      </div>
      <div id="products" class="w3-container city" style="display:none">
        <div class="owl-carousel owl-carousel2" id="slider6">
          @foreach($topProducts as $product)
            <div class="item">
              <div class="offers_box deals_box pro_box">
                <a href="{{ base_url('product/' . $product->slug) }}">
                  <div class="box_img"> 
                    <img src="{{ isset($product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$product->productImages[0]->raw_name}310x310{$product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $product->title }}" style="width: 167px; height: 146px;">
                    <h3>{{ $product->title }}</h3>
                  </div>
                </a>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
@push('scripts')
<script type="text/javascript">
  $('#slider4').owlCarousel({
    loop:true,
    margin: 30,
    nav:true,
    responsiveClass: true,
    autoplay: true,
    autoplayTimeout: 5000,
    navText: ["<img src='{{base_url('assets/images/prev.png')}}'>","<img src='{{base_url('assets/images/next.png')}}'>"],
    responsive: {
      0: {
          items: 1
      },
      600: {
          items: 3
      },
      1000: {
          items: 5
      }
    }
  });
  $('#slider5').owlCarousel({
    loop:true,
    margin: 30,
    nav:true,
    responsiveClass: true,
    autoplay: true,
    autoplayTimeout: 5000,
    navText: ["<img src='{{base_url('assets/images/prev.png')}}'>","<img src='{{base_url('assets/images/next.png')}}'>"],
    responsive: {
      0: {
          items: 1
      },
      600: {
          items: 3
      },
      1000: {
          items: 5
      }
    }
  });
  $('#slider6').owlCarousel({
    loop:true,
    margin: 30,
    nav:true,
    responsiveClass: true,
    autoplay: true,
    autoplayTimeout: 5000,
    navText: ["<img src='{{base_url('assets/images/prev.png')}}'>","<img src='{{base_url('assets/images/next.png')}}'>"],
    responsive: {
      0: {
          items: 1
      },
      600: {
          items: 3
      },
      1000: {
          items: 5
      }
    }
  });  
</script>
@endpush