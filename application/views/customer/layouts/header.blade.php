<div class="header">
  <div class="container">
    <div class="logo"> <a href="{{base_url()}}"><img src="{{base_url('assets/images/logo.svg')}}" alt="logo"></a> </div>
    <div class="options">
      <ul>
        <li class="islamicLogoList"><a href="{{base_url('islamic-financing')}}"><img src="{{base_url('assets/images/islamic_financing.svg')}}" alt="Islamic Financing" class="islamicLogo">Shariah</a></li>
        <li><a href="<?php echo base_url(); ?><?php echo _VENDOR_ROUTE_PREFIX_; ?>/login"><span class="icon-shop"></span>seller</a></li>
        <li><a href="{{ base_url('my-account') }}"><span class="icon-profile"></span>My Account</a></li>
        <li><a href="{{ base_url('wishlist') }}"><img class="wish" src="{{base_url('assets/images/wshlist.svg')}}" alt="wishlist">Wishlist <span class="cart">{{ count($wishlist) }}</span></a></li>
      </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</div>