<div class="header">
  <div class="container">
    <div class="logo"> <a href="{{base_url()}}"><img src="{{base_url('assets/images/logo.png')}}" alt="logo"></a> </div>
    <div class="options">
      <ul>
        <li><img src="{{base_url('assets/images/riba-free.png')}}" alt="Riba Free"></li>
        <li><a href="<?php echo base_url(); ?><?php echo _VENDOR_ROUTE_PREFIX_; ?>/login"><span class="icon-shop"></span>vendor</a></li>
        <li><a href="{{ base_url('my-account') }}"><span class="icon-profile"></span>My Account</a></li>
        <li><a href="{{ base_url('wishlist') }}"><img class="wish" src="{{base_url('assets/images/wlist-sm.png')}}" alt="wishlist">Wishlist <span class="cart">{{ count($wishlist) }}</span></a></li>
      </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</div>