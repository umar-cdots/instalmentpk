<div class="top">
  <div class="container">
    <div class="top_left">
      <p><span class="icon-auricular-phone-symbol-in-a-circle"></span> For Inquiries: <b>0323-0677776</b></p>
    </div>
    <div class="top_right">
      <ul>
        @if(LoginInfo::getInstance()->isLogin())
          <li><a href="{{ base_url("my-account")}}">Welcome {{LoginInfo::getInstance()->getName()}}</a> / <a href="{{ LoginInfo::getInstance()->getRole() == 'vendor' ? base_url(_VENDOR_ROUTE_PREFIX_ . "/logout") : base_url("logout")}}"> Logout</a> |</li>
        @else
          <li><a href="{{ base_url("login")}}">Login</a> / <a href="{{ base_url("signup")}}"> Register</a> |</li>
        @endif
        <li><a href="#">Help</a> | </li>
        <li><a href="#">Contact</a></li>
        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
      </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</div>