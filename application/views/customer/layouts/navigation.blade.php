<div class="navigation">
  <div class="container"> 
    <div class="menu"> 
      <div class="dropdown">
        <button class="dropbtn"><i class="fas fa-bars"></i> Categories{{-- {{ isCategoryPage() ? getCategoryName(getCategorySlugBySegment()) : "Categories" }} --}} <img src="{{base_url('assets/images/arrow_down.png')}}" alt="img"></button>
        <div id="myDropdown" class="dropdown-content">
          <ul class="dropdown-menu" role="menu">
              @foreach($categories as $category)
              <li>
                <a href="{{base_url('category/' .$category->slug)}}"><span class="{{ $category->icon_class }}"></span>{{ $category->category_title }} <span class="arrow">&gt;</span></a>
                <ul class="sub_menu_list fade-in">
                  @foreach($category->childCategories->chunk(4) as $childCatagories)
                    @foreach($childCatagories as $childCatagory)
                  	<div class="col3">
                  		{{-- <h4>{{ $childCatagory->category_title }}</h4> --}}
                  		<ul>
                  			<li><a href="{{base_url('category/' .$childCatagory->slug)}}">{{ $childCatagory->category_title }}</a></li>
                  		</ul>
                  	</div>
                    @endforeach
                  @endforeach
                	<div class="clearfix"></div>
                </ul>
              </li>
              @endforeach
          </ul>
        </div>
      </div>
       
    </div>
    
    <div class="nav_right">
      <div class="in_left">
        <form action="{{ base_url('search') }}" type="POST" id="search_form" autocomplete="off">
          <input type="text" name="keyword" id="keyword" placeholder="Search Products..." value="{{ $keyword ?? '' }}" required="" autocomplete="off">
          <ul class="output" style="display:none;"></ul>
          <button type="submit" name="search_btn"><span class="icon-magnifier-tool"></span> Search</button>
        </form>
        @push('scripts')
        <script type="text/javascript">
		      $(document).ready(function(){
            $('.dropdown-menu a').click(function(e){
              e.stopPropagation();
            });
            $('.dropbtn').click(function(e){
              e.stopPropagation();
              $('#myDropdown').addClass('opennav').slideToggle(500);
            });
            $('body').click(function(){
              if($('.opennav').is(':visible') && $(window).width() > 768)
              {
                $('#myDropdown').addClass('opennav').slideToggle(500);
              }
            });
          });
          </script>
        @endpush
      </div>
      <div class="in_right toolTipHold">
        <button type="button" name="location" class="map_pop" href="#map" data-user-location="{{ $location['user_location'] }}" data-user-location-full="{{ $location['user_location_full'] }}" data-location-lat="{{ $location['location_lat'] }}" data-location-lon="{{ $location['location_lon'] }}"><i class="fas fa-map-marker-alt"></i> {{ $location['user_location'] }}</button>
        <span class="toolTip">{{ $location['user_location_full'] }}</span>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>

<div id="map" class="white-popup mfp-hide">
    <div class="popup-content">
        <p class="popup-hero">Map</p>
        <div class="input_main pop_input">
          <input type="text" name="heading" id="location-query" value="{{ $location['user_location_full'] }}" autocomplete="off">
            <ul class="output" style="display:none;"></ul>
          <button type="button" onclick="getLocation()"><img src="{{ base_url("assets/images/loc_point.png") }}" alt="img"></button>
        </div>
        <div class="input_main">
          <div id="myMap" style="width: 100%; height: 300px;"></div>
        </div>

        <div class="in_right popupSubmitBtn">
          <button class="map_pop update_location_btn" type="button" onclick="triggerUpdate()">Update</button>
        </div>
        <div class="clearfix"></div>
  </div>
</div>


@push('scripts')
  <script type="text/javascript">
    var map, marker, autocomplete, locationUpdated = false, mapData;
    /* JavaScript for Location Services */
    function initMap()
    {
      var fieldId = "location-query";
      var mapId   = "myMap";
      var input   = document.getElementById(fieldId);
      var options = {
        componentRestrictions: {country: 'pk'},
        types: ['establishment']
      };

      var mapOpt      = { zoom: 10 };
      var myLatLng    = {lat: 31.4848634, lng: 74.3830406};
      var markerTitle = "";
      var showMarker  = false;

      @if(!empty($location['location_lat']) && !empty($location['location_lon']))
      options.bounds    = new google.maps.LatLngBounds(
        new google.maps.LatLng({{ $location['location_lat'] }}, {{ $location['location_lon'] }}),
        new google.maps.LatLng({{ $location['location_lat'] }}, {{ $location['location_lon'] }}));
        var myLatLng    = { lat: {{ $location['location_lat'] }}, lng: {{ $location['location_lon'] }} };
        mapOpt.center   = myLatLng;
        showMarker      = true;
        markerTitle     = "{{ $location['user_location_full'] }}";
      @endif

      map       = new google.maps.Map(document.getElementById(mapId), mapOpt);

      if(showMarker)
      {
        marker  = new google.maps.Marker({
          position:   myLatLng,
          anchorPoint: new google.maps.Point(0, -29),
          draggable:  true,
          animation:  google.maps.Animation.DROP,
          title:      markerTitle
        });
        marker.setMap(map);
        map.setZoom(14);
        marker.addListener('click', toggleBounceMarker);
        marker.addListener('dragend', markerDragged);
      }

      autocomplete = new google.maps.places.Autocomplete(input, options);
      autocomplete.bindTo('bounds', map);

      autocomplete.addListener('place_changed', function () {
        marker.setVisible(false);
        var place = autocomplete.getPlace();uuu=place;
        mapData   = {
                    title: $('#' + fieldId).val(), 
                    latitude: place.geometry.location.lat(), 
                    longitude: place.geometry.location.lng()
                  };

        if (!place.geometry) 
        {
          // User entered the name of a Place that was not suggested and
          // pressed the Enter key, or the Place Details request failed.
          window.alert("No details available for input: '" + place.name + "'");
          return;
        }

        if (place.geometry.viewport) 
        {
          map.fitBounds(place.geometry.viewport);
        } 
        else 
        {
          map.setCenter(place.geometry.location);
          map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        // setTimeout(function(){
        //   updateLocation(mapData);
        // }, 1000);
      });
    }

    function toggleBounceMarker()
    {
      if (marker.getAnimation() !== null) 
      {
        marker.setAnimation(null);
      } 
      else 
      {
        marker.setAnimation(google.maps.Animation.BOUNCE);
      }
    }

    function markerDragged(e)
    {
      console.log(e);
      mapData = {title: "", latitude: e.latLng.lat(), longitude: e.latLng.lng()};
      getMarkerLocation(mapData, true);
    }

    function geoMarkerSet(geometry)
    {
      marker.setVisible(false);
      marker.setPosition(geometry);
      marker.setVisible(true);
    }

    $('.map_pop').magnificPopup({
      type: 'inline',
      midClick: true,
      removalDelay: 300,
      mainClass: 'mfp-fade'
    });
    // $.magnificPopup.instance.close = function () {
    //   $.magnificPopup.proto.close.call(this);
    //   if(locationUpdated)
    //   {
    //     setTimeout(function()
		  //   {
  		// 	  if(localStorage.getItem("reloadStop") == 'Yes')
  		// 		{
  		// 			localStorage.setItem("reloadStop", "No");
  		// 			locationUpdate();
  		// 		} 
    //       else 
    //       {
  		// 			location.reload(); 
  		// 		}
    //     }, 1000);
    //   }
    // };

    is_loc_denied = false;
    function getLocation()
    {
      if(!is_loc_denied)
      {
        $(document).trigger('ajaxStart'); //Little Hack to Show Loader
        if (navigator.geolocation) 
        {
          navigator.geolocation.getCurrentPosition(geoToData, function(){
            is_loc_denied   = true;
            $(document).trigger('ajaxStop');
          });
        } 
        else 
        { 
          alert("Geolocation is not supported by this browser.");
          $(document).trigger('ajaxStop');
        }
      }
      else
      {
        alert("You need to reload this page.");
        $(document).trigger('ajaxStop');
      }
    }

    function geoToData(pos)
    {
      updateLocation({title: "", latitude: pos.coords.latitude, longitude: pos.coords.longitude}, true);
    }

    function getMarkerLocation(data, triggerAutocomplete = false)
    {
      $.ajax({
        type: "POST",
        url: "{{ base_url('ajax/get_location') }}",
        data: {
          location_title: data.title,
          location_lat:   data.latitude,
          location_lon:   data.longitude
        },
        dataType: "json",
        success: function(response)
        {
          locationUpdated = true;
          if(response.status == 'success')
          {
            if(triggerAutocomplete)
            {
              myLatLng    = new google.maps.LatLng(response.data.location_lat, response.data.location_lon);
              $('#location-query').val(response.data.user_location_full);
              map.setCenter(myLatLng);
              map.setZoom(17);
              geoMarkerSet(myLatLng);
            }    
          }
          else
          {
            alert(response.message);
          }
        },
        error: function(response)
        {
          alert("Unable to Update Location.");
        }
      });
    }

    function updateLocation(data, triggerAutocomplete = false)
    {
      $.ajax({
        type: "POST",
        url: "{{ base_url('ajax/set_location') }}",
        data: {
          location_title: data.title,
          location_lat:   data.latitude,
          location_lon:   data.longitude
        },
        dataType: "json",
        success: function(response)
        {
          locationUpdated = true;
          if(response.status == 'success')
          {
            if(triggerAutocomplete)
            {
              myLatLng    = new google.maps.LatLng(response.data.location_lat, response.data.location_lon);
              $('#location-query').val(response.data.user_location_full);
              map.setCenter(myLatLng);
              map.setZoom(17);
              geoMarkerSet(myLatLng);
            }

            $('button[name="location"]').html('<i class="fas fa-map-marker-alt"></i> ' + response.data.user_location);
            $('button[name="location"]').attr('data-user-location-full', response.data.user_location_full);
            $('button[name="location"]').attr('data-location-lat', response.data.location_lat);
            $('button[name="location"]').attr('data-location-lon', response.data.location_lon); 

            // if(!$.magnificPopup.instance.isOpen)
            // {
              $.magnificPopup.close();
      				// setTimeout(function()
      				// {
                debugger;
      					if(localStorage.getItem("reloadStop") == 'Yes' && typeof locationUpdate == 'function')
      					{
      						localStorage.setItem("reloadStop", "No");
      						locationUpdate();
      					} 
                else 
                {
      						location.reload();
      					}
      				// }, 1000);
            // }           
          }
          else
          {
            $('button[href="#map"]').trigger('click');
            alert(response.message);
          }
        },
        error: function(response)
        {
          alert("Unable to Update Location.");
        }
      });
    }
    function triggerUpdate()
    {
      console.log(mapData);
      updateLocation(mapData);
    }
    @if(get_instance()->session->default_location)
      getLocation();
    @endif
    /* JavaScript for Location Services End */
  
    $terms  = [];
    $slugs  = [];
    $return = [];
    
    function strInArray(str, strArray) 
    {
      for (var j=0; j<strArray.length; j++) 
      {
        if (strArray[j].toLowerCase().match(str) && $return.length < 5) 
        {
          var $h = strArray[j].replace(str, '<strong>'+str+'</strong>');
          $return.push('<li class="prediction-item"><span class="prediction-text">' + $h + '</span></li>');
        }
      }
    }
    
    // function nextItem(kp) 
    // {
    //   if($('.focus').length > 0) 
    //   {
    //     var $next = $('.focus').next(),
    //         $prev = $('.focus').prev();
    //   }
    
    //   if(kp == 38) 
    //   { // Up
    //     if($('.focus').is(':first-child')) 
    //     {
    //       $prev = $('.prediction-item:last-child');
    //     }
    //     $('.prediction-item').removeClass('focus');
    //     $prev.addClass('focus');
    //   } 
    //   else if (kp == 40) 
    //   { // Down
    //     if($('.focus').is(':last-child')) 
    //     {
    //       $next = $('.prediction-item:first-child');
    //     }
    //     $('.prediction-item').removeClass('focus');
    //     $next.addClass('focus');
    //   }
    // }

    function setupPredictions()
    {
      // $key = e.keyCode;
      // if ($key == 38 || $key == 40) 
      // {
      //   nextItem($key);
      //   return;
      // }
      setTimeout(function() {
        var $search = $('#keyword').val().toLowerCase();
        $return     = [];
        
        strInArray($search, $terms);
        
        if($search == '') 
        {
          $('.output').html('').slideUp();
        } 
        else 
        {
          $('.output').html($return).slideDown();
        }
        $('.prediction-item:first-child').addClass('focus');
      }, 50);
    }
    
    $('.output').on('click', '.prediction-item', function(){
      $text = $(this).find('span').text();
      // alert($text);
      $('.output').slideUp(function(){
        $(this).html('');
      });
      $('#keyword').val($text);
      $('#search_form').submit();
    });

    $('#search_form').submit(function(e){
      e.preventDefault();
      var action  = $(this).attr('action');
      var keyword = $(this).find('input[name="keyword"]').val();
      if(typeof $slugs[keyword.toLowerCase()] != 'undefined')
        window.location = '{{ base_url() }}product/' + $slugs[keyword.toLowerCase()];
      else
        window.location = action + '/' + keyword;
    });

    $('.search_btn').click(function(){
      $('#search_form').trigger('submit');
    });

    $('#keyword').on('keyup', function(){
      var keyword   = $(this).val();
      $('.output').html('');
      if(keyword != '')
      {
        $.ajax({
          type: "GET",
          url: "{{ base_url('ajax/suggestions') }}?keyword=" + keyword,
          dataType: "json",
          cache: true,
          success: function(response){
            console.log(response);
            if(response.status == 'success')
            {
              $terms = [];
              $.each(response.data, function(i, row){
                $terms.push(row.title);
                $slugs[row.title.toLowerCase()] = row.slug;
              });
              $terms = $terms.sort();
              console.log($terms);
              setupPredictions();
            }
          },
          error: function(response){

          }
        });
      }
    });

    $('#keyword').focus(function(){
      if($('.prediction-item').length > 0) 
      {
        $('.output').slideDown();
      }
    });

    $('#keyword').blur(function(){
      if($('.prediction-item').length > 0) 
      {
        $('.output').slideUp();
      }
    });
    // $('.popupSubmitBtn .map_pop').on('click', function(){
    //     $('#map').hide(500);
    // });
  </script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ _GOOGLE_MAP_KEY_ }}&libraries=places&callback=initMap"></script>
@endpush