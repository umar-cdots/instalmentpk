<div class="easy_section">
  <div class="container">
    <h2>it’s easy as</h2>
    <div class="easy_row">
      <div class="easy_box"> <span class="icon-loupe"></span>
        <h4>Explore Products</h4>
      </div>
      <div class="easy_box"> <span class="icon-shop"></span>
        <h4>select vendor</h4>
      </div>
      <div class="easy_box"> <span class="icon-clipboard-with-a-check-mark"></span>
        <h4>Verfiy Your Self</h4>
      </div>
      <div class="easy_box"> <span class="icon-handshake"></span>
        <h4>meet your vendor</h4>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>