<div class="offers">
  <div class="container">
    <h2>Best offers near you</h2>
    <div class="owl-carousel owl-carousel1" id="slider2">
      @foreach($best_offers as $offer)
        <div class="item">
          <div class="offers_box">
            <div class="box_img deals_box_img deals_box_img2"> 
              <div class="wishlist_img">
                <img src="{{ base_url('assets/images/clr_shape.png') }}" style="display: {{ in_array($offer->id, $wishlist) ? "inline-block" : "none" }}" class="active_product ar_wishlist" data-target-class="not_active_product" data-product-offer-id="{{ $offer->id }}">
                <img src="{{ base_url('assets/images/no_clr.png') }}" style="display: {{ in_array($offer->id, $wishlist) ? "none" : "inline-block" }}" class="not_active_product ar_wishlist" data-target-class="active_product" data-product-offer-id="{{ $offer->id }}">
              </div>
              <img src="{{ isset($offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$offer->product->productImages[0]->raw_name}338x170{$offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $offer->product->title }}" style="width: 338px; height: 170px;">
            </div>
            <div class="box_details">
              <div class="details_left">
                <h3>{{ $offer->product->title }}</h3>
                <h2>Rs.{{ number_format($offer->amount_per_month) }}<span>/month</span></h2>
                <h4>By: <a href="{{ base_url('vendor/' . $offer->vendor->slug) }}"><span>{{ $offer->vendor->businessName() }}</span></a></h4>
              </div>
              <div class="details_right"> 
                <a href="{{ base_url('offer/' . $offer->slug) }}">View Details</a> 
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    <div class="clearfix"></div>
  </div>
</div>