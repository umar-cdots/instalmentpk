@if ($paginator->hasPages())
<div class="pagination">
    <span class="pagiArrow {{ $paginator->onFirstPage() ? 'stayDisabled' : '' }}"><a href="{{ $paginator->onFirstPage() ? '#' :  $paginator->previousPageUrl() }}">◄</a></span> <span class="pagesCount"> <span>Page {{ $paginator->currentPage() }}</span> of {{ $paginator->lastPage() }} </span> <span class="pagiArrow  {{ $paginator->lastPage() == $paginator->currentPage() ? 'stayDisabled' : '' }}"><a href="{{ $paginator->lastPage() == $paginator->currentPage() ? '#' :  $paginator->nextPageUrl() }}">►</a></span>
</div>
@push('styles')
	<style type="text/css">
		.pagiArrow.stayDisabled a:hover
		{
			color: #cdcdcd !important;
		}
	</style>
@endpush
@push('scripts')
	<script type="text/javascript">
		$('.stayDisabled a').click(function(e){
			e.preventDefault();
		});
	</script>
@endpush
@endif
