<div class="offers deals">
  <div class="container">
    <h2>Featured Deals</h2>
    <div class="owl-carousel" id="slider3">
      @foreach($featuredOffers as $offer) {{-- Coming from View Composer config.view_composers.php --}}
      <div class="item">
        <div class="offers_box deals_box">
          <div class="box_img deals_box_img deals_box_img2 {{ in_array($offer->id, $wishlist) ? "activepro" : "" }}" data-product-offer-id="{{ $offer->id }}"> 
            <div class="wishlist_img">
              <img src="{{ base_url('assets/images/clr_shape.png') }}" style="display: {{ in_array($offer->id, $wishlist) ? "inline-block" : "none" }}" class="active_product ar_wishlist" data-target-class="not_active_product" data-product-offer-id="{{ $offer->id }}">
              <img src="{{ base_url('assets/images/no_clr.png') }}" style="display: {{ in_array($offer->id, $wishlist) ? "none" : "inline-block" }}" class="not_active_product ar_wishlist" data-target-class="active_product" data-product-offer-id="{{ $offer->id }}">
            </div>


 <div class="image-container" data-large="{{ isset($offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$offer->product->productImages[0]->raw_name}190x120{$offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}">
                <img class="zoomImageHover placeholder img-small" src="{{ base_url('assets/images/logo.png') }}" alt="{{ $offer->product->title }}" style="width: 189px; height: auto;">
             </div>


            {{-- <img class="zoomImageHover" src="" alt="{{ $offer->product->title }}" style="width: 189px; height: 117px;"> --}}
            <h3><a href="{{ base_url('offer/' . $offer->slug) }}" title="{{ $offer->product->title }}">{{ str_limit($offer->product->title, 20) }}</a></h3>
          </div>
          <div class="box_details deals_box_details areaTextHeight">
            <h2><span class="rupeesText">Rs.</span>{{ number_format($offer->amount_per_month) }}<span>/month</span></h2>
            <h4> {{ $offer->vendor->businessName() }} / <span> {{ $offer->vendor->business_address }}</span></h4>
            <a href="{{ base_url('offer/' . $offer->slug) }}">View Details</a> 
          </div>
        </div>
      </div>
      @endforeach

    </div>
    <div class="clearfix"></div>
  </div>
</div>
@push('scripts')
<script type="text/javascript">
  $('#slider3').owlCarousel({
      loop:true,
      margin: 10,
      nav:true,
      navText: ["<img src='{{ base_url('assets/images/prev.png') }}'>","<img src='{{ base_url('assets/images/next.png') }}'>"],
      responsiveClass: true,
      autoplay: true,
      autoplayTimeout: 5000,
      responsive: {
          0: {
              items: 1
          },
          600: {
              items: 4
          },
          1000: {
              items: 5
          }
      }
  });
</script>
@endpush