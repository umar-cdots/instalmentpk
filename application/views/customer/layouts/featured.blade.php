<div class="offers deals">
  <div class="container">
    <h2>Featured Deals</h2>
    <div class="owl-carousel" id="slider3">
      @foreach($featuredOffers as $offer) {{-- Coming from View Composer config.view_composers.php --}}
      <div class="item">
        <div class="offers_box deals_box">
          <div class="box_img deals_box_img deals_box_img2 {{ in_array($offer->id, $wishlist) ? "activepro" : "" }}" data-product-offer-id="{{ $offer->id }}"> 
            <div class="wishlist_img">
              <img src="{{ base_url('assets/images/clr_shape.png') }}" style="display: {{ in_array($offer->id, $wishlist) ? "inline-block" : "none" }}" class="active_product ar_wishlist" data-target-class="not_active_product" data-product-offer-id="{{ $offer->id }}">
              <img src="{{ base_url('assets/images/no_clr.png') }}" style="display: {{ in_array($offer->id, $wishlist) ? "none" : "inline-block" }}" class="not_active_product ar_wishlist" data-target-class="active_product" data-product-offer-id="{{ $offer->id }}">
            </div>
            <img src="{{ isset($offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$offer->product->productImages[0]->raw_name}190x120{$offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $offer->product->title }}" style="width: 189px; height: 117px;">
            <h3>{{ $offer->product->title }}</h3>
          </div>
          <div class="box_details deals_box_details">
            <h2>Rs.{{ number_format($offer->amount_per_month) }}<span>/month</span></h2>
            <h4><span>By:</span> {{ $offer->vendor->businessName() }}</h4>
            <a href="{{ base_url('offer/' . $offer->slug) }}">View Details</a> 
          </div>
        </div>
      </div>
      @endforeach
    </div>
    <div class="clearfix"></div>
  </div>
</div>
@push('scripts')
<script type="text/javascript">
  $('#slider3').owlCarousel({
      loop:true,
      margin: 0,
      nav:false,
      responsiveClass: true,
      autoplay: true,
      autoplayTimeout: 5000,
      responsive: {
          0: {
              items: 1
          },
          600: {
              items: 4
          },
          1000: {
              items: 5
          }
      }
  });
</script>
@endpush