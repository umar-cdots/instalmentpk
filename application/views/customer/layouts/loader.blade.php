<div id="loader" style="display: none;">
	<div class="cover-body"></div>
    <div class="sk-folding-cube bring-up">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>
@push('scripts')
	<script type="text/javascript">
		$(document).ajaxStart(function(){
			$('#loader').show();
		});
		$(document).ajaxStop(function(){
			$('#loader').hide();
		});
	</script>
@endpush