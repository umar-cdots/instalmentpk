<div class="links_section">
  <div class="container">
    <div class="links1">
      <h3>top brands</h3>
      <ul>
         @foreach($topBrands as $brand)
            <li><a href="{{ base_url('brand/' . $brand->slug) }}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; {{ $brand->brand_title }}</a></li>
         @endforeach
      </ul>
    </div>
    <div class="links1">
      <h3>top categories</h3>
      <ul>
         @foreach($categories as $category)
            <li><a href="{{base_url('category/' .$category->slug)}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; {{ $category->category_title }} </a></li>
          @endforeach
      </ul>
    </div>
    <div class="links1">
      <h3>get to know us</h3>
      <ul>
        <li><a href="{{base_url('about-us')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; About Us</a></li>
        <li><a href="{{base_url('terms-condition')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Terms & Conditions</a></li>
        <li><a href="{{base_url('privacy-policy')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Privacy Policy</a></li>
        <li><a href="{{base_url('corporate-installments')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Corporate Installments</a></li>
        <li><a href="{{base_url('why-buy-from-us')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp;	Why buy from Us</a></li>
      </ul>
    </div>
    <div class="links1">
      <h3>let us help you</h3>
      <ul>
        <li><a href="{{base_url('contact-us')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Contact Us</a></li>
        <li><a href="{{base_url('faq')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; FAQs</a></li>
      </ul>
    </div>
    <div class="links1">
      <h3>my account</h3>
      <ul>
        <li><a href="{{ base_url('my-account') }}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; My Account</a></li>
        <li><a href="{{ base_url("my-account/orders") }}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Order History</a></li>
      </ul>
    </div>
    <div class="links1">
      <h3>Contact info</h3>
      <ul>
        <li><a href="#"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; support@installment.pk</a></li>
        <li><a href="#"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Call Us: 0323-4567891</a></li>
        <ul class="social_footer">
          <li><a href="#"><img src="{{base_url('assets/images/fb.png')}}" alt="img"></a></li>
          <li><a href="#"><img src="{{base_url('assets/images/yt.png')}}" alt="img"></a></li>
          <li><a href="#"><img src="{{base_url('assets/images/tw.png')}}" alt="img"></a></li>
        </ul>
      </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</div>