<div class="links_section">
  <div class="container">
    @if (count($topBrands) > 0)
    <div class="links1">
      <h3>top brands</h3>
      <ul>
         @foreach($topBrands as $brand)
            <li><a href="{{ base_url('brand/' . $brand->slug) }}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; {{ $brand->brand_title }}</a></li>
         @endforeach
      </ul>
    </div>
     @endif
      @if (count($topVendors) > 0)
    <div class="links1">
      <h3>top sellers</h3>
      <ul>
         @foreach($topVendors as $vendor)
            <li><a href="{{ base_url('seller/' . $vendor->slug) }}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; {{ $vendor->businessName() }} </a></li>
          @endforeach
      </ul>
    </div>
     @endif
    <div class="links1 {{ count($topBrands) == 0 && count($topVendors) == 0 ? 'show-full' : (count($topBrands) == 0 || count($topVendors) == 0 ? 'show-half' : '')}}">
      <h3>get to know us</h3>
      <ul>
        <li><a href="{{base_url('about-us')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; About Us</a></li>
        <li><a href="{{base_url('terms-condition')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Terms & Conditions</a></li>
        <li><a href="{{base_url('privacy-policy')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Privacy Policy</a></li>
        <li><a href="{{base_url('corporate-installments')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Corporate Instalments</a></li>
        <li><a href="{{base_url('why-buy-from-us')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp;	Why buy from Us</a></li>
      </ul>
    </div>
    <div class="links1 {{ count($topBrands) == 0 && count($topVendors) == 0 ? 'show-full' : (count($topBrands) == 0 || count($topVendors) == 0 ? 'show-half' : '')}}">
      <h3>let us help you</h3>
      <ul>
        <li><a href="{{base_url('contact-us')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Contact Us</a></li>
        <li><a href="{{base_url('faq')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; FAQs</a></li>
        @if(LoginInfo::getInstance()->getRole() == 'vendor') 
          <li><a href="{{base_url('product-request')}}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Request For Product</a></li>
        @endif 
      </ul>
    </div>
    <div class="links1 {{ count($topBrands) == 0 && count($topVendors) == 0 ? 'show-full' : (count($topBrands) == 0 || count($topVendors) == 0 ? 'show-half' : '')}}">
      <h3>my account</h3>
      <ul>
        <li><a href="{{ base_url('my-account') }}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; My Account</a></li>
        <li><a href="{{ base_url("my-account/orders") }}"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Order History</a></li>
      </ul>
    </div>
    <div class="links1 {{ count($topBrands) == 0 && count($topVendors) == 0 ? 'show-full' : (count($topBrands) == 0 || count($topVendors) == 0 ? 'show-half' : '')}}">
      <h3>Contact info</h3>
      <ul>
        <li><a href="mailto:support@installment.pk?subject=For Support"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; support@instalment.pk</a></li>
        <li><a href="tel:0300-5556347"><span>&raquo;</span> &nbsp;&nbsp;&nbsp; Call Us: 0300-5556347</a></li>
        {{-- 0323-4567891 --}}
        <ul class="social_footer">
          <li><a href="https://www.facebook.com/"><img src="{{base_url('assets/images/fb.png')}}" alt="img"></a></li>
          <li><a href="https://www.youtube.com/"><img src="{{base_url('assets/images/yt.png')}}" alt="img"></a></li>
          <li><a href="https://twitter.com/"><img src="{{base_url('assets/images/tw.png')}}" alt="img"></a></li>
        </ul>
      </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</div>