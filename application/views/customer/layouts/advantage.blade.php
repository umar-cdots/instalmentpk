<div class="easy_section advantage_section">
  <div class="container">
    <h2>advantages</h2>
    <div class="easy_row">
      <div class="easy_box adv_box"> <span class="icon-shop-1"></span>
        <h4>200+ <span>sellers</span></h4>
      </div>
      <div class="easy_box adv_box"> <span class="icon-computer"></span>
        <h4>62,497,664 <span>products</span></h4>
      </div>
      <div class="easy_box adv_box"> <span class="icon-customer"></span>
        <h4>40,971,062 <span>satisfied customers</span></h4>
      </div>
      <div class="easy_box adv_box"> <span class="icon-shield"></span>
        <h4>secure <span>system</span></h4>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>