@extends('customer.layouts.master')
@section('title', "{$vendor->business_name} - Offers")

{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start --}}
@push('scripts')
    <script src="{{ base_url("assets/js/owl.carousel.min.js")}}"></script> 
@endpush
{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start End --}}

@section('content')
  @include('customer.layouts.featured')
  <div class="offers deals results">
    <div class="container">
      <h2>Latest Deals</h2>
      <div class="lat_left">
        <img style="width: 100%;" src="{{ isset($vendor->user->logo->file_name) && is_file(_MEDIA_UPLOAD_PATH_ . $vendor->user->logo->file_name) ? _MEDIA_UPLOAD_URL_ . $vendor->user->logo->file_name : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $vendor->businessName() }}">
        <h3 style="font-size: 16px;">{{ $vendor->businessName() }}</h3>
        {{-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> --}}
        {{-- <p><a href="#">www.honda.com.pk</a></p> --}}
        <h4 style="font-size: 16px;">Total: {{ $offers->count() }}</h4>
      </div>
      <div class="lat_hold">
        @foreach($offers as $offer) 
          <div class="deals_box offer_search_box">
            <div class="box_img deals_box_img deals_box_img2"> 
              <img src="{{ isset($offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . $offer->product->productImages[0]->file_name : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $offer->product->title }}" style="width: 189px; height: 117px;">
              <h3>{{ $offer->product->title }}</h3>
            </div>
            <div class="box_details deals_box_details">
              <h2>Rs.{{ number_format($offer->amount_per_month) }}<span>/month</span></h2>
              <h4><span>By:</span> {{ $offer->vendor->businessName() }}</h4>
              <a href="{{ base_url('offer/' . $offer->slug) }}">View Details</a>
            </div>
          </div> 
        @endforeach
        <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
@endsection
