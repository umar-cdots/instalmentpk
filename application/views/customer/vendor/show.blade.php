@extends('customer.layouts.master')
@section('title', "{$vendor->business_name} - Instalment Offers")

@push('seo')
  <meta name="og:title" content="{{ "{$vendor->business_name} - Instalment Offers" }}">
  <meta name="og:site_name" content="Instalment.pk">
  <meta name="og:description " content="{{ $vendor->vendor_description }}">
  <meta name="og:image" content="{{ isset($vendor->user->logo->file_name) && is_file(_MEDIA_UPLOAD_PATH_ . $vendor->user->logo->file_name) ? _MEDIA_UPLOAD_URL_ . $vendor->user->logo->file_name : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}">
  <meta name="og:url" content="{{ getCurrentRequestURI() }}">
@endpush

{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start --}}
@push('scripts')
    <script src="{{ base_url("assets/js/owl.carousel.min.js")}}"></script> 
@endpush
{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start End --}}

@section('content')
  @include('customer.layouts.featured')
  <div class="offers deals results">
    <div class="container">
      <h2 class="brandTitle">{{ $vendor->businessName() }} <span class="totalPro">Total Products: {{ $offers->total() }} , &nbsp Total Offers: {{ totalOffersByVendor($vendor->id)  }}</h2>
       {{-- <h2>Latest Deals</h2> --}}
      <div class="lat_left">
        <img style="width: 100%;" src="{{ isset($vendor->user->logo->file_name) && is_file(_MEDIA_UPLOAD_PATH_ . $vendor->user->logo->file_name) ? _MEDIA_UPLOAD_URL_ . $vendor->user->logo->file_name : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $vendor->businessName() }}">

        {{-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> --}}
        {{-- <p><a href="#">www.honda.com.pk</a></p> --}}
    
      </div>
       <div class="lat_hold">
        <p class="">{{ $vendor->vendor_description }}</p>
        {{-- <a class="showMore">{{ !empty($vendor->vendor_description )? 'Read More' : ''  }}</a> --}}
        </div>
      <div class="clearfix"></div>
      <div class="dealsBrandPage">
        @foreach($offers as $offer) 
          <div class="deals_box offer_search_box mrgn">
            <div class="box_img deals_box_img deals_box_img2 sellerTotalOffersHasLink"> 
             <a class="sellerTotalOffersLink" href="{{ base_url('product-offers/' . $offer->slug) }}"><span class="totalPro">Total Offers: {{ totalOffersByProduct($vendor->id, $offer->product_id)  }}</span></a>
              <img class="zoomImageHover" src="{{ isset($offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$offer->product->productImages[0]->raw_name}190x120{$offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $offer->product->title }}" style="width: 189px; height: 117px;">
              <h3><a href="{{ base_url('offer/' . $offer->slug) }}" title="{{ $offer->product->title }}">{{ str_limit($offer->product->title, 20) }}</a></h3>
            </div>
            <div class="box_details deals_box_details">
              <h2>Rs.{{ number_format($offer->amount_per_month) }}<span>/month</span></h2>
              <h4><span>By:</span> {{ $offer->vendor->businessName() }}</h4>
              <a href="{{ base_url('offer/' . $offer->slug) }}">View Details</a>
            </div>
          </div> 
        @endforeach
        <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
       {{ $offers->links() }}
    </div>
  </div>
@endsection

@push('scripts')
{{-- <script type="text/javascript">
  $(document).ready(function(){
    $('.showMore').on('click', function(){
      $('.lat_hold p').toggleClass("heightPara");
      if($('.lat_hold p').hasClass("heightPara"))
      {
          $('.showMore').text("Read More");
      }
      else
      {
        $('.showMore').text("Read Less");
      }
    });
  });
</script> --}}
@endpush
