@extends('customer.layouts.master')
@section('title', "{$product_offer->product->title} - Offers")

@push('seo')
  <meta name="og:title" content="{{ "{$product_offer->product->title} - Instalment Offers" }}">
  <meta name="og:site_name" content="Instalment.pk">
  <meta name="og:description " content="{{ $product_offer->product->description }}">
  <meta name="og:image" content="{{ isset($product_offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $product_offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$product_offer->product->productImages[0]->raw_name}{$product_offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}">
  <meta name="og:url" content="{{ getCurrentRequestURI() }}">
@endpush

{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start --}}
@push('scripts')
    <script src="{{ base_url("assets/js/owl.carousel.min.js")}}"></script> 
@endpush
{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start End --}}

@section('content')
  @include('customer.layouts.featured')
  <div class="offers deals results">
    <div class="container">
      <h2 class="brandTitle"> Latest Deals By {{ $product_offer->vendor->business_name }} <span class="totalPro">Total Offers: {{ $offers->count() }}</span></h2>
      <div class="lat_left">
        <img src="{{ isset($product_offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $product_offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$product_offer->product->productImages[0]->raw_name}190x120{$product_offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $product_offer->product->title }}" width="100%">
        
        {{-- <h4 style="font-size: 16px;">Total: {{ $offers->count() }}</h4> --}}
      </div>
       <div class="lat_hold">
          <h3>{{ $product_offer->product->title }}</h3>
          <p class="heightPara">{{ $product_offer->product->description }}</p>
          <a class="showMore">Show More</a>
       </div>
       <div class="clearfix"></div>
       <div class="sellers_table mrgn">
          <table cellpadding="0" cellspacing="0" border="0" style="text-align:center;"  align="center" width="100%">
            <tr class="table_head">
              <th>Sr#</th>
              <th>Total Amount</th>
              <th>Advance</th>
              <th>Instalments</th>
              <th>EMI</th>
              <th></th>
            </tr>
            <tbody>
              @forelse($offers as $offer)
               @continue(empty($offer->vendor))
                <tr style="font-size: 14px;">
                  <td>
                    {{ $loop->iteration }}
                    {{-- <a href="{{ base_url('seller/' . $offer->vendor->slug) }}" style="text-decoration: none;">
                      <span style="color: #144d88; font-weight: 600; font-size: 14px;">{{ $offer->vendor->fullName() }}</span> - {{ $offer->vendor->businessName() }}
                    </a> --}}
                  </td>
                  <td>Rs.{{ number_format($offer->total_price_on_installment) }}</td>
                  <td>Rs.{{ number_format($offer->down_payment) }}</td>
                  <td>{{ $offer->total_installments }}</td>
                  <td>Rs.{{ number_format($offer->amount_per_month) }}/month</td>
                  <td>
                    <a href="{{ base_url('offer/' . $offer->slug) }}" style="color: white;">
                      <button type="submit" name="button" class="table_btn">View</button>
                    </a>
                  </td>
                </tr>
              @empty
              <tr>
                <td colspan="6">No Offers Availble at This Time.</td>
              </tr>
              @endforelse
            </tbody>
          </table>
          <div class="clearfix"></div>
        </div>
    <div class="clearfix"></div>
  </div>
@endsection
@push('styles')
  <style type="text/css">
    .table_head th{
      text-align: center;
    }
    .noBtmPadding
    {
      padding-bottom: 0;
    }
  </style>
@endpush
@push('scripts')
<script type="text/javascript">
  $(document).ready(function(){
    $('.showMore').on('click', function(){
      $('.lat_hold p').toggleClass("heightPara");
      if($('.lat_hold p').hasClass("heightPara"))
      {
          $('.showMore').text("Show More");
      }
      else
      {
        $('.showMore').text("Show Less");
      }
    });
  });
</script>
@endpush
