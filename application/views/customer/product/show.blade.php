@extends('customer.layouts.master')
@section('title', "{$product->title} - Offers")

@push('seo')
  <meta name="og:title" content="{{ "{$product->title} - Instalment Offers" }}">
  <meta name="og:site_name" content="Instalment.pk">
  <meta name="og:description " content="{{ $product->description }}">
  <meta name="og:image" content="{{ isset($product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$product->productImages[0]->raw_name}{$product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}">
  <meta name="og:url" content="{{ getCurrentRequestURI() }}">
@endpush

{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start --}}
@push('scripts')
    <script src="{{ base_url("assets/js/owl.carousel.min.js")}}"></script> 
@endpush
{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start End --}}

@section('content')
  @include('customer.layouts.featured')
  <div class="offers deals results ">
    <div class="container">
      <h2 class="brandTitle">Latest Deals <span class="totalPro">Total offers : {{ $offers->count() }}</span></h2>
      <div class="lat_left">
        <img  src="{{ isset($product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$product->productImages[0]->raw_name}190x120{$product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $product->title }}" width="100%">
        
        {{-- <h4 style="font-size: 16px;">Total: {{ $offers->count() }}</h4> --}}
      </div>
       <div class="lat_hold">
          <h3>{{ $product->title }}</h3>
          <p class="">{!!$product->description ?? ''!!}</p>
         {{--  <a class="showMore">Show More</a> --}}
       </div>
       <div class="clearfix"></div>
        <h2 id="redirection">More seller to consider</h2>
       <div class="result_box pro_sold_result" id="scrl_div">
      <form action="#redirection" method="post">
        <div class="result_sm_fld">
          <h3>Advance:</h3>
          <div id="slider-range"></div>
            <input type="text" id="amount" readonly style="border:0; color:#1e9fbc; font-weight:bold;">
            <input type="hidden" name="max_down_payment" value="{{ $max_down_payment ?: '200000' }}">
            <input type="hidden" name="min_down_payment" value="{{ $min_down_payment ?: '0' }}">
        </div>
        <div class="result_sm_fld_hf">
          <h3>Max Instalments:</h3>
          <input type="text" name="max_total_installments" minlength="1">
         {{--  <select name="max_total_installments">
            @foreach(range(24, 1) as $month)
              <option value="{{ $month }}" {{ isset($max_total_installments) && $max_total_installments == $month ? 'selected' : '' }}>{{ $month }}</option>
            @endforeach
          </select> --}}
        </div>
        <div class="result_sm_fld">
          <h3>EMI:</h3>
          <div id="slider-range2"></div>
           <input type="text" id="amount2" readonly style="border:0; color:#1e9fbc; font-weight:bold;">
            <input type="hidden" name="max_amount_per_month" value="{{ $max_amount_per_month ?: '200000' }}">
            <input type="hidden" name="min_amount_per_month" value="{{ $min_amount_per_month ?: '0' }}">
        </div>
        <div class="result_sm_fld_hf">
          <h3>Sort By:</h3>
          <select name="sort_by">
            <option value="lowest" {{ isset($sort_by) && $sort_by  == 'lowest' ? 'selected' : '' }}>Lowest</option>
            <option value="highest" {{ isset($sort_by) && $sort_by == 'highest' ? 'selected' : '' }}>Highest</option>
            <option value="latest" {{ isset($sort_by) && $sort_by == 'latest' ? 'selected' : '' }}>Latest</option>
            <option value="popular" {{ isset($sort_by) && $sort_by == 'popular' ? 'selected' : '' }}>Most Popular</option>
          </select>
        </div>
        <div class="result_sm_fld_hf result_sm_fld_hf2">
          <button type="submit" name="filter" style="margin-top: 24px;">Apply Search</button>   
        </div>
        <div class="clearfix"></div>
      </form>
    </div>
       <div class="sellers_table mrgn">
          <table cellpadding="0" cellspacing="0" border="0" style="text-align:center;"  align="center" width="100%">
            <tr class="table_head">
              <th>Seller</th>
              <th>Total Amount</th>
              <th>Advance</th>
              <th>Instalments</th>
              <th>EMI</th>
              <th></th>
            </tr>
            <tbody>
              @forelse($offers as $offer)
               @continue(empty($offer->vendor))
                <tr style="font-size: 14px;">
                  <td>
                    <a href="{{ base_url('seller/' . $offer->vendor->slug) }}" style="text-decoration: none;">
                      <span style="color: #144d88; font-weight: 600; font-size: 14px;">{{ $offer->vendor->fullName() }}</span> - {{ $offer->vendor->businessName() }}
                    </a>
                  </td>
                  <td>Rs.{{ number_format($offer->total_price_on_installment) }}</td>
                  <td>Rs.{{ number_format($offer->down_payment) }}</td>
                  <td>{{ $offer->total_installments }}</td>
                  <td>Rs.{{ number_format($offer->amount_per_month) }}/month</td>
                  <td>
                    <a href="{{ base_url('offer/' . $offer->slug) }}" style="color: white;">
                      <button type="submit" name="button" class="table_btn">View</button>
                    </a>
                  </td>
                </tr>
              @empty
              <tr>
                <td colspan="6">No Offers Availble at This Time.</td>
              </tr>
              @endforelse
            </tbody>
          </table>
          <div class="clearfix"></div>
        </div>
    <div class="clearfix"></div>
  </div>
@endsection
@push('styles')
  <style type="text/css">
    .table_head th{
      text-align: center;
    }
    .noBtmPadding
    {
      padding-bottom: 0;
    }
  </style>
   <link rel="stylesheet" type="text/css" href="{{ base_url('/assets/css/magiczoom.css') }}"> 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <style type="text/css">
    .table_head th{
      text-align: center;
    }
  </style>
@endpush
@push('scripts')
<script type="text/javascript">
  $(document).ready(function(){
    $('.showMore').on('click', function(){
      $('.lat_hold p').toggleClass("heightPara");
      if($('.lat_hold p').hasClass("heightPara"))
      {
          $('.showMore').text("Show More");
      }
      else
      {
        $('.showMore').text("Show Less");
      }
    });
  });
</script>
 <script type="text/javascript" src="{{ base_url('/assets/js/magiczoom.js') }}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">
    $('#grabITButton').click(function ()
    {
      var url = $(this).closest('form').attr('action');
      $.ajax({
        type: "GET",
        url: url,
        cache: false,
        success: function(response)
        {
          toastr.success("Order Has Been Placed.")
        },
        error: function(response)
        {
          toastr.error("Something Went Wrong.");
        }
      });
      // $.get(url);
      $('#OfferCommentBox').slideToggle('slow');
    });
    $('#cancel_grabbing').click(function(){
      $('#OfferCommentBox').slideToggle('slow');
    });
    $('#slider-range').slider({
      range: true,
      min: 0,
      max: {{ $max_adv_limit ?: 200000 }},
      values: [{{ $min_down_payment ?: 0 }}, {{ $max_down_payment ?: 200000 }}],
      slide: function(event, ui) {
        $("#amount").val( "Rs " + ui.values[0] + " - Rs " + ui.values[1]);
        $('input[name="min_down_payment"]').val(ui.values[0]);
        $('input[name="max_down_payment"]').val(ui.values[1]);
      }
    });
    $('#amount').val("Rs " + $('#slider-range').slider('values', 0) + " - Rs " + $('#slider-range').slider("values", 1));
    $( "#slider-range2" ).slider({
      range: true,
      min: 0,
      max: {{ $max_emi_limit ?: 200000 }},
      values: [{{ $min_amount_per_month ?: 0 }}, {{ $max_amount_per_month ?: 200000 }}],
      slide: function( event, ui ) {
        $('#amount2').val("Rs " + ui.values[0] + " - Rs " + ui.values[1]);
        $('input[name="min_amount_per_month"]').val(ui.values[0]);
        $('input[name="max_amount_per_month"]').val(ui.values[1]);
      }
    });
    $('#amount2').val("Rs " + $('#slider-range2').slider("values", 0) + " - Rs " + $('#slider-range2').slider('values', 1));
    $('.scrollToDiv a').click(function () {
      $('html, body').animate({scrollTop: $('#scrl_div').offset().top}, 700);
      return false;
    });
    $('.change_offer').change(function(){
      var href = $(this).find('option:selected').data('href');
      if(href != '' && typeof href != 'undefined')
      {
        window.location.href = href;
      }
    });
  </script>
@endpush
