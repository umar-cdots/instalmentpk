@extends('customer.layouts.master')
@section('title', "{$product->title} - Offers")

{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start --}}
@push('scripts')
    <script src="{{ base_url("assets/js/owl.carousel.min.js")}}"></script> 
@endpush
{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start End --}}

@section('content')
  @include('customer.layouts.featured')
  <div class="offers deals results">
    <div class="container">
      <h2>Latest Deals</h2>
      <div class="lat_left">
        <img  src="{{ isset($product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$product->productImages[0]->raw_name}190x120{$product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $product->title }}" width="100%">
        <h3>{{ $product->title }}</h3>
        <p>{{ $product->description }}</p>
        <h4>Total: {{ $offers->count() }}</h4>
      </div>
       <div class="lat_hold">
        <div class="sellers_table">
          <table cellpadding="0" cellspacing="0" border="0" style="text-align:center;"  align="center" width="100%">
            <tr class="table_head">
              <th>Vendor</th>
              <th>Total Amount</th>
              <th>Advance</th>
              <th>Instalments</th>
              <th>EMI</th>
              <th></th>
            </tr>
            <tbody>
              @forelse($offers as $offer)
                <tr style="font-size: 14px;">
                  <td>
                    <a href="{{ base_url('vendor/' . $offer->vendor->slug) }}" style="text-decoration: none;">
                      <span style="color: #144d88; font-weight: 600; font-size: 14px;">{{ $offer->vendor->fullName() }}</span> - {{ $offer->vendor->businessName() }}
                    </a>
                  </td>
                  <td>Rs.{{ number_format($offer->total_price_on_installment) }}</td>
                  <td>Rs.{{ number_format($offer->down_payment) }}</td>
                  <td>{{ $offer->total_installments }}</td>
                  <td>Rs.{{ number_format($offer->amount_per_month) }}/month</td>
                  <td>
                    <a href="{{ base_url('offer/' . $offer->slug) }}" style="color: white;">
                      <button type="submit" name="button" class="table_btn">View</button>
                    </a>
                  </td>
                </tr>
              @empty
              <tr>
                <td colspan="6">No Offers Availble at This Time.</td>
              </tr>
              @endforelse
            </tbody>
          </table>
          <div class="clearfix"></div>
        </div>
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
  </div>
@endsection
@push('styles')
  <style type="text/css">
    .table_head th{
      text-align: center;
    }
  </style>
@endpush
