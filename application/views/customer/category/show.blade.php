@extends('customer.layouts.master')
@section('title', isset($category->seoMeta) ?  $category->seoMeta->meta_title : "{$category->category_title} - Instalment Offers")

@if(isset($category->seoMeta))
  @push('seo')
    <meta name="keywords" content="{{ $category->seoMeta->meta_keywords }}">
    <meta name="description" content="{{ $category->seoMeta->meta_description }}">
  @endpush
@endif

@push('seo')
  <meta name="og:title" content="{{ isset($category->seoMeta) ?  $category->seoMeta->meta_title : "{$category->category_title} - Instalment Offers" }}">
  <meta name="og:site_name" content="Instalment.pk">
  <meta name="og:description " content="{{ isset($category->seoMeta) ?  $category->seoMeta->meta_description : "{$category->category_title} - Instalment Offers" }}">
  <meta name="og:image" content="{{ _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}">
  <meta name="og:url" content="{{ getCurrentRequestURI() }}">
@endpush

{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start --}}
@push('scripts')
    <script src="{{ base_url("assets/js/owl.carousel.min.js")}}"></script> 
@endpush
{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start End --}}

@section('content')
  @include('customer.layouts.featured')
  <div class="offers deals results">
    <div class="container">
      <h2 class="brandTitle">Latest Deals <span class="categoryName">({{ $category->category_title }})</span> <span class="totalPro">Total Product: {{ $offers->total() }}</span></h2>
      <div class="lat_left">
        @php
          $requestURI = urldecode(getCurrentRequestURI());
        @endphp
        <div class="sideBar">
          <h3>Refine By</h3>
          @if(isset($brands) && !empty($brands))
            <h4>Brand</h4>
            <ul>
              @foreach($brands as $ind => $brand)
                <li>
                  <a href="{{ $requestURI . (strpos($requestURI, '?') !== false ? '&' : '?') . 'brand['. $ind .']=' . $brand->id }}{{-- {{ base_url('brand/' . $brand->slug) }} --}}">{{ $brand->brand_title }}</a>{!! strpos($requestURI, 'brand['. $ind .']=' . $brand->id) !== false ? '<span style="cursor: pointer;" class="remove_filter" data-removeable="brand['. $ind .']=' . $brand->id  .'"> x</span>' : '' !!}
                </li>
              @endforeach
            </ul>
          @endif
          @if($category->childCategories->count() > 0)
            <h4>Category</h4>
            <ul>
              @foreach($category->childCategories as $child_category)
                <li><a href="{{ base_url('category/' . $child_category->slug) }}">{{ $child_category->category_title }}</a></li>
              @endforeach
            </ul>
          @endif
          @if(isset($key_features) && !empty($key_features))
            @foreach($key_features as $ind => $key_feature)
              <h4>{{ $key_feature->title }}</h4>
              <ul>
                @foreach(getAvailableKeyFeatureValues($key_feature->id) as $key_feature_value)
                  <li><a href="{{ $requestURI . (strpos($requestURI, '?') !== false ? '&' : '?') . 'feature['. $ind .']='. $key_feature->id . ':' . $key_feature_value->meta_value }}">{{ $key_feature_value->meta_value }}</a>{!! strpos($requestURI, 'feature['. $ind .']='. $key_feature->id . ':' . $key_feature_value->meta_value) !== false ? '<span style="cursor: pointer;" class="remove_filter" data-removeable="feature['. $ind .']='. $key_feature->id . ':' . $key_feature_value->meta_value  .'"> x</span>' : '' !!}</li>
                @endforeach
              </ul>
            @endforeach
          @endif
        {{--  <ul>
              @foreach($categories as $category)
               <li>
                <a href="{{base_url('category/' .$category->slug)}}">{{ $category->category_title }}</a>
                <ul class="sub_menu_list fade-in">
                  @foreach($category->childCategories->chunk(4) as $childCatagories)
                    @foreach($childCatagories as $childCatagory)
                      <ul>
                        <li><a href="{{base_url('category/' .$childCatagory->slug)}}">{{ $childCatagory->category_title }}</a></li>
                      </ul>
                    @endforeach
                  @endforeach
                  <div class="clearfix"></div>
                </ul>
              </li>
            @endforeach
          </ul> --}}
        </div>
        {{-- <img style="width: 100%;" src="{{ isset($vendor->user->logo->file_name) && is_file(_MEDIA_UPLOAD_PATH_ . $vendor->user->logo->file_name) ? _MEDIA_UPLOAD_URL_ . $vendor->user->logo->file_name : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $vendor->businessName() }}"> --}}
        {{-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> --}}
        {{-- <p><a href="#">www.honda.com.pk</a></p> --}}
        {{-- <h4 style="font-size: 16px;">Total: {{ $offers->count() }}</h4> --}}
      </div>
{{-- <div class="navigation">
  <div class="container"> 
    <div class="menu"> 
      <div class="dropdown">
        <div id="" class="dropdown-content" style="display: block;">
          <ul class="dropdown-menu" role="menu">
              @foreach($categories as $category)
              <li>
                <a href="{{base_url('category/' .$category->slug)}}"><span class="{{ $category->icon_class }}"></span>{{ $category->category_title }} <span class="arrow">&gt;</span></a>
                <ul class="sub_menu_list fade-in">
                  @foreach($category->childCategories->chunk(4) as $childCatagories)
                    @foreach($childCatagories as $childCatagory)
                    <div class="col3">
                      <ul>
                        <li><a href="{{base_url('category/' .$childCatagory->slug)}}">{{ $childCatagory->category_title }}</a></li>
                      </ul>
                    </div>
                    @endforeach
                  @endforeach
                  <div class="clearfix"></div>
                </ul>
              </li>
              @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
</div> --}}
      <div class="lat_hold">
        @foreach($offers as $offer) 
          <div class="deals_box offer_search_box mrgn">
            <div class="box_img deals_box_img deals_box_img2"> 
              <img class="zoomImageHover" src="{{ isset($offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$offer->product->productImages[0]->raw_name}190x120{$offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $offer->product->title }}" style="width: 189px; height: 117px;">
              <h3><a href="{{ base_url('offer/' . $offer->slug) }}" title="{{ $offer->product->title }}">{{ str_limit($offer->product->title, 18) }}</a></h3>
            </div>
            <div class="box_details deals_box_details">
              <h2><span class="rupeesText">Rs.</span>{{ number_format($offer->amount_per_month) }}<span>/month</span></h2>
              <h4><span>By:</span> {{ $offer->vendor->businessName() }}</h4>
              <a href="{{ base_url('offer/' . $offer->slug) }}">View Details</a>
            </div>
          </div> 
        @endforeach
        <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
     {{ $offers->appends($_GET)->links() }}
  </div>
@endsection

@push('scripts')
  <script type="text/javascript">
    $('.remove_filter').click(function(){
      var uri = decodeURIComponent(window.location.href);
      var remove = $(this).data('removeable');
      window.location = uri.replace(remove, '');
    });
  </script>
@endpush