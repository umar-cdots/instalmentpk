@extends('customer.layouts.master')
@section('title', "Homepage")
@section('content')
   <div class="about_main">
   <div class="container">
   <div class="signup">
       @if(!empty(validation_errors()))
      <div class="validation_errors"> 
      {!!validation_errors()!!} 
    </div>
      @endif
      <div class="sign_heading">
      <h2>create a new account</h2>    
      </div>
      <div class="sign_details">
      <div class="sign_inner_head">
         <h2>Your Personal Details</h2>
         <hr>
      </div>
         <form action="{{base_url('register')}}" method="post" name="signupform" id="signup_form">
            {{-- <div class="input_main input_main2">
            <div class="field_left">
               <label>CNIC <span>*</span></label>
            </div>
            <div class="field_right">
               <input type="text" name="cnic_no" id="cnic_no" value="{{set_value('cnic_no')}}" maxlength="15">
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="input_main input_main2">
            <div class="field_left">
               <label>Designation <span>*</span></label>
            </div>
            <div class="field_right">
               <input type="text" name="designation" value="{{set_value('designation')}}" id="designation">
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="input_main input_main2">
            <div class="field_left">
               <label>Organization <span>*</span></label>
            </div>
            <div class="field_right">
               <input type="text" name="organization" value="{{set_value('organization')}}" id="organization">
            </div>
            <div class="clearfix"></div>
            </div> --}}
            <div class="input_main input_main2">
            <div class="field_left">
               <label>First Name <span>*</span></label>
            </div>
            <div class="field_right">
               <input type="text" name="first_name" value="{{set_value('first_name')}}" id="first_name">
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="input_main input_main2">
            <div class="field_left">
               <label>Last Name <span>*</span></label>
            </div>
            <div class="field_right">
               <input type="text" name="last_name" id="last_name" value="{{set_value('last_name')}}">
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="input_main input_main2">
            <div class="field_left">
               <label>E-Mail <span>*</span></label>
            </div>
            <div class="field_right">
               <input type="email" name="email_address" value="{{set_value('email_address')}}" id="email_address">
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="input_main input_main2">
            <div class="field_left">
               <label>Telephone <span>*</span></label>
            </div>
            <div class="field_right">
               <input type="phone" name="primary_phone" value="{{set_value('primary_phone')}}" id="primary_phone" maxlength="12">
            </div>
            <div class="clearfix"></div>
            </div>
              {{--  <div class="sign_inner_head sign_inner_head_2">
         <h2>Your Address</h2>
         <hr>
      </div>
         <div class="input_main input_main2">
            <div class="field_left">
               <label>Address <span>*</span></label>
            </div>
            <div class="field_right">
               <input type="text" name="current_address" value="{{set_value('current_address')}}" id="current_address">
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="input_main input_main2">
            <div class="field_left">
               <label>City <span>*</span></label>
            </div>
            <div class="field_right">
               <select name="city">
                  <option value="">Lahore</option>
                  <option value="">ISB</option>
               </select>
            </div>
            <div class="clearfix"></div>
            </div>
         <div class="input_main input_main2">
            <div class="field_left">
               <label>Postal Code</label>
            </div>
            <div class="field_right">
               <input type="text" name="postal_code" value="{{set_value('postal_code')}}" id="postal_code">
            </div>
            <div class="clearfix"></div>
            </div> --}}
            <div class="sign_inner_head sign_inner_head_2">
         <h2>Your Password</h2>
         <hr>
      </div>
         <div class="input_main input_main2">
            <div class="field_left">
               <label>Password <span>*</span></label>
            </div>
            <div class="field_right">
               <input type="password" name="password" id="password">
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="input_main input_main2">
            <div class="field_left">
               <label>Confirm Password <span>*</span></label>
            </div>
            <div class="field_right">
               <input type="password" name="confirm_password" id="confirm_password">
               <span id="message"></span>
            </div>
            <div class="clearfix"></div>
            </div>
            <hr>
            <div class="input_main input_main2">
               <div class="field_right">
                  <input type="checkbox" name="agreed"> <label>Please read and accept our terms and conditions</label>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="input_main input_main2">
               <div class="field_right">
                  <button type="submit" name="submitfrm">Register Now</button>
               </div>
               <div class="clearfix"></div>
            </div>
         </form>
      </div>
   </div>
   <div class="clearfix"></div>
  </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ base_url("assets/js/jquery.maskedinput.min.js") }}"></script>
<script type="text/javascript">
   $(document).ready(function(){
      $("#cnic_no").mask("99999-9999999-9");
   });
</script>
@endpush
