@extends('customer.layouts.master')
@section('title', "Homepage")

@section('content')
<div class="about_main">
   <div class="container">
   <div class="signup">
      @if(!empty(validation_errors()))
      <div class="validation_errors">
       {!!validation_errors()!!}
      </div>
      @endif
      <div class="sign_details">
         <form action="{{base_url('tryLogin')}}" method="post" name="signupform" id="signup_form">
            <div class="sign_inner_head sign_inner_head_2">
         <h2>Login</h2>
         <hr>
      </div>
         <div class="input_main input_main2">
            <div class="field_left">
               <label>Email<span>*</span></label>
            </div>
            <div class="field_right">
               <input type="email" name="email_address" id="email_address" value="{{set_value('email_address')}}" required="">
            </div>
            <div class="clearfix"></div>
            </div>
            <div class="input_main input_main2">
            <div class="field_left">
               <label>Password <span>*</span></label>
            </div>
            <div class="field_right">
               <input type="password" name="password" id="password" required="">
               <span id="message"></span>
            </div>
            <div class="clearfix"></div>
            </div>
            <hr>
            <div class="input_main input_main2">
               <div class="field_right">
                  <input type="checkbox" name="remember_me"> <label>Remember me</label>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="input_main input_main2">
               <div class="field_right">
                  <button type="submit" name="submitfrm">Login now</button>
               </div>
               <div class="clearfix"></div>
            </div>
              <div class="input_main input_main2">
            <div class="field_left facebookLLabel">
               <label>Or Login with</label>
            </div>
            <div class="field_right">
                  <button type="submit" name="submitfrm" class="facebookBtn">Facebook</button>
            </div>
            <div class="clearfix"></div>
            </div>
         </form>
      </div>
   </div>
   <div class="clearfix"></div>
  </div>
</div>
@endsection