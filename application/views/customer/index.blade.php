@extends('customer.layouts.master')
@section('title', "Homepage")

{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start --}}
@push('scripts')
    <script src="{{ base_url("assets/js/owl.carousel.min.js")}}"></script> 
@endpush
{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start End --}}

@section('content')
	@include('customer.layouts.slider')
	@include('customer.layouts.easysection')
	@include('customer.layouts.best-offers')
	@include('customer.layouts.featured')
	@include('customer.layouts.top-items')
	@include('customer.layouts.advantage')
@endsection

@push('scripts')
<script type="text/javascript">
   $('#slider').owlCarousel({
            loop: true,
            margin: 0,
            responsiveClass: true,
            autoplay: true,
            nav: false,
            autoplayTimeout: 5000,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });
   $('#slider2').owlCarousel({
        loop: true,
        margin: 0,
        nav:true,
        responsiveClass: true,
        autoplay: true,
        autoplayTimeout: 5000,
        navText: ["<img src='{{base_url('assets/images/prev1.png')}}'>","<img src='{{base_url('assets/images/next1.png')}}'>"],
        responsive: {
            0: {
                items: 1,
                center:true,
                nav: false
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });

    function openCity(cityName) 
    {
        var i;
        var x = document.getElementsByClassName("city");
        for (i = 0; i < x.length; i++) 
        {
           x[i].style.display = "none";  
        }
        document.getElementById(cityName).style.display = "block";  
    } 
    $(document).ready(function(e){
        $('.w3-button').click(function(e){
            $('.w3-button').removeClass('active_btn');
            $('.w3-button').removeClass('active_btn2');
            $(this).addClass('active_btn2');
        });
    });
</script> 
@endpush