@extends('customer.layouts.master')

@section('title', "Terms And Condition")

@section('content')
<div class="about_main">
  <div class="container">
  	<h2 class="head_bg">Terms & Condition</h2>
	   <div class="row">
  <div id="primary" class="content-area col-md-8 mx-auto">
    <div id="content" class="site-content" role="main">
      <article id="post-108" class="post-108 page type-page status-publish hentry">
        <!-- .entry-header -->
        <div class="entry-content">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
          <ol>
            <li><strong>What is Lorem Ipsum?</strong>
              <ol>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting</li>
              </ol>
            </li>
            <li><strong>What is Lorem Ipsum?</strong>
              <ol>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting</li>
              </ol>
            </li>
            <li><strong>Security &amp; Payments</strong>
              <ol>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting</li>
              </ol>
            </li>
            <li><strong>Refunds</strong>
              <ol>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting</li>
              </ol>
            </li>
            <li><strong>Support</strong>
              <ol>
               <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting</li>
              </ol>
            </li>
            <li><strong>Ownership</strong>
              <ol>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting</li>
              </ol>
            </li>
            <li><strong>Membership &amp; Content</strong>
              <ol>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting</li>
              </ol>
            </li>
            <li><strong>Liability</strong>
              <ol>
              <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting</li>
            </li>
            <li><strong>Requesting, Modifying or Deleting Your Data (GDPR)</strong>
              <ol>
               <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting</li>
              </ol>
            </li>
            <li><strong>Changes to terms</strong>
              <ol>
               <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting</li>
              </ol>
            </li>
          </ol>
        </div>
        <!-- .entry-content --> 
      </article>
      <!-- #post-108 --> 
      
    </div>
    <!-- #content .site-content --> 
  </div>
  <!-- #primary .content-area --> 
  
</div>

</div>
    </div>
@endsection

@push('scripts')
  
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
		<script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
		</script>
	@endif
@endpush