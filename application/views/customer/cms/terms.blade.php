@extends('customer.layouts.master')

@section('title', "Terms And Condition")

@section('content')
<div class="about_main">
  <div class="container">
  	<h2 class="head_bg">Terms & Condition</h2>
	   <div class="row">
  <div id="primary" class="content-area col-md-8 mx-auto">
    <div id="content" class="site-content" role="main">
      <article id="post-108" class="post-108 page type-page status-publish hentry">
        <!-- .entry-header -->
        {!!$terms_conditions->page_content ?? ''!!}
        <!-- .entry-content --> 
      </article>
      <!-- #post-108 --> 
      
    </div>
    <!-- #content .site-content --> 
  </div>
  <!-- #primary .content-area --> 
  
</div>

</div>
    </div>
@endsection

@push('scripts')
  
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
		<script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
		</script>
	@endif
@endpush