@extends('customer.layouts.master')
@section('title', "Contact Us")

@section('content')
<div class="about_main">
  <div class="container">
    <h2 class="head_bg">Contact Us</h2>
   <div class="who_left contact_left">
      <div class="who_heading">
      <h2>Email Us</h2>   
    </div>
    <div class="who_details">
      <form action="{{base_url('cms/contact-us/save')}}" method="post" name="contactform" id="contact_form">
        <div class="input_main">
          <input type="text" name="name" placeholder="Your Name" >
        </div>
      <div class="input_main">
          <input type="email" name="email" placeholder="E-Mail Address" required>
        </div>
      <div class="input_main">
        <textarea rows="5" name="enquiry" placeholder="Enter Your Message Here"></textarea>
        </div>
        <div class="input_main">
        <div class="input_quater">
          <input type="text" name="captcha" id="captcha" placeholder="Enter the code in the box on right">
        </div>
          <div class="input_halfer">
            <input type="text" name="captchaValue" id="captchaVal" readonly="">
          <span id="message"></span>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="input_main">
          <button type="submit" id="contactBtn" name="submit">Submit</button>
        </div>
      </form>
    </div>
   </div>
   <div class="who_right">
    <div class="who_heading">
      <h2>Customer Care</h2>    
    </div>
    <div class="who_details customer_details">
      <div class="row">
      <div class="img_sm"><img src="assets/images/mail.png"></div>
        <h3>Email:<span>support@installment.pk</span></h3>
      </div>
      <div class="row">
      <div class="img_sm"><img src="assets/images/mobi.png"></div>
        <h3>For Queries :<span>0300-5556347</span></h3>
      </div>
      <div class="row">
      <div class="img_sm"><img src="assets/images/head_phones.png"></div>
        <h3>Support Timings:<span>9:00 AM - 7:00 PM ( Monday to Saturday)</span></h3>
      </div>
    </div>
   </div>
   <div class="clearfix"></div>
  </div>
</div>

@endsection

@push('scripts')
  <script type="text/javascript">
    var numbers = ["2254","5013","2046","7954","2029","2008","4621","5975","2418","7842","3846","3278","9565","5937","5974","4001","9654","1978","3795","2897"];
    var value = Math.floor((Math.random()*20)+0);
    $('.input_halfer input').val(numbers[value]);
    $('#contact_form').on('submit', function(e){
      e.preventDefault();
      var form = $(this);
      var cap = $('#captcha').val(); 
      var capVal = $('#captchaVal').val();
      if(cap != capVal)
      {
        toastr.error('Please Enter the Correct Code');
        $('#captcha').addClass("validateField");
        $('#captchaVal').addClass("validateField");
      }
      else
      {
        $.ajax({
          type: form.attr('method'),
          url: form.attr('action'),
          data: form.serialize(),
          cache: false,
          dataType: "json",
          success: function(response)
          {
            toastr[response.status](response.message);
            if(response.status == 'success')
            {
              setTimeout(function(){
                window.location.reload()
              }, 500);
            }
          },
          error: function(response)
          {
            toastr.error('Something Went Wrong. Please Try Again.');
          }
        });
          // toastr.success('Congragulation Email Send Successfully.');
          // $('#captcha').removeClass("validateField");
          // $('#captchaVal').removeClass("validateField");
      }
    });
  </script>
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
		<script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
		</script>
	@endif
@endpush