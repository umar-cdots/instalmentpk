@extends('customer.layouts.master')
@section('title', "About Us | Easy Monthly Installment Plan")

@push('seo')
    <meta name="og:title" content="About Us | Easy Monthly Installment Plan – Installment Pk ">
	<meta name="og:site_name" content="Instalment.pk">
	<meta name="og:description " content="Buy now pay later - An online shopping for electronics. We offer electronics on easy installments ; buy lcd, mobile phones, refrigerator, ac, washing machines, fridge, bike , mobiles , laptops, generators, inverters on easy installments. Installment plans are available for 3, 6, 9, 12 and 18 months’ time period.">
	<meta name="og:image" content="{{ _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}">
	<meta name="og:url" content="{{ getCurrentRequestURI() }}">
@endpush

@section('content')
<div class="about_main">
  <div class="container">
  	<h2 class="head_bg">About Us</h2>
	  {!!$about->page_content ?? ''!!}
</div>
    </div>
@endsection

@push('scripts')
  
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
		<script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
		</script>
	@endif
@endpush