@extends('customer.layouts.master')

@section('title', "Islamic Financing")

@section('content')
<div class="about_main">
  <div class="container">
  	<h2 class="head_bg">Islamic Financing</h2>
    <div class="finPageLogo">
      <img src="{{ base_url('assets/images/islamic_financing.svg') }}" alt="Islamic Financing" class="financingPageLogo">
    </div>
     <p> {!!$islamic_financing->page_content ?? ''!!}</p>
	 
</div>
    </div>
@endsection
@push('scripts')
  <script type="text/javascript">
  	function openTab(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}
  </script>
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
		<script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
		</script>
	@endif
@endpush