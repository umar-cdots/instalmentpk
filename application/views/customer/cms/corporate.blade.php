@extends('customer.layouts.master')

@section('title', "Corporate Installments")

@section('content')
<div class="about_main">
  <div class="container">
  	<h2 class="head_bg">Corporate Installments</h2>
	   {!!$corporate_installments->page_content ?? ''!!}
</div>
    </div>
@endsection

@push('scripts')
  
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
		<script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
		</script>
	@endif
@endpush