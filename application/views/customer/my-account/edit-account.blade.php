@extends('customer.layouts.master')
@section('title', "My Account")

@section('content')
<div class="about_main">
	<div class="container">
		<h2 class="head_bg">My Account</h2>
   		<div class="MyAccountSection">
			
			
			@include('customer.my-account.navigation')
            
			<div class="woocommerce-MyAccount-content">
	<h1 class="myAccountPageTitle">Edit Profile</h1>
    <form action="" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group col-md-6">
                  <label for="first_name">First Name</label>
                  <input type="text" class="form-control" name="first_name" value="{{ $user->first_name}}" id="first_name" placeholder="First Name ">
                </div>
                <div class="form-group col-md-6">
                  <label for="last_name">Last Name</label>
                  <input type="text" class="form-control" name="last_name" value="{{ $user->last_name}}" id="last_name" placeholder="Last Name ">
                </div>
                <div class="form-group col-md-6">
                  <label for="primary_phone">Primary Phone</label>
                  <input type="number" class="form-control" name="primary_phone" value="{{ $customer->primary_phone}}" id="primary_phone" placeholder="Primary Phone">
                </div>
                <div class="form-group col-md-6">
                  <label for="current_address">Current Address</label>
                  <input type="text" class="form-control" name="current_address" value="{{ $customer->current_address}}" id="current_address" placeholder="Current Address">
                </div>
                <div class="form-group col-md-6">
                  <label for="cnic_no ">CNIC</label>
                  <input type="text" class="form-control" name="cnic_no" value="{{ $customer->cnic_no}}" id="cnic_no" placeholder="CNIC">
                </div>
                 <div class="form-group col-md-6">
                  <label for="password">Old Password</label>
                  <input type="password" class="form-control"  value="" name="password" id="password" placeholder="Old Password">
                </div>
                 <div class="form-group col-md-6">
                  <label for="new_password">New Password</label>
                  <input type="password" class="form-control" name="new_password" id="new_password" placeholder="New Password">
                </div>
                 <div class="form-group col-md-6">
                  <label for="confirm_password">Confirm Password</label>
                  <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password ">
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>

</div>
		</div>

  
   </div>

   <div class="clearfix"></div>
  </div>
</div>

@endsection

@push('scripts')
  
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
		<script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
		</script>
	@endif
@endpush