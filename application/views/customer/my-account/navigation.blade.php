<nav class="MyAccount-navigation">
            	<ul class="MyAccountNavigation">
					<li class="MyAccount-navigation-link--dashboard">
				<a href="{{ base_url("my-account") }}">Dashboard</a>
			</li>
					<li class="MyAccount-navigation-link--orders">
				<a href="{{ base_url("my-account/orders") }}">Orders</a>
			</li>
					<?php /*?><li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-address">
				<a href="http://www.suituptailors.com/demo/my-account/edit-address/">Addresses</a>
			</li><?php */?>
					<?php /*?><li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--payment-methods">
				<a href="http://www.suituptailors.com/demo/my-account/payment-methods/">Payment methods</a>
			</li><?php */?>
					<li class="MyAccount-navigation-link--edit-account">
				<a href="{{ base_url("my-account/edit-account") }}">Account details</a>
			</li>
            <li class="MyAccount-navigation-link--customer-logout">
				<a href="{{ base_url("logout") }}">Logout</a>
			</li>
			</ul>
</nav>
@push('scripts')
<script type="text/javascript">
  var url   = window.location.origin + window.location.pathname;
  var node  = $('.MyAccountNavigation').find('li a[href="' + url + '"]').parent();
  console.log(node);
  console.log('li a[href="' + url + '"]');
  $(node).addClass('is-active');
  $(node).closest('li').addClass('is-active');
</script>
@endpush