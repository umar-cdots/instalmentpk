@extends('customer.layouts.master')
@section('title', "My Account")

@section('content')
<div class="about_main">
  <div class="container">
    <h2 class="head_bg">My Account</h2>
    <div class="MyAccountSection">
    	@include('customer.my-account.navigation')
		<div class="woocommerce-MyAccount-content">
    	    <h1 class="myAccountPageTitle">Orders</h1>
	        <?php //var_dump($orders->toJSON()); ?>
        
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th align="left">Seller</th>
                        <th align="left">Product</th>
                        
                        <th align="left">Total Instalments</th>
                        <th align="left">Down Payment</th>
                        <th align="left">Amount Per Month</th>
                        
                        {{-- <th align="left">Your comment</th> --}}
                        <th align="left">Seller comment</th>                        
                        <th align="left">Status</th>
                        <th align="left">Date</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($orders as $order)
                         <tr>
                            <td align="left">{{ $order->vendor->business_name }}</td>
                            <td align="left"><a target="_blank" href=" {{ base_url("offer/" . $order->productOffer->slug) }} "> {{ $order->product->title }}</a></td>
                            
                            <td align="left">{{$order->productOffer->total_installments  }}</td>
                            <td align="left">{{ $order->productOffer->down_payment  }}</td>
                            <td align="left">{{ $order->productOffer->amount_per_month }}</td>
                            
                            {{-- <td align="left">{{ $order->customer_comment }}</td> --}}
                            <td align="left">{{ $order->vendor_comment }}</td>
                            <td align="left">{{ ucwords(str_replace('_', ' ', $order->status)) }}</td>
                            <td align="left">{{$order->created_at->format('Y-m-d')}}</td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="9">No Orders Available Yet</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
		</div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
@endsection

@push('scripts')
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
    <script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
            
		</script>
@endif
@endpush