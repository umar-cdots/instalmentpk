@extends('customer.layouts.master')
@section('title', "My Account")

@section('content')
<div class="about_main">
  <div class="container">
    <h2 class="head_bg">My Account</h2>
    <div class="MyAccountSection">
    	@include('customer.my-account.navigation')
		<div class="woocommerce-MyAccount-content">
    	    <h1 class="myAccountPageTitle">Orders</h1>
	        <?php //var_dump($orders->toJSON()); ?>
        
            <table border="1" cellpadding="5" cellspacing="3" width="100%">
                <thead>
                    <tr>
                        <th align="left">Vendor</th>
                        <th align="left">Product</th>
                        
                        <th align="left">Total Installments</th>
                        <th align="left">Down Payment</th>
                        <th align="left">Amount Per Month</th>
                        
                        <th align="left">Your comment</th>
                        <th align="left">Vendor comment</th>                        
                        <th align="left">Status</th>
                        <th align="left">Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($orders) > 0 ){ ?>
                    <?php foreach($orders as $order){?>
                    <tr>
                        <td align="left"><?php echo $order->vendor->business_name; ?></td>
                        <td align="left"><a target="_blank" href="<?php echo base_url("offer/" . $order->productOffer->slug); ?>"><?php echo $order->product->title; ?></a></td>
                        
                        <td align="left"><?php echo $order->productOffer->total_installments; ?></td>
                        <td align="left"><?php echo $order->productOffer->down_payment; ?></td>
                        <td align="left"><?php echo $order->productOffer->amount_per_month; ?></td>
                        
                        <td align="left"><?php echo $order->customer_comment; ?></td>
                        <td align="left"><?php echo $order->vendor_comment; ?></td>
                        <td align="left"><?php echo ucwords(str_replace('_', ' ', $order->status)); ?></td>
                        <td align="left"><?php echo $order->created_at->format('Y-m-d'); ?></td>
                    </tr>
                    <?php } ?>
                    <?php }?>
                </tbody>
            </table>
		</div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
@endsection

@push('scripts')
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
    <script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
		</script>
@endif
@endpush