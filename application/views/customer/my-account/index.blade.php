@extends('customer.layouts.master')
@section('title', "My Account")

@section('content')
<div class="about_main">
	<div class="container">
		<h2 class="head_bg">My Account</h2>
   		<div class="MyAccountSection">
			
			
			@include('customer.my-account.navigation')
            
			<div class="woocommerce-MyAccount-content">
	<h1 class="myAccountPageTitle">Dashboard</h1>
<p>
	<strong class="HelloName">Hello <strong>{{LoginInfo::getInstance()->getName()}}</strong></strong></p>

<p>
	From your account dashboard you can view your <a href="{{ base_url("my-account/orders") }}">recent orders</a>.</p>

</div>
		</div>
   <?php /*?>
   <div class="who_left contact_left">
      <div class="who_heading">
      <h2>Email Us</h2>   
    </div>
    <div class="who_details">
      <form action="{{base_url('cms/contact-us/save')}}" method="post" name="contactform" id="contact_form">
        <div class="input_main">
          <input type="text" name="name" placeholder="Your Name" >
        </div>
      <div class="input_main">
          <input type="email" name="email" placeholder="E-Mail Address">
        </div>
      <div class="input_main">
          <input type="text" name="enquiry" placeholder="Enquiry">
        </div>
        <div class="input_main">
        <div class="input_quater">
          <input type="text" name="captcha" id="captcha" placeholder="Enter the code in the box on right">
        </div>
          <div class="input_halfer">
            <p></p>
          <span id="message"></span>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="input_main">
          <button type="submit" name="submit">Submit</button>
        </div>
      </form>
    </div>
   </div>
   <div class="who_right">
    <div class="who_heading">
      <h2>Customer Care</h2>    
    </div>
    <div class="who_details customer_details">
      <div class="row">
      <div class="img_sm"><img src="assets/images/mail.png"></div>
        <h3>Email:<span>support@installment.pk</span></h3>
      </div>
      <div class="row">
      <div class="img_sm"><img src="assets/images/mobi.png"></div>
        <h3>For Queries :<span>0323-0677776</span></h3>
      </div>
      <div class="row">
      <div class="img_sm"><img src="assets/images/head_phones.png"></div>
        <h3>Support Timings:<span>9:00 AM - 7:00 PM ( Monday to Saturday)</span></h3>
      </div>
    </div>
   </div>
   <?php */?>
   <div class="clearfix"></div>
  </div>
</div>

@endsection

@push('scripts')
  
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
		<script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
		</script>
	@endif
@endpush