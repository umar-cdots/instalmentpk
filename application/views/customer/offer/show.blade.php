@extends('customer.layouts.master')
@section('title', "{$offer->product->title} - Instalment Offers")

@push('seo')
  <meta name="og:title" content="{{ "{$offer->product->title} - Instalment Offers" }}">
  <meta name="og:site_name" content="Instalment.pk">
  <meta name="og:description " content="{{ $offer->product->description }}">
  <meta name="og:image" content="{{ isset($offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$offer->product->productImages[0]->raw_name}{$offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}">
  <meta name="og:url" content="{{ getCurrentRequestURI() }}">
@endpush

@section('content')
  <div class="offers deals results pro_detalis_main">
  <div class="container">
    <div class="pro_top_main">
      <div class="pro_top_left">
       <div class="deals_box offer_search_box pro_top_box">
          <div class="box_img deals_box_img deals_box_img2 pro_top_box_img" data-product-offer-id="{{ $offer->id }}">
            <div class="wishlist_img">
              <img src="{{ base_url('assets/images/clr_shape.png') }}" style="display: {{ in_array($offer->id, $wishlist) ? "inline-block" : "none" }}" class="active_product ar_wishlist" data-target-class="not_active_product" data-product-offer-id="{{ $offer->id }}">
              <img src="{{ base_url('assets/images/no_clr.png') }}" style="display: {{ in_array($offer->id, $wishlist) ? "none" : "inline-block" }}" class="not_active_product ar_wishlist" data-target-class="active_product" data-product-offer-id="{{ $offer->id }}">
            </div>
            <a id="product_media" data-options="zoomWidth: 650; zoomHeight: 500;" href="{{ isset($offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . "{$offer->product->productImages[0]->raw_name}{$offer->product->productImages[0]->file_ext}") ? _MEDIA_UPLOAD_URL_ . "{$offer->product->productImages[0]->raw_name}{$offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" class="MagicZoom">
              <img src="{{ isset($offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . "{$offer->product->productImages[0]->raw_name}{$offer->product->productImages[0]->file_ext}") ? _MEDIA_UPLOAD_URL_ . "{$offer->product->productImages[0]->raw_name}310x310{$offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $offer->product->title }}">
            </a>
            <div class="small_images">
              <ul>
                @foreach($offer->product->productImages as $image)
                  <li>
                    <a href="{{ _MEDIA_UPLOAD_URL_ . "{$image->raw_name}310x310{$image->file_ext}" }}" data-zoom-id="product_media">
                      <img src="{{ _MEDIA_UPLOAD_URL_ . "{$image->raw_name}310x310{$image->file_ext}" }}">
                    </a>
                  </li>
                @endforeach
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="deals_box_details perMonth">
              <h3 class="total">Per Month:</h3>
              <h2><span class="rupeesText">Rs.</span>{{ number_format($offer->amount_per_month) }}<span>/-</span></h2>
            </div>
          </div>
          <div class="box_details deals_box_details pro_top_box_det">
            <h1>{{ $offer->product->title }}</h1>
            <div class="pro_feature proFeatureHeight">
              <h3><strong>KEY Features:</strong></h3>
              <ul class="proFeatureList">
                {{-- @foreach($offer->product->meta as $meta)
                  <li><span>►</span>{{ $meta->meta_key }}: {{ $meta->meta_value }}</li>
                @endforeach --}}
                {{-- <span>►</span> --}}
                @foreach($offer->meta as $meta)
                  <li>{{ $meta->meta_key }}: {{ $meta->meta_value }}</li>
                @endforeach             
              </ul>
            
            </div>
            {{--  <a class="showMore showMoreLink" id="showMore">View More</a> --}}
            <div class="clearfix"></div>
             <div class="pro_feature">
              <h3><strong>Offer Note</strong></h3>
             <p class="proFeaturePara">{{$offer->offer_notes}}</p>
            </div>
           {{--  <a class="showMore showMoreLink" id="showMore2">View More</a> --}}
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    <div class="pro_top_right">
      <div class="pro_top_head">
        <h3>Sold By: </h3>
        <h2>{{ $offer->vendor->fullName() }}</h2>
        <a href="{{ base_url('seller/' . $offer->vendor->slug) }}"><h4>{{ $offer->vendor->businessName() }}</h4></a>
      </div>
      <div class="pro_top_head_btm">
        <div class="head_btm_heading">
          {{--  }}
  {{ number_format($offer->down_payment) }}
           --}}
          <h2>Instalments: <span>
            <select name="instalment" class="change_offer">
            <option value="{{ $offer->total_installments }}">{{ $offer->total_installments }}</option>
            @foreach($other_offers as $other_offer)
              <option value="{{ $other_offer->total_installments }}" data-href="{{ $other_offer->slug }}">{{ $other_offer->total_installments }}</option>
            @endforeach
          </select></span></h2>
        </div>
        <div class="head_btm_heading">
          <h2>Down Payment: <span>
            <select name="instalment" class="change_offer">
            <option value="{{ number_format($offer->down_payment) }}">{{ number_format($offer->down_payment) }}</option>
            @foreach($other_offers as $other_offer)
              <option value="{{ $other_offer->down_payment }}" data-href="{{ $other_offer->slug }}">{{ $other_offer->down_payment }}</option>
            @endforeach
          </select></span></h2>
        </div>
        <div class="head_btm_heading">
          <h2>EMI (Equated Monthly Instalment): <span>Rs.{{ number_format($offer->amount_per_month) }}/month</span></h2>
        </div>
        <div class="head_btm_heading">
          <h2>Total Amount: <span>Rs.{{ number_format($offer->total_price_on_installment) }}</span></h2>
        </div>
        <div class="head_btm_form">
          <form action="{{ LoginInfo::getInstance()->isLogin() ? base_url('offer/' . $offer->slug .'?grabit=yes') : base_url('login') }}" method="post">
            <div class="input_main">
              @if(LoginInfo::getInstance()->isLogin())
                <button id="grabITButton" type="button" name="submit">GRAB IT</button>
              @else
                <a href="{{ base_url('login') }}">
                  <button type="button" name="submit">GRAB IT</button>
                </a>
              @endif
            </div>
          </form>
          <div id="OfferCommentBox" style="display:none;">
            <p>We will not share your contact information with anyone.</p>
            <h3>Sold By: </h3>
             <h4><span>Name: </span> {{ $offer->vendor->fullName() }}</h4>
             <h4><span>Business Name:</span> {{ $offer->vendor->businessName() }}</h4>
             <h4><span>Phone No: </span> {{ $offer->vendor->businessPhone() }}</h4>
             <h4><span>Email: </span>   {{ $offer->vendor->business_email }}</h4>
           {{--  <form action="{{ LoginInfo::getInstance()->isLogin() ? base_url('offer/' . $offer->slug .'?grabit=yes') : base_url('login') }}" method="post">
              <textarea class="commentWalaDaba" name="comment" placeholder="Enter Comments"></textarea>
              <input type="submit" value="Submit" class="leftWalaButton" style="float: left;" />
              <input class="rightWalaButton" id="cancel_grabbing" type="button" value="Cancel" style="float: left;" />
            </form> --}}
            <div class="clearfix"></div>
          </div>
          <div class="scrollToDiv">
             <a href="#">More seller to consider</a> 
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>

<h2>Product Description</h2>
<div class="result_box pro_sold_result">
  <p>{!!$offer->product->description ?? ''!!}</p>          
</div>

   {{-- More From Sellers --}}
  <h2 id="redirection">More seller to consider</h2>
    <div class="result_box pro_sold_result" id="scrl_div">
      <form action="#redirection" method="post">
        <div class="result_sm_fld">
          <h3>Advance:</h3>
          <div id="slider-range"></div>
            <input type="text" id="amount" readonly style="border:0; color:#1e9fbc; font-weight:bold;">
            <input type="hidden" name="max_down_payment" value="{{ $max_down_payment ?: '200000' }}">
            <input type="hidden" name="min_down_payment" value="{{ $min_down_payment ?: '0' }}">
        </div>
        <div class="result_sm_fld_hf">
          <h3>Max Instalments:</h3>
          <input type="text" name="max_total_installments" minlength="1">
         {{--  <select name="max_total_installments">
            @foreach(range(24, 1) as $month)
              <option value="{{ $month }}" {{ isset($max_total_installments) && $max_total_installments == $month ? 'selected' : '' }}>{{ $month }}</option>
            @endforeach
          </select> --}}
        </div>
        <div class="result_sm_fld">
          <h3>EMI:</h3>
          <div id="slider-range2"></div>
            <input type="text" id="amount2" readonly style="border:0; color:#1e9fbc; font-weight:bold;">
            <input type="hidden" name="max_amount_per_month" value="{{ $max_amount_per_month ?: '200000' }}">
            <input type="hidden" name="min_amount_per_month" value="{{ $min_amount_per_month ?: '0' }}">
        </div>
        <div class="result_sm_fld_hf">
          <h3>Sort By:</h3>
          <select name="sort_by">
            <option value="lowest" {{ isset($sort_by) && $sort_by  == 'lowest' ? 'selected' : '' }}>Lowest</option>
            <option value="highest" {{ isset($sort_by) && $sort_by == 'highest' ? 'selected' : '' }}>Highest</option>
            <option value="latest" {{ isset($sort_by) && $sort_by == 'latest' ? 'selected' : '' }}>Latest</option>
            <option value="popular" {{ isset($sort_by) && $sort_by == 'popular' ? 'selected' : '' }}>Most Popular</option>
          </select>
        </div>
        <div class="result_sm_fld_hf result_sm_fld_hf2">
          <button type="submit" name="filter" style="margin-top: 24px;">Apply Search</button>   
        </div>
        <div class="clearfix"></div>
      </form>
    </div>
    <div class="sellers_table">
      <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
        <tr class="table_head">
          <th>Seller</th>
          <th>Total Amount</th>
          <th>Advance</th>
          <th>Instalments</th>
          <th>EMI</th>
          <th></th>
        </tr>
        <tbody>
          @foreach($filtered_offers as $filtered_offer)
            <tr style="font-size: 14px;">
              <td>
                <a href="{{ base_url('seller/' . $filtered_offer->vendor->slug) }}" style="text-decoration: none;">
                  <span style="color: #144d88; font-weight: 600; font-size: 14px;">{{ $filtered_offer->vendor->fullName() }}</span> - {{ $filtered_offer->vendor->businessName() }}
                </a>
              </td>
              <td>Rs.{{ number_format($filtered_offer->total_price_on_installment) }}</td>
              <td>Rs.{{ number_format($filtered_offer->down_payment) }}</td>
              <td>{{ $filtered_offer->total_installments }}</td>
              <td>Rs.{{ number_format($filtered_offer->amount_per_month) }}/month</td>
              <td>
                <form action="{{ base_url('offer/' . $filtered_offer->slug) }}" method="POST">
                  <input type="hidden" name="max_down_payment" value="{{ $max_down_payment ?: '' }}">
                  <input type="hidden" name="min_down_payment" value="{{ $min_down_payment ?: '' }}">
                  <input type="hidden" name="max_total_installments" value="{{ $max_total_installments ?? '' }}">
                  <input type="hidden" name="max_amount_per_month" value="{{ $max_amount_per_month ?: '' }}">
                  <input type="hidden" name="min_amount_per_month" value="{{ $min_amount_per_month ?: '' }}">
                  <input type="hidden" name="sort_by" value="{{ $sort_by ?? '' }}">
                  <button type="submit" name="button" class="table_btn">View</button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      {{-- @foreach($filtered_offers->unique('vendor_id')->pluck('vendor_id') as $vendor_id)
      @php 
        $vendor = getVendor($vendor_id);
      @endphp
      <button class="accordion">{{ $vendor->fullName() }} - {{ $vendor->businessName() }}<span>Total Offers: {{ $filtered_offers->where('vendor_id', $vendor_id)->count() }}</span> <span class="arrowDown">▼</span></button>
      <div class="panel">
        <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
          <thead>
            <tr class="table_head">
              <th>Total Amount</th>
              <th>Advance</th>
              <th>Instalments</th>
              <th>EMI</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach($filtered_offers->where('vendor_id', $vendor_id)->all() as $filtered_offer)
              <tr style="font-size: 14px;">
                <td>Rs.{{ number_format($filtered_offer->total_price_on_installment) }}</td>
                <td>Rs.{{ number_format($filtered_offer->down_payment) }}</td>
                <td>{{ $filtered_offer->total_installments }}</td>
                <td>Rs.{{ number_format($filtered_offer->amount_per_month) }}/month</td>
                <td>
                  <form action="{{ base_url('offer/' . $filtered_offer->slug) }}" method="POST">
                    <input type="hidden" name="max_down_payment" value="{{ $max_down_payment ?? '' }}">
                    <input type="hidden" name="min_down_payment" value="{{ $min_down_payment ?? '' }}">
                    <input type="hidden" name="max_total_installments" value="{{ $max_total_installments ?? '' }}">
                    <input type="hidden" name="max_amount_per_month" value="{{ $max_amount_per_month ?? '' }}">
                    <input type="hidden" name="min_amount_per_month" value="{{ $min_amount_per_month ?? '' }}">
                    <input type="hidden" name="sort_by" value="{{ $sort_by ?? '' }}">
                    <button type="submit" name="button" class="table_btn">View</button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      @endforeach --}}
     
    </div>
  @include('customer.layouts.more-offers')
 </div>
</div>
@endsection

@push('styles')
  <link rel="stylesheet" type="text/css" href="{{ base_url('/assets/css/magiczoom.css') }}"> 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <style type="text/css">
    .table_head th{
      text-align: center;
    }
  </style>
@endpush

@push('scripts')
  <script type="text/javascript" src="{{ base_url('/assets/js/magiczoom.js') }}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">
    $('#grabITButton').click(function ()
    {
      var url = $(this).closest('form').attr('action');
      $.ajax({
        type: "GET",
        url: url,
        cache: false,
        success: function(response)
        {
          toastr.success("Order Has Been Placed.")
        },
        error: function(response)
        {
          toastr.error("Something Went Wrong.");
        }
      });
      // $.get(url);
      $('#OfferCommentBox').slideToggle('slow');
    });
    $('#cancel_grabbing').click(function(){
      $('#OfferCommentBox').slideToggle('slow');
    });
    $('#slider-range').slider({
      range: true,
      min: 0,
      max: {{ $max_adv_limit ?: 200000 }},
      values: [{{ $min_down_payment ?: 0 }}, {{ $max_down_payment ?: 200000 }}],
      slide: function(event, ui) {
        $("#amount").val( "Rs " + ui.values[0] + " - Rs " + ui.values[1]);
        $('input[name="min_down_payment"]').val(ui.values[0]);
        $('input[name="max_down_payment"]').val(ui.values[1]);
      }
    });
    $('#amount').val("Rs " + $('#slider-range').slider('values', 0) + " - Rs " + $('#slider-range').slider("values", 1));
    $( "#slider-range2" ).slider({
      range: true,
      min: 0,
      max: {{ $max_emi_limit ?: 200000 }},
      values: [{{ $min_amount_per_month ?: 0 }}, {{ $max_amount_per_month ?: 200000 }}],
      slide: function( event, ui ) {
        $('#amount2').val("Rs " + ui.values[0] + " - Rs " + ui.values[1]);
        $('input[name="min_amount_per_month"]').val(ui.values[0]);
        $('input[name="max_amount_per_month"]').val(ui.values[1]);
      }
    });
    $('#amount2').val("Rs " + $('#slider-range2').slider("values", 0) + " - Rs " + $('#slider-range2').slider('values', 1));
    $('.scrollToDiv a').click(function () {
      $('html, body').animate({scrollTop: $('#scrl_div').offset().top-200}, 700);
      return false;
    });
    $('.change_offer').change(function(){
      var href = $(this).find('option:selected').data('href');
      if(href != '' && typeof href != 'undefined')
      {
        window.location.href = href;
      }
    });
     $(document).ready(function(){
    $('#showMore2').on('click', function(){
      $(this).parent().find('.pro_feature p').toggleClass("proFeaturePara fullPara");
      if($('.pro_feature p').hasClass("proFeaturePara"))
      {
          $('#showMore2').text("View More");
      }
      else
      {
        $('#showMore2').text("View Less");
      }
    });
     $('#showMore').on('click', function(){
      $(this).parent().find('.proFeatureList').toggleClass(" fullPara");
      if($('.proFeatureList').hasClass("fullPara"))
      {
          $('#showMore').text("View Less");
      }
      else
      {
        $('#showMore').text("View More");
      }
    });
      var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
  });
  </script>
@endpush
