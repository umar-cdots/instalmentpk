@extends('customer.layouts.master')
@section('title', $offers[0] . "Offers")

{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start --}}
@push('scripts')
    <script src="{{ base_url("assets/js/owl.carousel.min.js")}}"></script> 
@endpush
{{-- Litte Hack, Becuase Owl Carousel is Required in featured.blade.php & products.blade.php Start End --}}

@section('content')
<div class="offers deals">
  <div class="container">
    <h2>featured deals</h2>
    <div class="owl-carousel" id="slider3">
      <div class="item">
        <div class="offers_box deals_box">
          <div class="box_img deals_box_img deals_box_img2"> <img src="assets/images/lapi_img.png" alt="tv">
            <h3>APPLE CINEMA 3D</h3>
          </div>
          <div class="box_details deals_box_details">
            <h2>Rs.208,90<span>/month</span></h2>
            <h4><span>By:</span> Al Fatah Electronics</h4>
            <a href="#">View Details</a> </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>

<div class="offers deals results">
  <div class="container">
    <h2>2102 Results Found</h2>
      <div class="result_box">
      <form action="#" method="post">
      	<div class="result_sm_fld">
			<h3>Advance Payment:</h3>
      		<div class="result_sm_fld_half">
      			<input type="text" name="advance_payment" placeholder="Rs.184a65">
      		</div> -
      		<div class="result_sm_fld_half">
      			<input type="text" name="advance_payment" placeholder="Rs.184a65">
      		</div>
      	</div>
      	<div class="result_sm_fld">
			<h3>Installment:</h3>
      		<div class="result_sm_fld_half">
      			<input type="text" name="installment" placeholder="Rs.184a65">
      		</div> -
      		<div class="result_sm_fld_half">
      			<input type="text" name="installment" placeholder="Rs.184a65">
      		</div>
      	</div>
      	<div class="result_sm_fld_hf">
			<h3>Time Period:</h3>
			<select name="time_period">
				<option value="">12 Months</option>
			</select>
      	</div>
      	<div class="result_sm_fld_hf">
			<h3>Sort By:</h3>
			<select name="most_popular">
				<option value="">Most Popular</option>
			</select>
      	</div>
      	<div class="result_sm_fld_hf result_sm_fld_hf2">
			<button type="submit" name="filter">Apply Search</button>		
	  	</div>
      	<div class="clearfix"></div>
      	</form>
      </div>
      
 		 <div class="deals_box offer_search_box">
          <div class="box_img deals_box_img deals_box_img2"> <img src="assets/images/lapi_img.png" alt="tv">
            <h3>APPLE CINEMA 3D</h3>
          </div>
          <div class="box_details deals_box_details">
            <h2>Rs.208,90<span>/month</span></h2>
            <h4><span>By:</span> Al Fatah Electronics</h4>
            <a href="#">View Details</a> </div>
        </div>
                   <div class="deals_box offer_search_box">
          <div class="box_img deals_box_img deals_box_img2"> <img src="assets/images/lapi_img.png" alt="tv">
            <h3>APPLE CINEMA 3D</h3>
          </div>
          <div class="box_details deals_box_details">
            <h2>Rs.208,90<span>/month</span></h2>
            <h4><span>By:</span> Al Fatah Electronics</h4>
            <a href="#">View Details</a> </div>
        </div>
                   <div class="deals_box offer_search_box">
          <div class="box_img deals_box_img deals_box_img2"> <img src="assets/images/lapi_img.png" alt="tv">
            <h3>APPLE CINEMA 3D</h3>
          </div>
          <div class="box_details deals_box_details">
            <h2>Rs.208,90<span>/month</span></h2>
            <h4><span>By:</span> Al Fatah Electronics</h4>
            <a href="#">View Details</a> </div>
        </div>
                   <div class="deals_box offer_search_box">
          <div class="box_img deals_box_img deals_box_img2"> <img src="assets/images/lapi_img.png" alt="tv">
            <h3>APPLE CINEMA 3D</h3>
          </div>
          <div class="box_details deals_box_details">
            <h2>Rs.208,90<span>/month</span></h2>
            <h4><span>By:</span> Al Fatah Electronics</h4>
            <a href="#">View Details</a> </div>
        </div>
                   <div class="deals_box offer_search_box">
          <div class="box_img deals_box_img deals_box_img2"> <img src="assets/images/lapi_img.png" alt="tv">
            <h3>APPLE CINEMA 3D</h3>
          </div>
          <div class="box_details deals_box_details">
            <h2>Rs.208,90<span>/month</span></h2>
            <h4><span>By:</span> Al Fatah Electronics</h4>
            <a href="#">View Details</a> </div>
        </div>
                   <div class="deals_box offer_search_box">
          <div class="box_img deals_box_img deals_box_img2"> <img src="assets/images/lapi_img.png" alt="tv">
            <h3>APPLE CINEMA 3D</h3>
          </div>
          <div class="box_details deals_box_details">
            <h2>Rs.208,90<span>/month</span></h2>
            <h4><span>By:</span> Al Fatah Electronics</h4>
            <a href="#">View Details</a> </div>
        </div>
                   <div class="deals_box offer_search_box">
          <div class="box_img deals_box_img deals_box_img2"> <img src="assets/images/lapi_img.png" alt="tv">
            <h3>APPLE CINEMA 3D</h3>
          </div>
          <div class="box_details deals_box_details">
            <h2>Rs.208,90<span>/month</span></h2>
            <h4><span>By:</span> Al Fatah Electronics</h4>
            <a href="#">View Details</a> </div>
        </div>
                   <div class="deals_box offer_search_box">
          <div class="box_img deals_box_img deals_box_img2"> <img src="assets/images/lapi_img.png" alt="tv">
            <h3>APPLE CINEMA 3D</h3>
          </div>
          <div class="box_details deals_box_details">
            <h2>Rs.208,90<span>/month</span></h2>
            <h4><span>By:</span> Al Fatah Electronics</h4>
            <a href="#">View Details</a> </div>
        </div>             
    <div class="clearfix"></div>
  </div>
</div>
@endsection

@push('scripts')
	<script type="text/javascript">
		$('#slider3').owlCarousel({
		    loop:true,
		    margin: 0,
			nav:false,
		    responsiveClass: true,
		    autoplay: true,
		    autoplayTimeout: 5000,
		    responsive: {
		        0: {
		            items: 1
		        },
		        600: {
		            items: 4
		        },
		        1000: {
		            items: 5
		        }
		    }
		});
	</script>
@endpush