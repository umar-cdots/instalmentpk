@extends('customer.layouts.master')
@section('title','Thanks')

@section('content')
<div class="about_main">
  <div class="container">
    <div class="signups">
      <div class="sign_details pkgs_det">
        <div id="pricing" class="container">
          <div class="row slideanim">
            <h3>Thank you! </h3>
            <h4>Thank you for buying our instalment plan.</h4>
            <h4>One of our representatives will get in touch with you very soon!</h4>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
@endsection 