@extends('customer.layouts.master')
@section('title', "Wishlist")

@section('content')
<div class="offers deals results" style="padding: 50px 0;">
	<div class="container">
		<h2>Wishlist</h2>
		
			<table cellspacing="0" cellpadding="0" width="100%" border="1" class="wishlistTable">
				<tr>
					<th></th>
					<th>Product Name</th>
					<th>Sold By</th>
					<th>Instalments</th>
					<th>Total Amount</th>
					<th>Down Payment</th>
					<th>EMI</th>
					<th></th>
				</tr>
        @foreach($offers as $offer)  
				<tr>
					<td> <img src="{{ isset($offer->product->productImages[0]) && is_file(_MEDIA_UPLOAD_PATH_ . $offer->product->productImages[0]->file_name) ? _MEDIA_UPLOAD_URL_ . "{$offer->product->productImages[0]->raw_name}170x120{$offer->product->productImages[0]->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" alt="{{ $offer->product->title }}" style="width: 100px; height: auto;"></td>
					<td><a href="{{ base_url('offer/' . $offer->slug) }}" target="_blank">{{ $offer->product->title }}</a></td>
					<td >{{ $offer->vendor->fullName() }}</td>
					<td style="text-align: center;">{{ $offer->total_installments }}</td>
					<td style="text-align: center;">{{ number_format($offer->total_price_on_installment) }}</td>
					<td style="text-align: center;">{{ number_format($offer->down_payment) }}</td>
					<td >{{ number_format($offer->amount_per_month) }}</td>
					<td><a href="#" data-product-offer-id="{{ $offer->id }}" class="ar_wishlist remove_btn">Remove</a></td>
				</tr>
				
					@endforeach
			</table>
	    	
 		  	
      
    	<div class="clearfix"></div>
  	</div>
</div>
@endsection


@push('styles')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <style type="text/css">
  	.wishlistTable
  	{
  		text-align: left;
  		margin-top: 30px;
  	}
  	.wishlistTable tr
  	{
  		margin:10px 0;
  	}
  	.remove_btn
  	{
  		width: 100%;
  		background: #d9534f;
  		border: #d9534f solid 1px;
  		padding:10px 15px;
  		box-sizing: border-box;
  		text-decoration: none;
  		color:#fff;
  		transition-duration: .3s;
  	}
  	.remove_btn:hover
  	{
  		background: none;
  		color: #d9534f;
 		transition-duration: .3s; 
  	}
   </style>
@endpush
@push('scripts')
{{-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
	$('#slider-range').slider({
      range: true,
      min: 0,
      max: 200000,
      values: [{{ $min_down_payment ?? 0 }}, {{ $max_down_payment ?? 200000 }}],
      slide: function(event, ui) {
        $("#amount").val( "Rs" + ui.values[0] + " - Rs" + ui.values[1]);
        $('input[name="min_down_payment"]').val(ui.values[0]);
        $('input[name="max_down_payment"]').val(ui.values[1]);
      }
    });
    $('#amount').val("Rs" + $('#slider-range').slider('values', 0) + " - Rs" + $('#slider-range').slider("values", 1));
    $( "#slider-range2" ).slider({
      range: true,
      min: 0,
      max: 200000,
      values: [{{ $min_amount_per_month ?? 0 }}, {{ $max_amount_per_month ?? 200000 }}],
      slide: function( event, ui ) {
        $('#amount2').val("Rs" + ui.values[0] + " - Rs" + ui.values[1]);
        $('input[name="min_amount_per_month"]').val(ui.values[0]);
        $('input[name="max_amount_per_month"]').val(ui.values[1]);
      }
    });
    $('#amount2').val("Rs" + $('#slider-range2').slider("values", 0) + " - Rs" + $('#slider-range2').slider('values', 1));
</script> --}}
@endpush