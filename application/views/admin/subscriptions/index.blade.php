@extends('admin.layouts.master')

@section('title', "Subscriptions History")
@section('page_heading', "Subscriptions")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "History") --}}
@section('content')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          {{-- <h3 class="box-title">Products</h3> --}}
          <form class="form-inline" method="POST" action="" style="float: left;">
            <div class="form-group">
              <label for="vendor_id">Seller:</label>
              <select name="vendor_id" class="form-control" id="vendor_id">
                <option value="">Select a Seller</option>
                @foreach($vendors as $vendor)
                  <option value="{{ $vendor->id }}" {{ $vendor->id == $vendor_id ? 'selected' : '' }}>{{ $vendor->fullName() }} | {{ $vendor->businessName() }}</option>
                @endforeach
              </select>
            </div>
            <button type="submit" class="btn btn-primary">Go</button>
          </form>
          <button class="btn btn-primary add-btn-right" data-toggle="modal" data-target="#subscribe">Subscribe</button>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="subscription" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>ID</th>
              <th>Seller</th>
              <th>Name</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Days left expired</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($subscriptions as $subscription)
                @continue(empty($subscription->vendor))
                @continue(empty($subscription->subscriptionPackage))
              <tr>
                <td>{{ $subscription->id }}</td>
                <td>{{ $subscription->vendor->fullName() }}</td>
                <td>{{ $subscription->subscriptionPackage->package_title }}</td>
                <td>{{ $subscription->subscription_date }}</td>
                <td>{{ $subscription->subscription_exp_date }}</td>
                <td>{{ ($subscription->daysLeft() == 0 || $subscription->subscription_status != 'subscribed') ? '-' : $subscription->daysLeft() . ' Day(s)' }}</td>
                <td>
                @if($subscription->subscription_status == 'subscribed')
                  <button class="btn btn-xs btn-success">Active</button>
                @elseif($subscription->status == 'payment_due')
                  <button class="btn btn-xs btn-warning">Payment Due</button>
                @else
                  <button class="btn btn-xs btn-danger">{{ ucfirst($subscription->subscription_status) }}</button>
                @endif
                </td>
                <td>
                  @if($subscription->subscription_status == 'subscribed')
                    <button class="btn btn-xs btn-danger subscribe_action" data-action="unsub" data-subscription-id="{{ $subscription->id }}">Unsubscribe</button>
                  @else
                    -
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
@endsection

@push('modals')
  <!-- Modal -->
  <div id="subscribe" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <form>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Package Subscription</h4>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <label for="pvendor_id">Sellers:</label>
                <select name="vendor_id" class="form-control" id="pvendor_id" required="">
                  <option>Select a Seller</option>
                  @foreach($vendors as $vendor)
                    <option value="{{ $vendor->id }}" {{ $vendor->id == $vendor_id ? 'selected' : '' }}>{{ $vendor->fullName() }} | {{ $vendor->businessName() }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="package_id">Package:</label>
                <select name="package_id" class="form-control" id="package_id" required="">
                  <option>Select a Package</option>
                  @foreach($packages as $package)
                    <option value="{{ $package->id }}">{{ $package->package_title }}</option>
                  @endforeach
                </select>
              </div>
              {{-- <div class="form-group">
                <label for="package_id">
                  <input type="checkbox" name="add_to_ledger" value="add_ledger">
                  Add to Ledger
                </label>
              </div> --}}
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endpush

@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
    #subscribe .modal-content
    {
      border-radius: 5px;
    }
    #subscribe .modal-header
    {
      background-color: #3c8dbc;
      color: white;
      padding: 5px 15px;
    }
    #subscribe .modal-header button
    {
      opacity: 1;
    }
  </style>
@endpush

@push('scripts')
  <!-- DataTables -->
  <script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script type="text/javascript">
    $('#subscription').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      "order"       : [[ 1, "desc" ]],
      'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1] /* 1st one, start by the right */
      }]
    });
    $('.subscribe_action').click(function(){
      var node    = $(this);
      var action  = $(this).data('action');
      var id      = $(this).data('subscription-id');
      var text    = $(this).text();
      if(confirm("Are You Sure? You Want to " + text + "?"))
      {
        if(action == 'unsub')
        {
          $.ajax({
            type: "GET",
            url: "{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/ajax/unsubscribe/") }}" + id,
            dataType: "json",
            cache: false,
            success: function(response){
              if(response.status == 'success')
              {
                toastr['success'](response.message);
                $(node).removeAttr('class');
                $(node).addClass('btn');
                $(node).addClass('btn-xs');
                $(node).addClass('btn-warning');
                $(node).text("Expired");
              }
              else
              {
                toastr['error'](response.message);
              }
            },
            error: function(){
              toastr['error']("Something Went Wrong.");
            }
          });
        }
      }
    });
    $('#subscribe form').submit(function(e){
      e.preventDefault();
      var request = $(this).serialize();
      $.ajax({
        type: "POST",
        url: "{{ base_url(_ADMIN_ROUTE_PREFIX_ . '/ajax/subscribe') }}",
        data: request,
        dataType: "json",
        cache: false,
        success: function(response)
        {
          if(response.status == 'success')
          {
            toastr['success'](response.message);
            $('#subscribe').modal('toggle');
          }
          else
          {
            toastr['error'](response.message);
          }
        },
        error: function()
        {
          toastr['error']("Something Went Wrong.");
        }
      });
    });
  </script>
@endpush