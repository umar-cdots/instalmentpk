@extends('admin.layouts.master')

@section('title', "Edit Key Feature")
@section('page_heading', "Key Feature")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "Edit") --}}
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              {{-- <h3 class="box-title">Edit Category</h3> --}}
              <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/key_feature/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form action="{{base_url(_ADMIN_ROUTE_PREFIX_ . '/key_feature/update/' . $key_feature->id)}}" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="product_category_id">Categories*</label>
                  <select class="form-control" name="product_category_id" id="product_category_id">
                    @foreach($product_categories as $category)
                      <option value="{{ $category->id }}" {{ $category->id == $key_feature->product_category_id }}>{{ $category->category_title }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="title">Title*</label>
                  <input type="text" class="form-control" name="title" id="title" placeholder="Feature Title" value="{{ $key_feature->title }}">
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection