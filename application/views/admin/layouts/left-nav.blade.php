<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ base_url("assets/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{ LoginInfo::getInstance()->getName() }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li>
        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/dashboard") }}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li>
        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/products") }}">
          <i class="fa fa-barcode"></i> <span>Products</span>
        </a>
      </li>
      <li>
        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/brand") }}">
          <i class="fa fa-bold"></i> <span>Brands</span>
        </a>
      </li>
      <li>
        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/category") }}">
          <i class="fa fa-braille"></i> <span>Categories</span>
        </a>
      </li>
      <li>
        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/offers") }}">
          <i class="fa fa-barcode"></i> <span>Offers</span>
        </a>
      </li>
      
      <li class="treeview">
        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/orders") }}">
          <i class="fa fa-shopping-cart"></i> 
          <span>Orders</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li>
            <a href="{{base_url(_ADMIN_ROUTE_PREFIX_ . "/orders/free") }}">
              <i class="fa fa-circle-o"></i><span>Free</span>
            </a>
          </li>
          <li>
            <a href="{{base_url(_ADMIN_ROUTE_PREFIX_ . "/orders/phone_verification") }}">
              <i class="fa fa-circle-o"></i><span>Phone Verification</span>
            </a>
          </li>
          <li>
            <a href="{{base_url(_ADMIN_ROUTE_PREFIX_ . "/orders/physical_verification") }}">
              <i class="fa fa-circle-o"></i><span>Physical Verification</span>
            </a>
          </li>
        </ul>
      </li>
      
      <li>
        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/ledger") }}">
          <i class="fa fa-book"></i> <span>Ledger</span>
        </a>
      </li>
      <li>
        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/subscriptions") }}">
          <i class="fa fa-tags"></i> <span>Subscriptions</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-connectdevelop"></i> <span>CMS</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li>
            <a href="{{base_url(_ADMIN_ROUTE_PREFIX_ . "/cms/about-us") }}">
              <i class="fa fa-circle-o"></i><span>About us</span>
            </a>
          </li>
          {{-- <li><a href="{{base_url("contact-us") }}"><i class="fa fa-circle-o"></i>Contact Us</a></li> --}}
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
@push('scripts')
<script type="text/javascript">
  var url   = window.location.origin + window.location.pathname;
  var node  = $('.sidebar-menu').find('li a[href="' + url + '"]').parent();
  console.log(node);
  console.log('li a[href="' + url + '"]');
  $(node).addClass('active');
  $(node).closest('li').addClass('active');
  
  $('.treeview-menu li.active').parent().parent().addClass('active');
  
</script>
@endpush