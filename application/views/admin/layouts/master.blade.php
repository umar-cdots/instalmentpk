<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title') - Instalment PK</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/bootstrap/dist/css/bootstrap.min.css") }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/font-awesome/css/font-awesome.min.css") }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/Ionicons/css/ionicons.min.css") }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ base_url("assets/css/AdminLTE.min.css") }}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ base_url("assets/css/skins/_all-skins.min.css") }}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{ base_url("assets/css/toastr.min.css") }}">
  <!-- Custom Designs -->
  <link rel="stylesheet" href="{{ base_url("assets/css/adminlte_custom.css") }}">
  @stack('css')

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  @include('admin.layouts.top-nav')
  
  @include('admin.layouts.left-nav')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    @component('admin.components.breadcrumb', ['breadcrumbs' => isset($breadcrumbs) ? $breadcrumbs : []])
      @slot('title')
        @yield('page_heading', 'Heading')
      @endslot
      @slot('sub_title')
        @yield('page_sub_heading', 'Sub Heading')
      @endslot
    @endcomponent

    @include('admin.layouts.error-bag')

    <!-- Main content -->
    <section class="content">
      @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-<?php echo date('Y'); ?> <a target="_blank" href="https://www.creative-dots.com/">Creative Dots</a>.</strong> All rights
    reserved.
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
@stack('modals')
<!-- jQuery 3 -->
<script src="{{ base_url("assets/bower_components/jquery/dist/jquery.min.js") }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ base_url("assets/bower_components/bootstrap/dist/js/bootstrap.min.js") }}"></script>
<!-- Slimscroll -->
<script src="{{ base_url("assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js") }}"></script>
<!-- FastClick -->
<script src="{{ base_url("assets/bower_components/fastclick/lib/fastclick.js") }}"></script>
<!-- Toastr -->
<script src="{{ base_url("assets/js/toastr.min.js") }}"></script>
<!-- AdminLTE App -->
<script src="{{ base_url("assets/js/adminlte.min.js") }}"></script>
<script type="text/javascript">
  $('.removeThisNode').click(function(e){
    if(!confirm("Do You Really Want to Remove This?"))
      e.preventDefault();
  });
</script>

@stack('scripts')

@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
  <script type="text/javascript">
    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
    toastr['{{ get_instance()->session->flashdata('status') }}']("{{ get_instance()->session->flashdata('message') }}");
  </script>
@endif
<!-- AdminLTE for demo purposes -->
<script src="{{ base_url("assets/js/demo.js") }}"></script>
</body>
</html>
