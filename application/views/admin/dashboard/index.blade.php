@extends('admin.layouts.master')

@section('title', "Dashboard")
@section('page_heading', "Dashboard")
@section('page_sub_heading', "Control Panel")
@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-aqua">
	    <div class="inner">
	      <h3>150</h3>
	      <p>Total Orders</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-bag"></i>
	    </div>
	    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-green">
	    <div class="inner">
	      <h3>53<sup style="font-size: 20px">%</sup></h3>

	      <p>Orders Completed</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-ios-cart-outline"></i>
	    </div>
	    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-yellow">
	    <div class="inner">
	      <h3>44</h3>

	      <p>Registered Users</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-person-add"></i>
	    </div>
	    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-red">
	    <div class="inner">
	      <h3>65</h3>

	      <p>Registered Vendors</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-person-stalker"></i>
	    </div>
	    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
</div>
<!-- /.row -->
@endsection

@push('css')
	
	<!-- Morris chart -->
	<link rel="stylesheet" href="{{ base_url("assets/bower_components/morris.js/morris.css") }}">
	<!-- jvectormap -->
	<link rel="stylesheet" href="{{ base_url("assets/bower_components/jvectormap/jquery-jvectormap.css") }}">
	<!-- Date Picker -->
	<link rel="stylesheet" href="{{ base_url("assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") }}">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="{{ base_url("assets/bower_components/bootstrap-daterangepicker/daterangepicker.css") }}">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="{{ base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css") }}">
@endpush

@push('scripts')
	<!-- jQuery UI 1.11.4 -->
	<script src="{{ base_url("assets/bower_components/jquery-ui/jquery-ui.min.js") }}"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Morris.js charts -->
	<script src="{{ base_url("assets/bower_components/raphael/raphael.min.js") }}"></script>
	<script src="{{ base_url("assets/bower_components/morris.js/morris.min.js") }}"></script>
	<!-- Sparkline -->
	<script src="{{ base_url("assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js") }}"></script>
	<!-- jvectormap -->
	<script src="{{ base_url("assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js") }}"></script>
	<script src="{{ base_url("assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js") }}"></script>
	<!-- jQuery Knob Chart -->
	<script src="{{ base_url("assets/bower_components/jquery-knob/dist/jquery.knob.min.js") }}"></script>
	<!-- daterangepicker -->
	<script src="{{ base_url("assets/bower_components/moment/min/moment.min.js") }}"></script>
	<script src="{{ base_url("assets/bower_components/bootstrap-daterangepicker/daterangepicker.js") }}"></script>
	<!-- datepicker -->
	<script src="{{ base_url("assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{ base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js") }}"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="{{ base_url("assets/js/pages/dashboard.js") }}"></script>
@endpush