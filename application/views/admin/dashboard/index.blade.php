@extends('admin.layouts.master')

@section('title', "Dashboard")
@section('page_heading', "Dashboard")
{{-- @section('page_sub_heading', "Control Panel") --}}
@section('page_sub_heading', "")
@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-aqua">
	    <div class="inner">
	      <h3>{{ $total_orders }}</h3>
	      <p>Total Orders</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-bag"></i>
	    </div>
	    <a href="{{base_url(_ADMIN_ROUTE_PREFIX_ . "/orders") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-green">
	    <div class="inner">
	     <h3>{{ $new_orders_count }}</h3>

	      <p>New Orders</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-ios-cart-outline"></i>
	    </div>
	    <a href="{{base_url(_ADMIN_ROUTE_PREFIX_ . "/orders") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-yellow">
	    <div class="inner">
	      <h3>{{ $total_customers }}</h3>
	      <p>Registered Customers</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-person-add"></i>
	    </div>
	    <a href="{{base_url(_ADMIN_ROUTE_PREFIX_ . "/customers") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-red">
	    <div class="inner">
	      <h3>{{ $total_vendors }}</h3>
	      <p>Registered Sellers</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-person-stalker"></i>
	    </div>
	    <a href="{{base_url(_ADMIN_ROUTE_PREFIX_ . "/seller") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
</div>
<div class="row">
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-aqua">
	    <div class="inner">
	      <h3>{{ $total_brands }}</h3>
	      <p>Total Brands</p>
	    </div>
	    <div class="icon">
	      <i class="fa fa-bold"></i>
	    </div>
	    <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/brand") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-green">
	    <div class="inner">
	      <h3>{{ $total_products }}</h3>
	      <p>Total Products</p>
	    </div>
	    <div class="icon">
	      <i class="fa fa-barcode"></i>
	    </div>
	    <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/products") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-yellow">
	    <div class="inner">
	      <h3>{{ $total_categories }}</h3>

	      <p>Total Categories</p>
	    </div>
	    <div class="icon">
	      <i class="fa fa-braille"></i>
	    </div>
	    <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/category") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-red">
	    <div class="inner">
	      <h3>{{ $total_offers }}</h3>

	      <p>Total Offers</p>
	    </div>
	    <div class="icon">
	      <i class="fa fa-barcode"></i>
	    </div>
	    <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/offers") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
</div>
<!-- /.row -->
@endsection

{{-- @push('modals')
    <div class="modal fade" id="viewRecentOrders">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Recent Orders</h4>
          </div>
          <div class="modal-body">
            <div class="woocommerce-MyAccount-content">
    	    <h1 class="myAccountPageTitle"> Recent Orders</h1>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-hover">
                <thead>

                    <tr>
                        <th align="left">Seller</th>
                        <th align="left">Customer</th>
                        <th align="left">Product</th>
                        <th align="left">Total Instalments</th>
                        <th align="left">Down Payment</th>
                        <th align="left">Amount Per Month</th>    
                        <th align="left">Date</th>
                    </tr>
                </thead>
                <tbody>
                	@forelse($order_view as $order)
                         <tr>
                            <td align="left">{{ $order->vendor->business_name }}</td>
                            <td align="left">{{ $order->customer->user->fullName() }}</td>
                            <td align="left">{{ $order->product->title }}</td>
                            <td align="left">{{$order->productOffer->total_installments  }}</td>
                            <td align="left">{{ $order->productOffer->down_payment  }}</td>
                            <td align="left">{{ $order->productOffer->amount_per_month }}</td>
                            <td align="left">{{$order->created_at->format('Y-m-d')}}</td>
                        </tr>
                     @empty
                        <tr>
                            <td colspan="9">No Orders Available Yet</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
		</div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endpush --}}

@push('css')
	
	<!-- Morris chart -->
	<link rel="stylesheet" href="{{ base_url("assets/bower_components/morris.js/morris.css") }}">
	<!-- jvectormap -->
	<link rel="stylesheet" href="{{ base_url("assets/bower_components/jvectormap/jquery-jvectormap.css") }}">
	<!-- Date Picker -->
	<link rel="stylesheet" href="{{ base_url("assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") }}">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="{{ base_url("assets/bower_components/bootstrap-daterangepicker/daterangepicker.css") }}">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="{{ base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css") }}">
@endpush

@push('scripts')

	<!-- jQuery UI 1.11.4 -->
	<script src="{{ base_url("assets/bower_components/jquery-ui/jquery-ui.min.js") }}"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	{{-- <script type="text/javascript">
	    $(window).on('load',function(){
	        $('#viewRecentOrders').modal('show');
     });
    </script> --}}
	<!-- Morris.js charts -->
	<script src="{{ base_url("assets/bower_components/raphael/raphael.min.js") }}"></script>
	<script src="{{ base_url("assets/bower_components/morris.js/morris.min.js") }}"></script>
	<!-- Sparkline -->
	<script src="{{ base_url("assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js") }}"></script>
	<!-- jvectormap -->
	<script src="{{ base_url("assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js") }}"></script>
	<script src="{{ base_url("assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js") }}"></script>
	<!-- jQuery Knob Chart -->
	<script src="{{ base_url("assets/bower_components/jquery-knob/dist/jquery.knob.min.js") }}"></script>
	<!-- daterangepicker -->
	<script src="{{ base_url("assets/bower_components/moment/min/moment.min.js") }}"></script>
	<script src="{{ base_url("assets/bower_components/bootstrap-daterangepicker/daterangepicker.js") }}"></script>
	<!-- datepicker -->
	<script src="{{ base_url("assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{ base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js") }}"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="{{ base_url("assets/js/pages/dashboard.js") }}"></script>
@endpush