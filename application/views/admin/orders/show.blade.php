@extends('admin.layouts.master')

@section('title', $order->product->title . " - Order")
@section('page_heading', $order->product->title)
@section('page_sub_heading', "View")
@section('content')
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
                <tr>
                 <th align="left">ID</th>
                  <td>{{$order->id}}</td>
                </tr>
                <tr>
                  <th align="left">Vendor</th>
                  <td><?php echo $order->vendor->business_name; ?></td>
                </tr>
                <tr>
                  <th align="left">Product</th>
                  <td><a target="_blank" href="<?php echo base_url("offer/" . $order->productOffer->slug); ?>"><?php echo $order->product->title; ?></a></td>
                </tr>
                <tr>
                  <th align="left">Total Installments</th>
                  <td><?php echo $order->productOffer->total_installments; ?></td>
                </tr>
                
                <tr>
                   <th align="left">Down Payment</th>
                  <td><?php echo number_format($order->productOffer->down_payment) . _RUPPEE_SIGN_; ?></td>
                </tr>
                <tr>
                  <th align="left">Amount Per Month</th>
                  <td><?php echo number_format($order->productOffer->amount_per_month) . _RUPPEE_SIGN_ ; ?></td>
                </tr>
                <tr>
                  <th align="left">Customer comment</th>
                  <td><?php echo $order->customer_comment; ?></td>
                </tr>
                
                <tr>
                  <th align="left">Vendor comment</th>
                  <td><?php echo $order->vendor_comment; ?></td>
                </tr>
                
                <tr>
                  <th align="left">Admin comment</th>
                  <td><?php echo $order->admin_comment; ?></td>
                </tr>
                <tr>
                  <th align="left">Verification Type</th>
                  <td>
                    @if($order->verification_type == 'phone_verification')
                      Phone Verification
                    @elseif($order->verification_type == 'physical_verification')
                      Physical Verification
                    @else
                      Free
                    @endif
                  </td>
                </tr>
                <tr>
                  <th align="left">Verification Status</th>
                  <td>{{ str_replace('_', ' ', ucfirst($order->phoneVerification->verification_status ?? 'Pending')) }}</td>
                </tr>
                <tr>
                  <th align="left">Order Status</th>
                  <td>{{ str_replace('_', ' ', ucfirst($order->status)) }}</td>
                </tr>
                <tr>
                  <th align="left">Date</th>
                  <td><?php echo $order->created_at->format('Y-m-d'); ?></td>
                </tr>
                <tr>
                  <td colspan="2">
                    @if(!isset($order->phoneVerification->verification_status) || $order->phoneVerification->verification_status != 'verified')
                      <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/orders/verify/" . $order->id) }}">
                        <button class="btn btn-warning"><i class="fa fa-certificate"></i> Verify</button>
                      </a>
                    @endif
                    <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/orders/edit/" . $order->id) }}">
                      <button class="btn btn-primary"><i class="fa fa-edit"></i> Edit</button>
                    </a>
                  </td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    @endsection
@push('css')
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush
