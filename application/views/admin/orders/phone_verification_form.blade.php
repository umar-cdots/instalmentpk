@php
	error_reporting(0);
@endphp

@extends('admin.layouts.master')

@section('title', "Edit an Orders")
@section('page_heading', "Orders")
@section('page_sub_heading', "Edit")
@section('content')
<div class="row phone_verification_form">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Phone Verification Form</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{base_url(_ADMIN_ROUTE_PREFIX_ . '/orders/save_verification/' . $order->id)}}" name="add_pv" id="add_pv" method="post">
              <div class="box-body">
                {{-- <div class="box-title text-center">
                	<h3>Phone Verification</h3>
                </div> --}}
                <div class="hold1">
                	<div>
                		<h3 style="color: #000">Personal Details</h3>
                	</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4">
								<label>Name</label>
								<input type="text" id="ps_name" name="ps_name" class="form-control" value="{{ $order->phoneVerification->name ?? $order_info['ps_name'] }}">
							</div>
							<div class="col-md-4">
								<label>Father's / Husband's Name</label>
								<input type="text" id="ps_guardian_name" name="ps_guardian_name" class="form-control" value="{{ $order->phoneVerification->guardian_name ?? $order_info['ps_guardian_name'] }}">
							</div>
							<div class="col-md-4">
								<label>CNIC #</label>
								<input type="text" name="ps_cnic_no" id="ps_cnic_no" class="form-control" value="{{ $order->phoneVerification->cnic_no ?? $order_info['ps_cnic_no'] }}">
							</div>
							<div class="col-md-4 mrgn">
								<label>Nominee Name</label>
								<input type="text" name="ps_nominee_name" id="ps_nominee_name" class="form-control" value="{{ $order->phoneVerification->nominee_name ?? $order_info['ps_nominee_name'] }}">
							</div>
							<div class="col-md-4 mrgn">
								<label>Nominee's Father / Husband Name</label>
								<input type="text" name="ps_nominee_guardian_name" id="ps_nominee_guardian_name" class="form-control" value="{{ $order->phoneVerification->nominee_guardian_name ?? $order_info['ps_nominee_guardian_name'] }}">
							</div>
							<div class="col-md-4 mrgn">
								<label>Phone Number (Landline)</label>
		                        <input type="text" name="ps_landline_phone" id="ps_landline_phone" class="form-control" value="{{ $order->phoneVerification->landline_phone ?? $order_info['ps_landline_phone'] }}">
							</div>
							<div class="col-md-4 mrgn">
								<label>Mobile Number</label>
								<input type="text" name="ps_mobile_phone" id="ps_mobile_phone" class="form-control" value="{{ $order->phoneVerification->mobile_phone ?? $order_info['ps_mobile_phone'] }}">
							</div>
							<div class="col-md-4 mrgn">
								<label>Current Address as on CNIC</label>
								<input type="text"  name="ps_current_address" id="ps_current_address" class="form-control address_field" value="{{ $order->phoneVerification->current_address ?? $order_info['ps_current_address'] }}">
							</div>
							<div class="col-md-4 mrgn">
								<label>Permanent Address as on CNIC</label>
								<input type="text" name="ps_permanent_address" id="ps_permanent_address" class="form-control address_field" value="{{ $order->phoneVerification->permanent_address ?? $order_info['ps_permanent_address'] }}">
							</div>
						</div>
					</div>
	               	<div class="seperator_div"><hr></div>
			        <div class="sub_heading">
	                	<h3>Professional Details</h3>
	                </div>
					<div class="pro_radio">
						<div class="col-md-4">
							<label><input class="customRadio" type="radio" value="government_employee" data-id="governmentEmp" name="professional_type" {{ $order->phoneVerification->professional_type == 'government_employee' ? 'checked' : '' }}>Government Employee</label>
						</div>	
						<div class="col-md-4">
							<label><input class="customRadio" type="radio" value="private_employee" data-id="PrivateEmployees"  name="professional_type" {{ $order->phoneVerification->professional_type == 'private_employee' ? 'checked' : '' }}>Private employee</label>
						</div>
						<div class="col-md-4">
							<label><input type="radio" class="customRadio" name="professional_type" data-id="businessShop" value="business" {{ $order->phoneVerification->professional_type == 'business' ? 'checked' : '' }}>Business – Shop Owner / Service Provider</label>
							<div class="clearfix"></div>
						</div>
						<div class="col-md-4">
							<label><input type="radio" class="customRadio" name="professional_type" data-id="agriculturalLand" value="agriculture" {{ $order->phoneVerification->professional_type == 'agriculture' ? 'checked' : '' }}>Agricultural Land Owner</label>
						</div>		
						<div class="col-md-4">
							<label><input type="radio" class="customRadio" name="professional_type" data-id="selfEmp" value="self_employed" {{ $order->phoneVerification->professional_type == 'self_employed' ? 'checked' : '' }}>Self Employed – Freelancer/Service</label>
						</div>		
						<div class="col-md-4">
							<label><input type="radio" class="customRadio" name="professional_type" data-id="student" value="student" {{ $order->phoneVerification->professional_type == 'student' ? 'checked' : '' }}>Student</label>
						</div>		
						<div class="clearfix"></div>
					</div>

					@php
						$professional_json = json_decode($order->phoneVerification->profession_json);
					@endphp	

	                <div class="organization_area mrgn pro_radio CustomTabs professional_type_tab" id="governmentEmp" style="display:{{ $order->phoneVerification->professional_type == 'government_employee' ? 'blocked' : 'none' }};">
	                	<div class="row">
	                		<div class="col-md-4">
								<label>Organization Name</label>
								<input type="text" name="personal[json][personal_orgnization_name]" id="personal_orgnization_name" class="form-control" value="{{ $professional_json->personal_orgnization_name ?? '' }}">
	                		</div>
	                		<div class="col-md-4">
	                			<label>Department</label>
								<input type="text" name="personal[json][personal_department_name]" id="personal_department_name" class="form-control" value="{{ $professional_json->personal_department_name ?? '' }}">
	                		</div>
	                		<div class="col-md-4">
	                			<label>Scale / Grade</label>
								<input type="text" name="personal[json][personal_scale]" id="personal_scale" class="form-control" value="{{ $professional_json->personal_scale ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
								<label>Year of Service</label>
								<input type="text" id="year_of_services" name="personal[json][year_of_services]" class="form-control" value="{{ $professional_json->year_of_services ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
								<label>Monthly Salary</label>
								<input type="text" name="personal[json][personal_salary_month]" id="personal_salary_month" class="form-control" value="{{ $professional_json->personal_salary_month ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
								<label>Office Address</label>
								<input type="text" name="personal[json][personal_office_address]" id="personal_office_address" class="form-control" value="{{ $professional_json->personal_office_address ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
								<label>Active Number of years for you on your business address</label>
								<input type="text" id="personal_active_no_of_year_business" name="personal[json][personal_active_no_of_year_business]" class="form-control" value="{{ $professional_json->personal_active_no_of_year_business ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
								<label>Landline</label>
								<input type="text" name="personal[json][personal_landline_number]" id="personal_landline_number" class="form-control" value="{{ $professional_json->personal_landline_number ?? '' }}">
	                		</div>
	                		<div class="clearfix"></div>
							<div class="col-md-4 mrgn">
								<div class="col-md-12">
									<label>Working Status</label>
								</div>
								<div class="col-md-6">
									<label><input type="radio" name="personal[json][ps_working_status]" class="statusFC" value="Full Time" {{ $professional_json->ps_working_status == 'Full Time' ? 'checked' : '' }}>Full Time
									</label>
								</div>
								<div class="col-md-6">
									<label>
										<input type="radio" name="personal[json][ps_working_status]" class="statusFC" value="Contract / Part Time" {{ $professional_json->ps_working_status == 'Contract / Part Time' ? 'checked' : ''}}>Contract / Part Time
									</label>
								</div>
							</div>
	                		{{-- <div class="clearfix"></div> --}}
		               		<div class="col-md-4 mrgn contract_part_field" id="contract_working_field">
		               			<label>Year of Contract commitment left</label>
		               			<input type="text" name="personal[json][ps_contract_year_left]" id="ps_contract_year_left" class="form-control" value="{{ $professional_json->ps_contract_year_left ?? '' }}">
		               		</div>
	                		<div class="clearfix"></div>
	                	</div>
	                </div>

	                <div class="private_area mrgn pro_radio CustomTabs professional_type_tab" id="PrivateEmployees" style="display:{{ $order->phoneVerification->professional_type == 'private_employee' ? 'blocked' : 'none' }};">
	                	<div class="row">
	                		<div class="col-md-4">
								<label>Employer's Name</label>
								<input type="text" name="personal[json][employer_name]" id="employer_name" class="form-control" value="{{ $professional_json->employer_name ?? '' }}">
	                		</div>
	                		<div class="col-md-4">
	                			<label>Department</label>
								<input type="text" name="personal[json][personal_dep_name_employe]" id="personal_dep_name_employe" class="form-control" value="{{ $professional_json->personal_dep_name_employe ?? '' }}">
	                		</div>
	                		<div class="col-md-4">
	                			<label>Designation</label>
								<input type="text" name="personal[json][personal_designation_name]" id="personal_designation_name" class="form-control" value="{{ $professional_json->personal_designation_name ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
								<label>Year of Service</label>
								<input type="text" name="personal[json][personal_service_year]" id="personal_service_year" class="form-control" value="{{ $professional_json->personal_service_year ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
								<label>Monthly Salary</label>
								<input type="text" name="personal[json][personal_month_sallery]" id="personal_month_sallery" class="form-control" value="{{ $professional_json->personal_month_sallery ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
								<label>Office Address</label>
								<input type="text" name="personal[json][personal_office_address_one]" id="personal_office_address_one" class="form-control" value="{{ $professional_json->personal_office_address_one ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
								<div class="col-md-12">
									<label>Working Status</label>
								</div>
								<div class="col-md-6">
									<label><input type="radio" name="personal[json][ps_working_status]" class="statusFC" value="Full Time" {{ $professional_json->ps_working_status == 'Full Time' ? 'checked' : '' }}>Full Time
									</label>
								</div>
								<div class="col-md-6">
									<label>
										<input type="radio" name="personal[json][ps_working_status]" class="statusFC" value="Contract / Part Time" {{ $professional_json->ps_working_status == 'Contract / Part Time' ? 'checked' : ''}}>Contract / Part Time
									</label>
								</div>
								<div class="clearfix"></div>
							</div>
	                		<div class="col-md-4 mrgn">
								<label>Phone Number (Landline)</label>
								<input type="text" name="personal[json][personal_phone_landline]" id="personal_phone_landline" class="form-control" value="{{ $professional_json->personal_phone_landline ?? '' }}">
	                		</div>
	                		<div class="clearfix"></div>
		               		<div class="col-md-4 mrgn contract_part_field">
		               			<label>Year of Contaract commitment left</label>
		               			<input type="text" name="personal[json][commited_contarct_year]" id="commited_contarct_year" class="form-control" value="{{ $professional_json->commited_contarct_year ?? '' }}">
		               		</div>
	                		<div class="clearfix"></div>
	                	</div>
	                </div>
	                
	                <div class="bussiness_area mrgn pro_radio CustomTabs professional_type_tab" id="businessShop" style="display:{{ $order->phoneVerification->professional_type == 'business' ? 'blocked' : 'none' }};">
	                	<div class="row">
	                		<div class="col-md-4">
								<label>Bussiness's Name</label>
								<input type="text" name="personal[json][personal_name_of_businees]" id="personal_name_of_businees" class="form-control" value="{{ $professional_json->personal_name_of_businees ?? '' }}">
	                		</div>
	                		<div class="col-md-4">
	                			<label>Bussiness Address</label>
								<input type="text" name="personal[json][personal_address_of_businees]" id="personal_address_of_businees" class="form-control" value="{{ $professional_json->personal_address_of_businees ?? '' }}">
	                		</div>
	                		<div class="col-md-4">
	                			<label>Phone Number (Landline)</label>
								<input type="text" name="personal[json][personal_business_landline]" id="personal_business_landline" class="form-control" value="{{ $professional_json->personal_business_landline ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
	                			<label>Monthly Income Estimated</label>
								<input type="text" name="personal[json][personal_income_monthly]" id="personal_income_monthly" class="form-control" value="{{ $professional_json->personal_income_monthly ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
	                			<label>Active Number of years for you on your business address</label>
								<input type="text" name="personal[json][personal_active_years_business_one]" id="personal_active_years_business_one" class="form-control" value="{{ $professional_json->personal_active_years_business_one ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
	                			<label>Active Number of years for you on your business</label>
								<input type="text" name="personal[json][personal_active_years_business_two]" id="personal_active_years_business_two" class="form-control" value="{{ $professional_json->personal_active_years_business_two ?? '' }}">
	                		</div>               		
							<div class="col-md-4 mrgn">
								<div class="col-md-12">
									<label>Category Of Bussiness</label>
								</div>
								<div class="col-md-4">
									<label>
										<input type="radio" class="has_dependency" name="personal[json][ps_category_of_bussiness]" value="shop" {{ $professional_json->ps_category_of_bussiness == 'shop' ? 'checked' : '' }} data-show="nature_of_business_div" data-hide="">Shop
									</label>
								</div>
								<div class="col-md-4">
									<label>
										<input type="radio" class="has_dependency" name="personal[json][ps_category_of_bussiness]" value="factory" data-show="" data-hide="nature_of_business_div"  {{ $professional_json->ps_category_of_bussiness == 'factory' ? 'checked' : '' }}>Factory
									</label>
								</div>
								<div class="col-md-4 no_padding">
									<label>
										<input type="radio" class="has_dependency" name="personal[json][ps_category_of_bussiness]" value="services_provider" data-show="" data-hide="nature_of_business_div" {{ $professional_json->ps_category_of_bussiness == 'services_provider' ? 'checked' : '' }}>Services Provider
									</label>
								</div>
								<div class="clearfix"></div>
							</div>
	               			<div class="col-md-8 mrgn">
								<div class="col-md-12">
									<label>Type Of Bussiness</label>
								</div>
								<div class="col-md-4">
									<label>
										<input type="radio" name="personal[json][ps_type_of_business]" id="sole_proprietory" class="has_dependency" value="sole_proprietory" {{ $professional_json->ps_type_of_business == 'sole_proprietory' ? 'checked' : '' }} data-show="" data-hide="affiliation_years">Sole Proprietory
									</label>
								</div>
								<div class="col-md-4">
									<label>
										<input type="radio" name="personal[json][ps_type_of_business]" id="partnership" class=" has_dependency" value="partnership" {{ $professional_json->ps_type_of_business == 'partnership' ? 'checked' : '' }} data-show="affiliation_years" data-hide="">Partnership
									</label>
								</div>
								<div class="col-md-4">
									<label>
										<input type="radio" name="personal[json][ps_type_of_business]" id="family_business" class="has_dependency" value="family_business" {{ $professional_json->ps_type_of_business == 'family_business' ? 'checked' : '' }} data-show="" data-hide="affiliation_years">Family Bussiness
									</label>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
	               			<div class="col-md-8 mrgn bussiness_nature_div nature_of_business_div">
								<div class="col-md-12">
									<label>Nature Of Bussiness</label>
								</div>
								<div class="col-md-4">
									<label><input type="radio" name="personal[json][nature_of_bussiness]" id="whole_seller" value="whole_seller" {{ $professional_json->nature_of_bussiness == 'whole_seller' ? 'checked' : '' }}>Whole Seller</label>
								</div>
								<div class="col-md-4">
									<label>
										<input type="radio" name="personal[json][nature_of_bussiness]" id="distributer" value="distributer"  {{ $professional_json->nature_of_bussiness == 'distributer' ? 'checked' : '' }}>Distributor
									</label>
								</div>
								<div class="col-md-4 no_padding">
									<label>
										<input type="radio" name="personal[json][nature_of_bussiness]" id="retailer" value="retailer"{{ $professional_json->nature_of_bussiness == 'retailer' ? 'checked' : '' }}>Retailer
									</label>
								</div>
								<div class="clearfix"></div>
							</div>
	              			<div class="col-md-4 mrgn contract_part_field affiliation_years">
	                			<label>Affiliations Year</label>
								<input type="text" name="personal[json][personal_affliate_year]" id="personal_affliate_year" class="form-control" value="{{ $professional_json->personal_affliate_year ?? '' }}">
	                		</div>
	                		<div class="clearfix"></div>
	                	</div>
	                </div>
	                
	                <div class="agri_area mrgn pro_radio CustomTabs professional_type_tab" id="agriculturalLand" style="display:{{ $order->phoneVerification->professional_type == 'agriculture' ? 'blocked' : 'none' }};">
	                	<div class="row">
	                		<div class="col-md-6">
	                			<label>Address Of Land</label>
	                			<input type="text" name="personal[json][personal_land_address]" id="personal_land_address" class="form-control" value="{{ $professional_json->personal_land_address ?? '' }}">
	                		</div>
	                	</div>
	                </div>
	                
	                <div class="self_area mrgn pro_radio CustomTabs professional_type_tab" id="selfEmp" style="display:{{ $order->phoneVerification->professional_type == 'self_employed' ? 'blocked' : 'none' }};">
	                	<div class="row">
	                		<div class="col-md-4">
								<label>Bussiness's Name</label>
								<input type="text" name="personal[json][personal_name_business]" id="personal_name_business" class="form-control" value="{{ $professional_json->personal_name_business ?? '' }}">
	                		</div>
	                		<div class="col-md-4">
	                			<label>Bussiness Address</label>
								<input type="text" name="personal[json][personal_address_business]" id="personal_address_business" class="form-control" value="{{ $professional_json->personal_address_business ?? '' }}">
	                		</div>
	                		<div class="col-md-4">
	                			<label>Phone Number (Landline)</label>
								<input type="text" name="personal[json][personal_landline_conatct]" id="personal_landline_conatct" class="form-control" value="{{ $professional_json->personal_landline_conatct ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
	                			<label>Monthly Income Estimated</label>
								<input type="text" name="personal[json][personal_estiamte_incom_month]" id="personal_estiamte_incom_month" class="form-control" value="{{ $professional_json->personal_estiamte_incom_month ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
	                			<label>Active Number of years for you on your business address</label>
								<input type="text" name="personal[json][personal_active_year_business]" id="personal_active_year_business" class="form-control" value="{{ $professional_json->personal_active_year_business ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
	                			<label>Active Number of years for you on your business</label>
								<input type="text" name="personal[json][personal_active_year_bus1]" id="personal_active_year_bus1" class="form-control" value="{{ $professional_json->personal_active_year_bus1 ?? '' }}">
	                		</div>               		
							<div class="col-md-4 mrgn ">
								<div class="col-md-12"><label>Category Of Bussiness</label></div>
								<div class="col-md-4">
									<label>
										<input type="radio" class="has_dependency" id="shop" name="personal[json][category_of_bussiness]" value="shop" {{ $professional_json->category_of_bussiness == 'shop' ? 'checked' : '' }} data-show="nature_of_business_div" data-hide="">Shop
									</label>
								</div>
								<div class="col-md-4">
									<label>
										<input type="radio" class="has_dependency" id="factory" name="personal[json][category_of_bussiness]" value="factory" {{ $professional_json->category_of_bussiness == 'factory' ? 'checked' : '' }} data-show="" data-hide="nature_of_business_div">Factory
									</label>
								</div>
								<div class="col-md-4 no_padding">
									<label>
										<input type="radio" class="has_dependency" id="service_provider" name="personal[json][category_of_bussiness]" value="service_provider" {{ $professional_json->category_of_bussiness == 'shop' ? 'checked' : '' }} data-show="" data-hide="nature_of_business_div">
									Services Provider
									</label>
								</div>
								<div class="clearfix"></div>
							</div>
	               			<div class="col-md-8 mrgn">
								<div class="col-md-12">
									<label>Type Of Bussiness</label>
								</div>
								<div class="col-md-4">
									<label><input type="radio" name="personal[json][type_of_business]" class="has_dependency" value="sole_proprietory" {{ $professional_json->type_of_business == 'sole_proprietory' ? 'checked' : '' }} data-show="" data-hide="affiliation_years">Sole Proprietory</label>
								</div>
								<div class="col-md-4">
									<label><input type="radio" name="personal[json][type_of_business]" class="has_dependency" value="partnership"{{ $professional_json->type_of_businesstype_of_business == 'partnership' ? 'checked' : '' }} data-show="affiliation_years" data-hide="">Partnership</label>
								</div>
								<div class="col-md-4">
									<label><input type="radio" name="personal[json][type_of_business]" class="has_dependency" value="family_bussiness" {{ $professional_json->type_of_businesstype_of_businesstype_of_business == 'family_bussiness' ? 'checked' : '' }} data-show="" data-hide="affiliation_years">Family Bussiness</label>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
		               		<div class="col-md-8 mrgn bussiness_nature_div nature_of_business_div">
								<div class="col-md-12"><label>Nature Of Bussiness</label></div>
								<div class="col-md-4">
									<label><input type="radio" name="personal[json][nature_of_bussiness]" value="Whole Seller" <?php if($order_info['nature_of_bussiness'] == 'Whole Seller'){ ?>checked="checked"<?php } ?>>Whole Seller</label>
								</div>
								<div class="col-md-4">
									<label><input type="radio" name="personal[json][nature_of_bussiness]" value="Distributor" <?php if($order_info['nature_of_bussiness'] == 'Distributor'){ ?>checked="checked"<?php } ?>>Distributor</label>
								</div>
								<div class="col-md-4 no_padding">
									<label><input type="radio" name="personal[json][nature_of_bussiness]" value="Retailer" <?php if($order_info['nature_of_bussiness'] == 'Retailer'){ ?>checked="checked"<?php } ?>>Retailer</label>
								</div>
								<div class="clearfix"></div>
							</div>
	          				<div class="col-md-4 mrgn contract_part_field affiliation_years">
	                			<label>Affiliations Year</label>
								<input type="text" name="personal[json][year_affliation]" id="year_affliation" class="form-control" value="{{ $professional_json->year_affliation ?? '' }}">
	                		</div>
	            			<div class="clearfix"></div>
	            		</div>
	            	</div>
	                
	                <div class="student_area mrgn pro_radio CustomTabs professional_type_tab" id="student" style="display:{{ $order->phoneVerification->professional_type == 'student' ? 'blocked' : 'none' }};">
	                	<div class="row">
	                		<div class="col-md-4">
								<label>Collage / Institute Name</label>
								<input type="text" name="personal[json][personal_college_name]" id="personal_college_name" class="form-control" value="{{ $professional_json->personal_college_name ?? '' }}">
	                		</div>
	                		<div class="col-md-4">
	                			<label>Department of Study</label>
								<input type="text" name="personal[json][department_name]" id="department_name" class="form-control" value="{{ $professional_json->department_name ?? '' }}">
	                		</div>
	                		<div class="col-md-4">
	                			<label>Pursuing Degree</label>
								<input type="text" name="personal[json][pursuing_degree]" id="pursuing_degree" class="form-control" value="{{ $professional_json->pursuing_degree ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
								<label>Year of Study Left</label>
								<input type="text" name="personal[json][year_left_study]" id="year_left_study" class="form-control" value="{{ $professional_json->year_left_study ?? '' }}">
	                		</div>
	                		<div class="col-md-4 mrgn">
								<label>Phone Number</label>
								<input type="text" name="personal[json][phone_number]" id="phone_number" class="form-control" value="{{ $professional_json->phone_number ?? '' }}">
	                		</div>
	                		<div class="clearfix"></div>
	                	</div>
	                </div>

	                <div class="mrgn">
	                	<div class="col-md-12">
	                		<div class="col-md-3">Attachments</div>
	                		<div class="col-md-4">
	                			<input type="file" name="personal[json][attachement]" class="form-control">
	                		</div>
	                	</div>
	                </div>
	                <div class="mrgn">
	                	<div class="col-md-12">
	                		<div class="col-md-3">Do you have a bank account?</div>
	                		<div class="col-md-6">
	                			<div class="col-md-2">
	                            	<label>Yes</label>&nbsp;&nbsp;&nbsp;<input type="radio" name="ps_has_bank_account" value="1" {{ $order->phoneVerification->has_bank_account ? 'checked' : ''}}>
	                			</div>
	                			<div class="col-md-2">
	                            	<label>No</label>&nbsp;&nbsp;&nbsp;<input type="radio" name="ps_has_bank_account" value="0" {{ $order->phoneVerification->has_bank_account ? '' : 'checked' }}>
	                            </div>
	                		</div>
	                	</div>
	                </div>
	             	<div class="mrgn">
	                	<div class="col-md-12">
	                		<div class="col-md-3">Can you provide cheque?</div>
	                		<div class="col-md-6">
	                			<div class="col-md-2">
	                				<label>Yes</label>&nbsp;&nbsp;&nbsp;<input type="radio" value="1" name="ps_can_provide_cheque" {{ $order->phoneVerification->can_provide_cheque ? 'checked' : '' }}>
				             	</div>
	                			<div class="col-md-2">
	                				<label>No</label>&nbsp;&nbsp;&nbsp;<input type="radio" value="0" name="ps_can_provide_cheque" {{ $order->phoneVerification->can_provide_cheque ? '' : 'checked' }}>
					             </div>
	                		</div>
	                	</div>
	                </div>
	                <div class="mrgn">
	                	<div class="col-md-12">
	                		<div class="col-md-3">Commitment to live at the same residency?</div>
	                		<div class="col-md-4">
	                			<input type="text" name="ps_commitment_to_residency" id="ps_commitment_to_residency" class="form-control" value="{{ $order->commitment_to_residency }}">
	                		</div>
	                	</div>
	                </div>
	                <div class="clearfix"></div>
                </div>
               	<div class="seperator_div"><hr></div>
                <div class="hold2 mrgn">
                	@forelse($order->guarantors as $guarantor)
                		<div class="guarantor_div">
	            		 	<div class="row">
		            		 	<div class="col-md-12">
				                	<div class="col-md-6">
				                		<div>
				                			<h3>Guarantor Information</h3>
			                			</div>
				                	</div>
				                	<div class="col-md-6 text-right">
				                		<button type="button" class="btn btn-danger remove_guarantor">Remove Guarantor</button>
				                	</div>
		                			<div class="clearfix"></div>
		                		</div>
		                	</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-4">
										<label>Name</label>
										<input type="text" name="guarantor[name][{{ $loop->index }}]" class="form-control" value="{{ $guarantor->name }}">
									</div>
									<div class="col-md-4">
										<label>Father's / Husband's Name</label>
										<input type="text" name="guarantor[guardian_name][{{ $loop->index }}]" class="form-control" value="{{ $guarantor->guardian_name }}">
									</div>
									<div class="col-md-4">
										<label>CNIC #</label>
										<input type="text" id="cnic_no" name="guarantor[cnic_no][{{ $loop->index }}]" value="{{ $guarantor->cnic_no }}" class="form-control">
									</div>
									<div class="col-md-4 mrgn">
										<label>Relationship with Applicant</label>
										<input type="text" name="guarantor[relation_with_applicant][{{ $loop->index }}]" class="form-control" value="{{ $guarantor->relation_with_applicant }}">
									</div>
									
									<div class="col-md-4 mrgn">
										<label>Phone Number (Landline)</label>
										<input type="text" name="guarantor[landline_phone][{{ $loop->index }}]" class="form-control" value="{{ $guarantor->landline_phone }}">
									</div>
									<div class="col-md-4 mrgn">
										<label>Mobile Number</label>
										<input type="text" name="guarantor[mobile_phone][{{ $loop->index }}]" class="form-control" value="{{ $guarantor->mobile_phone }}">
									</div>
									
									<div class="col-md-4 mrgn">
										<label>Current Address as on CNIC</label>
										<input type="text" name="guarantor[current_address][{{ $loop->index }}]" value="{{ $guarantor->current_address }}" class="form-control">
									</div>
									<div class="col-md-4 mrgn">
										<label>Permanent Address as on CNIC</label>
										<input type="text" name="guarantor[permanent_address][{{ $loop->index }}]" class="form-control" value="{{ $guarantor->permanent_address }}">
									</div>
									<div class="col-md-4 mrgn">
										<label>Attachments</label>
										<input type="file" name="guarantor[attachement][{{ $loop->index }}]" class="form-control">
									</div>
								</div>
							</div>
					        <div class="sub_heading">
			                	<h3>Professional Details</h3>
			                </div>
							<div class="pro_radio">
								<div class="col-md-4">
									<label>
										<input class="guarantor_profession" data-show="government_employee_tab" type="radio" value="government_employee" data-id="governmentEmp2" name="guarantor[professional_type][{{ $loop->index }}]" {{ $guarantor->professional_type == 'government_employee' ? 'checked' : '' }}>Government Employee
									</label>
								</div>	
								<div class="col-md-4">
									<label>
										<input class="guarantor_profession" data-show="private_employee_tab" type="radio" value="private_employee" data-id="PrivateEmployees2"  name="guarantor[professional_type][{{ $loop->index }}]" {{ $guarantor->professional_type == 'private_employee' ? 'checked' : '' }}>Private Employee
									</label>
								</div>
								<div class="col-md-4">
									<label>
										<input type="radio" class="guarantor_profession" data-show="business_tab" name="guarantor[professional_type][{{ $loop->index }}]" data-id="businessShop2" value="business" {{ $guarantor->professional_type == 'business' ? 'checked' : '' }}>Business – Shop Owner / Service Provider
									</label>
					                <div class="clearfix"></div>
								</div>
								<div class="col-md-4">
									<label>
										<input type="radio" data-show="agriculture_tab" class="guarantor_profession" name="guarantor[professional_type][{{ $loop->index }}]" data-id="agriculturalLand2" value="agriculture" {{ $guarantor->professional_type == 'agriculture' ? 'checked' : '' }}>Agricultural Land Owner
									</label>
								</div>		
								<div class="col-md-4">
									<label>
										<input type="radio" data-show="selff_employed_tab" class="guarantor_profession" name="guarantor[professional_type][{{ $loop->index }}]" data-id="selfEmp2" value="selff_employed" {{ $guarantor->professional_type == 'selff_employed' ? 'checked' : '' }}>Self Employed – Freelancer/Service
									</label>
								</div>		
								<div class="col-md-4">
									<label>
										<input type="radio" data-show="student_tab" class="guarantor_profession" name="guarantor[professional_type][{{ $loop->index }}]" data-id="student2" value="student" {{ $guarantor->professional_type == 'student' ? 'checked' : '' }}>Student
									</label>
								</div>		
								<div class="clearfix"></div>
							</div>                    
	                		
							@php
								$professional_json = json_decode($guarantor->profession_json);
							@endphp

			                <div class="organization_area pro_radio mrgn government_employee_tab" style="display:  {{ $guarantor->professional_type == 'government_employee' ? 'block' : 'none' }};">
				            	<div class="row">
				            		<div class="col-md-4">
										<label>Organization Name</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][professional_organization_name]" class="form-control" value="{{ $professional_json->professional_organization_name ?? '' }}">
				            		</div>
				            		<div class="col-md-4">
				            			<label>Department</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][professional_department_name]" class="form-control" value="{{ $professional_json->professional_department_name ?? '' }}">
				            		</div>
				            		<div class="col-md-4">
				            			<label>Scale / Grade</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][professional_scale_grade]" class="form-control" value="{{ $professional_json->professional_scale_grade ?? '' }}">
				            		</div>
				            		<div class="col-md-4 mrgn">
										<label>Year of Service</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][professional_servic_year]" class="form-control" value="{{ $professional_json->professional_servic_year ?? '' }}">
				            		</div>
				            		<div class="col-md-4 mrgn">
										<label>Monthly Salary</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][professional_month_sallery]" class="form-control" value="{{ $professional_json->professional_month_sallery ?? '' }}">
				            		</div>
				            		<div class="col-md-4 mrgn">
										<label>Office Address</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][professional_office_address]" class="form-control" value="{{ $professional_json->professional_office_address ?? '' }}">
				            		</div>
				            		<div class="col-md-4 mrgn">
										<label>Active Number of years for you on your business address</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][professional_active_year_business]" class="form-control" value="{{ $professional_json->professional_active_year_business ?? '' }}">
				            		</div>
				            		<div class="col-md-4 mrgn">
										<label>Landline</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][professional_landline]" class="form-control" value="{{ $professional_json->professional_landline ?? '' }}">
				            		</div>
									<div class="col-md-4 mrgn">
										<div class="col-md-12">
											<label>Working Status</label>
										</div>
										<div class="col-md-6">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][working_status]" class="has_dependency" value="Full Time" {{ $professional_json->working_status == 'Full Time' ? 'checked' : '' }} data-show="years_of_contract" data-hide="">Full Time
											</label>
										</div>
										<div class="col-md-6">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][working_status]" class="has_dependency" value="Contract / Part Time" {{ $professional_json->working_status == 'Contract / Part Time' ? 'checked' : '' }} data-show="" data-hide="years_of_contract"> Contract / Part Time
											</label>
										</div>
										<div class="clearfix"></div>
									</div>
				               		<div class="col-md-4 mrgn contract_part_field years_of_contract" style="display: none;">
				               			<label>Year of Contaract commitment left</label>
				               			<input type="text" name="guarantor[json][{{ $loop->index }}][contarct_commitment_prof]" class="form-control" value="{{ $professional_json->contarct_commitment_prof }}">
				               		</div>
			                		<div class="clearfix"></div>
			                	</div>
			                </div>
			                
			                <div class="private_area mrgn pro_radio private_employee_tab"  style="display:  {{ $guarantor->professional_type == 'private_employee' ? 'block' : 'none' }};">
			                	<div class="row">
			                		<div class="col-md-4">
										<label>Employer's Name</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][personal_employer_name]" class="form-control" value="{{ $professional_json->personal_employer_name ?? '' }}">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Department</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][personal_department_name]" class="form-control" value="{{ $professional_json->personal_department_name ?? '' }}">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Designation</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][personal_designation]" class="form-control" value="{{ $professional_json->personal_designation ?? '' }}">
			                		</div>
			                		<div class="col-md-4 mrgn">
										<label>Year of Service</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][personal_services_year]" class="form-control" value="{{ $professional_json->personal_services_year ?? '' }}">
			                		</div>
			                		<div class="col-md-4 mrgn">
										<label>Monthly Salary</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][month_sallery]" class="form-control" value="{{ $professional_json->month_sallery ?? '' }}">
			                		</div>
			                		<div class="col-md-4 mrgn">
										<label>Office Address</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][office_address_job]" class="form-control" value="{{ $professional_json->office_address_job ?? '' }}">
			                		</div>
			                		<div class="col-md-4 mrgn">
										<label>Phone Number (Landline)</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][office_phone_no]" class="form-control" value="{{ $professional_json->office_phone_no ?? '' }}">
			                		</div>
									<div class="col-md-4 mrgn">
										<div class="col-md-12">
											<label>Working Status</label>
										</div>
										<div class="col-md-6">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][working_status]" class="has_dependency" value="Full Time" {{ $professional_json->working_status == 'Full Time' ? 'checked' : '' }} data-show="years_of_contract" data-hide="">Full Time
											</label>
										</div>
										<div class="col-md-6">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][working_status]" class="has_dependency" value="Contract / Part Time" {{ $professional_json->working_status == 'Contract / Part Time' ? 'checked' : '' }} data-show="" data-hide="years_of_contract"> Contract / Part Time
											</label>
										</div>
										<div class="clearfix"></div>
									</div>
				               		<div class="col-md-4 mrgn contract_part_field years_of_contract" style="display: none;">
				               			<label>Year of Contaract commitment left</label>
				               			<input type="text" name="guarantor[json][{{ $loop->index }}][contarct_commitment_prof]" class="form-control" value="{{ $professional_json->contarct_commitment_prof }}">
				               		</div>
			                		<div class="clearfix"></div>
			                	</div>
			                </div>
			                
			                <div class="bussiness_area pro_radio mrgn business_tab "  style="display:  {{ $guarantor->professional_type == 'business' ? 'block' : 'none' }};">
			                	<div class="row">
			                		<div class="col-md-4">
										<label>Bussiness's Name</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][businees_name]" class="form-control" value="{{ $professional_json->businees_name ?? '' }}">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Bussiness Address</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][businees_address]" class="form-control" value="{{ $professional_json->businees_address ?? '' }}">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Phone Number (Landline)</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][business_phone_no]" class="form-control" value="{{ $professional_json->business_phone_no ?? '' }}">
			                		</div>
			                		<div class="col-md-4 mrgn">
			                			<label>Monthly Income Estimated</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][estimate_income_month]" class="form-control" value="{{ $professional_json->estimate_income_month ?? '' }}">
			                		</div>
			                		<div class="col-md-4 mrgn">
			                			<label>Active Number of years for you on your business address</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][business_duration]" class="form-control" value="{{ $professional_json->business_duration ?? '' }}">
			                		</div>
			                		<div class="col-md-4 mrgn">
			                			<label>Active Number of years for you on your business</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][business_duration]" class="form-control" value="{{ $professional_json->business_duration ?? '' }}">
			                		</div>               		
									<div class="col-md-4 mrgn">
										<div class="col-md-12"><label>Category Of Bussiness</label></div>
										<div class="col-md-4">
											<label>
												<input type="radio"  name="guarantor[json][{{ $loop->index }}][category_of_business]" class="has_dependency" value="shop" {{ $professional_json->category_of_business == 'shop' ? 'checked' : '' }} data-show="nature_of_business_div" data-hide="">Shop
							             	</label>
										</div>
										<div class="col-md-4">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][category_of_business]" class="has_dependency" value="factory" {{ $professional_json->category_of_business == 'factory' ? 'checked' : '' }} data-show="" data-hide="nature_of_business_div">Factory
							             	</label>
										</div>
										<div class="col-md-4 no_padding">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][category_of_business]" class="has_dependency" value="services_provider" {{ $professional_json->category_of_business == 'services_provider' ? 'checked' : '' }} data-show="" data-hide="nature_of_business_div">Services Provider
							             	</label>
										</div>
										<div class="clearfix"></div>
									</div>
			               			<div class="col-md-8 mrgn">
										<div class="col-md-12">
											<label>Type Of Bussiness</label>
										</div>
										<div class="col-md-3">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][type_of_business]" value="sole_proprietory" class="has_dependency" {{ $professional_json->type_of_business == 'sole_proprietory' ? 'checked' : '' }} data-show="" data-hide="affliation_years">Sole Proprietory
											</label>
										</div>
										<div class="col-md-3">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][type_of_business]" value="partnership" class="has_dependency" {{ $professional_json->type_of_business == 'partnership' ? 'checked' : '' }} data-show="affliation_years" data-hide="">Partnership
											</label>
										</div>
										<div class="col-md-3">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][type_of_business]" value="family_business" class="has_dependency" {{ $professional_json->type_of_business == 'family_business' ? 'checked' : '' }} data-show="" data-hide="affliation_years">Family Bussiness
											</label>
										</div>
										<div class="col-md-3 no_padding">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][type_of_business]" value="other"  {{ $professional_json->type_of_business == 'other' ? 'checked' : '' }}>Others
											</label>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="clearfix"></div>
			               			<div class="col-md-6 mrgn nature_of_business_div" style="display: none;">
										<div class="col-md-12">
											<label>Nature Of Bussiness</label>
										</div>
										<div class="col-md-4">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][nature_of_business]" value="whole_seller" {{ $professional_json->type_of_business == 'whole_seller' ? 'checked' : '' }}>Whole Seller
											</label>
										</div>
										<div class="col-md-4">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][nature_of_business]" value="distributor" {{ $professional_json->type_of_business == 'distributor' ? 'checked' : '' }}>Distributor
											</label>
										</div>
										<div class="col-md-4 no_padding">
											<label><input type="radio" name="guarantor[json][{{ $loop->index }}][retailer]" value="retailer" {{ $professional_json->type_of_business == 'retailer' ? 'checked' : '' }}>Retailer</label>
										</div>
										<div class="clearfix"></div>
									</div>
				              		<div class="col-md-4 mrgn affliation_years" style="display: none;">
				                			<label>Affiliations Years</label>
											<input type="text" name="guarantor[json][{{ $loop->index }}][affiliations_years]" class="form-control" value="{{ $professional_json->affiliations_years }}">
				            		</div>
				            		<div class="clearfix"></div>
			                	</div>
			                </div>
			                
			                <div class="agri_area mrgn pro_radio agriculture_tab" style="display:  {{ $guarantor->professional_type == 'agriculture' ? 'block' : 'none' }};">
			                	<div class="row">
			                		<div class="col-md-6">
			                			<label>Address Of Land</label>
			                			<input type="text" name="guarantor[json][{{ $loop->index }}][land_address]"  class="form-control" value="{{ $professional_json->land_address ?? '' }}">
			                		</div> 
			                		<div class="col-md-6">
										<label>Attachments</label>
										<input type="file" name="guarantor[json][{{ $loop->index }}][attachment]" class="form-control">
									</div>
			                	</div>
			                </div>
			                
			                <div class="self_area mrgn pro_radio selff_employed_tab "  style="display:  {{ $guarantor->professional_type == 'selff_employed' ? 'block' : 'none' }};">
			                	<div class="row">
			                		<div class="col-md-4">
										<label>Bussiness's Name</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][business_name]" class="form-control" value="{{ $professional_json->business_name ?? '' }}">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Bussiness Address</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][business_address]" class="form-control" value="{{ $professional_json->business_address ?? '' }}">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Phone Number (Landline)</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][landline_phone_no]" class="form-control" value="{{ $professional_json->landline_phone_no ?? '' }}">
			                		</div>
			                		<div class="col-md-4 mrgn">
			                			<label>Monthly Income Estimated</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][estimate_net_incom]" class="form-control" value="{{ $professional_json->estimate_net_incom ?? '' }}">
			                		</div>
			                		<div class="col-md-4 mrgn">
			                			<label>Active Number of years for you on your business address</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][business_year_one]" class="form-control" value="{{ $professional_json->business_year_one ?? '' }}">
			                		</div>
			                		<div class="col-md-4 mrgn">
			                			<label>Active Number of years for you on your business</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][business_year_two]" class="form-control" value="{{ $professional_json->business_year_two ?? '' }}">
			                		</div>               		
									<div class="col-md-4 mrgn">
										<div class="col-md-12"><label>Category Of Bussiness</label></div>
										<div class="col-md-4">
											<label>
												<input type="radio"  name="guarantor[json][{{ $loop->index }}][category_of_business]" class="has_dependency" value="shop" {{ $professional_json->category_of_business == 'shop' ? 'checked' : '' }} data-show="nature_of_business_div" data-hide="">Shop
							             	</label>
										</div>
										<div class="col-md-4">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][category_of_business]" class="has_dependency" value="factory" {{ $professional_json->category_of_business == 'factory' ? 'checked' : '' }} data-show="nature_of_business_div" data-hide="">Factory
							             	</label>
										</div>
										<div class="col-md-4 no_padding">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][category_of_business]" class="has_dependency" value="services_provider" {{ $professional_json->category_of_business == 'services_provider' ? 'checked' : '' }} data-show="nature_of_business_div" data-hide="">Services Provider
							             	</label>
										</div>
										<div class="clearfix"></div>
									</div>             		
			               			<div class="col-md-8 mrgn">
										<div class="col-md-12">
											<label>Type Of Bussiness</label>
										</div>
										<div class="col-md-3">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][type_of_business]" value="sole_proprietory" class="has_dependency" {{ $professional_json->type_of_business == 'sole_proprietory' ? 'checked' : '' }} data-show="" data-hide="affiliation_years">Sole Proprietory</label>
										</div>
										<div class="col-md-3">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][type_of_business]" value="partnership" class="has_dependency" {{ $professional_json->type_of_business == 'partnership' ? 'checked' : '' }} data-show="affiliation_years" data-hide="">Partnership
											</label>
										</div>
										<div class="col-md-3">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][type_of_business]" value="family_business" class="has_dependency" {{ $professional_json->type_of_business == 'family_business' ? 'checked' : '' }} data-show="" data-hide="affiliation_years">Family Bussiness
											</label>
										</div>
										<div class="col-md-3 no_padding">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][type_of_business]" value="other" class="has_dependency" {{ $professional_json->type_of_business == 'other' ? 'checked' : '' }} data-show="" data-hide="affiliation_years">Others
											</label>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="col-md-4 mrgn nature_of_business_div" style="display: none;">
										<div class="col-md-12"><label>Nature Of Business</label></div>
										<div class="col-md-4">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][nature_of_business]" value="whole_seller" {{ $professional_json->type_of_business == 'whole_seller' ? 'checked' : '' }}>Whole Seller
											</label>
										</div>
										<div class="col-md-4">
											<label>
												<input type="radio" name="guarantor[json][{{ $loop->index }}][nature_of_business]" value="distributor" {{ $professional_json->type_of_business == 'distributor' ? 'checked' : '' }}>Distributor
											</label>
										</div>
										<div class="col-md-4 no_padding">
											<label><input type="radio" name="guarantor[json][{{ $loop->index }}][retailer]" value="retailer" {{ $professional_json->type_of_business == 'retailer' ? 'checked' : '' }}>Retailer</label>
										</div>
										<div class="clearfix"></div>
									</div>
				              		<div class="col-md-4 mrgn affiliation_years" style="display: none;">
				                			<label>Affiliations Years</label>
											<input type="text" name="guarantor[json][{{ $loop->index }}][affiliations_years]" class="form-control" value="{{ $professional_json->affiliations_years }}">
				            		</div>
			            			<div class="clearfix"></div>
			                	</div>
			                </div>
		                
			                <div class="student_area pro_radio mrgn student_tab"  style="display:  {{ $guarantor->professional_type == 'student' ? 'block' : 'none' }};">
			                	<div class="row">
			                		<div class="col-md-4">
										<label>Collage / Institute Name</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][institute_name]" class="form-control" value="{{ $professional_json->institute_name ?? '' }}">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Department of Study</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][institute_department]" class="form-control" value="{{ $professional_json->institute_department ?? '' }}">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Pursuing Degree</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][institute_degree]" class="form-control" value="{{ $professional_json->institute_degree ?? '' }}">
			                		</div> 
			                		<div class="col-md-4 mrgn">
										<label>Year of Study Left</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][institute_year_left]" class="form-control" value="{{ $professional_json->institute_year_left ?? '' }}">
			                		</div>
			                		<div class="col-md-4 mrgn">
										<label>Phone Number</label>
										<input type="text" name="guarantor[json][{{ $loop->index }}][institute_phone]" class="form-control" value="{{ $professional_json->institute_phone ?? '' }}">
			                		</div>
			                		<div class="col-md-4 mrgn">
										<label>Attachments</label>
										<input type="file" name="guarantor[json][{{ $loop->index }}][attachement]" class="form-control">
									</div>
			                		<div class="clearfix"></div>
			                	</div>
		                	</div>

			                <div class="bank_info mrgn pro_radio">
			                	<div class="row mrgn">
			                		<div class="col-md-4">
				                		<label>Attachments</label>
				                	</div>
				                	<div class="col-md-6">
				                		<input type="file" class="form-control" name="guarantor[json][{{ $loop->index }}]attachment_paths">
				                	</div>
			                	</div>
			                	<div class="row mrgn">
			                		<div class="col-md-4">
				                		<label>Do you have an Active Bank Account Detail</label>
				                	</div>
				                	<div class="col-md-2">
				                		<div class="col-md-6">
				                			<label>
				                				<input type="radio" value="1" name="has_bank_account[{{ $loop->index }}]" {{ $guarantor->has_bank_account ? 'checked' : '' }}>Yes
								             </label>
							             </div>
				                		<div class="col-md-6">
				                			<label>
				                				<input type="radio" value="0" name="has_bank_account[{{ $loop->index }}]" {{ $guarantor->has_bank_account ? 'checked' : '' }}>No
				                			</label>
				                		</div>
				                	</div>
			                	</div>
			                	<div class="row mrgn">
			                		<div class="col-md-4">
				                		<label>Can you provide a cheque against balance of payment</label>
				                	</div>
				                	<div class="col-md-2">
				                		<div class="col-md-6">
				                			<label>
				                				<input type="radio" value="1" name="can_provide_cheque[{{ $loop->index }}]"  {{ $guarantor->can_provide_cheque ? 'checked' : '' }}>Yes
				                			</label>	
						             	</div>
				                		<div class="col-md-6">
				                			<label>
				                				<input type="radio" value="0" name="can_provide_cheque[{{ $loop->index }}]" {{ $guarantor->can_provide_cheque ? 'checked' : '' }}>No
				                			</label>
						             	</div>
				                	</div>
			                	</div>
			                	<div class="row mrgn">
			                		<div class="col-md-4">
				                		<label>Commitment to live at the same residency</label>
				                	</div>
				                	<div class="col-md-6">
				                		<input type="text" class="form-control" name="commitment_to_residency[]" value="{{ $guarantor->commitment_to_residency }}">
				                	</div>
			                	</div>
			            	</div>
	        			</div>
                	@empty
	                	<div class="guarantor_div">
	            		 	<div class="row">
		            		 	<div class="col-md-12">
				                	<div class="col-md-6">
				                		<div class="sub_heading">
				                			<h3>Guarantor Information</h3>
			                			</div>
				                	</div>
				                	<div class="col-md-6 text-right" style="display: none;">
				                		<button type="button" class="btn btn-danger remove_guarantor">Remove Guarantor</button>
				                	</div>
		                			<div class="clearfix"></div>
		                		</div>
		                	</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-4">
										<label>Name</label>
										<input type="text" name="guarantor[name][0]" class="form-control" value="">
									</div>
									<div class="col-md-4">
										<label>Father's / Husband's Name</label>
										<input type="text" name="guarantor[guardian_name][0]" class="form-control" value="">
									</div>
									<div class="col-md-4">
										<label>CNIC #</label>
										<input type="text" name="guarantor[cnic_no][0]" value="" class="form-control">
									</div>
									<div class="col-md-4 mrgn">
										<label>Relationship with Applicant</label>
										<input type="text" name="guarantor[relation_with_applicant][0]" class="form-control" value="">
									</div>
									
									<div class="col-md-4 mrgn">
										<label>Phone Number (Landline)</label>
										<input type="text" name="guarantor[landline_phone][0]" class="form-control" value="">
									</div>
									<div class="col-md-4 mrgn">
										<label>Mobile Number</label>
										<input type="text" name="guarantor[mobile_phone][0]" class="form-control" value="">
									</div>
									
									<div class="col-md-4 mrgn">
										<label>Current Address as on CNIC</label>
										<input type="text" name="guarantor[current_address][0]" value="" class="form-control">
									</div>
									<div class="col-md-4 mrgn">
										<label>Permanent Address as on CNIC</label>
										<input type="text" name="guarantor[permanent_address][0]" class="form-control" value="">
									</div>
								</div>
							</div>
					        <div class="sub_heading">
			                	<h3>Professional Details</h3>
			                </div>
							<div class="pro_radio">
								<div class="col-md-4">
									<label>
										<input class="guarantor_profession" type="radio" value="government_employee" data-show="government_employee_tab" name="guarantor[professional_type][0]">Government Employee
									</label>
								</div>	
								<div class="col-md-4">
									<label>
										<input class="guarantor_profession" type="radio" value="private_employee" data-show="private_employee_tab"  name="guarantor[professional_type][0]">Private Employee
									</label>
								</div>
								<div class="col-md-4">
									<label>
										<input type="radio" class="guarantor_profession" name="guarantor[professional_type][0]" data-show="business_tab" value="business">Business – Shop Owner / Service Provider
									</label>
					                <div class="clearfix"></div>
								</div>
								<div class="col-md-4">
									<label>
										<input type="radio" class="guarantor_profession" name="guarantor[professional_type][0]" data-show="agriculture_tab" value="agriculture">Agricultural Land Owner
									</label>
								</div>		
								<div class="col-md-4">
									<label>
										<input type="radio" class="guarantor_profession" name="guarantor[professional_type][0]" data-show="selff_employed_tab" value="selff_employed">Self Employed – Freelancer/Service
									</label>
								</div>		
								<div class="col-md-4">
									<label>
										<input type="radio" class="guarantor_profession" name="guarantor[professional_type][0]" data-show="student_tab" value="student">Student
									</label>
								</div>		
								<div class="clearfix"></div>
							</div>                     
	                
			                <div class="organization_area mrgn pro_radio government_employee_tab professional_type_tab" style="display: none;">
				            	<div class="row">
				            		<div class="col-md-4">
										<label>Organization Name</label>
										<input type="text" name="guarantor[json][0][professional_organization_name]" class="form-control" value="">
				            		</div>
				            		<div class="col-md-4">
				            			<label>Department</label>
										<input type="text" name="guarantor[json][0][department_name]" id="professional_department_name" class="form-control" value="">
				            		</div>
				            		<div class="col-md-4">
				            			<label>Scale / Grade</label>
										<input type="text" name="guarantor[json][0][professional_scale_grade]" class="form-control" value="">
				            		</div>
				            		<div class="col-md-4 mrgn">
										<label>Year of Service</label>
										<input type="text" name="guarantor[json][0][professional_servic_year]" class="form-control" value="">
				            		</div>
				            		<div class="col-md-4 mrgn">
										<label>Monthly Salary</label>
										<input type="text" name="guarantor[json][0][professional_month_sallery]" class="form-control" value="">
				            		</div>
				            		<div class="col-md-4 mrgn">
										<label>Office Address</label>
										<input type="text" name="guarantor[json][0][professional_office_address]" class="form-control" value="">
				            		</div>
				            		<div class="col-md-4 mrgn">
										<label>Active Number of years for you on your business address</label>
										<input type="text" name="guarantor[json][0][professional_active_year_business]" class="form-control" value="">
				            		</div>
				            		<div class="col-md-4 mrgn">
										<label>Landline</label>
										<input type="text" name="guarantor[json][0][professional_landline]" class="form-control" value="">
				            		</div>
									<div class="col-md-4 mrgn">
										<div class="col-md-12">
											<label>Working Status</label>
										</div>
										<div class="col-md-6">
											<label>
												<input type="radio" name="guarantor[json][0][working_status]" class="has_dependency" data-show="" data-hide="years_of_contract" value="Full Time">Full Time
											</label>
										</div>
										<div class="col-md-6">
											<label>
												<input type="radio" name="guarantor[json][0][working_status]" class="has_dependency" data-show="years_of_contract" data-hide="" value="Contract / Part Time">Contract / Part Time
											</label>
										</div>
										<div class="clearfix"></div>
									</div>
				               		<div class="col-md-4 mrgn contract_part_field years_of_contract" style="display: none;">
				               			<label>Year of Contaract commitment left</label>
				               			<input type="text" name="guarantor[json][0][contarct_commitment_prof]" class="form-control" value="">
				               		</div>
			                		<div class="clearfix"></div>
			                	</div>
			                </div>
			                
			                <div class="private_area mrgn pro_radio private_employee_tab professional_type_tab" style="display: none;">
			                	<div class="row">
			                		<div class="col-md-4">
										<label>Employer's Name</label>
										<input type="text" name="guarantor[json][0][personal_employer_name]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Department</label>
										<input type="text" name="guarantor[json][0][personal_department_name]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Designation</label>
										<input type="text" name="guarantor[json][0][personal_designation]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4 mrgn">
										<label>Year of Service</label>
										<input type="text" name="guarantor[json][0][personal_services_year]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4 mrgn">
										<label>Monthly Salary</label>
										<input type="text" name="guarantor[json][0][month_sallery]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4 mrgn">
										<label>Office Address</label>
										<input type="text" name="guarantor[json][0][office_address_job]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4 mrgn">
										<label>Phone Number (Landline)</label>
										<input type="text" name="guarantor[json][0][office_phone_no]" class="form-control" value="">
			                		</div>
									<div class="col-md-4 mrgn">
										<div class="col-md-12">
											<label>Working Status</label>
										</div>
										<div class="col-md-6">
											<label>
												<input type="radio" name="guarantor[json][0][working_status]" class="has_dependency" data-show="" data-hide="years_of_contract" value="Full Time">Full Time
											</label>
										</div>
										<div class="col-md-6">
											<label>
												<input type="radio" name="guarantor[json][0][working_status]" class="has_dependency" data-show="years_of_contract" data-hide="" value="Contract / Part Time">Contract / Part Time
											</label>
										</div>
										<div class="clearfix"></div>
									</div>
				               		<div class="col-md-4 mrgn contract_part_field years_of_contract" style="display: none;">
				               			<label>Year of Contaract commitment left</label>
				               			<input type="text" name="guarantor[json][0][contarct_commitment_prof]" class="form-control" value="">
				               		</div>
			                		<div class="clearfix"></div>
			                	</div>
			                </div>
			                
			                <div class="bussiness_area mrgn pro_radio business_tab professional_type_tab" style="display: none;">
			                	<div class="row">
			                		<div class="col-md-4">
										<label>Bussiness's Name</label>
										<input type="text" name="guarantor[json][0][businees_name]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Bussiness Address</label>
										<input type="text" name="guarantor[json][0][businees_address]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Phone Number (Landline)</label>
										<input type="text" name="guarantor[json][0][business_phone_no]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4 mrgn">
			                			<label>Monthly Income Estimated</label>
										<input type="text" name="guarantor[json][0][estimate_income_month]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4 mrgn">
			                			<label>Active Number of years for you on your business address</label>
										<input type="text" name="guarantor[json][0][business_duration]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4 mrgn">
			                			<label>Active Number of years for you on your business</label>
										<input type="text" name="guarantor[json][0][business_duration]" class="form-control" value="">
			                		</div>               		
									<div class="col-md-4 mrgn">
										<div class="col-md-12"><label>Category Of Bussiness</label></div>
										<div class="col-md-4">
											<label><input type="radio" class="has_dependency"  name="guarantor[json][0][category_of_business]" value="Shop" data-show="nature_of_business_div" data-hide="">
												Shop
											</label>
										</div>
										<div class="col-md-4">
											<label><input type="radio" class="has_dependency" name="guarantor[json][0][category_of_business]" value="Factory" data-show="" data-hide="nature_of_business_div">Factory</label>
										</div>
										<div class="col-md-4 no_padding">
											<label><input type="radio"  class="has_dependency" name="guarantor[json][0][category_of_business]" value="Services Provider" data-show="" data-hide="nature_of_business_div">Services Provider</label>
										</div>
										<div class="clearfix"></div>
									</div>
			               			<div class="col-md-8 mrgn">
										<div class="col-md-12">
											<label>Type Of Bussiness</label>
										</div>
										<div class="col-md-3">
											<label>
												<input type="radio" class="has_dependency" name="guarantor[json][0][type_of_business]" value="solepro_prietory" data-show="" data-hide="affliation_years">Sole Proprietory</label>
										</div>
										<div class="col-md-3">
											<label>
												<input type="radio" class="has_dependency" name="guarantor[json][0][type_of_business]" value="partnership" data-show="affliation_years" data-hide="">Partnership
											</label>
										</div>
										<div class="col-md-3">
											<label>
												<input type="radio" class="has_dependency" name="guarantor[json][0][type_of_business]" value="family_business" data-show="" data-hide="affliation_years">Family Bussiness
											</label>
										</div>
										<div class="col-md-3 no_padding">
											<label>
												<input type="radio" name="guarantor[json][0][type_of_business]" value="other">Others
											</label>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-4 mrgn nature_of_business_div" style="display: none;">
										<div class="col-md-12">
											<label>Nature Of Bussiness</label>
										</div>
										<div class="col-md-4">
											<label><input type="radio" name="guarantor[json][0][nature_of_business]" value="whole_seller">Whole Seller</label>
										</div>
										<div class="col-md-4">
											<label>
												<input type="radio" name="guarantor[json][0][nature_of_business]" value="distributor">Distributor
											</label>
										</div>
										<div class="col-md-4 no_padding">
											<label><input type="radio" name="guarantor[json][0][retailer]" value="retailer">Retailer</label>
										</div>
										<div class="clearfix"></div>
									</div>
				              		<div class="col-md-4 mrgn affliation_years" style="display: none;">
				                			<label>Affiliations Year</label>
											<input type="text" name="guarantor[json][0][affiliations_year]" id="affiliations_year" class="form-control" value="">
				            		</div>
				            		<div class="clearfix"></div>
			                	</div>
			                </div>
			                
			                <div class="agri_area mrgn pro_radio agriculture_tab professional_type_tab" style="display: none;">
			                	<div class="row">
			                		<div class="col-md-6">
			                			<label>Address Of Land</label>
			                			<input type="text" name="guarantor[json][0][land_address]" class="form-control" value="">
			                		</div>
			                	</div>
			                </div>
			                
			                <div class="self_area mrgn pro_radio selff_employed_tab professional_type_tab" style="display: none;">
			                	<div class="row">
			                		<div class="col-md-4">
										<label>Bussiness's Name</label>
										<input type="text" name="guarantor[json][0][business_name]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Bussiness Address</label>
										<input type="text" name="guarantor[json][0][business_address]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Phone Number (Landline)</label>
										<input type="text" name="guarantor[json][0][landline_phone_no]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4 mrgn">
			                			<label>Monthly Income Estimated</label>
										<input type="text" name="guarantor[json][0][estimate_net_incom]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4 mrgn">
			                			<label>Active Number of years for you on your business address</label>
										<input type="text" name="guarantor[json][business_year][0]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4 mrgn">
			                			<label>Active Number of years for you on your business</label>
										<input type="text" name="guarantor[json][0][business_year]" class="form-control" value="">
			                		</div>               		
									<div class="col-md-4 mrgn">
										<div class="col-md-12"><label>Category Of Bussiness</label></div>
										<div class="col-md-4">
											<label><input type="radio" class="has_dependency"  name="guarantor[json][0][category_of_business]" value="Shop" data-show="nature_of_business_div" data-hide="">Shop</label>
										</div>
										<div class="col-md-4">
											<label><input type="radio" class="has_dependency" name="guarantor[json][0][category_of_business]" value="Factory" data-show="" data-hide="nature_of_business_div">Factory</label>
										</div>
										<div class="col-md-4 no_padding">
											<label><input type="radio" class="has_dependency" name="guarantor[json][0][category_of_business]" value="Services Provider" data-show="" data-hide="nature_of_business_div">Services Provider</label>
										</div>
										<div class="clearfix"></div>
									</div>
			               			<div class="col-md-8 mrgn">
										<div class="col-md-12">
											<label>Type Of Bussiness</label>
										</div>
										<div class="col-md-3">
											<label>
												<input type="radio" class="has_dependency" name="guarantor[json][0][type_of_business]" value="solepro_prietory" data-show="" data-hide="affiliation_years">Sole Proprietory</label>
										</div>
										<div class="col-md-3">
											<label>
												<input type="radio" class="has_dependency" name="guarantor[json][0][type_of_business]" value="partnership" data-show="affiliation_years" data-hide="">Partnership
											</label>
										</div>
										<div class="col-md-3">
											<label>
												<input type="radio" class="has_dependency" name="guarantor[json][0][type_of_business]" value="family_business" data-show="" data-hide="affiliation_years">Family Bussiness
											</label>
										</div>
										<div class="col-md-3 no_padding">
											<label>
												<input type="radio" name="guarantor[json][0][type_of_business]" value="other">Others
											</label>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-4 mrgn nature_of_business_div" style="display: none;">
										<div class="col-md-12">
											<label>Nature Of Bussiness</label>
										</div>
										<div class="col-md-4">
											<label><input type="radio" name="guarantor[json][0][nature_of_business]" value="whole_seller">Whole Seller</label>
										</div>
										<div class="col-md-4">
											<label>
												<input type="radio" name="guarantor[json][0][nature_of_business]" value="distributor">Distributor
											</label>
										</div>
										<div class="col-md-4 no_padding">
											<label><input type="radio" name="guarantor[json][0][retailer]" value="retailer">Retailer</label>
										</div>
										<div class="clearfix"></div>
									</div>
				              		<div class="col-md-4 mrgn affiliation_years" style="display: none;">
				                			<label>Affiliations Year</label>
											<input type="text" name="guarantor[json][0][affiliations_year]" class="form-control" value="">
				            		</div>
			            			<div class="clearfix"></div>
			                	</div>
			                </div>
		                
			                <div class="student_area mrgn pro_radio student_tab professional_type_tab" style="display: none;">
			                	<div class="row">
			                		<div class="col-md-4">
										<label>Collage / Institute Name</label>
										<input type="text" name="guarantor[json][0][institute_name]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Department of Study</label>
										<input type="text" name="guarantor[json][0][institute_department]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4">
			                			<label>Pursuing Degree</label>
										<input type="text" name="guarantor[json][0][institute_degree]" class="form-control" value="">
			                		</div> 
			                		<div class="col-md-4 mrgn">
										<label>Year of Study Left</label>
										<input type="text" name="guarantor[json][0][institute_year_left]" class="form-control" value="">
			                		</div>
			                		<div class="col-md-4 mrgn">
										<label>Phone Number</label>
										<input type="text" name="guarantor[json][0][institute_phone]" class="form-control" value="">
			                		</div>
			                	</div>
		                	</div>

			                <div class="bank_info mrgn pro_radio">
			                	<div class="row mrgn">
			                		<div class="col-md-4">
				                		<label>Attachments</label>
				                	</div>
				                	<div class="col-md-6">
				                		<input type="file" class="form-control" name="guarantor[json][0][attachment_paths]">
				                	</div>
			                	</div>
			                	<div class="row mrgn">
			                		<div class="col-md-4">
				                		<label>Do you have an Active Bank Account Detail</label>
				                	</div>
				                	<div class="col-md-2">
				                		<div class="col-md-6">
				                			<label>
				                				<input type="radio" value="1" name="guarantor[has_bank_account][0]">Yes
								             </label>
							             </div>
				                		<div class="col-md-6">
				                			<label>
				                				<input type="radio" value="0" name="guarantor[has_bank_account][0]">No
				                			</label>
				                		</div>
				                	</div>
			                	</div>
			                	<div class="row mrgn">
			                		<div class="col-md-4">
				                		<label>Can you provide a cheque against balance of payment</label>
				                	</div>
				                	<div class="col-md-2">
				                		<div class="col-md-6">
				                			<label>
				                				<input type="radio" value="1" name="guarantor[can_provide_cheque][0]">Yes
				                			</label>	
						             	</div>
				                		<div class="col-md-6">
				                			<label>
				                				<input type="radio" value="0" name="guarantor[can_provide_cheque][0]">No
				                			</label>
						             	</div>
				                	</div>
			                	</div>
			                	<div class="row mrgn">
			                		<div class="col-md-4">
				                		<label>Commitment to live at the same residency</label>
				                	</div>
				                	<div class="col-md-6">
				                		<input type="text" class="form-control" name="guarantor[commitment_to_residency][0]" value="">
				                	</div>
			                	</div>
			            	</div>
	        			</div>
        			@endforelse
        			<div class="gurantor_place mrgn"></div>
                	<div class="col-md-12 text-right pull-left">
                		<button type="button" class="btn btn-primary" id="add_guarantor">Add Guarantor</button>
                	</div>
        			<div class="clearfix"></div>
             		<div class="seperator_div"><hr></div>
            		<div class="clearfix"></div>
            		@php
						$commitment_json = json_decode($order->phoneVerification->commitment_json, true);
					@endphp	
                	<div class="col-md-12 checklist">
                		<h3>Commitment to the Location of his Residency.</h3>
                		<p>
	                		A customer who has strong ties to his residence does not lead to a bad debt situation. Ideally, a person who lives in his owned (not rented) house with somebody of his extending family living under one roof with him or nearby has lessor probability for defaulting on his payment. He can also be easily reached in case of non- payment. Similarly migrated people from other nearby regions are usually avoided for sale because they may take the motorcycle to their hometown to avoid repossession in case of default. The following questions needs to be asked before making a risk assessment of the customer profile
	                	</p>
               			<div class="chckoptions pro_radio">
		               		<div class="col-md-12">
		               			<label>
		               				<input type="checkbox" value="Is the Client a permanent resident of his stated address (stated on ID card) ?" name="commitment[]" {{ in_array("Is the Client a permanent resident of his stated address (stated on ID card) ?", $commitment_json)? 'checked':'' }}>Is the Client a permanent resident of his stated address (stated on ID card)?</label>
		               		</div>
               				<div class="col-md-12">
               					<label><input type="checkbox" value="Is the Current Residence in name of the customer?" name="commitment[]" {{ in_array("Is the Current Residence in name of the customer?", $commitment_json)? 'checked':'' }}>Is the Current Residence in name of the customer?</label>
               				</div>
               				<div class="col-md-12">
               					<label><input type="checkbox" value="(If not) Is the name of the owner relative or Family Head of the customer?" name="commitment[]" {{ in_array("(If not) Is the name of the owner relative or Family Head of the customer?", $commitment_json)? 'checked':'' }}>(If not) Is the name of the owner relative or Family Head of the customer?</label>
               				</div>
               				<div class="col-md-12">
               					<label><input type="checkbox" value="Does the client have a rented the property?" name="commitment[]" {{ in_array("Does the client have a rented the property?", $commitment_json)? 'checked':'' }}>Does the client have a rented the property?</label>
               				</div>
               				<div class="col-md-12">
               					<label><input type="checkbox" value="In case of rented property, how much Rent does client pay as cash outflows?"  name="commitment[]" {{ in_array("In case of rented property, how much Rent does client pay as cash outflows?", $commitment_json)? 'checked':'' }}>In case of rented property, how much Rent does client pay as cash outflows?</label>
               				</div>
               				<div class="col-md-12">
               					<label><input type="checkbox"  value="How long the client has been living in the premises?" name="commitment[]" {{ in_array("How long the client has been living in the premises?", $commitment_json)? 'checked':'' }}>How long the client has been living in the premises?</label>
               				</div>
               				<div class="col-md-12">
               					<label><input type="checkbox" value="Is the Clients Extended Family living with him, nearby or within the city?" name="commitment[]" {{ in_array("Is the Clients Extended Family living with him, nearby or within the city?", $commitment_json)? 'checked':'' }}>Is the Clients Extended Family living with him, nearby or within the city?</label>
               				</div>
               				<div class="col-md-12">
               					<label><input type="checkbox" value="Picture of Residence Uploaded for cross-reference has to be taken." name="commitment[]" {{ in_array("Picture of Residence Uploaded for cross-reference has to be taken.", $commitment_json)? 'checked':'' }} >Picture of Residence Uploaded for cross-reference has to be taken.</label>
               				</div>
               				<div class="col-md-12">
               					<label><input type="checkbox" value="Nearby Landmark/Business Shop close to residence should be mentioned." name="commitment[]" {{ in_array("Nearby Landmark/Business Shop close to residence should be mentioned.", $commitment_json)? 'checked':'' }}>Nearby Landmark/Business Shop close to residence should be mentioned.</label>
               				</div>
               			</div>
               			<div class="scale mrgn">
               				<label>On a scale of 1-5, How sure you are that the Client is likely to remain a resident of verified premises during the lease period? (1 being Not Sure at all)</label>
               				<div class="col-md-6">
               					<select class="form-control" name="commitment[]">
	               					@for($i = 1; $i <= 5; $i++)
	               						<option value="{{ $i }}" {{ in_array($i, $commitment_json)? 'selected':'' }}>{{ $i }}</option>
               				     @endfor
               			        </select>
               				</div>
               			</div>
                	</div>
                	<div class="col-md-12 checklist">
                		<h3>Commitment to his Profession/Employment</h3>
                		<p>
                			A stable business/employability or strong professional ties proves in avoiding bad debt situation. A person working for one organization for more than three years has a better chance of payment on time. For e.g. a shop owner with stock worth more than 500,000 is less likely to leave/change his professional commitment during first year of payments with our company. Similarly a property dealer, marketing agent or service provider has lessor commitment to work place and needs to have higher favorable score against other scorecard attributes. 
                		</p>
                		<p>Usually, a customer has a following professional categories :</p>
	    				<div class="list_itms">
	    					<ul>
	    						<li><strong>Government Employee </strong>– These are best kind of customers for financing but they shouldn't be of any forces i.e. Police, Army etc as forces can easily be posted to far places.</li>
	    						<li><strong>Shop Owner</strong> – In the business of selling (Non-Dealer of motorcycle)</li>
	    						<li><strong>Service Provider </strong> People who are not selling any product i.e. Property Dealer, Tailor, Hair dresser etc. (Lawyers are usually avoided) </li>
	    						<li><strong>Private Employee  </strong> People who work as a private employee for another employer i.e. Salesman, Accountant, Executives etc.</li>
	    						<li><strong>Agricultural Land Owner   </strong> People who are generally dependent on income from Agriculture.</li>
	    						<li><strong>Student  </strong> People who are studding in local college/universities </li>
	    						<li><strong>Others  </strong> Any other type if undecided of the above. </li>
	    					</ul>
	    					{{-- <img src="{{ base_url('assets/images/govt_img.png') }}" width="70%"> --}}
	    					<p><strong>Note</strong> Incase of less than two Years of Professional Commitment - Last employment or business detail with reference is required for cross checking</p>
	    				</div> 
		               	<div class="scale mrgn">
		               		<label>On a Scale of 1-10 (How confident is the verification officer that the client professional profile will not change during the lease period)? (1 being Not confidant at all)</label>
			               <div class="col-md-6">
		               		<select class="form-control" name="profession_commitment">
		               			@for($j = 1; $j <= 10; $j++)
	               						<option value="{{ $j }}" {{ $j == $order->phoneVerification->profession_commitment ? 'selected':'' }}>{{ $j }}</option>
               				     @endfor
				            </select>
			               	</div>
		               	</div>
	                </div>
	                @php
						$income_json  = json_decode($order->phoneVerification->income_json);
					@endphp
            		<div class="col-md-12 checklist">
	                	<h3>Profiling of Customer Income & Seriousness to Pay</h3>
	                	<p>
		                	Determining the income of the customer with respect to his expenses makes the most out of the verification process. Similarly, assessment of his seriousness to payback his debt needs to be taken into account along with post dated cheques of installment /security cheque from the customer or guarantor. 
		                </p>
	                	<h4>Profiling of Customer Income by Preparing his Income Statement</h4>
	                	<p>Following model can be employed in determining the income of the potential client.</p>
	                 	<div class="col-md-12 no_padding mrgn">
	                     	<div class="col-md-6">
	                     		<label>Net Salary</label>
	                     		<input type="text" class="form-control" name="income_employment[net_sallery]" value="{{ $income_json->net_sallery ?? '' }}">
	                     	</div>
	                     	<div class="col-md-6">
	                     		<label>(or)Net Salary</label>
	                     		<input type="text" name="income_employment[or_net_sallery]" class="form-control" value="{{ $income_json->or_net_sallery ?? '' }}">
	                     	</div>
	                     	<div class="col-md-6 mrgn">
	                     		<label>Average Monthly Sales</label>
	                     		<input type="text" name="income_employment[average_month_sallery]" class="form-control" value="{{ $income_json->average_month_sallery ?? '' }}">
	                     	</div>
	                     	<div class="col-md-6 mrgn">
	                     		<label>Less Operational </label>
	                     		<input type="text" name="income_employment[opertion_less]" class="form-control" value="{{ $income_json->opertion_less ?? '' }}">
	                     	</div>
	                     	<div class="col-md-6 mrgn">
	                     		<label>Net Cash flows after Operational Cost </label>
	                     		<input type="text" name="income_employment[net_cash_flow]"  class="form-control" value="{{ $income_json->net_cash_flow ?? '' }}">
	                     	</div>
	                     	<div class="col-md-6 mrgn">
	                     		<label>Less Unavoidable Expenses  </label>
	                     		<input type="text" name="income_employment[lees_expenses]" class="form-control" value="{{ $income_json->lees_expenses ?? '' }}">
	                     	</div>
	                     	<div class="col-md-6 mrgn">
	                     		<label>Net free Cash flows before savings  </label>
	                     		<input type="text" name="income_employment[before_savings]" class="form-control" value="{{ $income_json->before_savings ?? '' }}">
	                     	</div>
	                     	<div class="clearfix"></div>
                     	</div>
		               	<div class="scale mrgn">
		               		<label>On a Scale of 1-10 (Is the Client present to be serious in matters of debt payback i.e How likely the customer will payback his debt to the company on time) </label>
			               	<div class="col-md-6">
		               			<select class="form-control" name="income_employment[scale]">
		               				@for($k = 1; $k <= 10; $k++)
	               						<option value="{{ $k }}" {{ $k == $income_json->scale ? 'selected':'' }}>{{ $k }}</option>
               				     @endfor
				               	</select>
			               	</div>
		               </div>
                	</div>
                	<div class="clearfix"></div>
                </div>
                <div class="hold3 mrgn pro_radio">
                    <h3>Final Assesment</h3>
                	<div class="col-md-12">
                		<label>
                			<input type="radio"  name="assesment_type" value="champion_customers" {{ $order->phoneVerification->assesment_type  == 'champion_customers' ? 'checked' : '' }}>CHAMPION CUSTOMERS
                		</label>
                	</div>
                	<div class="col-md-12">
                		<label>
                			<input type="radio" name="assesment_type" value="good_customers" {{ $order->phoneVerification->assesment_type == 'good_customers' ? 'checked' : '' }}>GOOD CUSTOMER
                		</label>
                	</div>
                	<div class="col-md-12">
                		<label>
                			<input type="radio" name="assesment_type" value="affordable_customers"  {{ $order->phoneVerification->assesment_type == 'affordable_customers' ? 'checked' : '' }}>AFFORDABLE CUSTOMERS
                		</label>
                	</div>
                	<div class="col-md-12">
                		<label>
                			<input type="radio" name="assesment_type" value="ignorable_customers" {{ $order->phoneVerification->assesment_type == 'ignorable_customers' ? 'checked' : '' }}>IGNORABLE CUSTOMERS
                		</label>
                	</div>
                	<div class="clearfix"></div>
                </div>
                <div class="mrgn final_btn">
                	<div class="col-md-2">
                		<button type="submit" name="submit" value="save" class="btn btn-primary">Save</button>
                	</div>
                	<div class="col-md-2">
                		<button type="submit" name="submit" value="final_save" class="btn btn-success">Final Approved</button>
                	</div>
                	<div class="clearfix"></div>
                </div>
                </div>
                <input type="hidden" name="total_guarantors" id="total_guarantors" value="{{ $order->guarantors->count() > 0 ? $order->guarantors->count() - 1 : 0 }}">
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection

@push('modals')
	<!-- Modal -->
	<div id="mapModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Map</h4>
	      </div>
	      <div class="modal-body">
	        <div class="input_main pop_input">
	          <input type="text" name="heading" id="location-query" value="" autocomplete="off">
	            <ul class="output" style="display:none;"></ul>
	          	<button type="button" onclick="getLocation()">
	          		<img src="{{ base_url("assets/images/loc_point.png") }}" alt="img">
	          	</button>
	        </div>
	        <div class="input_main">
	          <div id="myMap" style="width: 100%; height: 300px;"></div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Done</button>
	      </div>
	    </div>

	  </div>
	</div>
@endpush

@push('css')
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/select2/dist/css/select2.min.css") }}">
  <style type="text/css">
    .add_row button, .remove_row button
    {
      margin-top: 42%;
    }
    .add_meta_row button, .remove_meta_row button
    {
      margin-top: 42%;
    }
    .nl-padding
    {
      padding-left: 0px;
    }
  	.mrgn
	{
		margin: 10px 0;
	}
	.pro_radio input[type=radio]{
	  margin-right: 10px;
	}
	.pro_radio input[type=checkbox]{
	  margin-right: 10px;
	}
	.no_padding{
	  padding: 0;
	}
	.scale select
	{
	  height: 43px !important;
	}
	.final_btn button
	{
	  width: 100%;
	}
	.phone_verification_form h1,h2,h3
	{
		text-align:  left;
		font-weight: 700;
		color:#3c8dbc;
	}
	.phone_verification_form h3
	{
		margin-top: 0;
	}
	.phone_verification_form label
	{
		font-weight: 400;
	}
	.gurantor_div .sub_heading{
	  margin: 0;
	}
	.phone_verification_form .sub_heading h3
	{
		color:#000;
		font-size: 22px;
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered 
	{
	    line-height: 21px;
	}
	.sub_heading
	{
		margin-top: 30px;
	}
	.seperator_div hr
	{
		height: 2px;
		background: #3c8dbc;
	}
	#map 
	{ 
	    height: 300px;    
	    width: 300px;            
	} 
	#mapsecond 
	{ 
	    height: 300px;    
	    width: 300px;            
  	}      
  	.input_main input 
  	{
	    width: 100%;
	    padding: 10px 10px;
	    box-sizing: border-box;
	    border: #d0d0d0 solid 1px;
	    font-size: 14px;
	    color: #7c7c7c;
	    font-family: 'Raleway', sans-serif;
	    font-weight: 400;
	    transition-duration: .4s;
	} 
	.pop_input button 
	{
	    position: absolute;
	    top: -7px;
	    right: 0;
	    width: 10%;
	    background: none;
	    border: 0;
	    outline: 0;
	}
	.pop_input button img
	{
		width: 80%;
	}
	.pop_input button 
	{
	    top: 19px;
	    right: 6px;
	}
	.pac-container 
	{     
		z-index: 1100 !important; 
	}
  </style>
@endpush
@push('scripts')
	<script src="https://maps.googleapis.com/maps/api/js?key={{ _GOOGLE_MAP_KEY_ }}&libraries=places&callback=initMap" async defer></script>
	<script type="text/javascript" src="{{ base_url("assets/bower_components/select2/dist/js/select2.min.js") }}"></script>
	<script type="text/javascript" src="{{ base_url("assets/js/jquery.mask.min.js") }}"></script>
	<script type="text/javascript">
  $('select').select2();
  $('#ps_cnic_no').mask('00000-0000000-0');
  $('#cnic_no').mask('00000-0000000-0');

  /*$('form').on('keyup', '.total_installments, .total_price_on_installment, .down_payment', function(){
    var node        = $(this).closest('.offer_row');
    var ti          = parseInt($(node).find('.total_installments').val());
    var tpi         = parseInt($(node).find('.total_price_on_installment').val());
    var dp          = parseInt($(node).find('.down_payment').val());
    var apm_node    = $(node).find('.amount_per_month');

    var result      = (tpi - dp) / ti;

    if(!isNaN(result))
      $(apm_node).val(Math.ceil(result));
  });*/

  $(document).on('click', '.add_meta_row', function(){
    var node  = $(this).closest('.meta_row');
    var clone = $(node).clone();
    $(this).removeClass('add_meta_row');
    $(this).addClass('remove_meta_row');
    $(node).find('button').html('<i class="fa fa-minus"></i>');
    $(node).find('button').removeClass('btn-primary');
    $(node).find('button').addClass('btn-danger');
    $(clone).find('input').val('');
    $(node).after(clone);
  });

	$(document).on('click', '.remove_meta_row', function(){
		var node      = $(this).closest('.meta_row');
		$(node).remove();
		var last_node = $('.meta_row').last();
		$(this).removeClass('remove_meta_row');
		$(this).addClass('add_meta_row');
		$(last_node).find('button').html('<i class="fa fa-plus"></i>');
		$(last_node).find('button').removeClass('btn-danger');
		$(last_node).find('button').addClass('btn-primary');
	});


	/*$('.organization_area').hide();
	$('.private_area').hide();
	$('.bussiness_area').hide();
	$('.agri_area').hide();
	$('.self_area').hide();
	$('.student_area').hide();
	$('.bank_info').hide();*/

	$('.customRadio').click(function ()
	{
		$('.CustomTabs').hide();
		$('#' + $(this).data('id')).show();
  	});
  	$('body').on('click', '.customRadio2', function(){
  		$('.CustomTabs').hide();
		$('#' + $(this).data('id')).show();
  	});
	$('.contract_part_field').hide();
	$('.bussiness_nature_div').hide();
	$('.customRadio:checked').trigger('click');

	function showFields()
	{
		var fieldsShow = document.getElementById('contract_part_area');
		if(fieldsShow.checked == true)
		{
			$('.contract_part_field').show();
		}
		else
		{
			$('.contract_part_field').hide();
		}
	}

	$('.CustomTabs').on('click', '.statusFC', function(){
		var value 	= $(this).val();
		var node 	= $(this).closest('.CustomTabs').find('.contract_part_field');
		if(value == 'Full Time')
		{
			$(node).hide();
		}
		else
		{
			$(node).show();
		}
	});

	$('.CustomTabs').on('click', '.typeBsns', function(){
		var value 	= $(this).val();
		var node 	= $(this).closest('.CustomTabs').find('.contract_part_field');
		if(value == 'Partnership')
		{
			$(node).show();
		}
		else
		{
			$(node).hide();
		}
	});

	// $('.CustomTabs').on('click', '.category_bsns', function(){
	// 	var value 	= $(this).val();
	// 	var node 	= $(this).closest('.CustomTabs').find('.bussiness_nature_div');
	// 	if(value == 'Shop')
	// 	{
	// 		$(node).show();
	// 	}
	// 	else
	// 	{
	// 		$(node).hide();
	// 	}
	// });

	// custom 2
	$('.organization_area2').hide();
		$('.private_area2').hide();
		$('.bussiness_area2').hide();
		$('.agri_area2').hide();
		$('.self_area2').hide();
		$('.student_area2').hide();
		$('.bank_info2').hide();

		$('.customRadio2').click(function ()
		{
			$('.CustomTabs2').hide();
			$('#' + $(this).data('id')).show();
	  	});

	$('.contract_part_field2').hide();

	$('.bussiness_nature_div2').hide();

	function showFields()
	{
		var fieldsShow = document.getElementById('contract_part_area2');
		if(fieldsShow.checked == true)
		{
			$('.contract_part_field2').show();
		}
		else
		{
			$('.contract_part_field2').hide();
		}
	}

	$('.CustomTabs2').on('click', '.statusFC2', function(){
		var value 	= $(this).val();
		var node 	= $(this).closest('.CustomTabs2').find('.contract_part_field2');
		if(value == 'full_time')
		{
			$(node).hide();
		}
		else
		{
			$(node).show();
		}
	});

	$('.CustomTabs2').on('click', '.typeBsns2', function(){
		var value 	= $(this).val();
		var node 	= $(this).closest('.CustomTabs2').find('.contract_part_field2');
		if(value == 'Partnership')
		{
			$(node).show();
		}
		else
		{
			$(node).hide();
		}
	});

	$('.CustomTab2s').on('click', '.category_bsns2', function(){
		var value 	= $(this).val();
		var node 	= $(this).closest('.CustomTabs2').find('.bussiness_nature_div2');
		if(value == 'Shop')
		{
			$(node).show();
		}
		else
		{
			$(node).hide();
		}
	});
	// custom 2 end
	$('#add_gurantor').click(function(){
		var $copyDiv = $('.gurantor_div').clone();
		$('.gurantor_place').html($copyDiv);
	});

	var map, marker, autocomplete, addressNode, addressUpdated = false;
    function initMap() 
    {
        var myLatLng = {lat: 31.4848634, lng: 74.3830406};
        
        map = new google.maps.Map(document.getElementById('myMap'), {
          center: myLatLng,
          zoom: 14,
          disableDoubleClickZoom: true, // disable the default map zoom on double click
        });

        var mapOpt      = { zoom: 10 };
		var myLatLng    = {lat: 31.4848634, lng: 74.3830406};
		var markerTitle = "";
		marker  = new google.maps.Marker({
			position:   myLatLng,
			anchorPoint: new google.maps.Point(0, -29),
			draggable:  true,
			animation:  google.maps.Animation.DROP,
			title:      markerTitle
		});
		marker.setMap(map);
		map.setZoom(14);

		marker.addListener('click', toggleBounceMarker);
		marker.addListener('dragend', markerDragged);

        var input   = document.getElementById('location-query');
		var options = {
			componentRestrictions: {country: 'pk'},
			types: ['establishment']
		};
        autocomplete = new google.maps.places.Autocomplete(input, options);
      	autocomplete.bindTo('bounds', map);
      	autocomplete.addListener('place_changed', function () {
	        marker.setVisible(false);
	        var place = autocomplete.getPlace();
	        var data  = {
	                    title: $(input).val(), 
	                    latitude: place.geometry.location.lat(), 
	                    longitude: place.geometry.location.lng()
                  	};

	        if(!place.geometry) 
	        {
	          // User entered the name of a Place that was not suggested and
	          // pressed the Enter key, or the Place Details request failed.
	          window.alert("No details available for input: '" + place.name + "'");
	          return;
	        }

	        if(place.geometry.viewport) 
	        {
	          map.fitBounds(place.geometry.viewport);
	        } 
	        else 
	        {
	          map.setCenter(place.geometry.location);
	          map.setZoom(17);  // Why 17? Because it looks good.
	        }
	        marker.setPosition(place.geometry.location);
	        marker.setVisible(true);
          	addressUpdated = true;
      	});
    }

    function toggleBounceMarker()
    {
      if (marker.getAnimation() !== null) 
      {
        marker.setAnimation(null);
      } 
      else 
      {
        marker.setAnimation(google.maps.Animation.BOUNCE);
      }
    }

    function updateLocation(data, triggerAutocomplete = false)
    {
      $.ajax({
        type: "POST",
        url: "{{ base_url(_ADMIN_ROUTE_PREFIX_ . '/ajax/get_location') }}",
        data: {
          location_title: data.title,
          location_lat:   data.latitude,
          location_lon:   data.longitude
        },
        dataType: "json",
        success: function(response)
        {
          locationUpdated = true;
          if(response.status == 'success')
          {
            if(triggerAutocomplete)
            {
				myLatLng    = new google.maps.LatLng(response.data.location_lat, response.data.location_lon);
				$('#location-query').val(response.data.user_location_full);
				map.setCenter(myLatLng);
				map.setZoom(17);
				geoMarkerSet(myLatLng);
          		addressUpdated = true;
            }          
          }
          else
          {
            alert(response.message);
          }
        },
        error: function(response)
        {
          alert("Unable to Update Location.");
        }
      });
    }

    function markerDragged(e)
    {
      updateLocation({title: "", latitude: e.latLng.lat(), longitude: e.latLng.lng()}, true);
    }

    function geoMarkerSet(geometry)
    {
      marker.setVisible(false);
      marker.setPosition(geometry);
      marker.setVisible(true);
    }

    is_loc_denied = false;
    function getLocation()
    {
      if(!is_loc_denied)
      {
        $(document).trigger('ajaxStart'); //Little Hack to Show Loader
        if (navigator.geolocation) 
        {
          navigator.geolocation.getCurrentPosition(geoToData, function(){
            is_loc_denied   = true;
            $(document).trigger('ajaxStop');
          });
        } 
        else 
        { 
          alert("Geolocation is not supported by this browser.");
          $(document).trigger('ajaxStop');
        }
      }
      else
      {
        alert("You need to reload this page.");
        $(document).trigger('ajaxStop');
      }
    }

    function geoToData(pos)
    {
      updateLocation({title: "", latitude: pos.coords.latitude, longitude: pos.coords.longitude}, true);
    }

    $('.address_field').focus(function(){
    	addressNode = $(this);
    	$('#mapModal').modal('show');
    });
    $('#mapModal').on('hidden.bs.modal', function () {
    	if(addressUpdated)
    	{
	    	var address = $('#location-query').val();
	    	$(addressNode).val(address);
	    	addressUpdated = false;
	    }
	});
	var guarators_count = {{ $order->guarantors->count() > 0 ? $order->guarantors->count() - 1 : 0 }};
	$('#add_guarantor').click(function(){
		var tmp_element = $('.guarantor_div').last().wrap('<div class="wrap-unwrap"></div>');
		var html 		= tmp_element.parent().html();
		tmp_element.unwrap();
		$('.guarantor_div').last().after(html);
		guarators_count++;
		$.each($('.guarantor_div').last().find("input"), function(i, element){
			var name = $(element).attr('name').replace(/[\d]/, guarators_count);
			$(element).attr('name', name);
			if($(element).attr('type') != 'radio')
				$(element).val("");
			$(element).prop('checked', false);
		});
		$('#total_guarantors').val(guarators_count);
		$('.remove_guarantor').parent().show();
		if($('.guarantor_div').length == 1)
			$('.remove_guarantor').parent().hide();
	});

	$('body').on('click', '.remove_guarantor', function(){
		if(confirm("Do You Really Want to Remove This?"))
		{
			$(this).closest('.guarantor_div').remove();
			guarators_count = 0;
			$.each($('.guarantor_div'), function(i, div){
				$.each($(div).find("input"), function(i, element){
					var name = $(element).attr('name').replace(/[\d]/, guarators_count);
					$(element).attr('name', name);
					$(element).val("");
					$(element).prop('checked', false);
				});
				guarators_count++;
			});
			guarators_count--;
			if($('.guarantor_div').length == 1)
				$('.remove_guarantor').parent().hide();
		}
	});

	$('body').on('click', '.has_dependency', function(){
		var node = $(this);
		var show_classes = $(node).data('show').split(',');
		console.log(show_classes);
		var hide_classes = $(node).data('hide').split(',');
		console.log(hide_classes);
		$(show_classes).each(function(i, class_name){
			if(class_name == '')
				return;
			$(node).closest('.professional_type_tab').find('.' + class_name).show();
		});
		$(hide_classes).each(function(i, class_name){
			if(class_name == '')
				return;
			console.log($(node).closest('.professional_type_tab').find('.' + class_name));
			$(node).closest('.professional_type_tab').find('.' + class_name).hide();
		});
	});

	$('body').on('click', '.guarantor_profession', function(){
		var node 		= $(this);
		var show_class 	= $(node).data('show');
		console.log(node);
		$(node).closest('.guarantor_div').find('.professional_type_tab').hide();
		$(node).closest('.guarantor_div').find('.' + show_class).show();
	});
	$('input[type=radio]:checked').trigger('click');
</script>
@endpush