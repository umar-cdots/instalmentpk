@extends('admin.layouts.master')

@section('title', "Edit an Orders")
@section('page_heading', "Orders")
@section('page_sub_heading', "Edit")
@section('content')
<?php error_reporting(0); ?>
<div class="row phone_verification_form">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Verification Form</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{base_url(_ADMIN_ROUTE_PREFIX_ . '/orders/physical_verification/add/' . $order_id)}}" name="add_pv" id="add_pv" method="post">
              <div class="box-body">
                <div class="box-title text-center">
                  <h3>Physical Verification</h3>
                </div>
                <div class="hold1">
                  <div class="sub_heading">
                  <h3>Personal Details</h3>
                </div>
        <div class="row">
          <div class="col-md-8">
            <div class="col-md-4">
          <label>Name</label>
            <input type="text" id="full_name" name="full_name" class="form-control" value="<?php echo $orderdata['full_name']?>">
          </div>
          <div class="col-md-4">
          <label>Father's / Husband's Name</label>
            <input type="text" id="father_husband_name" name="father_husband_name" class="form-control" value="<?php echo $orderdata['father_husband_name']?>">
          </div>
          <div class="col-md-4">
          <label>CNIC #</label>
            <input type="text" name="cnic_no" id="cnic_no" class="form-control" value="<?php echo $orderdata['cnic_no']?>">
          </div>
          <div class="col-md-4 mrgn">
          <label>Nominee Name</label>
            <input type="text" name="nominee_name" id="nominee_name" class="form-control" value="<?php echo $orderdata['nominee_name']?>">
          </div>
          <div class="col-md-4 mrgn">
          <label>Nominee's Father / Husband Name</label>
            <input type="text" name="nominee_father_or_husband_name" id="nominee_father_or_husband_name" class="form-control" value="<?php echo $orderdata['nominee_father_or_husband_name']?>">
          </div>
          <div class="col-md-4 mrgn">
          <label>Phone Number (Landline)</label>
                        <input type="text" name="phone_number_landline" id="phone_number_landline" class="form-control" value="<?php echo $orderdata['phone_number_landline']?>">
          </div>
          <div class="col-md-4 mrgn">
          <label>Mobile Number</label>
            <input type="text" name="mobile_number" id="mobile_number" class="form-control" value="<?php echo $orderdata['mobile_number']?>">
          </div>
          
          <div class="col-md-4 mrgn">
          <label>Current Address as on CNIC</label>
            <input type="text"  name="current_address_cnic" id="current_address_cnic" class="form-control" value="<?php echo $orderdata['current_address_cnic']?>">
          </div>
          <div class="col-md-4 mrgn">
          <label>Permanent Address as on CNIC</label>
            <input type="text" name="permanent_address_cnic" id="permanent_address_cnic" class="form-control" value="<?php echo $orderdata['permanent_address_cnic']?>">
          </div>
          <div class="col-md-4 mrgn">
          <label>Attachments</label>
            <input type="file" name="upload" class="form-control" id="upload">
          </div>
          </div>
          <div class="col-md-4">
            <div class="col-md-12">
          <label>Current Address on Map</label>
            <div class="mapouter">
              <div class="gmap_canvas">
              <div id="latvalue"></div>
                    <div id="longvalue"></div>
                
                    <div id="latmoved"></div>
                    <div id="longmoved"></div>
                
                <div style="padding:10px">
                    <div id="map"></div>
                </div>
            </div>
            </div>
          </div>
          
          </div>
        </div>
               <div class="seperator_div"><hr></div>
            <div class="sub_heading">
                  <h3>Professional Details</h3>
                </div>
        <div class="pro_radio">
          <form action="#" method="post" id="form_options">
          <div class="col-md-3">
            <label><input class="customRadio" type="radio" value="Government Employee" data-id="governmentEmp" name="pro_det" <?php if($orderdata['pro_det'] == 'Government Employee'){ ?>checked="checked"<?php } ?>>Government Employee</label>
          </div>  
          <div class="col-md-3">
            <label><input class="customRadio" type="radio" value="Private Employee" data-id="PrivateEmployees"  name="pro_det" <?php if($orderdata['pro_det'] == 'Private Employee'){ ?>checked="checked"<?php } ?>>Private Employee</label>
          </div>
          <div class="col-md-3">
            <label><input type="radio" class="customRadio" name="pro_det" data-id="businessShop" value="Business – Shop Owner / Service Provider" <?php if($orderdata['pro_det'] == 'Business – Shop Owner / Service Provider'){ ?>checked="checked"<?php } ?>>Business – Shop Owner / Service Provider</label>
          </div>
          <div class="col-md-3">
            <label><input type="radio" class="customRadio" name="pro_det" data-id="agriculturalLand" value="Agricultural Land Owner" <?php if($orderdata['pro_det'] == 'Agricultural Land Owner'){ ?>checked="checked"<?php } ?>>Agricultural Land Owner</label>
          </div>    
          <div class="col-md-3">
            <label><input type="radio" class="customRadio" name="pro_det" data-id="selfEmp" value="Self Employed – Freelancer/Service" <?php if($orderdata['pro_det'] == 'Self Employed – Freelancer/Service'){ ?>checked="checked"<?php } ?>>Self Employed – Freelancer/Service</label>
          </div>    
          <div class="col-md-3">
            <label><input type="radio" class="customRadio" name="pro_det" data-id="student" value="Student" <?php 
        if($orderdata['pro_det'] == 'Student'){ ?>checked="checked"<?php } ?>>Student</label>
          </div>    
          <div class="clearfix"></div>
          </form> 
        </div>                  
                
                <div class="organization_area mrgn pro_radio CustomTabs" id="governmentEmp" <?php if($orderdata['pro_det'] == 'Government Employee'){ ?>style="display:block";<?php } else { ?>style="display:none;"<?php } ?>>
                  <div class="row">
                    <div class="col-md-4">
              <label>Organization Name</label>
              <input type="text" name="personal_orgnization_name" id="personal_orgnization_name" class="form-control" value="<?php echo $orderdata['personal_orgnization_name']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Department</label>
              <input type="text" name="personal_department_name" id="personal_department_name" class="form-control" value="<?php echo $orderdata['personal_department_name']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Scale / Grade</label>
              <input type="text" name="personal_scale" id="personal_scale" class="form-control" value="<?php echo $orderdata['personal_scale']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Year of Service</label>
              <input type="text" id="year_of_services" name="year_of_services" class="form-control" value="<?php echo $orderdata['year_of_services']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Monthly Salary</label>
              <input type="text" name="personal_salary_month" id="personal_salary_month" class="form-control" value="<?php echo $orderdata['personal_salary_month']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Office Address</label>
              <input type="text" name="personal_office_address" id="personal_office_address" class="form-control" value="<?php echo $orderdata['personal_office_address']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Active Number of years for you on your business address</label>
              <input type="text" id="personal_active_no_of_year_business" name="personal_active_no_of_year_business" class="form-control" value="<?php echo $orderdata['personal_active_no_of_year_business']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Landline</label>
              <input type="text" name="personal_landline_number" id="personal_landline_number" class="form-control" value="<?php echo $orderdata['personal_landline_number']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Status</label></div>
              <div class="col-md-6">
                <label><input type="radio" name="full_time" id="full_time" class="statusFC" value="Full Time" <?php 
        if($orderdata['full_time'] == 'Full Time'){ ?> checked="checked"<?php } ?>>Full Time</label>
              </div>
              <div class="col-md-6">
                <label><input type="radio" name="part_time_or_contact" id="part_time_or_contact" class="statusFC" value="Contract" <?php 
        if($orderdata['part_time_or_contact'] == 'Contract'){ ?>checked="checked"<?php } ?>>Contract / Part Time</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn contract_part_field">
                    <label>Year of Contaract commitment left</label>
                    <input type="text" name="contract_year_left" id="contract_year_left" class="form-control" value="<?php echo $orderdata['personal_landline_number']?>">
                  </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="private_area mrgn pro_radio CustomTabs" id="PrivateEmployees" <?php 
        if($orderdata['pro_det'] == 'Private Employee'){ ?>style="display:block";<?php } else { ?>style="display:none;"<?php } ?> >
                  <div class="row">
                    <div class="col-md-4">
              <label>Employer's Name</label>
              <input type="text" name="employer_name" id="employer_name" class="form-control" value="<?php echo $orderdata['employer_name']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Department</label>
              <input type="text" name="personal_dep_name_employe" id="personal_dep_name_employe" class="form-control" value="<?php echo $orderdata['personal_dep_name_employe']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Designation</label>
              <input type="text" name="personal_designation_name" id="personal_designation_name" class="form-control" value="<?php echo $orderdata['personal_designation_name']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Year of Service</label>
              <input type="text" name="personal_service_year" id="personal_service_year" class="form-control" value="<?php echo $orderdata['personal_service_year']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Monthly Salary</label>
              <input type="text" name="personal_month_sallery" id="personal_month_sallery" class="form-control" value="<?php echo $orderdata['personal_month_sallery']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Office Address</label>
              <input type="text" name="personal_office_address" id="personal_office_address"class="form-control" value="<?php echo $orderdata['personal_office_address']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Status</label></div>
              <div class="col-md-6">
                <label><input type="radio" name="full_time" id="full_time" class="statusFC" value="Full Time" <?php 
        if($orderdata['full_time'] == 'Full Time'){ ?>checked="checked"<?php } ?>>Full Time</label>
              </div>
              <div class="col-md-6">
                <label><input type="radio" name="half_time" id="half_time" class="statusFC" value="contract_or_part_time" <?php 
        if($orderdata['half_time'] == 'contract_or_part_time'){ ?>checked="checked"<?php } ?>>Contract / Part Time</label>
              </div>
              <div class="clearfix"></div>
            </div>
                    <div class="col-md-4 mrgn">
              <label>Phone Number (Landline)</label>
              <input type="text" name="personal_phone_landline" id="personal_phone_landline" class="form-control" value="<?php echo $orderdata['personal_phone_landline']?>">
                    </div>
                    
            
            <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  <div class="col-md-4 mrgn contract_part_field">
                    <label>Year of Contaract commitment left</label>
                    <input type="text" name="commited_contarct_year" id="commited_contarct_year" class="form-control" value="<?php echo $orderdata['commited_contarct_year']?>">
                  </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="bussiness_area mrgn pro_radio CustomTabs" id="businessShop" <?php 
        if($orderdata['pro_det'] == 'Business – Shop Owner / Service Provider'){ ?>style="display:block";<?php } else { ?>style="display:none;"<?php } ?>>
                  <div class="row">

                    <div class="col-md-4">
              <label>Bussiness's Name</label>
              <input type="text" name="personal_name_of_businees" id="personal_name_of_businees" class="form-control" <?php echo $orderdata['personal_name_of_businees']?>>
                    </div>
                    <div class="col-md-4">
                      <label>Bussiness Address</label>
              <input type="text" name="personal_address_of_businees" id="personal_address_of_businees" class="form-control" value="<?php echo $orderdata['personal_address_of_businees']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Phone Number (Landline)</label>
              class="form-control">
              <input type="text" name="personal_business_landline" id="personal_business_landline" class="form-control" value="<?php echo $orderdata['personal_business_landline']?>">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Monthly Income Estimated</label>
              <input type="text" name="personal_income_monthly" id="personal_income_monthly" class="form-control" value="<?php echo $orderdata['personal_income_monthly']?>">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business address</label>
              <input type="text" name="personal_active_years_business" id="personal_active_years_business" class="form-control" value="<?php echo $orderdata['personal_active_years_business']?>">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business</label>
              <input type="text" name="personal_active_years_business1" id="personal_active_years_business1" class="form-control" value="<?php echo $orderdata['personal_active_years_business1']?>">
                    </div>                  
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Category Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio" class="category_bsns" name="shop" value="Shop" <?php 
        if($orderdata['shop'] == 'Shop'){ ?>checked="checked"<?php } ?>>Shop</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" class="category_bsns" name="factory" value="Factory" <?php 
        if($orderdata['factory'] == 'Factory'){ ?>checked="checked"<?php } ?>>Factory</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio" class="category_bsns" name="services_provider" value="Services Provider" <?php 
        if($orderdata['services_provider'] == 'Services Provider'){ ?>checked="checked"<?php } ?>>Services Provider</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  
                  <div class="col-md-8 mrgn">
              <div class="col-md-12"><label>Type Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio" name="sole_proprietory" id="sole_proprietory" class="typeBsns" value="Sole Proprietory" <?php 
        if($orderdata['sole_proprietory'] == 'Sole Proprietory'){ ?>checked="checked"<?php } ?>>Sole Proprietory</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" name="partnership" id="partnership" class="typeBsns" value="Partnership" <?php 
        if($orderdata['partnership'] == 'Partnership9'){ ?>checked="checked"<?php } ?>>Partnership</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" name="family_business" id="family_business" class="typeBsns" value="Family Bussiness" <?php 
        if($orderdata['family_business'] == 'Family Bussiness'){ ?>checked="checked"<?php } ?>>Family Bussiness</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn bussiness_nature_div">
              <div class="col-md-12"><label>Nature Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio" name="whole_sale" id="whole_sale" value="Whole Seller" <?php 
        if($orderdata['whole_sale'] == 'Whole Seller'){ ?>checked="checked"<?php } ?>>Whole Seller</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" name="distributer" id="distributer" value="Distributor" <?php 
        if($orderdata['distributer'] == 'Distributor'){ ?>checked="checked"<?php } ?>>Distributor</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio" name="retailer" id="retailer" value="Retailer" <?php 
        if($orderdata['retailer'] == 'Retailer'){ ?>checked="checked"<?php } ?>>Retailer</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  <div class="col-md-4 mrgn contract_part_field">
                      <label>Affiliations Year</label>
              <input type="text" name="personal_affliate_year" id="personal_affliate_year" class="form-control" value="<?php echo $orderdata['personal_affliate_year']?>">
                    </div>
                  
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="agri_area mrgn pro_radio CustomTabs" id="agriculturalLand" <?php 
        if($orderdata['pro_det'] == 'Agricultural Land Owner'){ ?>style="display:block";<?php } else { ?>style="display:none;"<?php } ?>>
                  <div class="row">
                    <div class="col-md-6">
                      <label>Address Of Land</label>
                      <input type="text" name="personal_land_address" id="personal_land_address" class="form-control" value="<?php echo $orderdata['personal_land_address']?>">
                    </div>
                    <div class="col-md-6">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  </div>
                </div>
                
                <div class="self_area mrgn pro_radio CustomTabs" id="selfEmp" <?php 
        if($orderdata['pro_det'] == 'Self Employed – Freelancer/Service'){ ?>style="display:block";<?php } else { ?>style="display:none;"<?php } ?>>
                  <div class="row">
                    <div class="col-md-4">
              <label>Bussiness's Name</label>
              <input type="text" name="personal_name_business" id="personal_name_business" class="form-control" value="<?php echo $orderdata['personal_name_business']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Bussiness Address</label>
              <input type="text" name="personal_address_business" id="personal_address_business" class="form-control" value="<?php echo $orderdata['personal_address_business']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Phone Number (Landline)</label>
              <input type="text" name="personal_landline_conatct" id="personal_landline_conatct" class="form-control" value="<?php echo $orderdata['personal_landline_conatct']?>">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Monthly Income Estimated</label>
              <input type="text" name="personal_estiamte_incom_month" id="personal_estiamte_incom_month" class="form-control" value="<?php echo $orderdata['personal_estiamte_incom_month']?>">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business address</label>
              <input type="text" name="personal_active_year_business" id="personal_active_year_business" class="form-control" value="<?php echo $orderdata['personal_active_year_business']?>">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business</label>
              <input type="text" name="personal_active_year_bus1" id="personal_active_year_bus1" class="form-control" value="<?php echo $orderdata['personal_active_year_bus1']?>">
                    </div>                  
            <div class="col-md-4 mrgn ">
              <div class="col-md-12"><label>Category Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio" class="category_bussiness" id="shop" name="shop" value="Shop" <?php 
        if($orderdata['shop'] == 'Shop'){ ?>checked="checked"<?php } ?>>Shop</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" class="category_bsns" id="factory" name="factory" value="Factory" <?php 
        if($orderdata['factory'] == 'Factory'){ ?>checked="checked"<?php } ?>>Factory</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio" class="category_bsns" id="service_provider" name="service_provider" value="Services Provider" <?php
        if($orderdata['service_provider'] == 'Services Provider'){ ?>checked="checked"<?php } ?>>Services Provider</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  
                  <div class="col-md-8 mrgn">
              <div class="col-md-12"><label>Type Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio" name="sole_proprietory" id="sole_proprietory" class="typeBsns" value="Sole Proprietory" <?php 
        if($orderdata['sole_proprietory'] == 'Sole Proprietory'){ ?>checked="checked"<?php } ?>>Sole Proprietory</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" name="partnership" id="partnership" class="typeBsns" value="Partnership" <?php 
        if($orderdata['partnership'] == 'Partnership'){ ?>checked="checked"<?php } ?>>Partnership</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" name="family_business" id="family_business" class="typeBsns" value="Family Bussiness" <?php 
        if($orderdata['family_business'] == 'Family Bussiness'){ ?>checked="checked"<?php } ?>>Family Bussiness</label>
              </div>
              
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn bussiness_nature_div">
              <div class="col-md-12"><label>Nature Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio" name="whole_seller" id="whole_seller" value="Whole Seller" <?php 
        if($orderdata['whole_seller'] == 'Whole Seller'){ ?>checked="checked"<?php } ?>>Whole Seller</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" name="distributor" id="distributor" value="Distributor" <?php 
        if($orderdata['distributor'] == 'Distributor'){ ?>checked="checked"<?php } ?>>Distributor</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio" name="retailer" id="retailer" value="Retailer" <?php 
        if($orderdata['retailer'] == 'Retailer'){ ?>checked="checked"<?php } ?>>Retailer</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  <div class="col-md-4 mrgn contract_part_field">
                      <label>Affiliations Year</label>
              <input type="text" name="year_affliation" id="year_affliation" class="form-control" value="<?php echo $orderdata['year_affliation']?>">
                    </div>
                  
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="student_area mrgn pro_radio CustomTabs" id="student" <?php 
        if($orderdata['pro_det'] == 'Student'){ ?>style="display:block";<?php } else { ?>style="display:none;"<?php } ?>>
                  <div class="row">
                    <div class="col-md-4">
              <label>Collage / Institute Name</label>
              <input type="text" name="personal_college_name" id="personal_college_name" class="form-control" value="<?php echo $orderdata['personal_college_name']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Department of Study</label>
              <input type="text" name="department_name" id="department_name" class="form-control" value="<?php echo $orderdata['department_name']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Pursuing Degree</label>
              <input type="text" name="pursuing_degree" id="pursuing_degree" class="form-control" value="<?php echo $orderdata['pursuing_degree']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Year of Study Left</label>
              <input type="text" name="year_left_study" id="year_left_study" class="form-control" value="<?php echo $orderdata['year_left_study']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Phone Number</label>
              <input type="text" name="phone_number" id="phone_number" class="form-control" value="<?php echo $orderdata['phone_number']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="bank_info mrgn pro_radio">
                  <div class="row mrgn">
                    <div class="col-md-4">
                    <label>Do you have an Active Bank Account Detail</label>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-6"><label><input type="radio" name="Do you have an Active Bank Account Detail" value="Yes" <?php 
        if($orderdata['Do you have an Active Bank Account Detail'] == 'Yes'){ ?> checked="checked"<?php } ?>>Yes</label></div>
                    <div class="col-md-6"><label><input type="radio" name="Do you have an Active Bank Account Detail" value="No" <?php 
        if($orderdata['Do you have an Active Bank Account Detail'] == 'No'){ ?> checked="checked"<?php } ?>>No</label></div>
                  </div>
                  </div>
                  <div class="row mrgn">
                    <div class="col-md-4">
                    <label>Can you provide a cheque against balance of payment</label>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-6"><label><input type="radio" value="Yes" name="Can you provide a cheque against balance of payment" <?php 
        if($orderdata['Can you provide a cheque against balance of payment'] == 'Yes'){ ?>checked="checked"<?php } ?>>Yes</label></div>
                    <div class="col-md-6"><label><input type="radio" value="Can you provide a cheque against balance of payment" name="No" <?php 
        if($orderdata['Can you provide a cheque against balance of payment'] == 'No'){ ?>checked="checked"<?php } ?>>No</label></div>
                  </div>
                  </div>
                  <div class="row mrgn">
                    <div class="col-md-4">
                    <label>Commitment to live at the same residency</label>
                  </div>
                  <div class="col-md-6">
                    <input type="text" name="commitement_live_residency" id="commitement_live_residency" class="form-control" value="<?php echo $orderdata['commitement_live_residency']?>">
                  </div>
                  </div>
                </div>
                
                <div class="mrgn">
                  <div class="col-md-12">
                    <div class="col-md-3">Do you have a bank account?</div>
                    <div class="col-md-6">
                      <div class="col-md-2">
                            <label>Yes</label>&nbsp;&nbsp;&nbsp;<input type="radio" name="Do you have a bank account?" value="Yes" <?php 
                         if($orderdata['Do you have a bank account?'] == 'Yes'){ ?>checked="checked"<?php } ?>>
                </div>
                      <div class="col-md-2">
                            <label>No</label>&nbsp;&nbsp;&nbsp;<input type="radio" name="Do you have a bank account?" value="No" <?php 
                        if($orderdata['Do you have a bank account?'] == 'No'){ ?>checked="checked"<?php } ?>>
                                </div>
                    </div>
                  </div>
                </div>
                 <div class="mrgn">
                  <div class="col-md-12">
                    <div class="col-md-3">Can you provide cheque?</div>
                    <div class="col-md-6">
                      <div class="col-md-2"><label>Yes</label>&nbsp;&nbsp;&nbsp;<input type="radio" value="Yes" name="Can you provide cheque?" <?php 
                         if($orderdata['Can you provide cheque?'] == 'Yes'){ ?>checked="checked"<?php } ?>></div>
                      <div class="col-md-2"><label>No</label>&nbsp;&nbsp;&nbsp;<input type="radio" value="No" name="Can you provide cheque?" <?php 
                         if($orderdata['Can you provide cheque?'] == 'No'){ ?>checked="checked"<?php } ?>></div>
                    </div>
                  </div>
                </div>
                <div class="mrgn">
                  <div class="col-md-12">
                    <div class="col-md-3">Commitment to live at the same residency?</div>
                    <div class="col-md-4">
                      <input type="text" name="commitement_live_year" id="commitement_live_year" class="form-control" value="<?php echo $orderdata['commitement_live_year']?>">
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                </div>
                   <div class="seperator_div"><hr></div>
                <div class="hold2 mrgn">
               
                  <div class="gurantor_div">
                     <div class="row">
                     <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="sub_heading">
                  <h3>Guarantor Information</h3>
                </div>
                  </div>
                  <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-primary" id="add_gurantor">Add Guarantor</button>
                  </div>
                  <div class="clearfix"></div>
                </div>
                </div>
        <div class="row">
          <div class="col-md-8">
            <div class="col-md-4">
          <label>Name</label>
            <input type="text" name="gurantier_name" id="gurantier_name" class="form-control" value="<?php echo $orderdata['gurantier_name']?>">
          </div>
          <div class="col-md-4">
          <label>Father's / Husband's Name</label>
            <input type="text" name="gurantier_father_name" id="gurantier_father_name" class="form-control" value="<?php echo $orderdata['gurantier_father_name']?>">
          </div>
          <div class="col-md-4">
          <label>CNIC #</label>
            <input type="text" name="gurantier_cnic" id="gurantier_cnic" value="<?php echo $orderdata['gurantier_cnic']?>" class="form-control">
          </div>
          <div class="col-md-4 mrgn">
          <label>Relationship with Applicant</label>
            <input type="text" name="relationship_with_applicant" id="relationship_with_applicant" class="form-control" value="<?php echo $orderdata['relationship_with_applicant']?>">
          </div>
          
          <div class="col-md-4 mrgn">
          <label>Phone Number (Landline)</label>
            <input type="text" name="gurantier_phone" id="gurantier_phone" class="form-control" value="<?php echo $orderdata['gurantier_phone']?>">
          </div>
          <div class="col-md-4 mrgn">
          <label>Mobile Number</label>
            <input type="text" name="gurantier_mobile_no" id="gurantier_mobile_no" class="form-control" value="<?php echo $orderdata['gurantier_mobile_no']?>">
          </div>
          
          <div class="col-md-4 mrgn">
          <label>Current Address as on CNIC</label>
            <input type="text" name="gurantier_address_cnic" id="gurantier_address_cnic" value="<?php echo $orderdata['gurantier_address_cnic']?>" class="form-control">
          </div>
          <div class="col-md-4 mrgn">
          <label>Permanent Address as on CNIC</label>
            <input type="text" name="gurantier_permanent_address_cnic" id="gurantier_permanent_address_cnic" class="form-control" value="<?php echo $orderdata['gurantier_permanent_address_cnic']?>">
          </div>
          <div class="col-md-4 mrgn">
          <label>Attachments</label>
            <input type="file" name="upload" class="form-control">
          </div>
          </div>
          <div class="col-md-4">
            <div class="col-md-12 mrgn">
          <label>Current Address on Map</label>
            <div class="mapouter">
              <div class="gmap_canvas">
              <div id="latclicked"></div>
                    <div id="longclicked"></div>
                
                    <div id="latmovedvalue"></div>
                    <div id="longmovedvalue"></div>
                
                <div style="padding:10px">
                    <div id="mapsecond"></div>
                </div>
            </div>
            </div>
          </div>
          </div>
        </div>
            <div class="sub_heading">
                  <h3>Professional Details</h3>
                </div>
        <div class="pro_radio">
          <form action="#" method="post" id="form_options">
          <div class="col-md-3">
            <label><input class="customRadio2" type="radio" value="Government Employee" data-id="governmentEmp2" name="pro_det" <?php 
                         if($orderdata['pro_det'] == 'Government Employee'){ ?>checked="checked"<?php } ?>>Government Employee</label>
          </div>  
          <div class="col-md-3">
            <label><input class="customRadio2" type="radio" value="Private Employee" data-id="PrivateEmployees2"  name="pro_det" <?php 
                         if($orderdata['pro_det'] == 'Private Employee'){ ?>checked="checked"<?php } ?>>Private Employee</label>
          </div>
          <div class="col-md-3">
            <label><input type="radio" class="customRadio2" name="pro_det" data-id="businessShop2" value="Business – Shop Owner / Service Provider" <?php 
                         if($orderdata['pro_det'] == 'Business – Shop Owner / Service Provider'){ ?>checked="checked"<?php } ?>>Business – Shop Owner / Service Provider</label>
          </div>
          <div class="col-md-3">
            <label><input type="radio" class="customRadio2" name="pro_det" data-id="agriculturalLand2" value="Agricultural Land Owner" <?php 
                         if($orderdata['pro_det'] == 'Agricultural Land Owner'){ ?>checked="checked"<?php } ?>>Agricultural Land Owner</label>
          </div>    
          <div class="col-md-3">
            <label><input type="radio" class="customRadio2" name="pro_det" data-id="selfEmp2" value="Self Employed – Freelancer/Service" <?php 
                         if($orderdata['pro_det'] == 'Self Employed – Freelancer/Service'){ ?>checked="checked"<?php } ?>>Self Employed – Freelancer/Service</label>
          </div>    
          <div class="col-md-3">
            <label><input type="radio" class="customRadio2" name="pro_det" data-id="student2" value="Student" <?php 
                         if($orderdata['pro_det'] == 'Student'){ ?>checked="checked"<?php } ?>>Student</label>
          </div>    
          <div class="clearfix"></div>
          </form> 
        </div>                    
                
                <div class="organization_area mrgn pro_radio CustomTabs2" id="governmentEmp2" <?php 
        if($orderdata['pro_det'] == 'Government Employee'){ ?>style="display:block";<?php } else { ?>style="display:none;"<?php } ?>>
                  <div class="row">
                    <div class="col-md-4">
              <label>Organization Name</label>
              <input type="text" name="professional_organization_name" class="form-control" id="professional_organization_name" value="<?php echo $orderdata['professional_organization_name']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Department</label>
              <input type="text" name="department_name" id="department_name" id="professional_department_name" class="form-control" value="<?php echo $orderdata['department_name']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Scale / Grade</label>
              <input type="text" name="professional_scale_grade" id="professional_scale_grade" class="form-control" value="<?php echo $orderdata['professional_scale_grade']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Year of Service</label>
              <input type="text" name="professional_servic_year" id="professional_servic_year" class="form-control" value="<?php echo $orderdata['professional_servic_year']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Monthly Salary</label>
              <input type="text" name="professional_month_sallery" id="professional_month_sallery" class="form-control" value="<?php echo $orderdata['professional_month_sallery']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Office Address</label>
              <input type="text" name="professional_office_address" id="professional_office_address" class="form-control" value="<?php echo $orderdata['professional_office_address']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Active Number of years for you on your business address</label>
              <input type="text" name="professional_active_year_business" id="professional_active_year_business" class="form-control" value="<?php echo $orderdata['professional_active_year_business']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Landline</label>
              <input type="text" name="professional_landline" id="professional_landline" class="form-control" value="<?php echo $orderdata['professional_landline']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Status</label></div>
              <div class="col-md-6">
                <label><input type="radio" name="full_time" class="statusFC" value="Full Time" <?php 
                         if($orderdata['full_time'] == 'Full Time'){ ?>checked="checked"<?php } ?>>Full Time</label>
              </div>
              <div class="col-md-6">
                <label><input type="radio" name="contract_or_part_time" class="statusFC" value="Contract" <?php 
                         if($orderdata['contract_or_part_time'] == 'Contract'){ ?>checked="checked"<?php } ?>>Contract / Part Time</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn contract_part_field">
                    <label>Year of Contaract commitment left</label>
                    <input type="text" type="contarct_commitment_prof" name="contarct_commitment_prof" id="contarct_commitment_prof" class="form-control" value="<?php echo $orderdata['contarct_commitment_prof']?>">
                  </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="private_area mrgn pro_radio CustomTabs2" id="PrivateEmployees2" <?php 
        if($orderdata['pro_det'] == 'Private Employee'){ ?>style="display:block";<?php } else { ?>style="display:none;"<?php } ?>>
                  <div class="row">
                    <div class="col-md-4">
              <label>Employer's Name</label>
              <input type="text" name="personal_employer_name" id="personal_employer_name" class="form-control" value="<?php echo $orderdata['personal_employer_name']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Department</label>
              <input type="text" name="personal_department_name" id="personal_department_name" class="form-control" value="<?php echo $orderdata['personal_department_name']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Designation</label>
              <input type="text" name="personal_designation" id="personal_designation" class="form-control" value="<?php echo $orderdata['personal_designation']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Year of Service</label>
              <input type="text" name="personal_services_year" id="personal_services_year" class="form-control" value="<?php echo $orderdata['personal_services_year']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Monthly Salary</label>
              <input type="text" name="month_sallery" id="month_sallery" class="form-control" value="<?php echo $orderdata['month_sallery']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Office Address</label>
              <input type="text" name="office_address_job" id="office_address_job" class="form-control" value="<?php echo $orderdata['office_address_job']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Phone Number (Landline)</label>
              <input type="text" name="office_phone_no" id="office_phone_no" class="form-control" value="<?php echo $orderdata['office_phone_no']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Status</label></div>
              <div class="col-md-6">
                <label><input type="radio" name="full_time" class="statusFC" value="Full Time" <?php 
                         if($orderdata['full_time'] == 'Full Time'){ ?>checked="checked"<?php } ?>>Full Time</label>
              </div>
              <div class="col-md-6">
                <label><input type="radio" name="contract_or_part_time" class="statusFC" value="Contract" <?php 
                         if($orderdata['full_time'] == 'Contract'){ ?>checked="checked"<?php } ?>>Contract / Part Time</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-12 mrgn contract_part_field">
                    <label> Year of Contaract commitment left</label>
                    <input type="text" name="commitment_left" id="commitment_left" class="form-control" value="<?php echo $orderdata['commitment_left']?>">
                  </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="bussiness_area mrgn pro_radio CustomTabs2" id="businessShop2" <?php 
        if($orderdata['pro_det'] == 'Business – Shop Owner / Service Provider'){ ?>style="display:block";<?php } else { ?>style="display:none;"<?php } ?>>
                  <div class="row">

                    <div class="col-md-4">
              <label>Bussiness's Name</label>
              <input type="text" name="businees_name" id="businees_name" class="form-control" value="<?php echo $orderdata['businees_name']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Bussiness Address</label>
              <input type="text" name="businees_address" id="businees_address" class="form-control" value="<?php echo $orderdata['businees_address']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Phone Number (Landline)</label>
              <input type="text" name="business_phone_no" id="business_phone_no" class="form-control" value="<?php echo $orderdata['business_phone_no']?>">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Monthly Income Estimated</label>
              <input type="text" name="estimate_income_month" id="estimate_income_month" class="form-control" value="<?php echo $orderdata['estimate_income_month']?>">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business address</label>
              <input type="text" name="business_duration" id="business_duration" class="form-control" value="<?php echo $orderdata['business_duration']?>">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business</label>
              <input type="text" name="business_duration" id="business_duration" class="form-control" value="<?php echo $orderdata['business_duration']?>">
                    </div>                  
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Category Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio"  name="shop" value="Shop" <?php 
                         if($orderdata['shop'] == 'Shop'){ ?>checked="checked"<?php } ?>>Shop</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" name="factory" value="Factory"  <?php 
                         if($orderdata['factory'] == 'Factory'){ ?>checked="checked"<?php } ?>>Factory</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio" name="services_provider" value="Services Provider" <?php 
                         if($orderdata['services_provider'] == 'Services Provider'){ ?>checked="checked"<?php } ?>>Services Provider</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  
                  <div class="col-md-8 mrgn">
              <div class="col-md-12"><label>Type Of Bussiness</label></div>
              <div class="col-md-3">
                <label><input type="radio" name="sole_proprietory" value="Sole Proprietory" <?php 
                         if($orderdata['sole_proprietory'] == 'Sole Proprietory'){ ?>checked="checked"<?php } ?>>Sole Proprietory</label>
              </div>
              <div class="col-md-3">
                <label><input type="radio" name="partnership" value="Partnership" <?php 
                         if($orderdata['partnership'] == 'Partnership'){ ?>checked="checked"<?php } ?>>Partnership</label>
              </div>
              <div class="col-md-3">
                <label><input type="radio" name="family_bussiness" value="Family Bussiness" <?php 
                         if($orderdata['family_bussiness'] == 'Family Bussiness'){ ?>checked="checked"<?php } ?>>Family Bussiness</label>
              </div>
              <div class="col-md-3 no_padding">
                <label><input type="radio" name="others" value="Others" <?php 
                         if($orderdata['others'] == 'Others'){ ?>checked="checked"<?php } ?>>Others</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Nature Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio" name="whole_seller" value="Whole Seller" <?php 
                         if($orderdata['whole_seller'] == 'Whole Seller'){ ?>checked="checked"<?php } ?>>Whole Seller</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" name="distributor" value="Distributor" <?php 
                         if($orderdata['distributor'] == 'Distributor'){ ?>checked="checked"<?php } ?>>Distributor</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio" name="retailer" value="Retailer" <?php 
                         if($orderdata['retailer'] == 'Retailer'){ ?>checked="checked"<?php } ?>>Retailer</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  <div class="col-md-4 mrgn">
                      <label>Affiliations Year</label>
              <input type="text" name="affiliations_year" id="affiliations_year" class="form-control" value="<?php echo $orderdata['affiliations_year']?>">
                    </div>
                  
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="agri_area mrgn pro_radio CustomTabs2" id="agriculturalLand2" <?php 
        if($orderdata['pro_det'] == 'Agricultural Land Owner'){ ?>style="display:block";<?php } else { ?>style="display:none;"<?php } ?> >
                  <div class="row">
                    <div class="col-md-6">
                      <label>Address Of Land</label>
                      <input type="text" name="land_address" id="land_address" class="form-control" value="<?php echo $orderdata['land_address']?>">
                    </div> 
                    <div class="col-md-6">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  </div>
                </div>
                
                <div class="self_area mrgn pro_radio CustomTabs2" id="selfEmp2"<?php 
        if($orderdata['pro_det'] == 'Self Employed – Freelancer/Service'){ ?>style="display:block";<?php } else { ?>style="display:none;"<?php } ?>>
                  <div class="row">
                    <div class="col-md-4">
              <label>Bussiness's Name</label>
              <input type="text" name="business_name" id="business_name" class="form-control" value="<?php echo $orderdata['business_name']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Bussiness Address</label>
              <input type="text" name="business_address" id="business_address" class="form-control" value="<?php echo $orderdata['business_address']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Phone Number (Landline)</label>
              <input type="text" name="landline_phone_no" id="landline_phone_no" class="form-control" value="<?php echo $orderdata['landline_phone_no']?>">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Monthly Income Estimated</label>
              <input type="text" name="estimate_net_incom" id="estimate_net_incom" class="form-control" value="<?php echo $orderdata['estimate_net_incom']?>">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business address</label>
              <input type="text" name="business_year" id="business_year" class="form-control" value="<?php echo $orderdata['business_year']?>">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business</label>
              <input type="text" name="business_year" id="business_year" class="form-control" value="<?php echo $orderdata['business_year']?>">
                    </div>                  
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Category Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio" name="shop" value="Shop" <?php 
                         if($orderdata['shop'] == 'Shop'){ ?>checked="checked"<?php } ?>>Shop</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" name="factory" value="Factory" <?php 
                         if($orderdata['factory'] == 'Factory'){ ?>checked="checked"<?php } ?>>Factory</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio" name="services_provider" value="Services Provider" <?php 
                         if($orderdata['services_provider'] == 'Services Provider'){ ?>checked="checked"<?php } ?>>Services Provider</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  
                  <div class="col-md-8 mrgn">
              <div class="col-md-12"><label>Type Of Bussiness</label></div>
              <div class="col-md-3">
                <label><input type="radio" name="sole_proprietory" value="Sole Proprietory" <?php 
                         if($orderdata['sole_proprietory'] == 'Sole Proprietory'){ ?>checked="checked"<?php } ?>>Sole Proprietory</label>
              </div>
              <div class="col-md-3">
                <label><input type="radio" name="partnership" value="Partnership" <?php 
                         if($orderdata['partnership'] == 'Partnership'){ ?>checked="checked"<?php } ?>>Partnership</label>
              </div>
              <div class="col-md-3">
                <label><input type="radio" name="family_business" value="Family Bussiness" <?php 
                         if($orderdata['family_business'] == 'Family Bussiness'){ ?>checked="checked"<?php } ?>>Family Bussiness</label>
              </div>
              <div class="col-md-3 no_padding">
                <label><input type="radio" name="others" value="Others">Others</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Nature Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio" name="whole_seller" value="Whole Seller" <?php 
                         if($orderdata['whole_seller'] == 'Whole Seller'){ ?>checked="checked"<?php } ?>>Whole Seller</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" name="distributer" value="Distributor" <?php 
                         if($orderdata['distributer'] == 'Distributor'){ ?>checked="checked"<?php } ?>>Distributor</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio" name="retailer" value="Retailer" <?php 
                         if($orderdata['retailer'] == 'Retailer'){ ?>checked="checked"<?php } ?>>Retailer</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  <div class="col-md-4 mrgn">
                      <label>Affiliations Year</label>
              <input type="text" name="affiliations_year" id="affiliations_year" class="form-control" value="<?php echo $orderdata['affiliations_year']?>">
                    </div>
                  
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="student_area mrgn pro_radio CustomTabs2" id="student2" <?php 
        if($orderdata['pro_det'] == 'Student'){ ?>style="display:block";<?php } else { ?>style="display:none;"<?php } ?> >
                  <div class="row">
                    <div class="col-md-4">
              <label>Collage / Institute Name</label>
              <input type="text" name="institute_name" id="institute_name" class="form-control" value="<?php echo $orderdata['institute_name']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Department of Study</label>
              <input type="text" name="institute_department" id="institute_department" class="form-control" value="<?php echo $orderdata['institute_department']?>">
                    </div>
                    <div class="col-md-4">
                      <label>Pursuing Degree</label>
              <input type="text" name="institute_degree" id="institute_degree" class="form-control" value="<?php echo $orderdata['institute_degree']?>">
                    </div> 
                    <div class="col-md-4 mrgn">
              <label>Year of Study Left</label>
              <input type="text" name="institute_year_left" id="institute_year_left" class="form-control" value="<?php echo $orderdata['institute_year_left']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Phone Number</label>
              <input type="text" name="institute_phone" id="institute_phone" class="form-control" value="<?php echo $orderdata['institute_phone']?>">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="bank_info mrgn pro_radio">
                  <div class="row mrgn">
                    <div class="col-md-4">
                    <label>Do you have an Active Bank Account Detail</label>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-6"><label><input type="radio" value="Yes" name="yes" <?php 
                         if($orderdata['yes'] == 'yes'){ ?> checked="checked"<?php } ?>>Yes</label></div>
                    <div class="col-md-6"><label><input type="radio" value="No" name="no"  <?php 
                         if($orderdata['no'] == 'No'){ ?> checked="checked"<?php } ?>>No</label></div>
                  </div>
                  </div>
                  <div class="row mrgn">
                    <div class="col-md-4">
                    <label>Can you provide a cheque against balance of payment</label>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-6"><label><input type="radio" value="Yes" name="yes" <?php 
                         if($orderdata['yes'] == 'Yes'){ ?>checked="checked"<?php } ?>>Yes</label></div>
                    <div class="col-md-6"><label><input type="radio" value="No" id="no" name="no" <?php 
                         if($orderdata['no'] == 'No'){ ?>checked="checked"<?php } ?>>No</label></div>
                  </div>
                  </div>
                  <div class="row mrgn">
                    <div class="col-md-4">
                    <label>Commitment to live at the same residency</label>
                  </div>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="commitment_residency" id="commitment_residency" value="<?php echo $orderdata['commitment_residency']?>">
                  </div>
                  </div>

                </div>
                 <div class="mrgn">
                  <div class="col-md-12">
                    <div class="col-md-3">Do you have a bank account?</div>
                    <div class="col-md-6">
                      <div class="col-md-2"><label>Yes</label>&nbsp;&nbsp;&nbsp;<input type="radio" value="Yes" name="Do you have a bank account?" <?php 
                         if($orderdata['Do you have a bank account?'] == 'Yes'){ ?>checked="checked"<?php } ?>></div>
                      <div class="col-md-2"><label>No</label>&nbsp;&nbsp;&nbsp;<input type="radio" value="No" name="Do you have a bank account?" <?php 
                         if($orderdata['Do you have a bank account?'] == 'No'){ ?>checked="checked"<?php } ?>></div>
                    </div>
                  </div>
                </div>
                 <div class="mrgn">
                  <div class="col-md-12">
                    <div class="col-md-3">Can you provide cheque?</div>
                    <div class="col-md-6">
                      <div class="col-md-2"><label>Yes</label>&nbsp;&nbsp;&nbsp;<input type="radio" value="Yes" name="Can you provide cheque?" <?php 
                         if($orderdata['Can you provide cheque?'] == 'Yes'){ ?>checked="checked"<?php } ?>></div>
                      <div class="col-md-2"><label>No</label>&nbsp;&nbsp;&nbsp;<input type="radio" value="No" name="Can you provide cheque?" <?php 
                         if($orderdata['Can you provide cheque?'] == 'No'){ ?>checked="checked"<?php } ?>></div>
                    </div>
                  </div>
                </div>
                <div class="mrgn">
                  <div class="col-md-12">
                    <div class="col-md-3">Commitment to live at the same residency?</div>
                    <div class="col-md-4">
                      <input type="text" name="commited_year" id="commited_year" class="form-control" value="<?php echo $orderdata['commited_year']?>">
                    </div>
                  </div>
                </div>
                  </div>
                  <div class="gurantor_place mrgn"></div>
                
                 <div class="seperator_div"><hr></div>
                <div class="col-md-12 checklist">
                <h3>Commitment to the Location of his Residency.</h3>
                  <p>A customer who has strong
ties to his residence does not lead to a bad debt situation. Ideally, a person
who lives in his owned (not rented) house with somebody of his extending
family living under one roof with him or nearby has lessor probability for
defaulting on his payment. He can also be easily reached in case of non-
payment. Similarly migrated people from other nearby regions are usually
avoided for sale because they may take the motorcycle to their hometown
to avoid repossession in case of default.
The following questions needs to be asked before making a risk assessment
of the customer profile</p>
               <div class="chckoptions pro_radio">
                <div class="col-md-12"><label><input type="checkbox" id="Is_the_Client_a_permanent_resident_of_his_stated_address_(stated_on_ID_card)_?" name="Is_the_Client_a_permanent_resident_of_his_stated_address_(stated_on_ID_card)?" value="Yes">Is the Client a permanent resident of his stated address (stated on ID card)?</label></div>
                <div class="col-md-12"><label><input type="checkbox" id="checkboxtwo" name="checkboxtwo" value="Is the Current Residence in name of the customer?">Is the Current Residence in name of the customer?</label></div>
                <div class="col-md-12"><label><input type="checkbox" id="checkboxthree" name="checkboxthree" value="(If not) Is the name of the owner relative or Family Head of the customer?">(If not) Is the name of the owner relative or Family Head of the customer?</label></div>
                <div class="col-md-12"><label><input type="checkbox" id="checkboxfour" name="checkboxfour" value="Does the client have a rented the property?">Does the client have a rented the property?</label></div>
                <div class="col-md-12"><label><input type="checkbox" id="checkboxfive" name="checkboxfive" value="In case of rented property, how much Rent does client pay as cash outflows?">In case of rented property, how much Rent does client pay as cash outflows?</label></div>
                <div class="col-md-12"><label><input type="checkbox" id="checkboxsix" name="checkboxsix" value="How long the client has been living in the premises?">How long the client has been living in the premises?</label></div>
                <div class="col-md-12"><label><input type="checkbox" id="checkboxseven" name="checkboxseven" value="Is the Clients Extended Family living with him, nearby or within the city?">Is the Clients Extended Family living with him, nearby or within the city?</label></div>
                <div class="col-md-12"><label><input type="checkbox" id="checkboxeight" name="checkboxeight" value="Picture of Residence Uploaded for cross-reference has to be taken.">Picture of Residence Uploaded for cross-reference has to be taken.</label></div>
                <div class="col-md-12"><label><input type="checkbox" id="checkboxnine" name="checkboxnine" value="Nearby Landmark/Business Shop close to residence should be mentioned.">Nearby Landmark/Business Shop close to residence should be mentioned.</label></div>
               </div>
               <div class="scale mrgn">
                <label>On a scale of 1-5, How sure you are that the Client is likely to remain a resident of verified premises during the lease period? (1 being Not Sure at all)</label>
                <div class="col-md-6">
                    <select class="form-control">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                </select>
                </div>
               </div>
                </div>
                
                <div class="col-md-12 checklist">
                <h3>Commitment to his Profession/Employment</h3>
                  <p>A stable business/employability or strong professional ties proves in avoiding bad debt situation. A person working for one organization for more than three years has a better chance of payment on time. For e.g. a shop owner with stock worth more than 500,000 is less likely to leave/change his professional commitment during first year of payments with our company. Similarly a property dealer, marketing agent or service provider has lessor commitment to work place and needs to have higher favorable score against other scorecard attributes. </p>
                  <p>Usually, a customer has a following professional categories :</p>
            <div class="list_itms">
              <ul>
                <li><strong>Government Employee </strong>– These are best kind of customers for financing but they shouldn't be of any forces i.e. Police, Army etc as forces can easily be posted to far places.</li>
                <li><strong>Shop Owner</strong> – In the business of selling (Non-Dealer of motorcycle)</li>
                <li><strong>Service Provider </strong> People who are not selling any product i.e. Property Dealer, Tailor, Hair dresser etc. (Lawyers are usually avoided) </li>
                <li><strong>Private Employee  </strong> People who work as a private employee for another employer i.e. Salesman, Accountant, Executives etc.</li>
                <li><strong>Agricultural Land Owner   </strong> People who are generally dependent on income from Agriculture.</li>
                <li><strong>Student  </strong> People who are studding in local college/universities </li>
                <li><strong>Others  </strong> Any other type if undecided of the above. </li>
              </ul>
              {{-- <img src="{{ base_url('assets/images/govt_img.png') }}" width="70%"> --}}
              <p><strong>Note</strong> Incase of less than two Years of Professional Commitment - Last employment or business detail with reference is required for cross checking</p>
            </div> 
                      
               <div class="scale mrgn">
                <label>On a Scale of 1-10 (How confident is the verification officer that the client professional profile will not change during the lease period)? (1 being Not confidant at all)</label>
                <div class="col-md-6">
                    <select class="form-control">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
                </div>
               </div>
                </div>
                
                <div class="col-md-12 checklist">
                <h3>Profiling of Customer Income & Seriousness to Pay</h3>
                  <p>Determining the income of the customer with respect to his expenses makes the most out of the verification process. Similarly, assessment of his seriousness to payback his debt needs to be taken into account along with post dated cheques of installment /security cheque from the customer or guarantor. </p>
                  <h4>Profiling of Customer Income by Preparing his Income Statement</h4>
                  <p>Following model can be employed in determining the income of the potential client.</p>
            
                     <div class="col-md-12 no_padding mrgn">
                      <div class="col-md-6">
                        <label>Net Salary</label>
                        <input type="text" name="customer_netsallery" id="customer_netsallery" class="form-control" value="<?php echo $orderdata['customer_netsallery']?>">
                      </div>
                      <div class="col-md-6">
                        <label>(or)Net Salary</label>
                        <input type="text" name="customer_sallery" class="form-control" value="<?php echo $orderdata['customer_sallery']?>">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Average Monthly Sales</label>
                        <input type="text" name="month_sallery" id="month_sallery" class="form-control" value="<?php echo $orderdata['month_sallery']?>">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Less Operational </label>
                        <input type="text" name="oppertion_less" id="opertion_less" class="form-control" value="<?php echo $orderdata['oppertion_less']?>">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Net Cash flows after Operational Cost </label>
                        <input type="text" name="net_cash_flow" id="net_cash_flow" class="form-control" value="<?php echo $orderdata['net_cash_flow']?>">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Less Unavoidable Expenses  </label>
                        <input type="text" name="lees_expenses" id="lees_expenses" class="form-control" value="<?php echo $orderdata['lees_expenses']?>">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Net free Cash flows before savings  </label>
                        <input type="text" name="before_savings" id="before_savings" class="form-control" value="<?php echo $orderdata['before_savings']?>">
                      </div>
                      <div class="clearfix"></div>
                     </div>
                      
               <div class="scale mrgn">
                <label>On a Scale of 1-10 (Is the Client present to be serious in matters of debt payback i.e How likely the customer will payback his debt to the company on time) </label>
                <div class="col-md-6">
                    <select class="form-control">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
                </div>
               </div>
                </div>
                <div class="clearfix"></div>
                </div>
                <div class="hold3 mrgn pro_radio">
                <h3>Final Assesment</h3>
                  <div class="col-md-12"><label><input type="checkbox" name="Assesment_One" id="Assesment_One" value="CHAMPION CUSTOMERS">CHAMPION CUSTOMERS</label></div>
                  <div class="col-md-12"><label><input type="checkbox" name="Assesment_Two" id="Assesment_Two" value="GOOD CUSTOMERS">GOOD CUSTOMERS</label></div>
                  <div class="col-md-12"><label><input type="checkbox" name="Assesment_Three" id="Assesment_Three" value="AFFORDABLE CUSTOMERS">AFFORDABLE CUSTOMERS</label></div>
                  <div class="col-md-12"><label><input type="checkbox" name="Assesment_Four" id="Assesment_Four" value="IGNORABLE CUSTOMERS">IGNORABLE CUSTOMERS</label></div>
                  <div class="clearfix"></div>
                </div>
                <div class="mrgn final_btn">
                  <div class="col-md-2">
                    <button type="submit" name="submit" class="btn btn-primary" onclick="document.getElementById('add_pv').submit();">Save</button>
                  </div>
                  <div class="col-md-2">
                    <button type="button" name="verify" class="btn btn-default">Verify</button>
                  </div>
                  <div class="col-md-2">
                    <button type="button" name="verify" class="btn btn-default">Final Approved</button>
                  </div>
                  <div class="clearfix"></div>
                </div>
                </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection
@push('css')
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/select2/dist/css/select2.min.css") }}">
  <style type="text/css">
    .add_row button, .remove_row button
    {
      margin-top: 42%;
    }
    .add_meta_row button, .remove_meta_row button
    {
      margin-top: 42%;
    }
    .nl-padding
    {
      padding-left: 0px;
    }
    .mrgn
{
  margin: 10px 0;
}
    .pro_radio input[type=radio]{
      margin-right: 10px;
    }
     .pro_radio input[type=checkbox]{
      margin-right: 10px;
    }
    .no_padding{
      padding: 0;
    }
    .scale select
    {
      height: 43px !important;
    }
    .final_btn button
    {
      width: 100%;
    }
    .phone_verification_form h1,h2,h3
    {
      text-align:  left;
      font-weight: 700;
      color:#3c8dbc;
    }
    .phone_verification_form h3
    {
      margin-top: 0;
    }
    .phone_verification_form label
    {
      font-weight: 400;
    }
    .gurantor_div .sub_heading{
      margin: 0;
    }
    .phone_verification_form .sub_heading h3
    {
      color:#000;
      font-size: 22px;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 21px;
}
.sub_heading
{
  margin-top: 30px;
}
.seperator_div hr
{
  height: 2px;
  background: #3c8dbc;
}
 #map { 
            height: 300px;    
            width: 300px;            
          } 
           #mapsecond { 
            height: 300px;    
            width: 300px;            
          }       
  </style>
@endpush
@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSozsDOYhjtqX8apR0AydJDDJZaI5ACO0&callback=initMap" async defer></script>
<script type="text/javascript" src="{{ base_url("assets/bower_components/select2/dist/js/select2.min.js") }}"></script>
<script type="text/javascript">
  $('select').select2();

  /*$('form').on('keyup', '.total_installments, .total_price_on_installment, .down_payment', function(){
    var node        = $(this).closest('.offer_row');
    var ti          = parseInt($(node).find('.total_installments').val());
    var tpi         = parseInt($(node).find('.total_price_on_installment').val());
    var dp          = parseInt($(node).find('.down_payment').val());
    var apm_node    = $(node).find('.amount_per_month');

    var result      = (tpi - dp) / ti;

    if(!isNaN(result))
      $(apm_node).val(Math.ceil(result));
  });*/

  $(document).on('click', '.add_meta_row', function(){
    var node  = $(this).closest('.meta_row');
    var clone = $(node).clone();
    $(this).removeClass('add_meta_row');
    $(this).addClass('remove_meta_row');
    $(node).find('button').html('<i class="fa fa-minus"></i>');
    $(node).find('button').removeClass('btn-primary');
    $(node).find('button').addClass('btn-danger');
    $(clone).find('input').val('');
    $(node).after(clone);
  });

  $(document).on('click', '.remove_meta_row', function(){
    var node      = $(this).closest('.meta_row');
    $(node).remove();
    var last_node = $('.meta_row').last();
    $(this).removeClass('remove_meta_row');
    $(this).addClass('add_meta_row');
    $(last_node).find('button').html('<i class="fa fa-plus"></i>');
    $(last_node).find('button').removeClass('btn-danger');
    $(last_node).find('button').addClass('btn-primary');
  });


  /*$('.organization_area').hide();
  $('.private_area').hide();
  $('.bussiness_area').hide();
  $('.agri_area').hide();
  $('.self_area').hide();
  $('.student_area').hide();
  $('.bank_info').hide();*/

  $('.customRadio').click(function ()
  {
    $('.CustomTabs').hide();
    $('#' + $(this).data('id')).show();
    });


$('.contract_part_field').hide();
$('.bussiness_nature_div').hide();
function showFields()
{
  var fieldsShow = document.getElementById('contract_part_area');
  if(fieldsShow.checked == true)
  {
    $('.contract_part_field').show();
  }
  else
  {
    $('.contract_part_field').hide();
  }

}
$('.CustomTabs').on('click', '.statusFC', function(){
  var value   = $(this).val();
  var node  = $(this).closest('.CustomTabs').find('.contract_part_field');
  if(value == 'full_time')
  {
    $(node).hide();
  }
  else
  {
    $(node).show();
  }
});
$('.CustomTabs').on('click', '.typeBsns', function(){
  var value   = $(this).val();
  var node  = $(this).closest('.CustomTabs').find('.contract_part_field');
  if(value == 'Partnership')
  {
    $(node).show();
  }
  else
  {
    $(node).hide();
  }
});
$('.CustomTabs').on('click', '.category_bsns', function(){
  var value   = $(this).val();
  var node  = $(this).closest('.CustomTabs').find('.bussiness_nature_div');
  if(value == 'Shop')
  {
    $(node).show();
  }
  else
  {
    $(node).hide();
  }
});




// custom 2
$('.organization_area2').hide();
  $('.private_area2').hide();
  $('.bussiness_area2').hide();
  $('.agri_area2').hide();
  $('.self_area2').hide();
  $('.student_area2').hide();
  $('.bank_info2').hide();

  $('.customRadio2').click(function ()
  {
    $('.CustomTabs2').hide();
    $('#' + $(this).data('id')).show();
    });


$('.contract_part_field2').hide();
$('.bussiness_nature_div2').hide();
function showFields()
{
  var fieldsShow = document.getElementById('contract_part_area2');
  if(fieldsShow.checked == true)
  {
    $('.contract_part_field2').show();
  }
  else
  {
    $('.contract_part_field2').hide();
  }

}
$('.CustomTabs2').on('click', '.statusFC2', function(){
  var value   = $(this).val();
  var node  = $(this).closest('.CustomTabs2').find('.contract_part_field2');
  if(value == 'full_time')
  {
    $(node).hide();
  }
  else
  {
    $(node).show();
  }
});
$('.CustomTabs2').on('click', '.typeBsns2', function(){
  var value   = $(this).val();
  var node  = $(this).closest('.CustomTabs2').find('.contract_part_field2');
  if(value == 'Partnership')
  {
    $(node).show();
  }
  else
  {
    $(node).hide();
  }
});
$('.CustomTab2s').on('click', '.category_bsns2', function(){
  var value   = $(this).val();
  var node  = $(this).closest('.CustomTabs2').find('.bussiness_nature_div2');
  if(value == 'Shop')
  {
    $(node).show();
  }
  else
  {
    $(node).hide();
  }
});

// custom 2 end

    var map;
        function initMap() {                            
            var latitude = 31.520370; // YOUR LATITUDE VALUE
            var longitude = 74.358749; // YOUR LONGITUDE VALUE
            
            var myLatLng = {lat: latitude, lng: longitude};
            
            map = new google.maps.Map(document.getElementById('map'), {
              center: myLatLng,
              zoom: 14,
              disableDoubleClickZoom: true, // disable the default map zoom on double click
            });
            
            // Update lat/long value of div when anywhere in the map is clicked    
            google.maps.event.addListener(map,'click',function(event) {                
                document.getElementById('latclicked').innerHTML = event.latLng.lat();
                document.getElementById('longclicked').innerHTML =  event.latLng.lng();
            });
            
            // Update lat/long value of div when you move the mouse over the map
            google.maps.event.addListener(map,'mousemove',function(event) {
                document.getElementById('latmoved').innerHTML = event.latLng.lat();
                document.getElementById('longmoved').innerHTML = event.latLng.lng();
            });
                    
            var marker = new google.maps.Marker({
              position: myLatLng,
              map: map,
              //title: 'Hello World'
              
              // setting latitude & longitude as title of the marker
              // title is shown when you hover over the marker
              title: latitude + ', ' + longitude 
            });    
            
            // Update lat/long value of div when the marker is clicked
            marker.addListener('click', function(event) {              
              document.getElementById('latclicked').innerHTML = event.latLng.lat();
              document.getElementById('longclicked').innerHTML =  event.latLng.lng();
            });
            
            // Create new marker on double click event on the map
            google.maps.event.addListener(map,'dblclick',function(event) {
                var marker = new google.maps.Marker({
                  position: event.latLng, 
                  map: map, 
                  title: event.latLng.lat()+', '+event.latLng.lng()
                });
                
                // Update lat/long value of div when the marker is clicked
                marker.addListener('click', function() {
                  document.getElementById('latclicked').innerHTML = event.latLng.lat();
                  document.getElementById('longclicked').innerHTML =  event.latLng.lng();
                });            
            });
            initMapsecond()
        }
    var mapsecond;
        function initMapsecond() {                            
            var latvalue = 31.520370; // YOUR LATITUDE VALUE
            var longvalue = 74.358749; // YOUR LONGITUDE VALUE
            
            var myLatLng = {lat: latvalue, lng: longvalue};
            
            mapsecond = new google.maps.Map(document.getElementById('mapsecond'), {
              center: myLatLng,
              zoom: 14,
              disableDoubleClickZoom: true, // disable the default map zoom on double click
            });
            
            // Update lat/long value of div when anywhere in the map is clicked    
            google.maps.event.addListener(mapsecond,'click',function(event) {                
                document.getElementById('latclickedvalue').innerHTML = event.latLng.lat();
                document.getElementById('longclickedvalue').innerHTML =  event.latLng.lng();
            });
            
            // Update lat/long value of div when you move the mouse over the map
            google.maps.event.addListener(map,'mousemove',function(event) {
                document.getElementById('latmovedvalue').innerHTML = event.latLng.lat();
                document.getElementById('longmovedvalue').innerHTML = event.latLng.lng();
            });
                    
            var marker = new google.maps.Marker({
              position: myLatLng,
              map: mapsecond,
              //title: 'Hello World'
              
              // setting latitude & longitude as title of the marker
              // title is shown when you hover over the marker
              title: latvalue + ', ' + longvalue 
            });    
            
            // Update lat/long value of div when the marker is clicked
            marker.addListener('click', function(event) {              
              document.getElementById('latclickedvalue').innerHTML = event.latLng.lat();
              document.getElementById('longclickedvalue').innerHTML =  event.latLng.lng();
            });
            
            // Create new marker on double click event on the map
            google.maps.event.addListener(mapsecond,'dblclick',function(event) {
                var marker = new google.maps.Marker({
                  position: event.latLng, 
                  mapsecond: mapsecond, 
                  title: event.latLng.lat()+', '+event.latLng.lng()
                });
                
                // Update lat/long value of div when the marker is clicked
                marker.addListener('click', function() {
                  document.getElementById('latclickedvalue').innerHTML = event.latLng.lat();
                  document.getElementById('longclickedvalue').innerHTML =  event.latLng.lng();
                });            
            });
        }
  $('#add_gurantor').click(function(){
    var $copyDiv = $('.gurantor_div').clone();
    $('.gurantor_place').html($copyDiv);
  });
</script>
@endpush