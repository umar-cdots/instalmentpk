@extends('admin.layouts.master')

@section('title', "Phone Verification")
@section('page_heading', "Verification")
@section('page_sub_heading', "Add New Verification")
@section('content')
<div class="row phone_verification_form">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Verification Form</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{base_url(_VENDOR_ROUTE_PREFIX_ . '/orders/update/')}}" method="post">
              <div class="box-body">
                <div class="box-title text-center">
                  <h3>Phone Verification</h3>
                </div>
                <div class="hold1">
                  <div class="sub_heading">
                  <h3>Personal Details</h3>
                </div>
        <div class="row">
          <div class="col-md-8">
            <div class="col-md-4">
          <label>Name</label>
            <input type="text" name="name" class="form-control">
          </div>
          <div class="col-md-4">
          <label>Father's / Husband's Name</label>
            <input type="text" name="name" class="form-control">
          </div>
          <div class="col-md-4">
          <label>CNIC #</label>
            <input type="text" name="name" class="form-control">
          </div>
          <div class="col-md-4 mrgn">
          <label>Nominee Name</label>
            <input type="text" name="name" class="form-control">
          </div>
          <div class="col-md-4 mrgn">
          <label>Nominee's Father / Husband Name</label>
            <input type="text" name="name" class="form-control">
          </div>
          <div class="col-md-4 mrgn">
          <label>Phone Number (Landline)</label>
            <input type="text" name="name" class="form-control">
          </div>
          <div class="col-md-4 mrgn">
          <label>Mobile Number</label>
            <input type="text" name="name" class="form-control">
          </div>
          
          <div class="col-md-4 mrgn">
          <label>Current Address as on CNIC</label>
            <input type="text" name="curr_add" class="form-control">
          </div>
          <div class="col-md-4 mrgn">
          <label>Permanent Address as on CNIC</label>
            <input type="text" name="per_add" class="form-control">
          </div>
          <div class="col-md-4 mrgn">
          <label>Attachments</label>
            <input type="file" name="upload" class="form-control">
          </div>
          </div>
          <div class="col-md-4">
            <div class="col-md-12">
          <label>Current Address on Map</label>
            <div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="280" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{text-align:right;height:280px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:280px;width:100%;}</style></div>
          </div>
          </div>
        </div>
               
            <div class="sub_heading">
                  <h3>Professional Details</h3>
                </div>
        <div class="pro_radio">
          <div class="col-md-3">
            <label><input type="radio" name="pro_det">Government Employee</label>
          </div>  
          <div class="col-md-3">
            <label><input type="radio" name="pro_det">Private Employee</label>
          </div>
          <div class="col-md-3">
            <label><input type="radio" name="pro_det">Business – Shop Owner / Service Provider</label>
          </div>
          <div class="col-md-3">
            <label><input type="radio" name="pro_det">Agricultural Land Owner</label>
          </div>    
          <div class="col-md-3">
            <label><input type="radio" name="pro_det">Self Employed – Freelancer/Service</label>
          </div>    
          <div class="col-md-3">
            <label><input type="radio" name="pro_det">Student</label>
          </div>    
          
          <div class="clearfix"></div>  
        </div>                  
                
                <div class="organization_area mrgn pro_radio">
                <h3>(IF Government Employees is Checked)</h3>
                  <div class="row">
                    <div class="col-md-4">
              <label>Organization Name</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Department</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Scale / Grade</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Year of Service</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Monthly Salary</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Office Address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Active Number of years for you on your business address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Landline</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Status</label></div>
              <div class="col-md-6">
                <label><input type="radio">Full Time</label>
              </div>
              <div class="col-md-6">
                <label><input type="radio">Contract / Part Time</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
                    <label>(if contract/part time is checked) Incase of Contractual Agreement Year of Contaract commitment left</label>
                    <input type="text" class="form-control">
                  </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="private_area mrgn pro_radio">
                <h3>(IF Private Employees is Checked)</h3>
                  <div class="row">
                    <div class="col-md-4">
              <label>Employer's Name</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Department</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Designation</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Year of Service</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Monthly Salary</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Office Address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Phone Number (Landline)</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Status</label></div>
              <div class="col-md-6">
                <label><input type="radio">Full Time</label>
              </div>
              <div class="col-md-6">
                <label><input type="radio">Contract / Part Time</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-12 mrgn">
                    <label>(if contract/part time is checked) Incase of Contractual Agreement Year of Contaract commitment left</label>
                    <input type="text" class="form-control">
                  </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="bussiness_area mrgn pro_radio">
                <h3>(IF Business – Shop Owner / Service Provider is Checked)</h3>
                  <div class="row">

                    <div class="col-md-4">
              <label>Bussiness's Name</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Bussiness Address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Phone Number (Landline)</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Monthly Income Estimated</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business</label>
              <input type="text" name="name" class="form-control">
                    </div>                  
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Category Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio">Shop</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio">Factory</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio">Services Provider</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  
                  <div class="col-md-8 mrgn">
              <div class="col-md-12"><label>Type Of Bussiness</label></div>
              <div class="col-md-3">
                <label><input type="radio">Sole Proprietory</label>
              </div>
              <div class="col-md-3">
                <label><input type="radio">Partnership</label>
              </div>
              <div class="col-md-3">
                <label><input type="radio">Family Bussiness</label>
              </div>
              <div class="col-md-3 no_padding">
                <label><input type="radio">Others</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Nature Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio">Whole Seller</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio">Distributor</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio">Retailer</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  <div class="col-md-4 mrgn">
                      <label>Affiliations Year (if partner is selected)</label>
              <input type="text" name="name" class="form-control">
                    </div>
                  
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="agri_area mrgn pro_radio">
                <h3>(IF Agricultural Land Owner is Checked)</h3>
                  <div class="row">
                    <div class="col-md-6">
                      <label>Address Of Land</label>
                      <input type="text" class="form-control">
                    </div>
                    <div class="col-md-6">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  </div>
                </div>
                
                <div class="self_area mrgn pro_radio">
                <h3>(IF Self Employed is Checked)</h3>
                  <div class="row">
                    <div class="col-md-4">
              <label>Bussiness's Name</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Bussiness Address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Phone Number (Landline)</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Monthly Income Estimated</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business</label>
              <input type="text" name="name" class="form-control">
                    </div>                  
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Category Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio">Shop</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio">Factory</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio">Services Provider</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  
                  <div class="col-md-8 mrgn">
              <div class="col-md-12"><label>Type Of Bussiness</label></div>
              <div class="col-md-3">
                <label><input type="radio">Sole Proprietory</label>
              </div>
              <div class="col-md-3">
                <label><input type="radio">Partnership</label>
              </div>
              <div class="col-md-3">
                <label><input type="radio">Family Bussiness</label>
              </div>
              <div class="col-md-3 no_padding">
                <label><input type="radio">Others</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Nature Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio">Whole Seller</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio">Distributor</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio">Retailer</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  <div class="col-md-4 mrgn">
                      <label>Affiliations Year (if partner is selected)</label>
              <input type="text" name="name" class="form-control">
                    </div>
                  
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="student_area mrgn pro_radio">
                <h3>(IF Student is Checked)</h3>
                  <div class="row">
                    <div class="col-md-4">
              <label>Collage / Institute Name</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Department of Study</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Pursuing Degree</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Year of Study Left</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Phone Number</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="bank_info mrgn pro_radio">
                  <div class="row mrgn">
                    <div class="col-md-4">
                    <label>Do you have an Active Bank Account Detail</label>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-6"><label><input type="radio">Yes</label></div>
                    <div class="col-md-6"><label><input type="radio">No</label></div>
                  </div>
                  </div>
                  <div class="row mrgn">
                    <div class="col-md-4">
                    <label>Can you provide a cheque against balance of payment</label>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-6"><label><input type="radio">Yes</label></div>
                    <div class="col-md-6"><label><input type="radio">No</label></div>
                  </div>
                  </div>
                  <div class="row mrgn">
                    <div class="col-md-4">
                    <label>Commitment to live at the same residency</label>
                  </div>
                  <div class="col-md-6">
                    <input type="text" class="form-control">
                  </div>
                  </div>
                </div>
                
                <div class="col-md-12 checklist">
                <h3>Commitment to the Location of his Residency.</h3>
                  <p>A customer who has strong
ties to his residence does not lead to a bad debt situation. Ideally, a person
who lives in his owned (not rented) house with somebody of his extending
family living under one roof with him or nearby has lessor probability for
defaulting on his payment. He can also be easily reached in case of non-
payment. Similarly migrated people from other nearby regions are usually
avoided for sale because they may take the motorcycle to their hometown
to avoid repossession in case of default.
The following questions needs to be asked before making a risk assessment
of the customer profile</p>
               <div class="chckoptions pro_radio">
                <div class="col-md-12"><label><input type="checkbox">Is the Client a permanent resident of his stated address (stated on ID card)?</label></div>
                <div class="col-md-12"><label><input type="checkbox">Is the Current Residence in name of the customer?</label></div>
                <div class="col-md-12"><label><input type="checkbox">(If not) Is the name of the owner relative or Family Head of the customer?</label></div>
                <div class="col-md-12"><label><input type="checkbox">Does the client have a rented the property?</label></div>
                <div class="col-md-12"><label><input type="checkbox">In case of rented property, how much Rent does client pay as cash outflows?</label></div>
                <div class="col-md-12"><label><input type="checkbox">How long the client has been living in the premises?</label></div>
                <div class="col-md-12"><label><input type="checkbox">Is the Clients Extended Family living with him, nearby or within the city?</label></div>
                <div class="col-md-12"><label><input type="checkbox">Picture of Residence Uploaded for cross-reference has to be taken.</label></div>
                <div class="col-md-12"><label><input type="checkbox">Nearby Landmark/Business Shop close to residence should be mentioned.</label></div>
               </div>
               <div class="scale mrgn">
                <label>On a scale of 1-5, How sure you are that the Client is likely to remain a resident of verified premises during the lease period? (1 being Not Sure at all)</label>
                <div class="col-md-6">
                    <select class="form-control">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                </select>
                </div>
               </div>
                </div>
                
                <div class="col-md-12 checklist">
                <h3>Commitment to his Profession/Employment</h3>
                  <p>A stable business/employability or strong professional ties proves in avoiding bad debt situation. A person working for one organization for more than three years has a better chance of payment on time. For e.g. a shop owner with stock worth more than 500,000 is less likely to leave/change his professional commitment during first year of payments with our company. Similarly a property dealer, marketing agent or service provider has lessor commitment to work place and needs to have higher favorable score against other scorecard attributes. </p>
                  <p>Usually, a customer has a following professional categories :</p>
            <div class="list_itms">
              <ul>
                <li><strong>Government Employee </strong>– These are best kind of customers for financing but they shouldn't be of any forces i.e. Police, Army etc as forces can easily be posted to far places.</li>
                <li><strong>Shop Owner</strong> – In the business of selling (Non-Dealer of motorcycle)</li>
                <li><strong>Service Provider </strong> People who are not selling any product i.e. Property Dealer, Tailor, Hair dresser etc. (Lawyers are usually avoided) </li>
                <li><strong>Private Employee  </strong> People who work as a private employee for another employer i.e. Salesman, Accountant, Executives etc.</li>
                <li><strong>Agricultural Land Owner   </strong> People who are generally dependent on income from Agriculture.</li>
                <li><strong>Student  </strong> People who are studding in local college/universities </li>
                <li><strong>Others  </strong> Any other type if undecided of the above. </li>
              </ul>
              <img src="{{ base_url('assets/images/govt_img.png') }}" width="70%">
              <p><strong>Note</strong> Incase of less than two Years of Professional Commitment - Last employment or business detail with reference is required for cross checking</p>
            </div> 
                      
               <div class="scale mrgn">
                <label>On a Scale of 1-10 (How confident is the verification officer that the client professional profile will not change during the lease period)? (1 being Not confidant at all)</label>
                <div class="col-md-6">
                    <select class="form-control">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
                </div>
               </div>
                </div>
                
                <div class="col-md-12 checklist">
                <h3>Profiling of Customer Income & Seriousness to Pay</h3>
                  <p>Determining the income of the customer with respect to his expenses makes the most out of the verification process. Similarly, assessment of his seriousness to payback his debt needs to be taken into account along with post dated cheques of installment /security cheque from the customer or guarantor. </p>
                  <h4>Profiling of Customer Income by Preparing his Income Statement</h4>
                  <p>Following model can be employed in determining the income of the potential client.</p>
            
                     <div class="col-md-12 no_padding mrgn">
                      <div class="col-md-6">
                        <label>Net Salary</label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-6">
                        <label>(or)Net Salary</label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Average Monthly Sales</label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Less Operational </label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Net Cash flows after Operational Cost </label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Less Unavoidable Expenses  </label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Net free Cash flows before savings  </label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="clearfix"></div>
                     </div>
                      
               <div class="scale mrgn">
                <label>On a Scale of 1-10 (Is the Client present to be serious in matters of debt payback i.e How likely the customer will payback his debt to the company on time) </label>
                <div class="col-md-6">
                    <select class="form-control">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
                </div>
               </div>
                </div>
                <div class="clearfix"></div>
                </div>
                <div class="hold2 mrgn">
                  <div class="sub_heading">
                  <h3>Guarantor Information</h3>
                </div>
        <div class="row">
          <div class="col-md-8">
            <div class="col-md-4">
          <label>Name</label>
            <input type="text" name="name" class="form-control">
          </div>
          <div class="col-md-4">
          <label>Father's / Husband's Name</label>
            <input type="text" name="name" class="form-control">
          </div>
          <div class="col-md-4">
          <label>CNIC #</label>
            <input type="text" name="name" class="form-control">
          </div>
          <div class="col-md-4 mrgn">
          <label>Relationship with Applicant</label>
            <input type="text" name="name" class="form-control">
          </div>
          
          <div class="col-md-4 mrgn">
          <label>Phone Number (Landline)</label>
            <input type="text" name="name" class="form-control">
          </div>
          <div class="col-md-4 mrgn">
          <label>Mobile Number</label>
            <input type="text" name="name" class="form-control">
          </div>
          
          <div class="col-md-4 mrgn">
          <label>Current Address as on CNIC</label>
            <input type="text" name="curr_add" class="form-control">
          </div>
          <div class="col-md-4 mrgn">
          <label>Permanent Address as on CNIC</label>
            <input type="text" name="per_add" class="form-control">
          </div>
          <div class="col-md-4 mrgn">
          <label>Attachments</label>
            <input type="file" name="upload" class="form-control">
          </div>
          </div>
          <div class="col-md-4">
            <div class="col-md-12 mrgn">
          <label>Current Address on Map</label>
            <div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="250" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.pureblack.de">pure black</a></div><style>.mapouter{text-align:right;height:250px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:250px;width:100%;}</style></div>
          </div>
          </div>
        </div>
               
            <div class="sub_heading">
                  <h3>Professional Details</h3>
                </div>
        <div class="pro_radio">
          <div class="col-md-3">
          <label><input type="radio" name="pro_det">Government Employee</label>
          </div>  
          <div class="col-md-3">
            <label><input type="radio" name="pro_det">Private Employee</label>
          </div>
          <div class="col-md-3">
            <label><input type="radio" name="pro_det">Business – Shop Owner / Service Provider</label>
          </div>
          <div class="col-md-3">
            <label><input type="radio" name="pro_det">Agricultural Land Owner</label>
          </div>    
          <div class="col-md-3">
            <label><input type="radio" name="pro_det">Self Employed – Freelancer/Service</label>
          </div>    
          <div class="col-md-3">
            <label><input type="radio" name="pro_det">Student</label>
          </div>    
          
          <div class="clearfix"></div>  
        </div>                  
                
                <div class="organization_area mrgn pro_radio">
                <h3>(IF Government Employees is Checked)</h3>
                  <div class="row">
                    <div class="col-md-4">
              <label>Organization Name</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Department</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Scale / Grade</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Year of Service</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Monthly Salary</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Office Address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Active Number of years for you on your business address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Landline</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Status</label></div>
              <div class="col-md-6">
                <label><input type="radio" name="pro_det">Full Time</label>
              </div>
              <div class="col-md-6">
                <label><input type="radio" name="pro_det">Contract / Part Time</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
                    <label>(if contract/part time is checked) Incase of Contractual Agreement Year of Contaract commitment left</label>
                    <input type="text" class="form-control">
                  </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="private_area mrgn pro_radio">
                <h3>(IF Private Employees is Checked)</h3>
                  <div class="row">
                    <div class="col-md-4">
              <label>Employer's Name</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Department</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Designation</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Year of Service</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Monthly Salary</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Office Address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Phone Number (Landline)</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Status</label></div>
              <div class="col-md-6">
                <label><input type="radio" name="pro_det">Full Time</label>
              </div>
              <div class="col-md-6">
                <label><input type="radio" name="pro_det">Contract / Part Time</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-12 mrgn">
                    <label>(if contract/part time is checked) Incase of Contractual Agreement Year of Contaract commitment left</label>
                    <input type="text" class="form-control">
                  </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="bussiness_area mrgn pro_radio">
                <h3>(IF Business – Shop Owner / Service Provider is Checked)</h3>
                  <div class="row">

                    <div class="col-md-4">
              <label>Bussiness's Name</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Bussiness Address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Phone Number (Landline)</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Monthly Income Estimated</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business</label>
              <input type="text" name="name" class="form-control">
                    </div>                  
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Category Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio" name="pro_det">Shop</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio" name="pro_det">Factory</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio" name="pro_det">Services Provider</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  
                  <div class="col-md-8 mrgn">
              <div class="col-md-12"><label>Type Of Bussiness</label></div>
              <div class="col-md-3">
                <label><input type="radio">Sole Proprietory</label>
              </div>
              <div class="col-md-3">
                <label><input type="radio">Partnership</label>
              </div>
              <div class="col-md-3">
                <label><input type="radio">Family Bussiness</label>
              </div>
              <div class="col-md-3 no_padding">
                <label><input type="radio">Others</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Nature Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio">Whole Seller</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio">Distributor</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio">Retailer</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  <div class="col-md-4 mrgn">
                      <label>Affiliations Year (if partner is selected)</label>
              <input type="text" name="name" class="form-control">
                    </div>
                  
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="agri_area mrgn pro_radio">
                <h3>(IF Agricultural Land Owner is Checked)</h3>
                  <div class="row">
                    <div class="col-md-6">
                      <label>Address Of Land</label>
                      <input type="text" class="form-control">
                    </div>
                    <div class="col-md-6">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  </div>
                </div>
                
                <div class="self_area mrgn pro_radio">
                <h3>(IF Self Employed is Checked)</h3>
                  <div class="row">
                    <div class="col-md-4">
              <label>Bussiness's Name</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Bussiness Address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Phone Number (Landline)</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Monthly Income Estimated</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business address</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
                      <label>Active Number of years for you on your business</label>
              <input type="text" name="name" class="form-control">
                    </div>                  
            <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Category Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio">Shop</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio">Factory</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio">Services Provider</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  
                  <div class="col-md-8 mrgn">
              <div class="col-md-12"><label>Type Of Bussiness</label></div>
              <div class="col-md-3">
                <label><input type="radio">Sole Proprietory</label>
              </div>
              <div class="col-md-3">
                <label><input type="radio">Partnership</label>
              </div>
              <div class="col-md-3">
                <label><input type="radio">Family Bussiness</label>
              </div>
              <div class="col-md-3 no_padding">
                <label><input type="radio">Others</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <div class="col-md-12"><label>Nature Of Bussiness</label></div>
              <div class="col-md-4">
                <label><input type="radio">Whole Seller</label>
              </div>
              <div class="col-md-4">
                <label><input type="radio">Distributor</label>
              </div>
              <div class="col-md-4 no_padding">
                <label><input type="radio">Retailer</label>
              </div>
              <div class="clearfix"></div>
            </div>
                  <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                  <div class="col-md-4 mrgn">
                      <label>Affiliations Year (if partner is selected)</label>
              <input type="text" name="name" class="form-control">
                    </div>
                  
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="student_area mrgn pro_radio">
                <h3>(IF Student is Checked)</h3>
                  <div class="row">
                    <div class="col-md-4">
              <label>Collage / Institute Name</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Department of Study</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4">
                      <label>Pursuing Degree</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Year of Study Left</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Phone Number</label>
              <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-md-4 mrgn">
              <label>Attachments</label>
              <input type="file" name="upload" class="form-control">
            </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="bank_info mrgn pro_radio">
                  <div class="row mrgn">
                    <div class="col-md-4">
                    <label>Do you have an Active Bank Account Detail</label>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-6"><label><input type="radio">Yes</label></div>
                    <div class="col-md-6"><label><input type="radio">No</label></div>
                  </div>
                  </div>
                  <div class="row mrgn">
                    <div class="col-md-4">
                    <label>Can you provide a cheque against balance of payment</label>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-6"><label><input type="radio">Yes</label></div>
                    <div class="col-md-6"><label><input type="radio">No</label></div>
                  </div>
                  </div>
                  <div class="row mrgn">
                    <div class="col-md-4">
                    <label>Commitment to live at the same residency</label>
                  </div>
                  <div class="col-md-6">
                    <input type="text" class="form-control">
                  </div>
                  </div>
                </div>
                
                <div class="col-md-12 checklist">
                <h3>Commitment to the Location of his Residency.</h3>
                  <p>A customer who has strong
ties to his residence does not lead to a bad debt situation. Ideally, a person
who lives in his owned (not rented) house with somebody of his extending
family living under one roof with him or nearby has lessor probability for
defaulting on his payment. He can also be easily reached in case of non-
payment. Similarly migrated people from other nearby regions are usually
avoided for sale because they may take the motorcycle to their hometown
to avoid repossession in case of default.
The following questions needs to be asked before making a risk assessment
of the customer profile</p>
               <div class="chckoptions pro_radio">
                <div class="col-md-12"><label><input type="checkbox">Is the Client a permanent resident of his stated address (stated on ID card)?</label></div>
                <div class="col-md-12"><label><input type="checkbox">Is the Current Residence in name of the customer?</label></div>
                <div class="col-md-12"><label><input type="checkbox">(If not) Is the name of the owner relative or Family Head of the customer?</label></div>
                <div class="col-md-12"><label><input type="checkbox">Does the client have a rented the property?</label></div>
                <div class="col-md-12"><label><input type="checkbox">In case of rented property, how much Rent does client pay as cash outflows?</label></div>
                <div class="col-md-12"><label><input type="checkbox">How long the client has been living in the premises?</label></div>
                <div class="col-md-12"><label><input type="checkbox">Is the Clients Extended Family living with him, nearby or within the city?</label></div>
                <div class="col-md-12"><label><input type="checkbox">Picture of Residence Uploaded for cross-reference has to be taken.</label></div>
                <div class="col-md-12"><label><input type="checkbox">Nearby Landmark/Business Shop close to residence should be mentioned.</label></div>
               </div>
               <div class="scale mrgn">
                <label>On a scale of 1-5, How sure you are that the Client is likely to remain a resident of verified premises during the lease period? (1 being Not Sure at all)</label>
                <div class="col-md-6">
                    <select class="form-control">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                </select>
                </div>
               </div>
                </div>
                
                <div class="col-md-12 checklist">
                <h3>Commitment to his Profession/Employment</h3>
                  <p>A stable business/employability or strong professional ties proves in avoiding bad debt situation. A person working for one organization for more than three years has a better chance of payment on time. For e.g. a shop owner with stock worth more than 500,000 is less likely to leave/change his professional commitment during first year of payments with our company. Similarly a property dealer, marketing agent or service provider has lessor commitment to work place and needs to have higher favorable score against other scorecard attributes. </p>
                  <p>Usually, a customer has a following professional categories :</p>
            <div class="list_itms">
              <ul>
                <li><strong>Government Employee </strong>– These are best kind of customers for financing but they shouldn't be of any forces i.e. Police, Army etc as forces can easily be posted to far places.</li>
                <li><strong>Shop Owner</strong> – In the business of selling (Non-Dealer of motorcycle)</li>
                <li><strong>Service Provider </strong> People who are not selling any product i.e. Property Dealer, Tailor, Hair dresser etc. (Lawyers are usually avoided) </li>
                <li><strong>Private Employee  </strong> People who work as a private employee for another employer i.e. Salesman, Accountant, Executives etc.</li>
                <li><strong>Agricultural Land Owner   </strong> People who are generally dependent on income from Agriculture.</li>
                <li><strong>Student  </strong> People who are studding in local college/universities </li>
                <li><strong>Others  </strong> Any other type if undecided of the above. </li>
              </ul>
              <img src="{{ base_url('assets/images/govt_img.png') }}" width="70%">
              <p><strong>Note</strong> Incase of less than two Years of Professional Commitment - Last employment or business detail with reference is required for cross checking</p>
            </div> 
                      
               <div class="scale mrgn">
                <label>On a Scale of 1-10 (How confident is the verification officer that the client professional profile will not change during the lease period)? (1 being Not confidant at all)</label>
                <div class="col-md-6">
                    <select class="form-control">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
                </div>
               </div>
                </div>
                
                <div class="col-md-12 checklist">
                <h3>Profiling of Customer Income & Seriousness to Pay</h3>
                  <p>Determining the income of the customer with respect to his expenses makes the most out of the verification process. Similarly, assessment of his seriousness to payback his debt needs to be taken into account along with post dated cheques of installment /security cheque from the customer or guarantor. </p>
                  <h4>Profiling of Customer Income by Preparing his Income Statement</h4>
                  <p>Following model can be employed in determining the income of the potential client.</p>
            
                     <div class="col-md-12 no_padding mrgn">
                      <div class="col-md-6">
                        <label>Net Salary</label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-6">
                        <label>(or)Net Salary</label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Average Monthly Sales</label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Less Operational </label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Net Cash flows after Operational Cost </label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Less Unavoidable Expenses  </label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-6 mrgn">
                        <label>Net free Cash flows before savings  </label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="clearfix"></div>
                     </div>
                      
               <div class="scale mrgn">
                <label>On a Scale of 1-10 (Is the Client present to be serious in matters of debt payback i.e How likely the customer will payback his debt to the company on time) </label>
                <div class="col-md-6">
                    <select class="form-control">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
                </div>
               </div>
                </div>
                <div class="clearfix"></div>
                </div>
                <div class="hold3 mrgn pro_radio">
                <h3>Final Assesment</h3>
                  <div class="col-md-12"><label><input type="checkbox">CHAMPION CUSTOMERS</label></div>
                  <div class="col-md-12"><label><input type="checkbox">GOOD CUSTOMERS</label></div>
                  <div class="col-md-12"><label><input type="checkbox">AFFORDABLE CUSTOMERS</label></div>
                  <div class="col-md-12"><label><input type="checkbox">IGNORABLE CUSTOMERS</label></div>
                  <div class="clearfix"></div>
                </div>
                <div class="mrgn final_btn">
                  <div class="col-md-2">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button>
                  </div>
                  <div class="col-md-2">
                    <button type="button" name="verify" class="btn btn-default">Verify</button>
                  </div>
                  <div class="clearfix"></div>
                </div>
                
                
                
                <div class="form-group">
                  <label for="parent_id">ID</label>
                </div>
                
                <div class="form-group">
                  <label for="parent_id">Vendor</label>
                 <?php echo $orders->vendor->business_name; ?>
                </div>
                
                <div class="form-group">
                  <label for="parent_id">Product</label>
                 <?php echo $orders->product->title; ?>
                </div>
                
                <div class="form-group">
                  <label for="parent_id">Total Installments</label>
                  <?php echo $orders->productOffer->total_installments; ?>
                </div>
                
                  <div class="form-group">
                   <label for="parent_id">Down Payment</label>
                  <?php echo number_format($orders->productOffer->down_payment) . _RUPPEE_SIGN_; ?>
                </div>
                <div class="form-group">
                  <label for="parent_id">Amount Per Month</label>
                  <?php echo number_format($orders->productOffer->amount_per_month) . _RUPPEE_SIGN_ ; ?>
                </div>
                
                
                <?php /*?><div class="form-group">
                  <label for="parent_id">Verification Type</label>
                  
                @if($orders->verification_type == 'phone_verification')
                  Phone Verification
                @elseif($orders->verification_type == 'physical_verification')
                  Physical Verification
                @else
                  Free
                @endif
                </div><?php */?>
                <?php /*?><div class="form-group">
                  <label for="parent_id">Verification Status</label>
                  <td><?php echo $orders->verification_status; ?></td>
                </div><?php */?>
                
                <div class="form-group">
                  <label for="parent_id">Customer Comment</label>
                 <?php echo $orders->customer_comment; ?>
                </div>
                <div class="form-group">
                  <label for="parent_id">Admin comment</label>
                 <?php echo $orders->admin_comment; ?>
                </div>
                
                 <div class="form-group">
                  <label for="VerificationType">Verification Type</label>
                    @if($orders->verification_type == 'phone_verification')
                  Phone Verification
                @elseif($orders->verification_type == 'physical_verification')
                  Physical Verification
                @else
                  Free
                @endif
                 <?php /*?><select class="form-control" id="VerificationType" name="VerificationType">
          <option value="free">Free</option>
                    <option value="phone_verification">Phone Verification</option>
                    <option value="physical_verification">Physical Verification</option>
                  </select><?php */?>
                </div>
                <?php /*?><script>document.getElementById("VerificationType").value = '<?php echo $orders->verification_type; ?>'; </script><?php */?>
                
                 <div class="form-group">
                  <label for="VerificationStatus">Verification Status</label>
                  <?php echo $orders->verification_status; ?>
                 <?php /*?><select class="form-control" id="VerificationStatus" name="VerificationStatus">
          <option value="">Select</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                  </select><?php */?>
                </div>
                <?php /*?><script>document.getElementById("VerificationStatus").value = '<?php echo $orders->verification_status; ?>'; </script><?php */?>
                
                
                <div class="form-group">
                  <label for="Status">Status</label>
                 <select class="form-control" id="Status" name="Status">
          <option value="pending">Pending</option>
                    <option value="approved">Approved</option>
                    <option value="completed">Completed</option>
                  </select>
                  <script>document.getElementById("Status").value = '<?php echo $orders->status; ?>'; </script>
                </div>
                
                
                <div class="form-group">
                  <label for="title">Comment</label>
                  <div class="input-group">
                    <span id="descriptions">
                    <textarea name="vendor_comment" rows="5" class="form-control descriptionTextArea" id="vendor_comment" placeholder="Comment"><?php echo $orders->vendor_comment; ?></textarea>
                  </span></div>
                </div>
                </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>

@endsection
@push('css')
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/select2/dist/css/select2.min.css") }}">
  <style type="text/css">
    .add_row button, .remove_row button
    {
      margin-top: 42%;
    }
    .add_meta_row button, .remove_meta_row button
    {
      margin-top: 42%;
    }
    .nl-padding
    {
      padding-left: 0px;
    }
    .mrgn
{
  margin: 10px 0;
}
    .pro_radio input[type=radio]{
      margin-right: 10px;
    }
     .pro_radio input[type=checkbox]{
      margin-right: 10px;
    }
    .no_padding{
      padding: 0;
    }
    .scale select
    {
      height: 43px !important;
    }
    .final_btn button
    {
      width: 100%;
    }
    .phone_verification_form h1,h2,h3
    {
      text-align:  left;
      font-weight: 700;
      color:#3c8dbc;
    }
    .phone_verification_form h3
    {
      margin-top: 0;
    }
    .phone_verification_form label
    {
      font-weight: 400;
    }
    .phone_verification_form .sub_heading h3
    {
      color:#000;
      font-size: 22px;
    }
  </style>
@endpush
@push('scripts')
<script type="text/javascript" src="{{ base_url("assets/bower_components/select2/dist/js/select2.min.js") }}"></script>
<script type="text/javascript">
  $('select').select2();

  $('form').on('keyup', '.total_installments, .total_price_on_installment, .down_payment', function(){
    var node        = $(this).closest('.offer_row');
    var ti          = parseInt($(node).find('.total_installments').val());
    var tpi         = parseInt($(node).find('.total_price_on_installment').val());
    var dp          = parseInt($(node).find('.down_payment').val());
    var apm_node    = $(node).find('.amount_per_month');

    var result      = (tpi - dp) / ti;

    if(!isNaN(result))
      $(apm_node).val(Math.ceil(result));
  });

  $(document).on('click', '.add_meta_row', function(){
    var node  = $(this).closest('.meta_row');
    var clone = $(node).clone();
    $(this).removeClass('add_meta_row');
    $(this).addClass('remove_meta_row');
    $(node).find('button').html('<i class="fa fa-minus"></i>');
    $(node).find('button').removeClass('btn-primary');
    $(node).find('button').addClass('btn-danger');
    $(clone).find('input').val('');
    $(node).after(clone);
  });

  $(document).on('click', '.remove_meta_row', function(){
    var node      = $(this).closest('.meta_row');
    $(node).remove();
    var last_node = $('.meta_row').last();
    $(this).removeClass('remove_meta_row');
    $(this).addClass('add_meta_row');
    $(last_node).find('button').html('<i class="fa fa-plus"></i>');
    $(last_node).find('button').removeClass('btn-danger');
    $(last_node).find('button').addClass('btn-primary');
  });

  // $(document).on('click', '.add_row', function(){
  //   var node  = $(this).closest('.offer_row');
  //   var clone = $(node).clone();
  //   $(this).removeClass('add_row');
  //   $(this).addClass('remove_row');
  //   $(node).find('button').html('<i class="fa fa-minus"></i>');
  //   $(node).find('button').removeClass('btn-primary');
  //   $(node).find('button').addClass('btn-danger');
  //   $(clone).find('input').val('');
  //   $(node).after(clone);
  // });

  // $(document).on('click', '.remove_row', function(){
  //   var node      = $(this).closest('.offer_row');
  //   $(node).remove();
  //   var last_node = $('.offer_row').last();
  //   $(this).removeClass('remove_row');
  //   $(this).addClass('add_row');
  //   $(last_node).find('button').html('<i class="fa fa-plus"></i>');
  //   $(last_node).find('button').removeClass('btn-danger');
  //   $(last_node).find('button').addClass('btn-primary');
  // });
</script>
@endpush