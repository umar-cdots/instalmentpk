@extends('admin.layouts.master')

@section('title', "Verification Info - Order")
@section('page_heading', $order->product->title)
@section('page_sub_heading', "Verification Info")
@section('content')
<div class="row">
  <div class="col-md-12"> 
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border"> 
        <h3 class="box-title">Order Details</h3>
      </div>
      <!-- /.box-header --> 
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>ID</th>
            <td{{ $order->id }}></td>
            <th>Vendor</th>
            <td>{{ $order->vendor->business_name }}</td>
          </tr>
          <tr>
            <th align="left">Product</th>
            <td><a target="_blank" href="<?php echo base_url("offer/" . $order->productOffer->slug); ?>"><?php echo $order->product->title; ?></a></td>
            <th align="left">Total Installments</th>
            <td><?php echo $order->productOffer->total_installments; ?></td>
          </tr>
          <tr>
             <th align="left">Down Payment</th>
            <td><?php echo number_format($order->productOffer->down_payment) . _RUPPEE_SIGN_; ?></td>
            <th align="left">Amount Per Month</th>
            <td><?php echo number_format($order->productOffer->amount_per_month) . _RUPPEE_SIGN_ ; ?></td>
          </tr>
          <tr>
            <th align="left">Customer comment</th>
            <td colspan="3"><?php echo $order->customer_comment; ?></td>
          </tr>
          <tr>
            <th align="left">Vendor comment</th>
            <td colspan="3"><?php echo $order->vendor_comment; ?></td>
          </tr>
          <tr>
            <th align="left">Admin comment</th>
            <td colspan="3"><?php echo $order->admin_comment; ?></td>
          </tr>
          <tr>
            <th align="left">Verification Type</th>
            <td>
              @if($order->verification_type == 'phone_verification')
                Phone Verification
              @elseif($order->verification_type == 'physical_verification')
                Physical Verification
              @else
                Free
              @endif
            </td>
            <th align="left">Verification Status</th>
            <td>{{ str_replace('_', ' ', ucfirst($order->phoneVerification->verification_status ?? 'Pending')) }}</td>
          </tr>
          <tr>
            <th align="left">Order Status</th>
            <td>{{ str_replace('_', ' ', ucfirst($order->status)) }}</td>
            <th align="left">Date</th>
            <td><?php echo $order->created_at->format('Y-m-d'); ?></td>
          </tr>
        </table>
      </div>
    </div>
    <!-- /.box-body -->
    <div class="clearfix"></div>
    <div class="box box-primary">
      <div class="box-header with-border"> 
        <h3 class="box-title">
          @if($order->verification_type == 'phone_verification')
            Phone Verified Personal Info
          @elseif($order->verification_type == 'physical_verification')
            Physically Verified Personal Info
          @else
            Free
          @endif
      </h3>
      </div>
      <!-- /.box-header --> 
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Name</th>
            <td>{{ $order->phoneVerification->name}}</td>
            <th>Father's / Husband's Name</th>
            <td>{{ $order->phoneVerification->guardian_name}}</td>
          </tr>
          <tr>
            <th align="left">CNIC #</th>
            <td>{{ $order->phoneVerification->cnic_no}}</td>
            <th align="left">Nominee Name</th>
            <td>{{ $order->phoneVerification->nominee_name}}</td>
          </tr>
           <tr>
            <th>Nominee's Father / Husband Name</th>
            <td>{{ $order->phoneVerification->nominee_guardian_name}}</td>
            <th>Phone Number (Landline)</th>
            <td>{{ $order->phoneVerification->landline_phone}}</td>
          </tr>
           <tr>
            <th align="left">Mobile Number</th>
            <td>{{ $order->phoneVerification->mobile_phone}}</td>
            <th align="left">Current Address as on CNIC</th>
            <td>{{ $order->phoneVerification->current_address}}</td>
          </tr>
            <tr>
            <th>Permanent Address as on CNIC</th>
            <td>{{ $order->phoneVerification->permanent_address}}</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="clearfix"></div>
     <div class="box box-primary">
      <div class="box-header with-border"> 
        <h3 class="box-title">
         Commitment to the Location of his Residency.
      </h3>
      </div>
      <!-- /.box-header --> 
          @php
            $commitment_json = json_decode($order->phoneVerification->commitment_json, true);
            $scale            = 0;
          @endphp 
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <ul>
               @foreach($commitment_json as $location_commitment)
                @if(is_numeric($location_commitment))
                  @php 
                    $scale = $location_commitment;
                  @endphp
                @endif
                <li>{{ $location_commitment }}</li>
              @endforeach
            </ul>
          </tr>
         <tr>
            <th>Scale</th>
            <td>{{ $scale }}</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="clearfix"></div>
     <div class="box box-primary">
      <div class="box-header with-border"> 
        <h3 class="box-title">
         Commitment to his Profession/Employment
      </h3>
      </div>
      <!-- /.box-header --> 
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Commitment to his Profession/Employment (Scale)</th>
            <td>{{ $order->phoneVerification->profession_commitment }}</td>
          </tr>
        </table>
      </div>
    </div>
     <div class="clearfix"></div>
     <div class="box box-primary">
      <div class="box-header with-border"> 
        <h3 class="box-title">
         Profiling of Customer Income & Seriousness to Pay
      </h3>
      </div>
      <!-- /.box-header --> 
        @php
            $income_json  = json_decode($order->phoneVerification->income_json);
          @endphp
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Net Salary</th>
            <td>{{ $income_json->net_sallery}}</td>
            <th>(or)Net Salary</th>
            <td>{{ $income_json->or_net_sallery}}</td>
          </tr>
           <tr>
            <th align="left">Average Monthly Sales</th>
            <td>{{ $income_json->average_month_sallery}}</td>
            <th align="left">Less Operational </th>
            <td>{{ $income_json->opertion_less}}</td>
          </tr>
          <tr>
            <th>Net Cash flows after Operational Cost </th>
            <td>{{ $income_json->net_cash_flow}}</td>
            <th>Less Unavoidable Expenses </th>
            <td>{{ $income_json->lees_expenses}}</td>
          </tr>
           <tr>
            <th>Net free Cash flows before savings </th>
            <td>{{ $income_json->before_savings}}</td>
            <th>Scale </th>-
            <td>{{ $income_json->scale }}</td>
          </tr>
        </table>
      </div>
    </div>
     <div class="clearfix"></div>
     <div class="box box-primary">
      <div class="box-header with-border"> 
        <h3 class="box-title">
         Final Assesment
      </h3>
      </div>
      <!-- /.box-header --> 
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Reviw About Customer</th>
            <td>{{ ucwords(str_replace('_', ' ', $order->phoneVerification->assesment_type)) }}</td>
          </tr>
         
        </table>
      </div>
    </div>
     <div class="clearfix"></div>
    <div class="box box-primary">
      <div class="box-header with-border"> 
        <h3 class="box-title">Professional Details - {{ ucwords(str_replace('_', ' ', $order->phoneVerification->professional_type)) }}</h3>
      </div>
      @php
            $professional_json = json_decode($order->phoneVerification->profession_json);
      @endphp 
      @if($order->phoneVerification->professional_type == 'government_employee')
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Organization Name</th>
            <td>{{ $professional_json->personal_orgnization_name}}</td>
            <th>Department</th>
            <td>{{ $professional_json->personal_department_name}}</td>
          </tr>
          <tr>
            <th align="left">Scale / Grade</th>
            <td>{{ $professional_json->personal_scale}}</td>
            <th align="left">Year of Service</th>
            <td>{{ $professional_json->year_of_services}}</td>
          </tr>
           <tr>
            <th>Monthly Salary</th>
            <td>{{ $professional_json->personal_salary_month}}</td>
            <th>Office Address</th>
            <td>{{ $professional_json->personal_office_address}}</td>
          </tr>
           <tr>
            <th align="left">Active Number of years for you on your business address</th>
            <td>{{ $professional_json->personal_active_no_of_year_business}}</td>
            <th align="left">Landline</th>
            <td>{{ $professional_json->personal_landline_number}}</td>
          </tr>
          <tr>
            <th> Working Status</th>
            <td><a href="#" class="btn btn-xs btn-{{ $professional_json->ps_working_status ? 'success' : 'warning' }}">{{ $professional_json->ps_working_status ? 'Full Time' : 'Part Time/Contract' }}</a></td>
            <th>Do you have a bank account?</th>
            <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_has_bank_account ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_has_bank_account ? 'Yes' : 'No' }}</a></td>
          </tr>
           <tr>
            <th> Can you provide cheque?</th>
             <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_can_provide_cheque ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_can_provide_cheque ? 'Yes' : 'No' }}</a></td>
            <th>Do you have a bank account?</th>
            <td>{{$order->phoneVerification->ps_commitment_to_residency}}</td>
          </tr>
        </table>
      </div>
      @elseif($order->phoneVerification->professional_type == 'private_employee')
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Employer's Name</th>
            <td>{{ $professional_json->employer_name}}</td>
            <th>Department</th>
            <td>{{ $professional_json->personal_dep_name_employe}}</td>
          </tr>
          <tr>
            <th align="left">Designation</th>
            <td>{{ $professional_json->personal_designation_name}}</td>
            <th align="left">Year of Service</th>
            <td>{{ $professional_json->personal_service_year}}</td>
          </tr>
           <tr>
            <th>Monthly Salary</th>
            <td>{{ $professional_json->personal_month_sallery}}</td>
            <th>Office Address</th>
            <td>{{ $professional_json->personal_office_address}}</td>
          </tr>
           <tr>
            <th align="left">Phone Number (Landline)</th>
            <td>{{ $professional_json->personal_phone_landline}}</td>
            <th align="left"> Working Status </th>
            <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_working_status ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_working_status ? 'Full Time' : 'Part Time/Contract' }}</a></td>
          </tr>
          <tr>
            <th> Year of Contaract commitment left </th>
            <td>{{ $professional_json->commited_contarct_year}}</td>
            <th> Do you have an Active Bank Account Detail?</th>
            <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_has_bank_account ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_has_bank_account ? 'Yes' : 'No' }}
          </tr>
           <tr>
            <th> Can you provide cheque?</th>
            <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_can_provide_cheque ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_can_provide_cheque ? 'Yes' : 'No' }}</a></td>
            <th>  Commitment to live at the same residency </th>
            <td>{{ $order->phoneVerification->ps_commitment_to_residency}}</td>
          </tr>
        </table>
      </div>
       @elseif($order->phoneVerification->professional_type == 'business')
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Bussiness's Name</th>
            <td>{{ $professional_json->personal_name_of_businees}}</td>
            <th>Bussiness Address</th>
            <td>{{ $professional_json->personal_address_of_businees}}</td>
          </tr>
          <tr>
            <th align="left">Phone Number (Landline)</th>
            <td>{{ $professional_json->personal_business_landline}}</td>
            <th align="left">Monthly Income Estimated</th>
            <td>{{ $professional_json->personal_income_monthly}}</td>
          </tr>
           <tr>
            <th>Active Number of years for you on your business address</th>
            <td>{{ $professional_json->personal_active_years_business_one}}</td>
            <th>Active Number of years for you on your business</th>
            <td>{{ $professional_json->personal_active_years_business_two}}</td>
          </tr>
           <tr>
            <th align="left"> Category Of Bussiness </th>
            <td>{{ $professional_json->ps_category_of_bussiness}}</td>
            <th align="left"> Type Of Bussiness </th>
            <td>{{ $professional_json->ps_type_of_business}}</td>
          </tr>
          <tr>
            <th>Do you have a bank account?</th>
            <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_has_bank_account ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_has_bank_account ? 'Yes' : 'No' }}
            </a></td>
            <th>Can you provide cheque?</th>
             <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_can_provide_cheque ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_can_provide_cheque ? 'Yes' : 'No' }}
            </a></td>
          </tr>
           <tr>
            <th>Commitment to live at the same residency?</th>
            <td>{{ $order->phoneVerification->ps_commitment_to_residency}}</td>
          </tr>
        </table>
      </div>
       @elseif($order->phoneVerification->professional_type == 'agriculture')
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Address Of Land</th>
            <td>{{ $professional_json->personal_land_address}}</td>
            <th>Do you have a bank account?</th>
             <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_has_bank_account ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_has_bank_account ? 'Yes' : 'No' }}
            </a></td>
          </tr>
          <tr>
            <th align="left">Can you provide cheque?</th>
             <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_can_provide_cheque ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_can_provide_cheque ? 'Yes' : 'No' }}
            </a></td>
            <th align="left">Commitment to live at the same residency?</th>
            <td>{{ $order->phoneVerification->ps_commitment_to_residency}}</td>
          </tr>
        </table>
      </div>
       @elseif($order->phoneVerification->professional_type == 'self_employed')
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Bussiness's Name</th>
            <td>{{ $professional_json->personal_name_business}}</td>
            <th>Bussiness Address</th>
            <td>{{ $professional_json->personal_address_business}}</td>
          </tr>
          <tr>
            <th align="left">Phone Number (Landline)</th>
            <td>{{ $professional_json->personal_landline_conatct}}</td>
            <th align="left">Monthly Income Estimated</th>
            <td>{{ $professional_json->personal_estiamte_incom_month}}</td>
          </tr>
           <tr>
            <th>Active Number of years for you on your business address</th>
            <td>{{ $professional_json->personal_active_year_business}}</td>
            <th>Active Number of years for you on your business</th>
            <td>{{ $professional_json->personal_active_year_bus1}}</td>
          </tr>
           <tr>
            <th align="left">Category Of Bussiness</th>
            <td>{{ $professional_json->category_of_bussiness}}</td>
            <th align="left">Type Of Bussiness </th>
            <td>{{ $professional_json->type_of_business}}</td>
          </tr>
          <tr>
            <th>Do you have a bank account?</th>
            <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_has_bank_account ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_has_bank_account ? 'Yes' : 'No' }}
            </a></td>
            <th>Can you provide cheque?</th>
            <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_can_provide_cheque ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_can_provide_cheque ? 'Yes' : 'No' }}
            </a></td>
          </tr>
           <tr>
            <th>Commitment to live at the same residency?</th>
            <td>{{ $order->phoneVerification->ps_commitment_to_residency}}</td>
          </tr>
        </table>
      </div>
       @elseif($order->phoneVerification->professional_type == 'student')
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Collage / Institute Name</th>
            <td>{{ $professional_json->personal_college_name}}</td>
            <th>Department of Study</th>
            <td>{{ $professional_json->department_name}}</td>
          </tr>
          <tr>
            <th align="left">Pursuing Degree</th>
            <td>{{ $professional_json->pursuing_degree}}</td>
            <th align="left">Year of Study Left</th>
            <td>{{ $professional_json->year_left_study}}</td>
          </tr>
           <tr>
            <th>Phone Number</th>
            <td>{{ $professional_json->phone_number}}</td>
           <th>Do you have a bank account?</th>
           <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_has_bank_account ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_has_bank_account ? 'Yes' : 'No' }}
            </a></td>
          </tr>
           <tr>
            <th align="left">Can you provide cheque?</th>
            <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->ps_can_provide_cheque ? 'success' : 'warning' }}">{{ $order->phoneVerification->ps_can_provide_cheque ? 'Yes' : 'No' }}
            </a></td>
            <th align="left">Commitment to live at the same residency?</th>
            <td>{{ $order->phoneVerification->ps_commitment_to_residency}}</td>
          </tr>
        </table>
      </div>
      @endif
    </div>
     <div class="clearfix"></div>
    @foreach($order->guarantors as $guarantor)
      <div class="box box-primary">
        <div class="box-header with-border"> 
          <h3 class="box-title"><i>#{{ $loop->iteration }}</i> Guarantor Perssonal Info</h3>
        </div>
        <!-- /.box-header --> 
        <div class="box-body table-responsive no-padding">
          <table class="table table-bordered">
            <tr>
              <th>Name</th>
              <td>{{ $guarantor->name}}</td>
              <th>Father's / Husband's Name</th>
              <td>{{ $guarantor->guardian_name}}</td>
            </tr>
            <tr>
              <th align="left">CNIC #</th>
              <td>{{$guarantor->cnic_no}}</td>
              <th align="left">Relationship with Applicant</th>
              <td>{{ $guarantor->relation_with_applicant}}</td>
            </tr>
             <tr>
              <th>Phone Number (Landline)</th>
              <td>{{ $guarantor->landline_phone}}</td>
              <th>Mobile Number</th>
              <td>{{ $guarantor->mobile_phone}}</td>
            </tr>
            <tr>
              <th align="left">Current Address as on CNIC</th>
              <td>{{$guarantor->current_address}}</td>
              <th align="left">Permanent Address as on CNIC</th>
              <td>{{ $guarantor->permanent_address}}</td>
            </tr>
          </table>
        </div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
      <div class="box box-primary">
        <div class="box-header with-border"> 
          <h3 class="box-title"><i>#{{ $loop->iteration }}</i> Guarantor Professional Details - {{ ucwords(str_replace('_', ' ', $guarantor->professional_type)) }}</h3>
        </div>
        @php
                $professional_json = json_decode($guarantor->profession_json);
        @endphp
          @if($guarantor->professional_type == 'government_employee')
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
                <tr>
                  <th>Organization Name</th>
                  <td>{{ $professional_json->professional_organization_name}}</td>
                  <th>Department</th>
                  <td>{{ $professional_json->professional_department_name}}</td>
                </tr>
                <tr>
                  <th align="left">Scale / Grade</th>
                  <td>{{ $professional_json->professional_scale}}</td>
                  <th align="left">Year of Service</th>
                  <td>{{ $professional_json->professional_year_of_services}}</td>
                </tr>
                 <tr>
                  <th>Monthly Salary</th>
                  <td>{{ $professional_json->professional_salary_month}}</td>
                  <th>Office Address</th>
                  <td>{{ $professional_json->professional_office_address}}</td>
                </tr>
                 <tr>
                  <th align="left">Active Number of years for you on your business address</th>
                  <td>{{ $professional_json->professional_active_no_of_year_business}}</td>
                  <th align="left">Landline</th>
                  <td>{{ $professional_json->professional_landline_number}}</td>
                </tr>
                <tr>
                  <th> Working Status</th>
                  <td>{{ $professional_json->working_status}}</td>
                  <th>Do you have a bank account?</th>
                  <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->has_bank_account ? 'success' : 'warning' }}">{{ $order->phoneVerification->has_bank_account ? 'Yes' : 'No' }}</a></td>
                </tr>
                 <tr>
                  <th> Can you provide cheque?</th>
                   <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->can_provide_cheque ? 'success' : 'warning' }}">{{ $order->phoneVerification->can_provide_cheque ? 'Yes' : 'No' }}</a></td>
                  <th>Commitment to live at the same residency?</th>
                  <td>{{$order->phoneVerification->commitment_to_residency}}</td>
                </tr>
              </table>
            </div>
          @elseif($guarantor->professional_type == 'private_employee')
          <div class="box-body table-responsive no-padding">
            <table class="table table-bordered">
              <tr>
                <th>Employer's Name</th>
                <td>{{ $professional_json->personal_employer_name}}</td>
                <th>Department</th>
                <td>{{ $professional_json->personal_department_name}}</td>
              </tr>
              <tr>
                <th align="left">Designation</th>
                <td>{{ $professional_json->personal_designation}}</td>
                <th align="left">Year of Service</th>
                <td>{{ $professional_json->personal_services_year}}</td>
              </tr>
               <tr>
                <th>Monthly Salary</th>
                <td>{{ $professional_json->month_sallery }}</td>
                <th>Office Address</th>
                <td>{{ $professional_json->office_address_job}}</td>
              </tr>
               <tr>
                <th align="left">Phone Number (Landline)</th>
                <td>{{ $professional_json->office_phone_no}}</td>
                <th align="left"> Working Status </th>
                <td>{{ $order->phoneVerification->working_status}}</td>
              </tr>
              <tr>
                <th> Year of Contaract commitment left </th>
                <td>{{ $professional_json->contarct_commitment_prof}}</td>
                <th> Do you have an Active Bank Account Detail?</th>
                 <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->has_bank_account ? 'success' : 'warning' }}">{{ $order->phoneVerification->has_bank_account ? 'Yes' : 'No' }}</a></td>
              </tr>
               <tr>
                <th> Can you provide cheque?</th>
                 <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->can_provide_cheque ? 'success' : 'warning' }}">{{ $order->phoneVerification->can_provide_cheque ? 'Yes' : 'No' }}</a></td>
                <th>  Commitment to live at the same residency </th>
                <td>{{ $order->phoneVerification->commitment_to_residency}}</td>
              </tr>
            </table>
          </div>
          @elseif($guarantor->professional_type == 'business')
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Bussiness's Name</th>
            <td>{{ $professional_json->businees_name}}</td>
            <th>Bussiness Address</th>
            <td>{{ $professional_json->businees_address}}</td>
          </tr>
          <tr>
            <th align="left">Phone Number (Landline)</th>
            <td>{{ $professional_json->business_phone_no}}</td>
            <th align="left">Monthly Income Estimated</th>
            <td>{{ $professional_json->estimate_income_month}}</td>
          </tr>
           <tr>
            <th>Active Number of years for you on your business address</th>
            <td>{{ $professional_json->business_duration}}</td>
            <th>Active Number of years for you on your business</th>
            <td>{{ $professional_json->business_duration}}</td>
          </tr>
           <tr>
            <th align="left"> Category Of Bussiness </th>
            <td>{{ $professional_json->category_of_business}}</td>
            <th align="left"> Type Of Bussiness </th>
            <td>{{ $professional_json->type_of_business}}</td>
          </tr>
          <tr>
            <th>Do you have a bank account?</th>
             <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->has_bank_account ? 'success' : 'warning' }}">{{ $order->phoneVerification->has_bank_account ? 'Yes' : 'No' }}</a></td>
       
            <th>Can you provide cheque?</th>
            <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->can_provide_cheque ? 'success' : 'warning' }}">{{ $order->phoneVerification->can_provide_cheque ? 'Yes' : 'No' }}</a></td>
          </tr>
           <tr>
            <th>Commitment to live at the same residency?</th>
            <td>{{ $order->phoneVerification->commitment_to_residency}}</td>
          </tr>
        </table>
      </div>
        @elseif($guarantor->professional_type == 'agriculture')
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Address Of Land</th>
            <td>{{ $professional_json->land_address}}</td>
            <th>Do you have a bank account?</th>
             <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->has_bank_account ? 'success' : 'warning' }}">{{ $order->phoneVerification->has_bank_account ? 'Yes' : 'No' }}</a></td>
          </tr>
          <tr>
            <th align="left">Can you provide cheque?</th>
             <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->can_provide_cheque ? 'success' : 'warning' }}">{{ $order->phoneVerification->can_provide_cheque ? 'Yes' : 'No' }}</a></td>
            <th align="left">Commitment to live at the same residency?</th>
            <td>{{ $order->phoneVerification->commitment_to_residency}}</td>
          </tr>
        </table>
      </div>
          @elseif($guarantor->professional_type == 'self_employed')
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Bussiness's Name</th>
            <td>{{ $professional_json->business_name}}</td>
            <th>Bussiness Address</th>
            <td>{{ $professional_json->business_address}}</td>
          </tr>
          <tr>
            <th align="left">Phone Number (Landline)</th>
            <td>{{ $professional_json->landline_phone_no}}</td>
            <th align="left">Monthly Income Estimated</th>
            <td>{{ $professional_json->estimate_net_incom}}</td>
          </tr>
           <tr>
            <th>Active Number of years for you on your business address</th>
            <td>{{ $professional_json->business_year_one}}</td>
            <th>Active Number of years for you on your business</th>
            <td>{{ $professional_json->business_year_two}}</td>
          </tr>
           <tr>
            <th align="left">Category Of Bussiness</th>
            <td>{{ $professional_json->category_of_bussiness}}</td>
            <th align="left">Type Of Bussiness </th>
            <td>{{ $professional_json->type_of_business}}</td>
          </tr>
          <tr>
            <th>Do you have a bank account?</th>
              <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->has_bank_account ? 'success' : 'warning' }}">{{ $order->phoneVerification->has_bank_account ? 'Yes' : 'No' }}</a></td>
            <th>Can you provide cheque?</th>
             <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->can_provide_cheque ? 'success' : 'warning' }}">{{ $order->phoneVerification->can_provide_cheque ? 'Yes' : 'No' }}</a></td>
          </tr>
           <tr>
            <th>Commitment to live at the same residency?</th>
            <td>{{ $order->phoneVerification->commitment_to_residency}}</td>
          </tr>
        </table>
      </div>
          @elseif($guarantor->professional_type == 'student')
      <div class="box-body table-responsive no-padding">
        <table class="table table-bordered">
          <tr>
            <th>Collage / Institute Name</th>
            <td>{{ $professional_json->institute_name}}</td>
            <th>Department of Study</th>
            <td>{{ $professional_json->institute_department}}</td>
          </tr>
          <tr>
            <th align="left">Pursuing Degree</th>
            <td>{{ $professional_json->institute_degree}}</td>
            <th align="left">Year of Study Left</th>
            <td>{{ $professional_json->institute_year_left}}</td>
          </tr>
           <tr>
            <th>Phone Number</th>
            <td>{{ $professional_json->institute_phone}}</td>
           <th>Do you have a bank account?</th>
            <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->has_bank_account ? 'success' : 'warning' }}">{{ $order->phoneVerification->has_bank_account ? 'Yes' : 'No' }}</a></td>
          </tr>
           <tr>
            <th align="left">Can you provide cheque?</th>
             <td><a href="#" class="btn btn-xs btn-{{ $order->phoneVerification->can_provide_cheque ? 'success' : 'warning' }}">{{ $order->phoneVerification->can_provide_cheque ? 'Yes' : 'No' }}</a></td>
            <th align="left">Commitment to live at the same residency?</th>
            <td>{{ $order->phoneVerification->commitment_to_residency}}</td>
          </tr>
        </table>
      </div>
          @endif
      </div>
    @endforeach
    <!-- /.box-body -->
  </div>
  <!-- /.box --> 
</div>
@endsection
@push('css')
<!-- Icons -->
<link rel="stylesheet" href="{{ base_url("assets/css/style.css") }}">
<!-- Custom -->
<style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush