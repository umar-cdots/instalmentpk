@extends('admin.layouts.master')

@section('title', "Edit Order")
@section('page_heading', "Orders")
@section('page_sub_heading', "Edit")
@section('content')
<div class="row">
  <div class="col-md-12"> 
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border"> {{--
        <h3 class="box-title">Edit Ledger</h3>
        --}}
        <?php /*?><a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/ledger/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a><?php */?>
      </div>
      <!-- /.box-header --> 
      <!-- form start -->
      <form action="{{base_url(_ADMIN_ROUTE_PREFIX_ . '/orders/update/' . $orders->id)}}" method="post">
        <div class="box-body">
          <div class="form-group">
            <label for="parent_id">ID</label>
            {{$orders->id}} </div>
          <div class="form-group">
            <label for="parent_id">Vendor</label>
            <?php echo $orders->vendor->business_name; ?> </div>
          <div class="form-group">
            <label for="parent_id">Product</label>
            <?php echo $orders->product->title; ?> </div>
          <div class="form-group">
            <label for="parent_id">Total Installments</label>
            <?php echo $orders->productOffer->total_installments; ?> </div>
          <div class="form-group">
            <label for="parent_id">Down Payment</label>
            <?php echo number_format($orders->productOffer->down_payment) . _RUPPEE_SIGN_; ?> </div>
          <div class="form-group">
            <label for="parent_id">Amount Per Month</label>
            <?php echo number_format($orders->productOffer->amount_per_month) . _RUPPEE_SIGN_ ; ?> </div>
          <div class="form-group">
            <label for="parent_id">Customer comment</label>
            <?php echo $orders->customer_comment; ?> </div>
          <div class="form-group">
            <label for="parent_id">Vendor Comment</label>
            <?php echo $orders->vendor_comment; ?> </div>
          <div class="form-group">
            <h2 style="text-align:center">Phone Verified Data</h2>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
                <?php 
				$orderData = get_order_meta($orders->id, 'all',true);
				$orderData = arrayDecode($orderData);
				if($orderData){
				if(count($orderData) > 0 && !empty($orderData) )
				{
					foreach($orderData as $keys => $valus)
					{ ?>
                <tr>
                  <th align="left"><?php echo ucwords( str_replace('_',' ',$keys)); ?></th>
                  <td><?php echo $valus; ?></td>
                </tr>
                <?php /*?><div class="form-group">
                <label for="parent_id"><?php echo $keys; ?></label>
                <?php echo $valus; ?>
                </div><?php */?>
                <?php 
					}
				}
				}
				?>
              </table>
            </div>
          </div>
          <div class="form-group">
            <label for="VerificationType">Verification Type</label>
            <select class="form-control" id="VerificationType" name="VerificationType">
              <option value="free">Free</option>
              <option value="phone_verification">Phone Verification</option>
              <option value="physical_verification">Physical Verification</option>
            </select>
          </div>
          <script>document.getElementById("VerificationType").value = '<?php echo $orders->verification_type; ?>'; </script>
          <div class="form-group">
            <label for="VerificationStatus">Verification Status</label>
            <select class="form-control" id="VerificationStatus" name="VerificationStatus">
              <option value="">Select</option>
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </div>
          <script>document.getElementById("VerificationStatus").value = '<?php echo $orders->verification_status; ?>'; </script>
          <div class="form-group">
            <label for="title">Comment</label>
            <div class="input-group"> <span id="descriptions">
              <textarea name="admin_comment" rows="5" class="form-control descriptionTextArea" id="admin_comment" placeholder="Comment"><?php echo $orders->admin_comment; ?></textarea>
              </span></div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
    <!-- /.box --> 
  </div>
</div>
@endsection
@push('css') 
<!-- data Tabels -->
<link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
<!-- Icons -->
<link rel="stylesheet" href="{{ base_url("assets/css/style.css") }}">
<!-- Custom -->
<style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush
@push('scripts') 
<!-- DataTables --> 
<script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script> 
<script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script> 
<script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });
  $('#icon_class').keyup(function(){
    var icon_class = $(this).val();
    $(this).find('~span i').addClass(icon_class);
  });
</script> 
@endpush