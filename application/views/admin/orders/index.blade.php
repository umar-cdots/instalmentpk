@extends('admin.layouts.master')

@section('title', (!empty($verification_type) ? ucwords(str_replace('_', ' ', $verification_type)) . ' - ' : '') . "Orders List")
@section('page_heading', (!empty($verification_type) ? ucwords(str_replace('_', ' ', $verification_type)) . ' - ' : '') . "Orders")
@section('page_sub_heading', "List")
@section('content')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          {{-- <h3 class="box-title">Categories</h3> --}}
          <form class="form-inline" method="POST" action="" style="float: left;">
            <div class="form-group">
              <label for="user_id">Vendor:</label>
              <select name="vendor_id" class="form-control" id="vendor_id">
                <option value="">Select a Vendor</option>
                @foreach($vendors as $vendor)
                  <option value="{{ $vendor->id }}" {{ $vendor->id == $vendor_id ? 'selected' : '' }}>{{ $vendor->fullName() }} | {{ $vendor->businessName() }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="date_from">From:</label>
              <input type="date" class="form-control" name="date_from" id="date_from" value="{{ $date_from ?? '' }}" placeholder="Date From">
            </div>
            <div class="form-group">
              <label for="date_to">To:</label>
              <input type="date" class="form-control" name="date_to" id="date_to" value="{{ $date_to ?? '' }}" placeholder="Date From">
            </div>
            <button type="submit" class="btn btn-primary">Go</button>
          </form>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="orders" class="table table-bordered table-hover">
            <thead>
            <tr>
            			<th align="left">ID</th>
                        <th align="left">Vendor</th>
                        <th align="left">Product</th>
                        @if(empty($verification_type))
                          <th align="left">Verification Type</th>
                        @endif
                        <th align="left">Verification Status</th>
                        <th align="left">Order Status</th>
                        <th align="left">Date</th>
                        <th>Action</th>
                    </tr>
            </thead>
            <tbody>
            	@php
                $balance = 0;
              @endphp
              @foreach ($orders as $order)
              <tr>
                <td>{{$order->id}}</td>
                <td>{{ $order->vendor->business_name }}</td>
                <td>
                  <a target="_blank" href="{{ base_url("offer/" . $order->productOffer->slug) }}">{{ $order->product->title }}</a>
                </td>
                @if(empty($verification_type))
                  <td>
                  @if($order->verification_type == 'phone_verification')
                  	Phone Verification
                  @elseif($order->verification_type == 'physical_verification')
                  	Physical Verification
                  @else
                  	Free
                  @endif
                  </td>
                @endif
                <td>{{ str_replace('_', ' ', ucfirst($order->verification_type == 'phone_verification' ? ($order->phoneVerification->verification_status ?? 'Pending') : ($order->physicalVerification->verification_status ?? 'Pending'))) }}</td>                
                <td>{{ str_replace('_', ' ', ucfirst($order->status)) }}</td>
                <td>{{ $order->created_at->format('Y-m-d') }}</td>
                <td>
               <div class="btn-group"> 
                  <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/orders/" . $order->id)}}" >
                    <button class="btn btn-xs btn-success" type="button" data-toggle="tooltip" title="View Order"><i class="fa fa-eye"></i>
                    </button>
                  </a> 
                  <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/orders/verify/" . $order->id)}}">
                    <button class="btn btn-xs btn-warning" type="button" data-toggle="tooltip" title="Verify">
                      <i class="fa fa-certificate"></i>
                    </button>
                  </a>
                  <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/orders/verification/" . $order->id)}}" >
                    <button class="btn btn-xs btn-info" type="button" data-toggle="tooltip" title="Verification Info"><i class="fa fa-file"></i>
                    </button>
                  </a> 
                  <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/orders/edit/" . $order->id)}}" >
                    <button class="btn btn-xs btn-primary" type="button" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i>
                    </button>
                  </a> 
                  <?php /*?><a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/orders/remove/" . $order->id)}}" class="removeThisNode" >
                    <button class="btn btn-xs btn-danger remove" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                    </button>
                  </a><?php */?> 
                    </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Icons -->
  <link rel="stylesheet" href="{{ base_url("assets/css/style.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
      color: #fff;
    }
  </style>
@endpush

@push('scripts')
  <!-- DataTables -->
  <script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script type="text/javascript">
    $('#orders').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  </script>
@endpush