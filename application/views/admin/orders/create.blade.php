@extends('admin.layouts.master')

@section('title', "Add New Payment")
@section('page_heading', "Payments")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "Add New") --}}
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            {{-- <div class="box-header with-border">
              <h3 class="box-title">Add Payment</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
           <form action="{{base_url(_ADMIN_ROUTE_PREFIX_.'/ledger/store')}}" method="post">
           
           	
              <div class="box-body">
                <div class="form-group">
                  <label for="parent_id">Users</label>
                 <select class="form-control" id="user_id" name="user_id">
                    <option value="">Select</option>
                    @foreach ($vendors as $vendor)
                    <option value="{{$vendor->user->id}}">{{$vendor->fullName()}}</option>
                    @endforeach
                  </select>
                </div>
                <script>document.getElementById("user_id").value = '<?php echo set_value('user_id'); ?>';</script>
                
                
                <div class="form-group">
                  <label for="parent_id">Payment Type</label>
                 <select class="form-control" id="payment_type" name="payment_type">
                    <option value="">Select</option>
					<option value="debit">Debit</option>
                    <option value="credit">Credit</option>
                  </select>
                </div>
                <script>document.getElementById("payment_type").value = '<?php echo set_value('payment_type'); ?>';</script>
                
                <div class="form-group">
                  <label for="category_title">Amount</label>
                  <input type="text" class="form-control" name="amount" id="amount" value="<?php echo set_value('amount'); ?>" placeholder="Amount">
                </div>
                <div class="form-group">
                  <label for="title">Description</label>
                  <div class="input-group">
                    <span id="descriptions">
                    <textarea name="description" rows="5" class="form-control descriptionTextArea" id="description" placeholder="Description"><?php echo set_value('description'); ?></textarea>
                  </span></div>
                </div>
                <?php /*?>
				<div class="form-group">
                    <span id="description">
                    <label for="show_at_homepage">
                      <input type="checkbox" name="show_at_homepage" id="show_at_homepage" value="show">
                    </label>
                    </span>
                    <label for="show_at_homepage">
                    Show in Nav
                </label>
                </div>
                <div class="form-group">
                  <label for="sort">Order</label>
                  <input type="number" class="form-control" name="sort" id="sort" placeholder="Order No">
                </div>
				<?php */?>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>

@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Icons -->
  <link rel="stylesheet" href="{{ base_url("assets/css/style.css") }}">
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
<script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });
  $('#icon_class').keyup(function(){
    var icon_class = $(this).val();
    $(this).find('~span i').addClass(icon_class);
  });
</script>
@endpush