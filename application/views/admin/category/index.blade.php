@extends('admin.layouts.master')

@section('title', "Categories List")
@section('page_heading', "Categories")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "List") --}}
@section('content')
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              {{-- <h3 class="box-title">Categories</h3> --}}
              <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/category/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Icon</th>
                  <th>Category Name</th>
                  <th>Parent Category</th>
                  <th>Show in Nav</th>
                  <th>Order</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($categories as $category)
                  <tr>
                    <td>{{$category->id}}</td>
                    <td>
                      @if(!empty($category->icon_class))
                        <span class="{{ $category->icon_class }}"> {{ $category->icon_class }}</span>
                      @endif
                    </td>
                    <td>{{ $category->category_title }}</td>
                    <td>{{ !empty($category->parentCategory->category_title) ? $category->parentCategory->category_title : '-' }}</td>
                    <td>
                      @if(!empty($category->show_at_homepage))
                        <button class="btn btn-success btn-xs">True</button>
                      @else
                        <button class="btn btn-danger btn-xs">False</button>
                      @endif
                    </td>
                    <td>{{ !empty($category->sort) ? $category->sort : '-' }}</td>
                    <td class="text-center">
                      <div class="btn-group"> 
                        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/category/edit/" . $category->id) }}" >
                          <button class="btn btn-xs btn-primary" type="button" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i>
                          </button>
                        </a> 
                        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/category/remove/" . $category->id) }}" class="removeThisNode" >
                          <button class="btn btn-xs btn-danger" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                          </button>
                        </a> 
                    </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Icons -->
  <link rel="stylesheet" href="{{ base_url("assets/css/style.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
      color: #fff;
    }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
<script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1] /* 1st one, start by the right */
      }]

    });
  })
</script>
@endpush