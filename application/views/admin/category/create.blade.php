@extends('admin.layouts.master')

@section('title', "Add New Category")
@section('page_heading', "Categories")
@section('page_sub_heading', "Add New")
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Categories</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form action="{{base_url(_ADMIN_ROUTE_PREFIX_.'/category/store')}}" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="parent_id">Parent Category</label>
                 <select class="form-control" id="parent_id" name="parent_id">
                    <option value="">Select Parent Category</option>
                    @foreach ($categories as $cat)
                    <option value="{{$cat->id}}">{{$cat->category_title}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="category_title">Category Name</label>
                  <input type="text" class="form-control" name="category_title" id="category_title" placeholder="Category Title">
                </div>
                <div class="form-group">
                  <label for="title">Icon Class</label>
                  <div class="input-group">
                    <input type="text" class="form-control" name="icon_class" id="icon_class" placeholder="Icon Class">
                    <span class="input-group-addon"><i></i></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="show_at_homepage">
                    <input type="checkbox" name="show_at_homepage" id="show_at_homepage" value="show">
                    Show in Nav
                </label>
                </div>
                <div class="form-group">
                  <label for="sort">Order</label>
                  <input type="number" class="form-control" name="sort" id="sort" placeholder="Order No">
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>

@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Icons -->
  <link rel="stylesheet" href="{{ base_url("assets/css/style.css") }}">
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
<script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });
  $('#icon_class').keyup(function(){
    var icon_class = $(this).val();
    $(this).find('~span i').addClass(icon_class);
  });
</script>
@endpush