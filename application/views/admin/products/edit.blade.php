@extends('admin.layouts.master')

@section('title', "Edit Product")
@section('page_heading', "Products")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "Edit") --}}
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              {{-- <h3 class="box-title">Add Products</h3> --}}
              <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/products/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form action="{{base_url(_ADMIN_ROUTE_PREFIX_ . '/products/update/' . $product->id)}}" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="title">Product Title</label>
                  <input type="text" class="form-control" name="title" id="title" placeholder="Product Title" value="{{$product->title}}" required="">
                </div>
                <div class="form-group">
                  <label for="title">Image</label>
                  <div class="input-group image-preview">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                          <span class="glyphicon glyphicon-remove"></span> Clear
                      </button>
                      <div class="btn btn-default image-preview-input">
                          <span class="glyphicon glyphicon-folder-open"></span>
                          <span class="image-preview-input-title">Browse</span>
                          <input type="file" accept="image/png, image/jpeg, image/gif, image/jpg" name="images[]" multiple="" id="images"/>
                          {{-- <input type="hidden" name="product_images_id" value="{{ $product->productImages[0]->id }}"> --}}
                      </div>
                    </span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title">Image Preview</label>
                  <div class="col-md-12" id="preview_images">
                    @foreach($product->productImages as $image)
                      <div class="col-md-2 add_images">
                        <img src="{{ _MEDIA_UPLOAD_URL_ . "{$image->raw_name}310x310{$image->file_ext}" }}" alt="Preview" height="100px;">
                          <input type="hidden" name="image_obj[]" value="{{ base64_encode(serialize($image)) }}">
                          <i class="del_img remove_preview fa fa-times"></i>
                      </div>
                    @endforeach
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="category_id">Brand</label>
                  <select class="form-control" id="brand_id" name="brand_id">
                    <option value="">Select a Brand</option>
                    @foreach ($brands as $brand)
                      <option value="{{$brand->id}}" {{ $brand->id == $product->brand_id ? "selected" : ""}}>{{$brand->brand_title}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="category_id">Category Title</label>
                  <select class="form-control" id="category_id" name="category_id[]" multiple="" required="">
                    @foreach ($categories as $cat)
                    <option value="{{$cat->id}}" {{$product->productCategories->contains('category_title', $cat->category_title) ? 'selected' : ''}}>{{$cat->category_title}}</option>
                    @endforeach
                  </select>
                </div>
                @foreach($product->meta as $meta)
                  <div class="col-md-12 meta_row nl-padding">
                      <div class="col-md-4 nl-padding">
                        <div class="form-group">
                          <label for="meta_key">Feature Key</label>
                          <select name="meta_key[]" class="form-control meta_keys" data-default-id="{{ $meta->key_feature_id }}">
                            <option>Select Key Feature</option>
                          </select>
                          {{-- <input type="text" name="meta_key[]" class="form-control meta_key" value="{{ $meta->meta_key }}"> --}}
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="meta_value">Feature Description</label>
                          <input type="text" name="meta_value[]" class="form-control meta_value" value="{{ $meta->meta_value }}">
                        </div>
                      </div>
                      <div class="col-md-1 remove_row">
                        <button type="button" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                      </div>
                    <div class="clearfix"></div>
                  </div>
                @endforeach
                <div class="col-md-12 meta_row nl-padding">
                      <div class="col-md-4 nl-padding">
                        <div class="form-group">
                          <label for="meta_key">Feature Key</label>
                          <select name="meta_key[]" class="form-control meta_keys">
                            <option>Select Key Feature</option>
                          </select>
                          {{-- <input type="text" name="meta_key[]" class="form-control meta_key"> --}}
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="meta_value">Feature Description</label>
                          <input type="text" name="meta_value[]" class="form-control meta_value">
                        </div>
                      </div>
                      <div class="col-md-1 add_row">
                        <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                      </div>
                    <div class="clearfix"></div>
                  </div>
                <div class="form-group">
                  <label for="description">Description</label>
                  <textarea class="textarea" id="description" name="description"  placeholder="Product Description"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required="">{{$product->description}}</textarea>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection
@push('css')
  <style type="text/css">
    .add_row button, .remove_row button
    {
      margin-top: 22%;
    }
    .nl-padding
    {
      padding-left: 0px;
    }
    .del_img 
    {
      width: 10% !important;
      position: absolute;
      right: 5px;
      top: 2px;
      cursor: pointer;
    }
    .add_images 
    {
      position: relative;
      height: 110px;
    }
    .add_images img
    {
      position: absolute;
      top: 30%;
      transform: translate(0%, -30%);
      max-width: 130px;
    }
    .mrgn
    {
      margin-top: 20px;
    }
  </style>
@endpush
@push('scripts')
<script type="text/javascript">
  $(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
      function () {
         $('.image-preview').popover('show');
      }, 
       function () {
         $('.image-preview').popover('hide');
      }
    );    
  });
  $('.remove_preview').click(function(){
      $(this).parent().hide(500);
  });

  $(function() {
      // Create the close button
      // var closebtn = $('<button/>', {
      //     type:"button",
      //     text: 'x',
      //     id: 'close-preview',
      //     style: 'font-size: initial;',
      // });
      // closebtn.attr("class","close pull-right");
      // // Set the popover default content
      // $('.image-preview').popover({
      //     trigger:'manual',
      //     html:true,
      //     title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
      //     content: "There's no image",
      //     placement:'bottom'
      // });
      // // Clear event
      // $('.image-preview-clear').click(function(){
      //     $('.image-preview').attr("data-content","").popover('hide');
      //     $('.image-preview-filename').val("");
      //     $('.image-preview-clear').hide();
      //     $('.image-preview-input input:file').val("");
      //     $(".image-preview-input-title").text("Browse"); 
      // }); 
      // Create the preview image
      $(".image-preview-input input:file").change(function (){     
        var node      = $(this);
        var formData  = new FormData($(node).parents('form')[0]);

        $.ajax({
            url: "{{ base_url(_ADMIN_ROUTE_PREFIX_ . '/ajax/upload') }}",
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
              if(response.status == 'success')
              {
                toastr['success'](response.message);

                $.each(response.data, function(key, obj){
                  var html = '<div class="col-md-2 add_images">';
                  html    += '<img src="'+ obj.path +'" alt="Preview">';
                  html    += '<input type="hidden" name="image_obj[]" value="'+ obj.object +'">';
                  html    += '<i class="del_img remove_preview fa fa-times"></i>';
                  html    += '</div>';
                  $('#preview_images .clearfix').before(html);
                  $(node).val('');
                });
              }
              else
              {
                toastr['error'](response.message);
              }
            },
            error: function(response){
              toastr['error']("Something Went Wrong.");
            }
        });

          // var img = $('<img/>', {
          //     id: 'dynamic',
          //     width:250,
          //     height:200
          // });      
          // var file = this.files[0];
          // var reader = new FileReader();
          // // Set preview image into the popover data-content
          // reader.onload = function (e) {
          //     $(".image-preview-input-title").text("Change");
          //     $(".image-preview-clear").show();
          //     $(".image-preview-filename").val(file.name);            
          //     img.attr('src', e.target.result);
          //     $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
          // }        
          // reader.readAsDataURL(file);
      });  
  });

  $(document).on('click', '.add_row', function(){
    var node  = $(this).closest('.meta_row');
    var clone = $(node).clone();
    $(this).removeClass('add_row');
    $(this).addClass('remove_row');
    $(node).find('button').html('<i class="fa fa-minus"></i>');
    $(node).find('button').removeClass('btn-primary');
    $(node).find('button').addClass('btn-danger');
    $(clone).find('input').val('');
    $(node).after(clone);
  });

  $(document).on('click', '.remove_row', function(){
    var node      = $(this).closest('.meta_row');
    $(node).remove();
    var last_node = $('.meta_row').last();
    $(this).removeClass('remove_row');
    $(this).addClass('add_row');
    $(last_node).find('button').html('<i class="fa fa-plus"></i>');
    $(last_node).find('button').removeClass('btn-danger');
    $(last_node).find('button').addClass('btn-primary');
  });

  $('#category_id').click(function(){
    var category_ids = $(this).val().join(',');
    console.log(category_ids);
    if(category_ids != "")
    {
      $.ajax({
        url: "{{ base_url(_ADMIN_ROUTE_PREFIX_ . '/ajax/get_key_features') }}",
        type: "GET",
        data: "category_ids=" + category_ids,
        dataType: "json",
        success: function(response){
          if(response.status == 'success')
          {
            $('.meta_keys').html($("<option></option>").attr("value", "").text("Select Key Feature"));

            $(response.data).each(function(i, v){
              $('.meta_keys').append($("<option></option>").attr("value", v.id).text(v.title));
              console.log(v); 
            });

            $('.meta_keys').each(function(i, node){
              var default_id = $(node).data('default-id');
              console.log(default_id);
              if(default_id != "")
              {
                $(node).find('option[value="' + default_id + '"]').prop('selected', true);
              }
            });
          }
          else
          {
            toastr['error'](response.message);
          }
        },
        error: function(response){
          toastr['error']("Something Went Wrong. Please Try Again.");
        }
      });
    }
  });
  setTimeout(function(){
    $('#category_id').trigger('click');
  })
</script>
@endpush