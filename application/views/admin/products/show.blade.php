@extends('admin.layouts.master')

@section('title', "View Products")
@section('page_heading', "Products")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "View") --}}
@section('content')
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/products/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
                <tr>
                  <th>Product Title</th>
                  <td>{{$product->title}}</td>
                </tr>
                <tr>
                  <th>Image Preview</th>
                  <td>
                    @foreach($product->productImages as $image)
                      <img src="{{ is_file(_MEDIA_UPLOAD_PATH_ . "{$image->raw_name}310x310{$image->file_ext}") ? _MEDIA_UPLOAD_URL_ . "{$image->raw_name}310x310{$image->file_ext}" : _MEDIA_UPLOAD_URL_ . "no-preview.jpg" }}" width="250" height="200">
                    @endforeach
                  </td>
                </tr>
                <tr>
                  <th>Features</th>
                  <td>
                    <ul>
                    @foreach($product->meta as $meta)
                      <li>{{ $meta->meta_key }}: {{ $meta->meta_value }}</li>
                    @endforeach
                    </ul>
                  </td>
                </tr>
                <tr>
                  <th>Description</th>
                  <td>{{$product->description}}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td>{{$product->brand->brand_title}}</td>
                </tr>
                <tr>
                  <th>Categories</th>
                  <td>
                    <ul>
                    @foreach($product->productCategories as $category)
                      <li>{{$category->category_title}}</li>
                    @endforeach
                    </ul>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/products/showOffers/" . $product->id)}}">
                      <button class="btn btn-success"><i class="fa fa-eye"></i> View Offers</button>
                    </a>
                    <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/products/edit/" . $product->id)}}">
                      <button class="btn btn-primary"><i class="fa fa-edit"></i> Edit</button>
                    </a>
                    <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/products/remove/" . $product->id)}}">
                      <button class="btn btn-danger"><i class="fa fa-times"></i> Remove</button>
                    </a>
                  </td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      @endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush
@push('scripts')
  <!-- DataTables -->
  <script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script type="text/javascript">
      $('.remove').click(function(e){
        if(!confirm("Do You Really Want to Remove This?"))
          e.preventDefault();
      });
  </script>
@endpush
