@extends('admin.layouts.master')

@section('title', "Products List")
@section('page_heading', "Products")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "List") --}}
@section('content')
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              {{-- <h3 class="box-title">Products</h3> --}}
              <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/products/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="products" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Product Title</th>
                  <th>Brand</th>
                  <th>Category Title</th>
                  <th>Offers</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($products as $product)
                  <tr>
                    <td>{{$product->id}}</td>
                    <td>{{$product->title}}</td>
                    <td>{{$product->brand->brand_title ?? ''}}</td>
                    <td>{{$product->productCategories->implode('category_title', ', ')}}</td>
                    <td>{{$product->productOffers->count()}}</td>
                    <td class="text-center">
                      <div class="btn-group"> 
                        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/products/" . $product->id)}}" >
                          <button class="btn btn-xs btn-success" type="button" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i>
                          </button>
                        </a> 
                        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/products/edit/" . $product->id)}}" >
                          <button class="btn btn-xs btn-primary" type="button" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i>
                          </button>
                        </a> 
                        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/products/remove/" . $product->id)}}" class="removeThisNode" >
                          <button class="btn btn-xs btn-danger remove" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                          </button>
                        </a> 
                    </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush

@push('scripts')
  <!-- DataTables -->
  <script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script>
    $('#products').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1] /* 1st one, start by the right */
      }]
    });
    // $('.remove').click(function(e){
    //   if(!confirm("Do You Really Want to Remove This?"))
    //     e.preventDefault();
    // });
  </script>
@endpush