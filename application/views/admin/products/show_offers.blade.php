@extends('admin.layouts.master')

@section('title', $product->title)
@section('page_heading', $product->title)
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "List") --}}
@section('content')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          {{-- <h3 class="box-title">Products</h3> --}}
          <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/offers/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="offers" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>ID</th>
              <th>Sellers</th>
              <th>No. of Instalments</th>
              <th>Total Amount</th>
              <th>Down Payment</th>
              <th>Per Month</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($offers as $offer)
               @continue(empty($offer->vendor))
              <tr>
                <td>{{ $offer->id}}</td>
                <td>{{ $offer->vendor->fullName() . ' - ' . $offer->vendor->businessName() }}</td>
                <td>{{ $offer->total_installments}} Months</td>
                <td>{{ _RUPPEE_SIGN_ . $offer->total_price_on_installment}}</td>
                <td>{{_RUPPEE_SIGN_ . $offer->down_payment}}</td>
                <td>{{ _RUPPEE_SIGN_ . $offer->amount_per_month }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush

@push('scripts')
  <!-- DataTables -->
  <script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script>
    $('#offers').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1] /* 1st one, start by the right */
      }]

    });
  </script>
@endpush