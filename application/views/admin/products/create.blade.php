@extends('admin.layouts.master')

@section('title', "Add New Product")
@section('page_heading', "Products")
@section('page_sub_heading', "Add New")
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Products</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form action="{{base_url(_ADMIN_ROUTE_PREFIX_.'/products/store')}}" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="title">Product Title</label>
                  <input type="text" class="form-control" name="title" id="title" placeholder="Product Title" required="">
                </div>
                <div class="form-group">
                  <label for="title">Image</label>
                  <div class="input-group image-preview">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                          <span class="glyphicon glyphicon-remove"></span> Clear
                      </button>
                      <div class="btn btn-default image-preview-input">
                          <span class="glyphicon glyphicon-folder-open"></span>
                          <span class="image-preview-input-title">Browse</span>
                          <input type="file" accept="image/png, image/jpeg, image/gif, image/jpg" name="images[]" multiple="" id="images"/>
                      </div>
                    </span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title">Image Preview</label>
                  <div class="col-md-12" id="preview_images">
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="form-group mrgn">
                  <label for="category_id">Brand</label>
                  <select class="form-control" id="brand_id" name="brand_id">
                    <option value="">Select a Brand</option>
                    @foreach ($brands as $brand)
                      <option value="{{$brand->id}}">{{$brand->brand_title}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="category_id">Category</label>
                 <select class="form-control" id="category_id" name="category_id[]" multiple="" required="">
                    @foreach ($categories as $cat)
                      <option value="{{$cat->id}}">{{$cat->category_title}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-12 meta_row nl-padding">
                    <div class="col-md-4 nl-padding">
                      <div class="form-group">
                        <label for="meta_key">Feature Key</label>
                        <input type="text" name="meta_key[]" class="form-control meta_key">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="meta_value">Feature Description</label>
                        <input type="text" name="meta_value[]" class="form-control meta_value">
                      </div>
                    </div>
                    <div class="col-md-1 add_row">
                      <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                    </div>
                  <div class="clearfix"></div>
                </div>
                <div class="form-group">
                  <label for="description">Description</label>
                  <textarea class="textarea" name="description" placeholder="Product Description"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required=""></textarea>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection
@push('css')
  <style type="text/css">
    .add_row button, .remove_row button
    {
      margin-top: 42%;
    }
    .nl-padding
    {
      padding-left: 0px;
    }
    .del_img 
    {
      width: 10% !important;
      position: absolute;
      right: 5px;
      top: 2px;
      cursor: pointer;
    }
    .add_images 
    {
      position: relative;
      height: 110px;
    }
    .add_images img
    {
      position: absolute;
      top: 30%;
      transform: translate(0%, -30%);
      max-width: 130px;
    }
    .mrgn
    {
      margin-top: 20px;
    }
  </style>
@endpush
@push('scripts')
<script type="text/javascript">
  $(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
      function () {
         $('.image-preview').popover('show');
      }, 
       function () {
         $('.image-preview').popover('hide');
      }
    );    
  });
  $('.remove_preview').click(function(){
      $(this).parent().hide(500);
  });

  $(function() {
      // Create the close button
      // var closebtn = $('<button/>', {
      //     type:"button",
      //     text: 'x',
      //     id: 'close-preview',
      //     style: 'font-size: initial;',
      // });
      // closebtn.attr("class","close pull-right");
      // // Set the popover default content
      // $('.image-preview').popover({
      //     trigger:'manual',
      //     html:true,
      //     title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
      //     content: "There's no image",
      //     placement:'bottom'
      // });
      // // Clear event
      // $('.image-preview-clear').click(function(){
      //     $('.image-preview').attr("data-content","").popover('hide');
      //     $('.image-preview-filename').val("");
      //     $('.image-preview-clear').hide();
      //     $('.image-preview-input input:file').val("");
      //     $(".image-preview-input-title").text("Browse"); 
      // }); 
      // Create the preview image
      $(".image-preview-input input:file").change(function (){     
        var node      = $(this);
        var formData  = new FormData($(node).parents('form')[0]);

        $.ajax({
            url: "{{ base_url(_ADMIN_ROUTE_PREFIX_ . '/ajax/upload') }}",
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
              if(response.status == 'success')
              {
                toastr['success'](response.message);

                $.each(response.data, function(key, obj){
                  var html = '<div class="col-md-2 add_images">';
                  html    += '<img src="'+ obj.path +'" alt="Preview">';
                  html    += '<input type="hidden" name="image_obj[]" value="'+ obj.object +'">';
                  html    += '<i class="del_img remove_preview fa fa-times"></i>';
                  html    += '</div>';
                  $('#preview_images .clearfix').before(html);
                  $(node).val('');
                });
              }
              else
              {
                toastr['error'](response.message);
              }
            },
            error: function(response){
              toastr['error']("Something Went Wrong.");
            }
        });

          // var img = $('<img/>', {
          //     id: 'dynamic',
          //     width:250,
          //     height:200
          // });      
          // var file = this.files[0];
          // var reader = new FileReader();
          // // Set preview image into the popover data-content
          // reader.onload = function (e) {
          //     $(".image-preview-input-title").text("Change");
          //     $(".image-preview-clear").show();
          //     $(".image-preview-filename").val(file.name);            
          //     img.attr('src', e.target.result);
          //     $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
          // }        
          // reader.readAsDataURL(file);
      });  
  });

  $(document).on('click', '.add_row', function(){
    var node  = $(this).closest('.meta_row');
    var clone = $(node).clone();
    $(this).removeClass('add_row');
    $(this).addClass('remove_row');
    $(node).find('button').html('<i class="fa fa-minus"></i>');
    $(node).find('button').removeClass('btn-primary');
    $(node).find('button').addClass('btn-danger');
    $(clone).find('input').val('');
    $(node).after(clone);
  });

  $(document).on('click', '.remove_row', function(){
    var node      = $(this).closest('.meta_row');
    $(node).remove();
    var last_node = $('.meta_row').last();
    $(this).removeClass('remove_row');
    $(this).addClass('add_row');
    $(last_node).find('button').html('<i class="fa fa-plus"></i>');
    $(last_node).find('button').removeClass('btn-danger');
    $(last_node).find('button').addClass('btn-primary');
  });
</script>
@endpush