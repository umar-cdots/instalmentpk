@extends('admin.layouts.master')

@section('title', "Why Buy From Us")
@section('page_heading', "Why Buy From Us")
{{-- @section('page_sub_heading', "Add New Content") --}}
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form action="{{base_url(_ADMIN_ROUTE_PREFIX_ . '/cms/Why-buy-from-us/save')}}" method="post">
          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-header">
                <h3 class="box-title">Why Buy From Us Editor
                </h3>
                <!-- tools box -->
                <!-- /. tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body pad">
                <textarea id="Why-buy-from-us" name="Why-buy-from-us" rows="10" cols="80">{!!$Why_buy_from_us->page_content ?? ''!!}</textarea>
              </div>
            </div>
            <div class="pull-left box-tools">
                  <button type="submit" class="btn btn-info btn-sm" data-widget="save" data-toggle="tooltip"
                          title="Save">Save</button>
                </div>
            <!-- /.box -->
          </div>
          <!-- /.col-->
        </form>
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->

@endsection
@push('css')
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{ base_url("assets/bower_components/ckeditor/ckeditor.js") }}"></script>
  <script>
  $(function () {
    CKEDITOR.config.allowedContent  = true;
    CKEDITOR.replace('Why-buy-from-us');
  })
</script>
@endpush