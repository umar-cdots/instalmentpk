@extends('admin.layouts.master')

@section('title', "About Us")
@section('page_heading', "About")
{{-- @section('page_sub_heading', "Add New Content") --}}
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form action="{{base_url(_ADMIN_ROUTE_PREFIX_ . '/cms/about-us/save')}}" method="post">
          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-header">
                <h3 class="box-title">About Us Editor</h3>
              </div>
              <div class="box-body pad">
                <textarea id="about_us" name="about_us" rows="10" cols="80">{!!$about->page_content ?? ''!!}</textarea>
              </div>
            </div>
            <div class="pull-left box-tools">
                  <button type="submit" class="btn btn-info btn-sm" data-widget="save" data-toggle="tooltip"
                          title="Save">Save</button>
                </div>
          </div>
        </form>
      </div>
    </section>
@endsection
@push('css')
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{ base_url("assets/bower_components/ckeditor/ckeditor.js") }}"></script>
  <script>
  $(function () {
    CKEDITOR.config.allowedContent  = true;
    CKEDITOR.replace('about_us');
  })
</script>
@endpush