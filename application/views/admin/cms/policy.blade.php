@extends('admin.layouts.master')

@section('title', "Privacy Policy")
@section('page_heading', "Privacy Policy")
{{-- @section('page_sub_heading', "Add New Content") --}}
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form action="{{base_url(_ADMIN_ROUTE_PREFIX_ . '/cms/privacy-policy/save')}}" method="post">
          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-header">
                <h3 class="box-title">Privacy Policy Editor
                </h3>
                <!-- tools box -->
                <!-- /. tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body pad">
                <textarea id="privacy-policy" name="privacy-policy" rows="10" cols="80">{!!$privacy_policy->page_content ?? ''!!}</textarea>
              </div>
            </div>
            <div class="pull-left box-tools">
                  <button type="submit" class="btn btn-info btn-sm" data-widget="save" data-toggle="tooltip"
                          title="Save">Save</button>
                </div>
            <!-- /.box -->
          </div>
          <!-- /.col-->
        </form>
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->

@endsection
@push('css')
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{ base_url("assets/bower_components/ckeditor/ckeditor.js") }}"></script>
  <script>
  $(function () {
    CKEDITOR.config.allowedContent  = true;
    CKEDITOR.replace('privacy-policy');
  })
</script>
@endpush