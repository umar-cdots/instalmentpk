@extends('admin.layouts.master')

@section('title', "View Categories")
@section('page_heading', "Categories")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "View") --}}
@section('content')
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/category/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
                <tr>
                  <th>Product Title</th>
                  <td>{{$product->title}}</td>
                </tr>
                <tr>
                  <th>Description</th>
                  <td>{{$product->description}}</td>
                </tr>
                <tr>
                  <th>Categories</th>
                  <td>
                    <ul>
                    @foreach($product->productCategories as $category)
                      <li>{{$category->category_title}}</li>
                    @endforeach
                    </ul>
                  </td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      @endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
<script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
@endpush
