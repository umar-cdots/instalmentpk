@extends('admin.layouts.master')

@section('title', "Edit Brand")
@section('page_heading', "Brands")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "Edit") --}}
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              {{-- <h3 class="box-title">Edit Category</h3> --}}
              <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/brand/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form action="{{base_url(_ADMIN_ROUTE_PREFIX_ . '/brand/update/' . $brand->id)}}" method="post" enctype="multipart/form-data">
              <div class="box-body">
                  <div class="form-group">
                    <label for="title">Brand Name</label>
                    <input type="text" class="form-control" value="{{ $brand->brand_title }}" name="brand_title" id="brand_title" placeholder="Category Title" required="">
                  </div>
                  <div class="form-group">
                    <label for="title">Image</label>
                    <div class="input-group image-preview">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                            <span class="glyphicon glyphicon-remove"></span> Clear
                        </button>
                        <div class="btn btn-default image-preview-input">
                            <span class="glyphicon glyphicon-folder-open"></span>
                            <span class="image-preview-input-title">Browse</span>
                            <input type="file" accept="image/png, image/jpeg, image/gif, image/jpg" name="logo"/>
                            <input type="hidden" name="upload_id" value="{{ $brand->upload_id }}">
                        </div>
                      </span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description">Description</label>
                   <textarea class="textarea" name="description" placeholder="Brand Description"
                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required="">{{ $brand->description }}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>

@endsection
@push('scripts')
<script type="text/javascript">
  $(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
      function () {
         $('.image-preview').popover('show');
      }, 
       function () {
         $('.image-preview').popover('hide');
      }
    );    
  });

  $(function() {
      // Create the close button
      var closebtn = $('<button/>', {
          type:"button",
          text: 'x',
          id: 'close-preview',
          style: 'font-size: initial;',
      });
      closebtn.attr("class","close pull-right");
      // Set the popover default content
      $('.image-preview').popover({
          trigger:'manual',
          html:true,
          title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
          content: "There's no image",
          placement:'bottom'
      });
      // Clear event
      $('.image-preview-clear').click(function(){
          $('.image-preview').attr("data-content","").popover('hide');
          $('.image-preview-filename').val("");
          $('.image-preview-clear').hide();
          $('.image-preview-input input:file').val("");
          $(".image-preview-input-title").text("Browse"); 
      }); 
      // Create the preview image
      $(".image-preview-input input:file").change(function (){     
          var img = $('<img/>', {
              id: 'dynamic',
              width:250,
              height:200
          }); 
          var file = this.files[0];
          var reader = new FileReader();
          // Set preview image into the popover data-content
          reader.onload = function (e) {
              $(".image-preview-input-title").text("Change");
              $(".image-preview-clear").show();
              $(".image-preview-filename").val(file.name);            
              img.attr('src', e.target.result);
              $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
          }        
          reader.readAsDataURL(file);
      });  
  });
  firstTime   = true;
  function firstTimeLoad()
  {
    if(firstTime)
    {
      firstTime = false;
      // Create the close button
      var closebtn = $('<button/>', {
          type:"button",
          text: 'x',
          id: 'close-preview',
          style: 'font-size: initial;',
      });
      closebtn.attr("class","close pull-right");
      // Set the popover default content
      $('.image-preview').popover({
          trigger:'manual',
          html:true,
          title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
          content: "There's no image",
          placement:'bottom'
      });
      var img   = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
              });  
      $(".image-preview-input-title").text("Change");
      $(".image-preview-clear").show();
      $(".image-preview-filename").val("Test");            
      img.attr('src', "{!! imageToBase64($brand->logo ?? '') !!}");
      $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");
    }
  }
  @if(!empty($brand->logo))
    firstTimeLoad();
  @endif
</script>
@endpush