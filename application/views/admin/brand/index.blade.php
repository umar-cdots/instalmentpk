@extends('admin.layouts.master')

@section('title', "Brands List")
@section('page_heading', "Brands")
@section('page_sub_heading', "List")
@section('content')
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              {{-- <h3 class="box-title">Categories</h3> --}}
              <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/brand/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="brands" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Brand Name</th>
                  <th>Brand Descriptions</th>
                  <th>Total Products</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($brands as $brand)
                  <tr>
                    <td>{{$brand->id}}</td>
                    <td>{{ $brand->brand_title }}</td>
                    <td>{{ $brand->description }}</td>
                    <td>{{ $brand->products->count() }}</td>
                    <td class="text-center">
                      <div class="btn-group">  
                        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/brand/edit/" . $brand->id) }}" >
                          <button class="btn btn-xs btn-primary" type="button" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i>
                          </button>
                        </a> 
                        <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/brand/remove/" . $brand->id) }}" class="removeThisNode" >
                          <button class="btn btn-xs btn-danger" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                          </button>
                        </a> 
                    </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Icons -->
  <link rel="stylesheet" href="{{ base_url("assets/css/style.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
      color: #fff;
    }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
<script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script>
  $(function () {
    $('#brands').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
@endpush