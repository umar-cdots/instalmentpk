@extends('admin.layouts.master')

@section('title', "Edit Profile")
@section('page_heading', "Profile")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "Edit Profile") --}}
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Profile</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form action="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/profile") }}" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group col-md-6">
                  <label for="first_name ">First Name</label>
                  <input type="text" class="form-control" name="first_name" value="{{ $user->first_name}}" id="first_name" placeholder="First Name ">
                </div>
                <div class="form-group col-md-6">
                  <label for="last_name ">Last Name</label>
                  <input type="text" class="form-control" name="last_name" value="{{ $user->last_name}}" id="last_name" placeholder="Last Name ">
                </div>
                 {{-- <div class="form-group col-md-6">
                  <label for="password">Old Password</label>
                  <input type="password" class="form-control"  value="" name="password" id="password" placeholder="Old Password">
                </div> --}}
                 <div class="form-group col-md-6">
                  <label for="new_password">New Password</label>
                  <input type="password" class="form-control" name="new_password" id="new_password" placeholder="New Password">
                </div>
                 <div class="form-group col-md-6">
                  <label for="confirm_password">Confirm Password</label>
                  <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password ">
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection

@push('scripts')
  
  @if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
    <script type="text/javascript">
      $.notify({
        // options
        message: '{{ get_instance()->session->flashdata('message') }}' 
      },{
        // settings
        type: '{{ get_instance()->session->flashdata('status') }}'
      });
    </script>
  @endif
@endpush