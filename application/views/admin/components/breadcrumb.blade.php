<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ $title }}
    <small>{{ $sub_title }}</small>
  </h1>
  <ol class="breadcrumb">
      @foreach($breadcrumbs as $breadcrumb)
        <li class="{{ $breadcrumb['is_active'] ? 'active' : '' }}"><a href="{{ $breadcrumb['url'] != "" ? $breadcrumb['url'] : '#' }}">{{ $breadcrumb['title'] }}</a></li>
      @endforeach
  </ol>
</section>