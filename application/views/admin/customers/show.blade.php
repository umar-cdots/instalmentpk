@extends('admin.layouts.master')

@section('title', "View Customer")
@section('page_heading', "Customer")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "View") --}}
@section('content')
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            {{-- <div class="box-header">
              <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/offers/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div> --}}
            <div class="box-body table-responsive no-padding">
              <div class="col-xs-12">
                <table class="table table-bordered">
                  <tr>
                    <th>First Name</th>
                    <td>{{ $customer->user->first_name}}</td>
                  </tr>
                   <tr>
                    <th>Last Name</th>
                    <td>{{ $customer->user->last_name}}</td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td>{{ $customer->user->email_address}}</td>
                  </tr>
                  <tr>
                    <th>Primary Phone</th>
                    <td>{{ $customer->primary_phone}}</td>
                  </tr>
                </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      @endsection
@push('css')
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush
@push('scripts')
  <script type="text/javascript">
    $('#update').click(function(){
      $.ajax({
        type: "POST",
        url: "{{ base_url() . _ADMIN_ROUTE_PREFIX_ . '/ajax/update_vendor_status/' . $vendor->id }}",
        data: "status=" + $('#status').val(),
        cache: false,
        dataType: "json",
        success: function(response){
          toastr[response.status](response.message);
          setTimeout(function(){
            window.location.reload();
          }, 100);
        },
        error: function(response){
          toastr['error']("Something Went Wrong.");
        }
      });
    });
  </script>
@endpush
