@extends('admin.layouts.master')

@section('title', "Edit Customer")
@section('page_heading', "Customer")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "Edit") --}}
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"> Customer Edit</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{base_url(_ADMIN_ROUTE_PREFIX_ . '/customers/update/' . $customer->id)}}" method="post" >
              <div class="box-body">
                <div class="form-group">
                  <label for="first_name">First Name</label>
                   <input type="text" value="{{$customer->user->first_name}}" name="first_name" class="form-control first_name">
                </div>
                 <div class="form-group">
                  <label for="last_name">Last Name</label>
                   <input type="text" value="{{$customer->user->last_name}}" name="last_name" class="form-control last_name">
                </div>
                <div class="form-group">
                  <label for="email_address">Email Address</label>
                  <input type="text" value="{{$customer->user->email_address}}" name="email_address" class="form-control email_address">
                </div>
                 <div class="form-group">
                  <label for="primary_phone">Primary Phone </label>
                  <input type="text" value="{{$customer->primary_phone}}" name="primary_phone" class="form-control primary_phone">
                </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection
@push('css')
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/select2/dist/css/select2.min.css") }}">
  <style type="text/css">
    .add_row button, .remove_row button
    {
      margin-top: 42%;
    }
  </style>
@endpush
@push('scripts')
<script type="text/javascript" src="{{ base_url("assets/bower_components/select2/dist/js/select2.min.js") }}"></script>
<script type="text/javascript">
  $('select').select2();

  $('form').on('keyup', '.total_installments, .total_price_on_installment, .down_payment', function(){
    var node        = $(this).closest('.offer_row');
    var ti          = parseInt($(node).find('.total_installments').val());
    var tpi         = parseInt($(node).find('.total_price_on_installment').val());
    var dp          = parseInt($(node).find('.down_payment').val());
    var apm_node    = $(node).find('.amount_per_month');

    var result      = (tpi - dp) / ti;

    if(!isNaN(result))
      $(apm_node).val(Math.ceil(result));
  });

  // $(document).on('click', '.add_row', function(){
  //   var node  = $(this).closest('.offer_row');
  //   var clone = $(node).clone();
  //   $(this).removeClass('add_row');
  //   $(this).addClass('remove_row');
  //   $(node).find('button').html('<i class="fa fa-minus"></i>');
  //   $(node).find('button').removeClass('btn-primary');
  //   $(node).find('button').addClass('btn-danger');
  //   $(clone).find('input').val('');
  //   $(node).after(clone);
  // });

  // $(document).on('click', '.remove_row', function(){
  //   var node      = $(this).closest('.offer_row');
  //   $(node).remove();
  //   var last_node = $('.offer_row').last();
  //   $(this).removeClass('remove_row');
  //   $(this).addClass('add_row');
  //   $(last_node).find('button').html('<i class="fa fa-plus"></i>');
  //   $(last_node).find('button').removeClass('btn-danger');
  //   $(last_node).find('button').addClass('btn-primary');
  // });
</script>
@endpush