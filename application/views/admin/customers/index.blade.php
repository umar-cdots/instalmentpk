@extends('admin.layouts.master')

@section('title', "Customers List")
@section('page_heading', "Customers")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "List") --}}
@section('content')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="offers" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>ID</th>
              <th>Customers Name</th>
              <th>Email Address </th>
              <th>Primary Phone </th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($customers as $customer)
              <tr style="background-color: {{ !$customer->is_admin_view ? '#ecf0f5' : 'unset'  }}">
                <td>{{ $customer->id}}</td>
                <td>{{ $customer->user->first_name . ' - ' . $customer->user->last_name}}</td>
                <td>{{ $customer->user->email_address}}</td>
                 <td>{{ $customer->primary_phone}}</td>
                  <td class="text-center">
                  <div class="btn-group"> 
                    <a href="{{ base_url() . _ADMIN_ROUTE_PREFIX_ . "/customers/" . $customer->id }}">
                      <button class="btn btn-xs btn-success" data-toggle="tooltip" title="View Customer"><i class="glyphicon glyphicon-eye-open"></i></button>
                    </a>
                     <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/customers/edit/" . $customer->id)}}" >
                      <button class="btn btn-xs btn-primary" type="button" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i>
                      </button>
                    </a> 
                     <a href="{{ base_url() . _ADMIN_ROUTE_PREFIX_ . "/customers/remove/" . $customer->id }}">
                      <button class="btn btn-xs btn-danger" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </a>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush

@push('scripts')
  <!-- DataTables -->
  <script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script>
    $('#offers').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
       'order'        : [],
      'autoWidth'   : false,
      'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1] /* 1st one, start by the right */
      }]

    });
  </script>
@endpush