@extends('admin.layouts.master')

@section('title', "Edit Seller")
@section('page_heading', "Seller")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "Edit") --}}
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"> Sellers Edit</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{base_url(_ADMIN_ROUTE_PREFIX_ . '/seller/update/' . $vendor->id)}}" method="post" >
              <div class="box-body">
                <div class="form-group">
                  <label for="owner_first_name">First Name</label>
                   <input type="text" value="{{$vendor->owner_first_name}}" name="owner_first_name" class="form-control owner_first_name">
                </div>
                <div class="form-group">
                  <label for="owner_last_name">Last Name</label>
                  <input type="text" value="{{$vendor->owner_last_name}}" name="owner_last_name" class="form-control owner_last_name">
                </div>
                 <div class="form-group">
                  <label for="designation">Designation</label>
                  <input type="text" value="{{$vendor->designation}}" name="designation" class="form-control designation">
                </div>
                 <div class="form-group">
                  <label for="business_name">Business Name</label>
                  <input type="text" value="{{$vendor->business_name}}" name="business_name" class="form-control business_name">
                </div>
                 <div class="form-group">
                  <label for="business_address">Address/Area Branch</label>
                  <input type="text" value="{{$vendor->business_address}}" name="business_address" class="form-control business_address">
                </div>
                  <div class="form-group">
                  <label for="operational_area">Operational Area Limit ( radius /km)</label>
                  <input type="text" value="{{$vendor->operational_area}}" name="operational_area" class="form-control operational_area">
                </div>
                 <div class="form-group">
                  <label for="feature_count">Update Faeture Count</label>
                  <input type="text" value="{{$vendor->feature_count}}" name="feature_count" class="form-control feature_count">
                </div>
                 <div class="form-group">
                  <label for="postal_code">Postal Code</label>
                  <input type="text" value="{{$vendor->user->postal_code}}" name="postal_code" class="form-control postal_code">
                </div>
                 <div class="form-group">
                  <label for="cnic">CNIC</label>
                  <input type="text" value="{{$vendor->user->cnic}}" name="cnic" class="form-control cnic">
                </div>
                 <div class="form-group">
                  <label for="organization">Organization</label>
                  <input type="text" value="{{$vendor->organization}}" name="organization" class="form-control organization">
                </div>
                 <div class="form-group">
                  <label for="city">City</label>
                  <input type="text" value="{{$vendor->city}}" name="city" class="form-control city">
                </div>
                 <div class="input_main input_main2">
            <div class="field_left">
              <label>Seller Description</label>
            </div>
            <div class="field_right">
               <textarea rows ="5" cols ="50" name ="vendor_description" placeholder="Seller Description">{{$vendor->vendor_description}}</textarea>
            </div>
            <div class="clearfix"></div>
          </div>

              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection
@push('css')
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/select2/dist/css/select2.min.css") }}">
  <style type="text/css">
    .add_row button, .remove_row button
    {
      margin-top: 42%;
    }
  </style>
@endpush
@push('scripts')
<script type="text/javascript" src="{{ base_url("assets/bower_components/select2/dist/js/select2.min.js") }}"></script>
<script type="text/javascript">
  $('select').select2();

  $('form').on('keyup', '.total_installments, .total_price_on_installment, .down_payment', function(){
    var node        = $(this).closest('.offer_row');
    var ti          = parseInt($(node).find('.total_installments').val());
    var tpi         = parseInt($(node).find('.total_price_on_installment').val());
    var dp          = parseInt($(node).find('.down_payment').val());
    var apm_node    = $(node).find('.amount_per_month');

    var result      = (tpi - dp) / ti;

    if(!isNaN(result))
      $(apm_node).val(Math.ceil(result));
  });

  // $(document).on('click', '.add_row', function(){
  //   var node  = $(this).closest('.offer_row');
  //   var clone = $(node).clone();
  //   $(this).removeClass('add_row');
  //   $(this).addClass('remove_row');
  //   $(node).find('button').html('<i class="fa fa-minus"></i>');
  //   $(node).find('button').removeClass('btn-primary');
  //   $(node).find('button').addClass('btn-danger');
  //   $(clone).find('input').val('');
  //   $(node).after(clone);
  // });

  // $(document).on('click', '.remove_row', function(){
  //   var node      = $(this).closest('.offer_row');
  //   $(node).remove();
  //   var last_node = $('.offer_row').last();
  //   $(this).removeClass('remove_row');
  //   $(this).addClass('add_row');
  //   $(last_node).find('button').html('<i class="fa fa-plus"></i>');
  //   $(last_node).find('button').removeClass('btn-danger');
  //   $(last_node).find('button').addClass('btn-primary');
  // });
</script>
@endpush