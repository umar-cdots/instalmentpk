@extends('admin.layouts.master')

@section('title', "Sellers List")
@section('page_heading', "Sellers")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "List") --}}
@section('content')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="sellers" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>ID</th>
              <th>Sellers Name</th>
              <th>Business Name</th>
              <th>Business Email</th>
              <th>Business Phone </th>
              <th>Business Address</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($vendors as $vendor)
               <tr style="background-color: {{ !$vendor->is_admin_view ? '#ecf0f5' : 'unset'  }}">
                <td>{{ $vendor->id}}</td>
                <td>{{ $vendor->owner_first_name . ' - ' . $vendor->owner_last_name }}</td>
                <td>{{ $vendor->business_name}}</td>
                <td>{{ $vendor->business_email }}</td>
                <td>{{ $vendor->business_phone}}</td>
                <td>{{ $vendor->business_address}}</td>
                <td class="text-center">
                  <div class="btn-group"> 
                    <a href="{{ base_url() . _ADMIN_ROUTE_PREFIX_ . "/seller/" . $vendor->id }}">
                      <button class="btn btn-xs btn-success" data-toggle="tooltip" title="View Seller"><i class="glyphicon glyphicon-eye-open"></i></button>
                    </a>
                     <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/seller/edit/" . $vendor->id)}}" >
                      <button class="btn btn-xs btn-primary" type="button" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i>
                      </button>
                    </a> 
                     <a href="{{ base_url() . _ADMIN_ROUTE_PREFIX_ . "/seller/remove/" . $vendor->id }}">
                      <button class="btn btn-xs btn-danger" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </a>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush

@push('scripts')
  <!-- DataTables -->
  <script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script>
    $('#sellers').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'order'        : [],
      'autoWidth'   : false,
      'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1] /* 1st one, start by the right */
      }]

    });
  </script>
@endpush