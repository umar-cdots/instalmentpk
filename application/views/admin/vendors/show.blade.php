@extends('admin.layouts.master')

@section('title', "View Seller")
@section('page_heading', "Seller")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "View") --}}
@section('content')
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            {{-- <div class="box-header">
              <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/offers/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div> --}}
            <div class="box-body table-responsive no-padding">
              <div class="col-xs-6">
                <table class="table table-bordered">
                  <tr>
                    <th>First Name</th>
                    <td>{{ $vendor->owner_first_name }}</td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td>{{ $vendor->business_email }}</td>
                  </tr>
                  <tr>
                    <th>Designation</th>
                    <td>{{ $vendor->designation }}</td>
                  </tr>
                  <tr>
                    <th>Business Name</th>
                    <td>{{ $vendor->business_name }}</td>
                  </tr>
                  <tr>
                    <th>Address/Area Branch</th>
                    <td>{{ $vendor->business_address }}</td>
                  </tr>
                  <tr>
                    <th>Postal Code</th>
                    <td>{{ $vendor->user->postal_code }}</td>
                  </tr>
                  <tr>
                    <th>Status</th>
                    <td>{{ $vendor->user->status == "active" ? "Active" : "Verification Pending" }}</td>
                  </tr>
                  <tr>
                    <th>Update Status</th>
                    <td>
                      <select name="status" id="status">
                        <option value="active" {{ $vendor->user->status == "active" ? 'selected' : '' }}>Active</option>
                        <option value="deactive" {{ $vendor->user->status != "active" ? 'selected' : '' }}>Deactive</option>
                      </select>
                      <button type="button" id="update">Update</button>
                    </td>
                     <th>Total Feature Assigned</th>
                    <td>
                       <td>{{ $vendor->feature_count  }}</td>
                    </td>
                  </tr>
                </table>
              </div>
              <div class="col-xs-6">
                <table class="table table-bordered">
                  <tr>
                    <th>Last Name</th>
                    <td>{{ $vendor->owner_last_name }}</td>
                  </tr>
                  <tr>
                    <th>CNIC</th>
                    <td>{{ $vendor->user->cnic }}</td>
                  </tr>
                  <tr>
                    <th>Organization</th>
                    <td>{{ $vendor->organization }}</td>
                  </tr>
                  <tr>
                    <th>City</th>
                    <td>{{ $vendor->city }}</td>
                  </tr>
                  <tr>
                    <th>Operational Area Limit ( radius /km)</th>
                    <td>{{ $vendor->operational_area }}</td>
                  </tr>
                  <tr>
                    <th>Seller Logo</th>
                    <td>
                      <img src="{{ base_url() . "assets/uploads/" . (empty($vendor->user->logo->file_name) ? 'no-preview.jpg' : $vendor->user->logo->file_name ) }}" style="width: 40%;">
                    </td>
                  </tr>
                  <tr>
                    <th>Seller Description</th>
                    <td>{{ $vendor->vendor_description }}</td>
                  </tr>
                  
                </table>
              </div>
              <div class="col-xs-12">
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      @endsection
@push('css')
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush
@push('scripts')
  <script type="text/javascript">
    $('#update').click(function(){
      $.ajax({
        type: "POST",
        url: "{{ base_url() . _ADMIN_ROUTE_PREFIX_ . '/ajax/update_vendor_status/' . $vendor->id }}",
        data: "status=" + $('#status').val(),
        cache: false,
        dataType: "json",
        success: function(response){
          toastr[response.status](response.message);
          setTimeout(function(){
            window.location.reload();
          }, 100);
        },
        error: function(response){
          toastr['error']("Something Went Wrong.");
        }
      });
    });
  </script>
@endpush
