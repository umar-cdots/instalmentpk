@extends('admin.layouts.master')

@section('title', "Edit Ledger")
@section('page_heading', "Ledgers")
@section('page_sub_heading', "Edit")
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              {{-- <h3 class="box-title">Edit Ledger</h3> --}}
              <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/ledger/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form action="{{base_url(_ADMIN_ROUTE_PREFIX_ . '/ledger/update/' . $ledger->id)}}" method="post">
              <div class="box-body">
                
                
                <div class="form-group">
                  <label for="parent_id">Users</label>
                 {{ $ledger->user->fullName() }}
                </div>
                
                <div class="form-group">
                  <label for="parent_id">Service name</label>
                 {{ $ledger->type == 'featured' ? $ledger->product->title : ( $ledger->type == 'subscription' ? $ledger->subscriptionHistory->subscriptionPackage->package_title . ' Package' : 'Payment ' . ucfirst($ledger->payment_type) ) }}
                </div>
                
                
                
                 <div class="form-group">
                  <label for="parent_id">Payment Type</label>
                 <select class="form-control" id="payment_type" name="payment_type">
                    <option value="">Select</option>
					<option value="debit">Debit</option>
                    <option value="credit">Credit</option>
                  </select>
                </div>
                <script>document.getElementById("payment_type").value = '{{ $ledger->payment_type }}'; </script>
                
                <div class="form-group">
                  <label for="category_title">Amount</label>
                  <input type="text" class="form-control" name="amount" id="amount" value="{{ $ledger->amount }}" placeholder="Amount">
                </div>
                <div class="form-group">
                  <label for="title">Description</label>
                  <div class="input-group">
                    <span id="descriptions">
                    <textarea name="description" rows="5" class="form-control descriptionTextArea" id="description" placeholder="Description">{{$ledger->description}}</textarea>
                  </span></div>
                </div>
                </div>
              
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>

@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Icons -->
  <link rel="stylesheet" href="{{ base_url("assets/css/style.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
<script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });
  $('#icon_class').keyup(function(){
    var icon_class = $(this).val();
    $(this).find('~span i').addClass(icon_class);
  });
</script>
@endpush