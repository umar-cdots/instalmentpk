@extends('admin.layouts.master')

@section('title', "View Payment")
@section('page_heading', "Payments")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "View") --}}
@section('content')
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/ledger/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
                <tr>
                  <th>Users</th>
                  <td>{{ $ledger->user->first_name . ' ' . $ledger->user->last_name }}</td>
                </tr>
                <tr>
                  <th>Service name</th>
                  <td>{{ $ledger->type == 'featured' ? $ledger->product->title : ( $ledger->type == 'subscription' ? $ledger->subscriptionHistory->subscriptionPackage->package_title . ' Package' : 'Payment ' . ucfirst($ledger->payment_type) ) }}</td>
                </tr>
                <tr>
                  <th>Description</th>
                  <td>{{$ledger->description}}</td>
                </tr>
                <tr>
                  <th>Payment Type</th>
                  <td>
                   {{ ucfirst($ledger->payment_type) }}
                  </td>
                </tr>
                <tr>
                  <th>Type</th>
                  <td>
                   {{ ucfirst($ledger->type) }}
                  </td>
                </tr>
                <tr>
                  <th>Amount</th>
                  <td>
                   {{ number_format($ledger->amount) . _RUPPEE_SIGN_  }}
                  </td>
                </tr>
                
                <tr>
                  <td colspan="2">
                    <a href="#" onclick="PrintElem('testing');">
                      <button class="btn btn-success"><i class="fa fa-print"></i> Print</button>
                    </a>
                    <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/ledger/edit/" . $ledger->id) }}">
                      <button class="btn btn-primary"><i class="fa fa-edit"></i> Edit</button>
                    </a>
                    
                  </td>
                </tr>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
      <div id="testing" style="display:none;">
      <style type="text/css">
	table#printTable
	{
		text-align: center;
		font-family: "Gill Sans", "Gill Sans MT", "Myriad Pro", "DejaVu Sans Condensed", Helvetica, Arial, "sans-serif";
	}
	.tbl_border th
	{
		border-top: #000 solid 1px;
	}

</style>
      <table id="printTable"  border="0" width="100%" align="center" cellpadding="5" cellspacing="5">
	<tr>
		<th colspan="5"><img src="{{ base_url() }}assets/images/logo.png" alt="img"><br /></th>
	</tr>
	<tr>
		<th colspan="2">{{ $ledger->user->fullName() }}</th>
		<th>Invoice Date : <?php echo date('d/m/Y')?></th>
        
	</tr>
	<tr>
		<td colspan="2"><?php echo isset($ledger->user->vendor) && is_object($ledger->user->vendor) ? $ledger->user->vendor->business_address : '' ?></td>
		<th>Number : {{ $ledger->id }}</th>
	</tr>
	<tr>
		<th colspan="2"></th>
		<th>Amount :  {{ number_format($ledger->amount) . _RUPPEE_SIGN_  }}</th>
	</tr>
	<tr class="tbl_border">
		<th colspan="3">INVOICE SUMMARY</th>
	</tr>
	<tr>
		<th>Service Name</th>
		<th>Description</th>
		<th>Total</th>
	</tr>
	<tr>
		<td>{{ $ledger->type == 'featured' ? $ledger->product->title : ( $ledger->type == 'subscription' ? $ledger->subscriptionHistory->subscriptionPackage->package_title . ' Package' : 'Payment ' . ucfirst($ledger->payment_type) ) }}</td>
		<td>{{$ledger->description}}</td>
				<td>{{ number_format($ledger->amount) . _RUPPEE_SIGN_  }}</td>
	</tr>
	
		<tr>
		<th>TOTAL AMOUNT</th>
		<th></th>
		<th>{{ number_format($ledger->amount) . _RUPPEE_SIGN_  }}</th>
	</tr>
	<tr class="tbl_border">
		<th colspan="3">&copy; Instalment.pk All rights reserved.</th>
	</tr>
	<tr>
		<th colspan="3">4B Commercial Area DHA Phase 1</th>
	</tr>
	
</table></div>
      
      @endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
<script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });
  
	function PrintElem(elem)
	{
		//alert('test');
		var mywindow = window.open('', 'PRINT', 'height=500,width=700');
		
		mywindow.document.write('<html><head><title>' + document.title  + '</title>');
		mywindow.document.write('</head><body >');
		//mywindow.document.write('<h1>' + document.title  + '</h1>');
		mywindow.document.write(document.getElementById(elem).innerHTML);
		mywindow.document.write('</body></html>');
		
		mywindow.document.close(); // necessary for IE >= 10
		mywindow.focus(); // necessary for IE >= 10*/
		
		mywindow.print();
		mywindow.close();
		
		return true;
	}
</script>
@endpush
