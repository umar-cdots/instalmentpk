@extends('admin.layouts.master')

@section('title', "Ledger List")
@section('page_heading', "Ledger")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "List") --}}
@section('content')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          {{-- <h3 class="box-title">Categories</h3> --}}
          <form class="form-inline" method="POST" action="" style="float: left;">
            <div class="form-group">
              <label for="user_id">Seller:</label>
              <select name="user_id" class="form-control" id="vendor_id">
                <option value="">Select a Seller</option>
                @foreach($vendors as $vendor)
                  @continue(empty($vendor->user))
                  <option value="{{ $vendor->user->id }}" {{ $vendor->user->id == $user_id ? 'selected' : '' }}>{{ $vendor->fullName() }} | {{ $vendor->businessName() }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="date_from">From:</label>
              <input type="date" class="form-control" name="date_from" id="date_from" value="{{ $date_from ?? '' }}" placeholder="Date From">
            </div>
            <div class="form-group">
              <label for="date_to">To:</label>
              <input type="date" class="form-control" name="date_to" id="date_to" value="{{ $date_to ?? '' }}" placeholder="Date From">
            </div>
            <button type="submit" class="btn btn-primary">Go</button>
          </form>
          <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/ledger/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
          <button type="button" class="btn btn-danger add-btn-right" id="print_ledger" title="Print Invoice" style="margin-right: 5px;">
            <i class="fa fa-print"></i>
          </button>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="ledger" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>ID</th>
              <th>Username</th>
              <th>Service name</th>
              <th>Description</th>
              <th>Debit</th>
              <th>Credit</th>
              <th>Type</th>
              <th>Balance</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            	@php
                $balance = 0;
              @endphp
              @foreach ($ledgers as $ledger)
              @continue(empty($ledger->user))
                @php
                  $balance = $ledger->payment_type == 'debit' ? $balance - $ledger->amount : $balance + $ledger->amount;
                @endphp
              <tr>
                <td>{{$ledger->id}}</td>
                <td>
                 {{ $ledger->user->fullName() }}
                </td>
                
                <td>{{ $ledger->type == 'featured' ? $ledger->product->title : ($ledger->type == 'subscription' ? ($ledger->subscriptionHistory->subscriptionPackage->package_title ?? '-') : 'Payment ' . ucfirst($ledger->payment_type)) }}</td>
                <td>{{ $ledger->description }}</td>
                <td>{{ $ledger->payment_type == 'debit' ? number_format($ledger->amount) : '-' }}</td>
                <td>{{ $ledger->payment_type == 'credit' ? number_format($ledger->amount) : '-' }}</td>
                <td>{{ ucwords($ledger->type) }}</td>
                <td>{{ $balance > 0 ? number_format($balance) . " Cr" : number_format($balance * -1) ." Dr" }}</td>
                <td class="text-center">
                  <div class="btn-group"> 
                    <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/ledger/view/" . $ledger->id) }}" >
                      <button class="btn btn-xs btn-success" type="button" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i>
                      </button>
                    </a> 
                    <a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/ledger/edit/" . $ledger->id) }}" class="removeThisNodes" >
                      <button class="btn btn-xs btn-primary" type="button" data-toggle="tooltip" title="edit"><i class="fa fa-pencil"></i>
                      </button>
                    </a> 
                </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
  <div id="print_screen" style="display:none;">
    <style type="text/css">
      table#ptintTable
      {
        text-align: center;
        font-family: "Gill Sans", "Gill Sans MT", "Myriad Pro", "DejaVu Sans Condensed", Helvetica, Arial, "sans-serif";
      }
      .tbl_border th
      {
        border-top: #000 solid 1px;
      }
      #printTable th, #printTable td
      {
        text-align: left;
      }
    </style>
    <table id="printTable" border="0" width="100%" align="center" cellpadding="5" cellspacing="5">
      <tr>
        <th colspan="7" style="text-align: center;">
          <img src="{{ base_url() }}assets/images/logo.png" alt="InstalmentPK">
          <br />
        </th>
      </tr>
      <tr>
        @if(!empty($user_id) && !empty($ledger->user))
          <th colspan="3">{{ $ledger->user->fullName() }} - {{ $ledger->user->businessName() }}</th>
        @endif
        <th colspan="{{ empty($user_id) ? '7' : '4' }}" style="text-align: right;">Date : {{ date('d/m/Y') }}</th>
      </tr>
      <tr>
        @if(!empty($user_id))
          <td colspan="3">{{ isset($ledger->user->vendor) && is_object($ledger->user->vendor) ? $ledger->user->vendor->business_address : ''  }}</td>
        @endif
        <th colspan="{{ empty($user_id) ? '7' : '4' }}" style="text-align: right;">Amount <i>({{ $balance > 0 ? 'Credit' : 'Debit' }})</i>:  {{ $balance > 0 ? number_format($balance) . _RUPPEE_SIGN_ : number_format($balance * -1) . _RUPPEE_SIGN_ }}</th>
      </tr>
      <tr class="tbl_border">
        <th colspan="{{ empty($user_id) ? '8' : '7' }}" style="text-align: center;">INVOICE SUMMARY</th>
      </tr>
      <tr>
        <th>ID</th>
        <th>Date</th>
        @if(empty($user_id))
          <th>Name</th>
        @endif
        <th>Service Name</th>
        <th>Description</th>
        <th>Debit</th>
        <th>Credit</th>
        <th>Balance</th>
      </tr>
      @php
        $balance = 0;
      @endphp
      @foreach($ledgers as $ledger)
        @php
          $balance = $ledger->payment_type == 'debit' ? $balance - $ledger->amount : $balance + $ledger->amount;
        @endphp
        <tr>
          <td>{{ $ledger->id }}</td>
          <td>{{ $ledger->created_at->format('d/m/Y') }}</td>
          @if(empty($user_id) && !empty($ledger->user))
            <td>{{ $ledger->user->fullName() }}</td>
          @endif
          <td>
            {{ $ledger->type == 'featured' ? $ledger->productOffer->product->title : ( $ledger->type == 'subscription' ? ($ledger->subscriptionHistory->subscriptionPackage->package_title ?? '-') . ' Package' : 'Payment ' . ucfirst($ledger->payment_type) ) }}
          </td>
          <td>{{$ledger->description}}</td>
          <td>{{ $ledger->payment_type == 'debit' ? number_format($ledger->amount) : '-' }}</td>
          <td>{{ $ledger->payment_type == 'credit' ? number_format($ledger->amount) : '-' }}</td>
          <td>{{ $balance > 0 ? number_format($balance) . " Cr" : number_format($balance * -1) ." Dr" }}</td>
        </tr>
      @endforeach
      <tr>
        <th colspan="{{ empty($user_id) ? '5' : '4' }}" style="text-align: right;">Total Amount <i>({{ $balance > 0 ? 'Credit' : 'Debit' }})</i></th>
        <th colspan="2"></th>
        <th>{{ $balance > 0 ? number_format($balance) . _RUPPEE_SIGN_ : number_format($balance * -1) . _RUPPEE_SIGN_ }}</th>
      </tr>
      <tr class="tbl_border">
        <th colspan="{{ empty($user_id) ? '4' : '3' }}">&copy; Instalment.pk All rights reserved.</th>
        <th colspan="4" style="text-align: right;">4B Commercial Area DHA Phase 1</th>
      </tr>
    </table>
  </div>
@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Icons -->
  <link rel="stylesheet" href="{{ base_url("assets/css/style.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
      color: #fff;
    }
  </style>
@endpush

@push('scripts')
  <!-- DataTables -->
  <script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script type="text/javascript">
    $('#ledger').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1] /* 1st one, start by the right */
      }]

    });
    $('#print_ledger').click(function(){
      var mywindow = window.open('', 'PRINT', 'height=500,width=700');
      mywindow.document.write('<html><head><title>' + document.title  + '</title>');
      mywindow.document.write('</head><body >');
      //mywindow.document.write('<h1>' + document.title  + '</h1>');
      mywindow.document.write(document.getElementById('print_screen').innerHTML);
      mywindow.document.write('</body></html>');
      
      mywindow.document.close(); // necessary for IE >= 10
      mywindow.focus(); // necessary for IE >= 10*/
      
      mywindow.print();
      mywindow.close();
    });
  </script>
@endpush