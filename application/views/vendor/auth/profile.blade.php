@extends('admin.layouts.master')

@section('title', "Edit Profile")
@section('page_heading', "Profile")
@section('page_sub_heading', "Edit Profile")
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Profile</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form action="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/profile") }}" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group col-md-6">
                  <label for="owner_first_name">First Name</label>
                  <input type="text" class="form-control" name="owner_first_name" value="{{ $user->owner_first_name}}" id="owner_first_name" placeholder="First Name ">
                </div>
                <div class="form-group col-md-6">
                  <label for="owner_last_name">Last Name</label>
                  <input type="text" class="form-control" name="owner_last_name" value="{{ $user->owner_last_name}}" id="owner_last_name" placeholder="Last Name ">
                </div>
                <div class="form-group col-md-6">
                  <label for="business_name">Business Name</label>
                  <input type="text" class="form-control" name="business_name" value="{{ $user->business_name}}" id="business_name" placeholder="Business Name ">
                </div>
                <div class="form-group col-md-6">
                  <label for="business_address">Business Address</label>
                  <input type="text" class="form-control" name="business_address" value="{{ $user->business_address}}" id="business_address" placeholder="Business Address">
                </div>
                <div class="form-group col-md-6">
                  <label for="business_phone">Business Phone</label>
                  <input type="number" class="form-control" name="business_phone" value="{{ $user->business_phone}}" id="business_phone" placeholder="Business Phone">
                </div>
                <div class="form-group col-md-6">
                  <label for="operational_area">Operational Area</label>
                  <input type="number" class="form-control" name="operational_area" value="{{ $user->operational_area}}" id="operational_area " placeholder="Operational Area">
                </div>
                 <div class="form-group col-md-6">
                  <label for="password">Old Password</label>
                  <input type="password" class="form-control"  value="" name="password" id="password" placeholder="Old Password">
                </div>
                 <div class="form-group col-md-6">
                  <label for="new_password">New Password</label>
                  <input type="password" class="form-control" name="new_password" id="new_password" placeholder="New Password">
                </div>
                 <div class="form-group col-md-6">
                  <label for="confirm_password">Confirm Password</label>
                  <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password ">
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection

@push('scripts')
  
  @if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
    <script type="text/javascript">
      $.notify({
        // options
        message: '{{ get_instance()->session->flashdata('message') }}' 
      },{
        // settings
        type: '{{ get_instance()->session->flashdata('status') }}'
      });
    </script>
  @endif
@endpush