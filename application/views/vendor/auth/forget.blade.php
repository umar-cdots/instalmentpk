@extends('customer.layouts.master')
@section('title', "Forget Password")

@push('css')
  <style type="text/css">
    .login-box{
      width: 90%;
      margin-top: 2%;
    }
  </style>
@endpush


@section('content')
<div class="about_main">
  <div class="container">
<!--    <h2 class="head_bg">Contact Us</h2>-->

   <div class="signup">
   @if(!empty(validation_errors()))
      <div class="validation_errors">
       {!!validation_errors()!!}
      </div>
      @endif
   
      <div class="sign_heading">
      <h2>Seller Forget Password</h2>   
    </div>
    <div class="sign_details">
      <form action="{{base_url(_VENDOR_ROUTE_PREFIX_.'/recover')}}" method="post" name="vendorform" id="vendor_form">
        <div class="input_main input_main2">
        <div class="field_left">
          <label>E-Mail <span>*</span></label>
        </div>
        <div class="field_right">
          <input type="email" id="email_address" name="email_address" placeholder="Enter Your Email" value="" required="">
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="input_main input_main2">
          <div class="field_right">
            <button type="submit">Submit</button>
          </div>
          <div class="clearfix"></div>
        </div>
      </form>
    </div>
   </div>
   <div class="clearfix"></div>
  </div>
</div>
<!-- /.login-box -->

@endsection

@push('scripts')
  
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
		<script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
		</script>
	@endif
@endpush