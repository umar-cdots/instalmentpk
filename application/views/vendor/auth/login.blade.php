@extends('customer.layouts.master')
@section('title', "Vendor Login")

@section('content')

<!--
<div id="" class="container">
	<div class="row">
    	 &nbsp;
    </div>
</div>
-->

<div class="about_main">
<div id="AuthVendorsLogin" class="container">
    <div class="row">
    	<div class="login-box">
  <?php /*?><div class="login-logo">
    <a href="{{ site_url(_VENDOR_ROUTE_PREFIX_ . "/login") }}"><b>Instalment</b>PK</a>
  </div><?php */?>
  <!-- /.login-logo -->
  <div class="login-box-body">
<!--    <p class="login-box-msg sign_inner_head sign_inner_head_2">Sign in to start your session</p>-->
	  <div class="sign_inner_head sign_inner_head_2">
	  	<h2>Seller Login</h2>
		  <hr>
	  </div>
    <form action="{{ site_url(_VENDOR_ROUTE_PREFIX_ . "/try") }}" method="post">
      <div class="form-group has-feedback input_main input_main2">
		  <div class="field_left">
			  <label>Email<span>*</span></label>
		  </div>
		  <div class="field_right">
		  	<input type="email" name="email_address" required="">
        	<span class="glyphicon glyphicon-envelope form-control-feedback"></span>	
		  </div>
        <div class="clearfix"></div>
      </div>
      <div class="form-group has-feedback input_main input_main2">
		  <div class="field_left">
			  <label>Password<span>*</span></label>
		  </div>
		  <div class="field_right">
		  	<input type="password" name="password" required="">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
		  </div>
		  <div class="clearfix"></div>
      </div>
		<hr>
		<div class="input_main input_main2">
               <div class="field_right">
                    <label>
              <input type="checkbox" name="remember_me"> Remember Me
            </label>
         
               </div>
               <div class="clearfix"></div>
            </div>
		<div class="input_main input_main2">
               <div class="field_right">
                  <button type="submit" name="submitfrm">Sign In</button>
               </div>
               <div class="clearfix"></div>
            </div>

<!--
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember_me"> Remember Me
            </label>
          </div>
        </div>
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>
-->
    </form>
      <hr>
    <div class="input_main input_main2">
               <div class="field_right">
               
    <div class="ven_for">
          <a href="{{ site_url(_VENDOR_ROUTE_PREFIX_ . "/forget") }}" class="RedClass">I forgot my password</a> | 
        <a href="{{ site_url(_VENDOR_ROUTE_PREFIX_ . "/register") }}" class="text-center">Register a new membership</a>
    </div>
               </div>
               <div class="clearfix"></div>
            </div>

	 
  </div>
  <!-- /.login-box-body -->
</div>
    </div>
</div>
</div>

<!--
<div id="" class="container">
	<div class="row">
    	 &nbsp;
    </div>
</div>
-->

<!-- /.login-box -->

@endsection

@push('scripts')
  
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
		<script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
		</script>
	@endif
@endpush