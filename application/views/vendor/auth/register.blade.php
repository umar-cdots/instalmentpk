@extends('customer.layouts.master')
@section('title', "Vendor Registeration")

@push('css')
  <style type="text/css">
    .login-box{
      width: 90%;
      margin-top: 2%;
    }
  </style>
@endpush


@section('content')
<div class="about_main">
  <div class="container">
<!--    <h2 class="head_bg">Contact Us</h2>-->

   <div class="signup">
   @if(!empty(validation_errors()))
      <div class="validation_errors">
       {!!validation_errors()!!}
      </div>
      @endif
   
      <div class="sign_heading">
      <h2>Vendor Registration</h2>   
    </div>
    <div class="sign_details">
    <div class="sign_inner_head">
      <h2>Your Personal Details</h2>
    </div>
      <form action="{{base_url(_VENDOR_ROUTE_PREFIX_.'/save')}}" method="post" name="vendorform" id="vendor_form">
        <!--<div class="input_main input_main2">
        <div class="field_left">
          <label>Packages <span>*</span></label>
        </div>
        <div class="field_right">
         <select name="package">
                  <option>Select Package</option>
                  @foreach($packages as $id => $package)
                    <option value="{{ $id }}">{{ $package }}</option>
                  @endforeach
                </select>
        </div>
        <div class="clearfix"></div>
        </div>-->
         <div class="input_main input_main2">
        <div class="field_left">
          <label>CNIC <span>*</span></label>
        </div>
        <div class="field_right">
          <input type="text" name="cnic" id="cnic_no" value="<?php echo set_value('cnic'); ?>" required>
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="input_main input_main2">
        <div class="field_left">
          <label>Designation <span>*</span></label>
        </div>
        <div class="field_right">
          <input type="text" name="designation" value="<?php echo set_value('designation'); ?>" required>
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="input_main input_main2">
        <div class="field_left">
          <label>Organization <span>*</span></label>
        </div>
        <div class="field_right">
          <input type="text" name="organization" value="<?php echo set_value('organization'); ?>" required>
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="input_main input_main2">
        <div class="field_left">
          <label>First Name <span>*</span></label>
        </div>
        <div class="field_right">
          <input type="text" name="first_name" value="<?php echo set_value('first_name'); ?>" required>
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="input_main input_main2">
        <div class="field_left">
          <label>Last Name <span>*</span></label>
        </div>
        <div class="field_right">
          <input type="text" name="last_name" value="<?php echo set_value('last_name'); ?>" required>
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="input_main input_main2">
        <div class="field_left">
          <label>E-Mail <span>*</span></label>
        </div>
        <div class="field_right">
          <input type="email" name="email_address" value="<?php echo set_value('email_address'); ?>" required>
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="input_main input_main2">
        <div class="field_left">
          <label>Telephone <span>*</span></label>
        </div>
        <div class="field_right">
          <input type="tel" name="primary_phone" value="<?php echo set_value('primary_phone'); ?>" maxlength="12" required>
        </div>
        <div class="clearfix"></div>
        </div>
          <div class="sign_inner_head sign_inner_head_2">
      <h2>Your Address</h2>
    </div>
      <div class="input_main input_main2">
        <div class="field_left">
          <label>Address <span>*</span></label>
        </div>
        <div class="field_right">
          <input type="text" id="vendorAddress" value="<?php echo set_value('current_address'); ?>" name="current_address" readonly="readonly" required>
        </div>
        <input type="hidden" name="userLat" id="vendorLat" value="<?php echo set_value('userLat'); ?>" />
        <input type="hidden" name="userLon" id="vendorLon" value="<?php echo set_value('userLon'); ?>" />
        
        @push('scripts')
		<script type="text/javascript" src="{{ base_url("assets/js/jquery.maskedinput.min.js") }}"></script>
        <script type="text/javascript">
        $(document).ready(function(){
        $("#cnic_no").mask("99999-9999999-9");
        });
        </script>
        <script>
		//alert('test');
		localStorage.setItem("reloadStop", "No");
		jQuery('input[name="current_address"]').click(function()
		{
			//alert('test1');
			$('button[name="location"]').trigger('click'); 
			localStorage.setItem("reloadStop", "Yes");
		});
		
		function locationUpdate()
		{
			userCity = $('button[name="location"]').data('user-location');
			userAddress = $('button[name="location"]').data('user-location-full');
			userLat = $('button[name="location"]').data('location-lat');
			userLon = $('button[name="location"]').data('location-lon');
			
			$('#vendorCity').val(userCity);
			$('#vendorAddress').val(userAddress);
			$('#vendorLat').val(userLat);
			$('#vendorLon').val(userLon);		
		}
		</script>
        @endpush
        <div class="clearfix"></div>
        </div>
        <div class="input_main input_main2">
        <div class="field_left">
          <label>City <span>*</span></label>
        </div>
        <div class="field_right">
          <input name="city" id="vendorCity" value="<?php echo set_value('city'); ?>" readonly="readonly" required>
        </div>
        <div class="clearfix"></div>
        </div>
      <div class="input_main input_main2">
        <div class="field_left">
          <label>Postal Code</label>
        </div>
        <div class="field_right">
          <input type="text" name="postal_code" value="<?php echo set_value('postal_code'); ?>">
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="sign_inner_head sign_inner_head_2">
      <h2>Your Password</h2>
      <hr>
    </div>
        <div class="input_main input_main2">
        <div class="field_left">
          <label>Password <span>*</span></label>
        </div>
        <div class="field_right">
          <input type="password" name="password" value="<?php echo set_value('password'); ?>" id="password" required>
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="input_main input_main2">
        <div class="field_left">
          <label>Confirm Password <span>*</span></label>
        </div>
        <div class="field_right">
          <input type="password" name="confirm_password" value="<?php echo set_value('confirm_password'); ?>" id="confirm_password" required>
          <span id="message"></span>
        </div>
        <div class="clearfix"></div>
        </div>
        <hr>
        <div class="input_main input_main2">
          <div class="field_right">
            <input type="checkbox" name="agreed" value="yes" required> <label>I have read and agree to the Privacy Policy </label>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="input_main input_main2">
          <div class="field_right">
            <button type="submit" name="submitfrm">register now</button>
          </div>
          <div class="clearfix"></div>
        </div>
      </form>
    </div>
   </div>
   <div class="clearfix"></div>
  </div>
</div>
<!-- /.login-box -->

@endsection

@push('scripts')
  
	@if(!empty(get_instance()->session->flashdata('status')) && !empty(get_instance()->session->flashdata('message')))
		<script type="text/javascript">
			$.notify({
				// options
				message: '{{ get_instance()->session->flashdata('message') }}' 
			},{
				// settings
				type: '{{ get_instance()->session->flashdata('status') }}'
			});
		</script>
	@endif
@endpush