@extends('vendor.layouts.master')

@section('title', "Subscription History")
@section('page_heading', "Subscription")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "History") --}}
@section('content')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          {{-- <h3 class="box-title">Products</h3> --}}
          <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/packages") }}"><button class="btn btn-primary add-btn-right">Subscribe to New</button></a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="subscription" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Days left expired</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($subscription as $rows)
              @continue(empty($rows->subscriptionPackage))
              <tr>
                <td>{{ $rows->id }}</td>
                <td>{{ $rows->subscriptionPackage->package_title }}</td>
                <td>{{ $rows->subscription_date }}</td>
                <td>{{ $rows->subscription_exp_date }}</td>
                <td>{{ $rows->daysLeft() == 0 ? '-' : $rows->daysLeft() . ' Day(s)' }}</td>
                <td>
                @if($rows->subscription_status == 'subscribed')
                  <button class="btn btn-xs btn-success">Active</button>
                @elseif($rows->status == 'payment_due')
                  <button class="btn btn-xs btn-warning">Payment Due</button>
                @else
                  <button class="btn btn-xs btn-danger">{{ ucfirst($rows->subscription_status) }}</button>
                @endif
                </td>
                <?php /*?><td>{{ $rows->id}}</td><?php */?>
                <?php /*?><td>
                  @if($offer->is_featured)
                    <button class="btn btn-xs btn-success">Yes</button>
                  @else
                    <button class="btn btn-xs btn-danger">No</button>
                  @endif
                </td><?php */?>
                <td>
                  @if($rows->subscription_status == 'subscribed')
                    <button class="btn btn-xs btn-danger subscribe_action" data-action="unsub" data-subscription-id="{{ $rows->id }}">Unsubscribe</button>
                  @else
                    -
                  @endif
                </td>
                <?php /*?><td class="text-center">
                  <div class="btn-group"> 
                    @if($rows->is_featured)
                      <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/un_feature/" . $offer->id)}}">
                        <button class="btn btn-xs btn-warning" data-toggle="tooltip" title="UnFeatured"><i class="glyphicon glyphicon-star"></i></button>
                      </a>
                    @else
                      <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/feature/" . $offer->id)}}">
                        <button class="btn btn-xs btn-info" data-toggle="tooltip" title="Featured"><i class="glyphicon glyphicon-star-empty"></i></button>
                      </a>
                    @endif
                    <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/" . $rows->id)}}" >
                      <button class="btn btn-xs btn-success" type="button" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i>
                      </button>
                    </a> 
                    <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/edit/" . $rows->id)}}" >
                      <button class="btn btn-xs btn-primary" type="button" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i>
                      </button>
                    </a> 
                    <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/remove/" . $rows->id)}}" class="removeThisNode">
                      <button class="btn btn-xs btn-danger" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                      </button>
                    </a> 
                </div>
                </td><?php */?>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush

@push('scripts')
  <!-- DataTables -->
  <script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script type="text/javascript">
    $('#subscription').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      "order"       : [[ 0, "desc" ]],
      'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1] /* 1st one, start by the right */
      }]

    });
    $('.subscribe_action').click(function(){
      var node    = $(this);
      var action  = $(this).data('action');
      var id      = $(this).data('subscription-id');
      var text    = $(this).text();
      if(confirm("Are You Sure? You Want to " + text + "?"))
      {
        if(action == 'unsub')
        {
          $.ajax({
            type: "GET",
            url: "{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/ajax/unsubscribe/") }}" + id,
            dataType: "json",
            cache: false,
            success: function(response){
              if(response.status == 'success')
              {
                toastr['success'](response.message);
                $(node).removeAttr('class');
                $(node).addClass('btn');
                $(node).addClass('btn-xs');
                $(node).addClass('btn-warning');
                $(node).text("Expired");
              }
              else
              {
                toastr['error'](response.message);
              }
            },
            error: function(){
              toastr['error']("Something Went Wrong.");
            }
          });
        }
      }
    });
  </script>
@endpush