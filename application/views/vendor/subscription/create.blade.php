@extends('vendor.layouts.master')

@section('title', "Add New Offer")
@section('page_heading', "Offers")
@section('page_sub_heading', "Add New")
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Installment Offer</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{base_url(_VENDOR_ROUTE_PREFIX_.'/offers/store')}}" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="product_id">Product</label>
                  <select name="product_id" id="product_id" class="form-control">
                    <option value="">Select a Product</option>
                    @foreach($products as $product)
                      <option value="{{ $product->id }}">{{ $product->title }} - {{ $product->brand->brand_title }} - ({{ $product->productCategories->implode('category_title', ', ') }})</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-12 meta_row nl-padding">
                    <div class="col-md-4 nl-padding">
                      <div class="form-group">
                        <label for="meta_key">Feature Key</label>
                        <input type="text" name="meta_key[]" class="form-control meta_key">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="meta_value">Feature Description</label>
                        <input type="text" name="meta_value[]" class="form-control meta_value">
                      </div>
                    </div>
                    <div class="col-md-1 add_meta_row">
                      <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                    </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-12 offer_row">
                  <div class="col-md-11"> 
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="total_installments">No. of Instalments</label>
                        <input type="text" name="total_installments[]" class="form-control total_installments">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="total_price_on_installment">Total Amount</label>
                        <input type="text" name="total_price_on_installment[]" class="form-control total_price_on_installment">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="down_payment">Down Payment</label>
                        <input type="text" name="down_payment[]" class="form-control down_payment">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="amount_per_month">Per Month</label>
                        <input type="text" name="amount_per_month[]" class="form-control amount_per_month">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-1 add_row">
                    <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="form-group">
                  <label for="description">Offer Notes</label>
                  <textarea class="textarea" name="offer_notes" placeholder="Offer Notes"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required=""></textarea>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection
@push('css')
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/select2/dist/css/select2.min.css") }}">
  <style type="text/css">
    .add_row button, .remove_row button
    {
      margin-top: 42%;
    }
    .add_meta_row button, .remove_meta_row button
    {
      margin-top: 42%;
    }
    .nl-padding
    {
      padding-left: 0px;
    }
  </style>
@endpush
@push('scripts')
<script type="text/javascript" src="{{ base_url("assets/bower_components/select2/dist/js/select2.min.js") }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#product_id').select2();

    $('#product_id').change(function(){
      var product_id = $(this).val();

      $.ajax({
        type: "GET",
        url: "{{ base_url(_VENDOR_ROUTE_PREFIX_ . '/ajax/product_meta/') }}" + product_id,
        dataType: "json",
        success: function(response){
          if(response.status == "success")
          {
            $(response.data.meta).each(function(k, v){
              console.log($('.meta_row:last-child').find('.meta_key'));
              $('.meta_row').last().find('.meta_key').val(v.meta_key);
              $('.meta_row').last().find('.meta_value').val(v.meta_value);
              $('.meta_row').last().find('button').trigger('click');
            });
          }
          else
          {
            toastr['error'](response.message);
          }
        },
        error: function(response){
          toastr['error']("Something Went Wrong. Please Try Again.");
        }
      });
    });
  });

  $('form').on('keyup', '.total_installments, .total_price_on_installment, .down_payment', function(){
    var node        = $(this).closest('.offer_row');
    var ti          = parseInt($(node).find('.total_installments').val());
    var tpi         = parseInt($(node).find('.total_price_on_installment').val());
    var dp          = parseInt($(node).find('.down_payment').val());
    var apm_node    = $(node).find('.amount_per_month');

    var result      = (tpi - dp) / ti;

    if(!isNaN(result))
      $(apm_node).val(Math.ceil(result));
  });

  $(document).on('click', '.add_row', function(){
    var node  = $(this).closest('.offer_row');
    var clone = $(node).clone();
    $(this).removeClass('add_row');
    $(this).addClass('remove_row');
    $(node).find('button').html('<i class="fa fa-minus"></i>');
    $(node).find('button').removeClass('btn-primary');
    $(node).find('button').addClass('btn-danger');
    $(clone).find('input').val('');
    $(node).after(clone);
  });

  $(document).on('click', '.remove_row', function(){
    var node      = $(this).closest('.offer_row');
    $(node).remove();
    var last_node = $('.offer_row').last();
    $(this).removeClass('remove_row');
    $(this).addClass('add_row');
    $(last_node).find('button').html('<i class="fa fa-plus"></i>');
    $(last_node).find('button').removeClass('btn-danger');
    $(last_node).find('button').addClass('btn-primary');
  });

  $(document).on('click', '.add_meta_row', function(){
    var node  = $(this).closest('.meta_row');
    var clone = $(node).clone();
    $(this).removeClass('add_meta_row');
    $(this).addClass('remove_meta_row');
    $(node).find('button').html('<i class="fa fa-minus"></i>');
    $(node).find('button').removeClass('btn-primary');
    $(node).find('button').addClass('btn-danger');
    $(clone).find('input').val('');
    $(node).after(clone);
  });

  $(document).on('click', '.remove_meta_row', function(){
    var node      = $(this).closest('.meta_row');
    $(node).remove();
    var last_node = $('.meta_row').last();
    $(this).removeClass('remove_meta_row');
    $(this).addClass('add_meta_row');
    $(last_node).find('button').html('<i class="fa fa-plus"></i>');
    $(last_node).find('button').removeClass('btn-danger');
    $(last_node).find('button').addClass('btn-primary');
  });
</script>
@endpush