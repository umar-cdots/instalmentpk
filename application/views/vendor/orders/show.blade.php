@extends('vendor.layouts.master')

@section('title', "View Orders")
@section('page_heading', "Orders")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "View") --}}
@section('content')
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <?php /*?><a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a><?php */?>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
                <tr>
                 <th align="left">ID</th>
                  <td>{{$orders->id}}</td>
                </tr>
                <tr>
                  <th align="left">Seller</th>
                  <td><?php echo $orders->vendor->business_name; ?></td>
                </tr>
                <tr>
                  <th align="left">Product</th>
                  <td><a target="_blank" href="<?php echo base_url("offer/" . $orders->productOffer->slug); ?>"><?php echo $orders->product->title; ?></a></td>
                </tr>
                <tr>
                  <th align="left">Total Instalments</th>
                  <td><?php echo $orders->productOffer->total_installments; ?></td>
                </tr>
                
                <tr>
                   <th align="left">Down Payment</th>
                  <td>{{_RUPPEE_SIGN_. (int)$orders->productOffer->down_payment}}</td>
                </tr>
                <tr>
                  <th align="left">Amount Per Month</th>
                  <td>{{_RUPPEE_SIGN_. (int)$orders->productOffer->amount_per_month}}</td>
                </tr>
                <tr>
                  <th align="left">Verification Type</th>
                  <td>
                @if($orders->verification_type == 'phone_verification')
                  Phone Verification
                @elseif($orders->verification_type == 'physical_verification')
                  Physical Verification
                @else
                  Free
                @endif</td>
                </tr>
                <tr>
                  <th align="left">Verification Status</th>
                  <td><?php echo $orders->verification_status; ?></td>
                </tr>
               <?php /*?> <tr>
                  <th align="left">Customer comment</th>
                  <td><?php echo $orders->customer_comment; ?></td>
                </tr>
                
                <tr>
                  <th align="left">Vendor comment</th>
                  <td><?php echo $orders->vendor_comment; ?></td>
                </tr>
                
                <tr>
                  <th align="left">Admin comment</th>
                  <td><?php echo $orders->admin_comment; ?></td>
                </tr><?php */?>
                
                <tr>
                  <th align="left">Seller Status</th>
                  <td><?php echo ucfirst($orders->status); ?></td>
                </tr>
                
                <tr>
                  <th align="left">Date</th>
                  <td><?php echo $orders->created_at; ?></td>
                </tr>
                
                <tr>
                  <td colspan="2">
                    <?php /*?><a href="#" onclick="PrintElem('testing');">
                      <button class="btn btn-success"><i class="fa fa-print"></i> Print</button>
                    </a><?php */?>
                    <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/orders/edit/" . $orders->id) }}">
                      <button class="btn btn-primary"><i class="fa fa-edit"></i> Edit</button>
                    </a>
                    
                  </td>
                </tr>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      @endsection
@push('css')
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush
@push('scripts')

@endpush
