@extends('vendor.layouts.master')

@section('title', "Edit an Orders")
@section('page_heading', "Orders")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "Edit") --}}
@section('content')
<div class="row">
      <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Instalment Order</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{base_url(_VENDOR_ROUTE_PREFIX_ . '/orders/update/' . $orders->id)}}" method="post">
              <div class="box-body">
                
                
                <div class="form-group">
                  <label for="parent_id">ID</label>
                {{$orders->id}}
                </div>
                
                <div class="form-group">
                  <label for="parent_id">Seller</label>
                 <?php echo $orders->vendor->business_name; ?>
                </div>
                
                <div class="form-group">
                  <label for="parent_id">Product:</label>
                 <?php echo $orders->product->title; ?>
                </div>
                
                <div class="form-group">
                  <label for="parent_id">Total Instalments:</label>
                  <?php echo $orders->productOffer->total_installments; ?>
                </div>
                
                  <div class="form-group">
                   <label for="parent_id">Down Payment:</label>
                  {{_RUPPEE_SIGN_. (int)$orders->productOffer->down_payment}}
                </div>
                <div class="form-group">
                  <label for="parent_id">Amount Per Month:</label>
                  {{_RUPPEE_SIGN_ . (int)$orders->productOffer->amount_per_month}}
                </div>
				
               
                
                <?php /*?><div class="form-group">
                  <label for="parent_id">Verification Type</label>
                  
                @if($orders->verification_type == 'phone_verification')
                	Phone Verification
                @elseif($orders->verification_type == 'physical_verification')
                	Physical Verification
                @else
                	Free
                @endif
                </div><?php */?>
                <?php /*?><div class="form-group">
                  <label for="parent_id">Verification Status</label>
                  <td><?php echo $orders->verification_status; ?></td>
                </div><?php */?>
                
               <?php /*?> <div class="form-group">
                  <label for="parent_id">Customer Comment</label>
                 <?php echo $orders->customer_comment; ?>
                </div> <?php */?>
                <div class="form-group">
                  <label for="parent_id">Admin comment</label>
                 <?php echo $orders->admin_comment; ?>
                </div>
                
                
                
                
                 <div class="form-group">
                  <label for="VerificationType">Verification Type</label>
                    @if($orders->verification_type == 'phone_verification')
                	Phone Verification
                @elseif($orders->verification_type == 'physical_verification')
                	Physical Verification
                @else
                	Free
                @endif
                 <?php /*?><select class="form-control" id="VerificationType" name="VerificationType">
					<option value="free">Free</option>
                    <option value="phone_verification">Phone Verification</option>
                    <option value="physical_verification">Physical Verification</option>
                  </select><?php */?>
                </div>
                <?php /*?><script>document.getElementById("VerificationType").value = '<?php echo $orders->verification_type; ?>'; </script><?php */?>
                
                 <div class="form-group">
                  <label for="VerificationStatus">Verification Status</label>
                  <?php echo $orders->verification_status; ?>
                 <?php /*?><select class="form-control" id="VerificationStatus" name="VerificationStatus">
					<option value="">Select</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                  </select><?php */?>
                </div>
                <?php /*?><script>document.getElementById("VerificationStatus").value = '<?php echo $orders->verification_status; ?>'; </script><?php */?>
                
                
                <div class="form-group">
                  <label for="Status">Status</label>
                 <select class="form-control" id="Status" name="Status">
					<option value="pending">Pending</option>
                    <option value="approved">Approved</option>
                    <option value="completed">Completed</option>
                  </select>
                  <script>document.getElementById("Status").value = '<?php echo $orders->status; ?>'; </script>
                </div>
                
                
                <div class="form-group">
                  <label for="title">Comment</label>
                  <div class="input-group">
                    <span id="descriptions">
                    <textarea name="vendor_comment" rows="5" class="form-control descriptionTextArea" id="vendor_comment" placeholder="Comment"><?php echo $orders->vendor_comment; ?></textarea>
                  </span></div>
                </div>
                </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>

@endsection
@push('css')
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/select2/dist/css/select2.min.css") }}">
  <style type="text/css">
    .add_row button, .remove_row button
    {
      margin-top: 42%;
    }
    .add_meta_row button, .remove_meta_row button
    {
      margin-top: 42%;
    }
    .nl-padding
    {
      padding-left: 0px;
    }
  </style>
@endpush
@push('scripts')
<script type="text/javascript" src="{{ base_url("assets/bower_components/select2/dist/js/select2.min.js") }}"></script>
<script type="text/javascript">
  $('select').select2();

  $('form').on('keyup', '.total_installments, .total_price_on_installment, .down_payment', function(){
    var node        = $(this).closest('.offer_row');
    var ti          = parseInt($(node).find('.total_installments').val());
    var tpi         = parseInt($(node).find('.total_price_on_installment').val());
    var dp          = parseInt($(node).find('.down_payment').val());
    var apm_node    = $(node).find('.amount_per_month');

    var result      = (tpi - dp) / ti;

    if(!isNaN(result))
      $(apm_node).val(Math.ceil(result));
  });

  $(document).on('click', '.add_meta_row', function(){
    var node  = $(this).closest('.meta_row');
    var clone = $(node).clone();
    $(this).removeClass('add_meta_row');
    $(this).addClass('remove_meta_row');
    $(node).find('button').html('<i class="fa fa-minus"></i>');
    $(node).find('button').removeClass('btn-primary');
    $(node).find('button').addClass('btn-danger');
    $(clone).find('input').val('');
    $(node).after(clone);
  });

  $(document).on('click', '.remove_meta_row', function(){
    var node      = $(this).closest('.meta_row');
    $(node).remove();
    var last_node = $('.meta_row').last();
    $(this).removeClass('remove_meta_row');
    $(this).addClass('add_meta_row');
    $(last_node).find('button').html('<i class="fa fa-plus"></i>');
    $(last_node).find('button').removeClass('btn-danger');
    $(last_node).find('button').addClass('btn-primary');
  });

  // $(document).on('click', '.add_row', function(){
  //   var node  = $(this).closest('.offer_row');
  //   var clone = $(node).clone();
  //   $(this).removeClass('add_row');
  //   $(this).addClass('remove_row');
  //   $(node).find('button').html('<i class="fa fa-minus"></i>');
  //   $(node).find('button').removeClass('btn-primary');
  //   $(node).find('button').addClass('btn-danger');
  //   $(clone).find('input').val('');
  //   $(node).after(clone);
  // });

  // $(document).on('click', '.remove_row', function(){
  //   var node      = $(this).closest('.offer_row');
  //   $(node).remove();
  //   var last_node = $('.offer_row').last();
  //   $(this).removeClass('remove_row');
  //   $(this).addClass('add_row');
  //   $(last_node).find('button').html('<i class="fa fa-plus"></i>');
  //   $(last_node).find('button').removeClass('btn-danger');
  //   $(last_node).find('button').addClass('btn-primary');
  // });
</script>
@endpush