@extends('vendor.layouts.master')

@section('title', "Orders History")
@section('page_heading', "Orders")
@section('page_sub_heading', "")
{{-- @section('page_sub_heading', "List") --}}
@section('content')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          {{-- <h3 class="box-title">Products</h3> --}}
          <?php /*?><a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/packages") }}"><button class="btn btn-primary add-btn-right">Subscribe to New</button></a><?php */?>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="subscription" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>ID</th>
              <th>Customer</th>
              <th>Product Offers</th>
              <?php /*?><th>Verification Type</th>
              <th>Verification Status</th>
        <th>Customer comment</th>
              <th>Your comment</th>
              <th>Admin comment</th><?php */?>
              <th>Status</th>
              <th>Date</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($orders as $order)
              <tr>
                <td>{{ $order->id }}</td>
                <td>{{ isset($order->customer->user) ? $order->customer->user->fullName() : '-' }}</td>
                <td><a target="_blank" href="{{base_url("offer/" . $order->productOffer->slug)}}">{{ $order->product->title}}</a></td>
                
               <?php /*?> <td>
                @if($order->verification_type == 'phone_verification')
                  Phone Verified
                @elseif($order->verification_type == 'physical_verification')
                  Physical Verified
                @else
                  Free
                @endif</td>
                <td>{{ str_replace('_', ' ', ucfirst($order->verification_type == 'phone_verification' ? ($order->phoneVerification->verification_status ?? 'Pending') : ($order->physicalVerification->verification_status ?? 'Pending'))) }}</td><?php */?>
                
                <td>{{ ucfirst($order->status) }}</td>
                <td>{{ $order->created_at->format('Y-m-d') }}</td>
                <td>
                <div class="btn-group"> 
                        <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/orders/view/" . $order->id)}}" >
                          <button class="btn btn-xs btn-success" type="button" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i>
                          </button>
                        </a> 
                        <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/orders/edit/" . $order->id)}}" >
                          <button class="btn btn-xs btn-primary" type="button" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i>
                          </button>
                        </a> 
                       {{--  <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/orders/verification/" . $order->id)}}" >
                          <button class="btn btn-xs btn-info" type="button" data-toggle="tooltip" title="Verification Info"><i class="fa fa-file"></i>
                          </button>
                        </a>  --}}
                        <?php /*?><a href="{{ base_url(_ADMIN_ROUTE_PREFIX_ . "/orders/remove/" . $order->id)}}" class="removeThisNode" >
                          <button class="btn btn-xs btn-danger remove" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                          </button>
                        </a><?php */?> 
                    </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush

@push('scripts')
  <!-- DataTables -->
  <script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script type="text/javascript">
    $('#subscription').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      "order"       : [[ 0, "desc" ]],
      'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1] /* 1st one, start by the right */
      }]

    });
    $('.subscribe_action').click(function(){
      var node    = $(this);
      var action  = $(this).data('action');
      var id      = $(this).data('subscription-id');
      var text    = $(this).text();
      if(confirm("Are You Sure? You Want to " + text + "?"))
      {
        if(action == 'unsub')
        {
          $.ajax({
            type: "GET",
            url: "{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/ajax/unsubscribe/") }}" + id,
            dataType: "json",
            cache: false,
            success: function(response){
              if(response.status == 'success')
              {
                toastr['success'](response.message);
                $(node).removeAttr('class');
                $(node).addClass('btn');
                $(node).addClass('btn-xs');
                $(node).addClass('btn-warning');
                $(node).text("Expired");
              }
              else
              {
                toastr['error'](response.message);
              }
            },
            error: function(){
              toastr['error']("Something Went Wrong.");
            }
          });
        }
      }
    });
  </script>
@endpush