@if(!empty(errorBag()))
  <div class="alert alert-danger alert-dismissible col-md-6 errorBag" style="margin: 15px 0 0 15px;">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Error!</h4>
      <ul>
        {!!  errorBag()  !!}
      </ul>
  </div>
  <div class="clearfix"></div>
@endif
  
