@extends('customer.layouts.master')
@section('title','Thanks')

@section('content')
<div class="about_main">
  <div class="container">
    <div class="signups">
      <div class="sign_details pkgs_det">
        <div id="pricing" class="container">
          <div class="row slideanim">
            
            <h3>Thank you! </h3>
            <h4>You are now subscribed to our <strong>{{ $Package->package_title }}</strong> package.</h4>
            
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
@endsection 