@extends('customer.layouts.master')

@section('title','Packages')
<?php //echo $Packages; ?>
<?php //var_dump($list[0]->package_title); ?>
@section('content')
<div class="about_main">
  <div class="container">
    <div class="signups">
      <div class="sign_details pkgs_det">
        <div id="pricing" class="container">
        	<h3>When you subscribe to a new package, <strong>Current/Active</strong> package will be unsubscribed.</h3>
            <br /><br />
        
          <div class="row slideanim">
            <div class="packagesList packagesList1">
                <div class="pkgHead pkgHead1">Free</div>
                <div class="pkgDet">
                    <h4>Free Package</h4>
                    <h2>0.00</h2>
                    <h3>per 30 Days</h3> 
                    <ul>
                      <li>You will be able to add 25 feature products.</li>
                      <li>You will be able to add unlimited offer on 25 feature products.</li>
                    </ul>
                    <a href="<?php echo base_url(_VENDOR_ROUTE_PREFIX_.'/register/?package_id=1'); ?>"><button class="pkgBtn packageLink" data-toggle="tooltip" data-title="Free">Subscribe</button></a>
                </div>  
            </div>
             <div class="packagesList packagesList2">
                <div class="pkgHead pkgHead2">Standard</div>
                <div class="pkgDet">
                    <h4>Standard Package</h4>
                    <h2>5,000.00</h2>
                    <h3>per 30 Days</h3> 
                    <a href="<?php //echo base_url(_VENDOR_ROUTE_PREFIX_.'/register/?package_id=2'); ?>"><button class="pkgBtn pkgNewBtn" data-toggle="tooltip" data-title="Standard">Comming Soon</button></a>
                </div>  
            </div>
             <!--<div class="packagesList packagesList3">
                <div class="pkgHead pkgHead3">Premium</div>
                <div class="pkgDet">
                    <h4>Premium Package </h4>
                    <h2>10,000.00</h2>
                    <h3>per 30 Days</h3> 
                    <a href="<?php // echo base_url(_VENDOR_ROUTE_PREFIX_.'/register/?package_id=3'); ?>"><button class="pkgBtn">Sign Up</button></a>
                </div>  
            </div> -->
            <div class="clearfix"></div>
            <?php /* if(count($Packages) > 0) { ?>
            @foreach($Packages as $Package)
            <a class="packageLink" data-name="{{$Package->package_title}}" href="<?php echo base_url(_VENDOR_ROUTE_PREFIX_.'/packages/?packageid='.$Package->id); ?>"><div class="custom_packages">
              <div class="panel panel-default text-center">
                <div class="panel-heading">
                  <h1>{{$Package->package_title}}</h1>
                </div>
                <div class="panel-body"> <?php echo $Package->description; ?> </div>
                <div class="panel-footer">
                  <h3><?php echo $Package->package_charges; ?></h3>
                  <h4>per <?php echo $Package->duration; ?> days</h4>
                  <button class="btn btn-lg">Sign Up</button>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
            </div></a>
            @endforeach
            <?php }*/ ?>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
@endsection 
@push('scripts')
    <script type="text/javascript">
    	$(document).ready(function ()
		{
			//alert('packageLink');
			$('.packageLink').click(function ()
			{
				name = $(this).data('title');
				if(confirm('are you sure you want to subscribe '+ name +' package?'))
				{
					//alert('Yes');
				} else {
					//alert('No');
					return false;
				}
			});
		});
    </script>
@endpush