@extends('customer.layouts.master')

@section('title','Packages')
<?php //echo $Packages; ?>
<?php //var_dump($list[0]->package_title); ?>
@section('content')
<div class="about_main">
  <div class="container">
    <div class="signups">
      <div class="sign_details pkgs_det">
        <div id="pricing" class="container">
        	<h3>When you subscribe to a new package, all <strong>Current/Active</strong> packages will be unsubscribed.</h3>
            <br /><br />
        
          <div class="row slideanim">
            <?php if(count($Packages) > 0) { ?>
            @foreach($Packages as $Package)
            <a class="packageLink" data-name="{{$Package->package_title}}" href="<?php echo base_url(_VENDOR_ROUTE_PREFIX_.'/packages/?packageid='.$Package->id); ?>"><div class="custom_packages">
              <div class="panel panel-default text-center">
                <div class="panel-heading">
                  <h1>{{$Package->package_title}}</h1>
                </div>
                <div class="panel-body"> <?php echo $Package->description; ?> </div>
                <div class="panel-footer">
                  <h3><?php echo $Package->package_charges; ?></h3>
                  <h4>per <?php echo $Package->duration; ?> days</h4>
                  <button class="btn btn-lg">Sign Up</button>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
            </div></a>
            @endforeach
            <?php } ?>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
@endsection 

@push('scripts')
    <script type="text/javascript">
    	$(document).ready(function ()
		{
			//alert('packageLink');
			$('.packageLink').click(function ()
			{
				name = $(this).data('name');
				if(confirm('are you sure you want to subscribe '+ name +' package?'))
				{
					//alert('Yes');
				} else {
					//alert('No');
					return false;
				}
			});
		});
    </script>
@endpush