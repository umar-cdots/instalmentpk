@extends('vendor.layouts.master')

@section('title', "Offers List")
@section('page_heading', "Offers")
@section('page_sub_heading', "List")
@section('content')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          {{-- <h3 class="box-title">Products</h3> --}}
          <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="offers" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>ID</th>
              <th>Product Title</th>
              <th>No. of Instalments</th>
              <th>Total Amount</th>
              <th>Down Payment</th>
              <th>Per Month</th>
              <th>Featured</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($offers as $offer)
              <tr>
                <td>{{ $offer->id}}</td>
                <td>{{ $offer->product->title}}</td>
                <td>{{ $offer->total_installments}} Months</td>
                <td>{{ $offer->total_price_on_installment . _RUPPEE_SIGN_}}</td>
                <td>{{ $offer->down_payment . _RUPPEE_SIGN_}}</td>
                <td>{{ $offer->amount_per_month . _RUPPEE_SIGN_}}</td>
                <td>
                  @if($offer->is_featured)
                    <button class="btn btn-xs btn-success">Yes</button>
                  @else
                    <button class="btn btn-xs btn-danger">No</button>
                  @endif
                </td>
                <td>
                  @if($offer->status == 'active')
                    <button class="btn btn-xs btn-success">Active</button>
                  @elseif($offer->status == 'pending')
                    <button class="btn btn-xs btn-warning">Pending</button>
                  @else
                    <button class="btn btn-xs btn-danger">Expired</button>
                  @endif
                </td>
                <td class="text-center">
                  <div class="btn-group"> 
                    @if($offer->is_featured)
                      <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/un_feature/" . $offer->id)}}">
                        <button class="btn btn-xs btn-warning" data-toggle="tooltip" title="UnFeatured"><i class="glyphicon glyphicon-star"></i></button>
                      </a>
                    @else
                      <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/feature/" . $offer->id)}}">
                        <button class="btn btn-xs btn-info" data-toggle="tooltip" title="Featured"><i class="glyphicon glyphicon-star-empty"></i></button>
                      </a>
                    @endif
                    <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/" . $offer->id)}}" >
                      <button class="btn btn-xs btn-success" type="button" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i>
                      </button>
                    </a> 
                    <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/edit/" . $offer->id)}}" >
                      <button class="btn btn-xs btn-primary" type="button" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i>
                      </button>
                    </a> 
                    <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/remove/" . $offer->id)}}" class="removeThisNode">
                      <button class="btn btn-xs btn-danger" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                      </button>
                    </a> 
                </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
@endsection
@push('css')
  <!-- data Tabels -->
  <link rel="stylesheet" href="{{ base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush

@push('scripts')
  <!-- DataTables -->
  <script src="{{ base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script>
    $('#offers').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  </script>
@endpush