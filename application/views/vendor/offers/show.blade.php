@extends('vendor.layouts.master')

@section('title', "View Offers")
@section('page_heading', "Offers")
@section('page_sub_heading', "View")
@section('content')
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/create") }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
                <tr>
                  <th>Product Title</th>
                  <td>{{ $offer->product->title }}</td>
                </tr>
                <tr>
                  <th>No. of Instalments</th>
                  <td>{{ $offer->total_installments }}</td>
                </tr>
                <tr>
                  <th>Total Amount</th>
                  <td>{{ $offer->total_price_on_installment }}</td>
                </tr>
                <tr>
                  <th>Down Payment</th>
                  <td>{{ $offer->down_payment }}</td>
                </tr>
                <tr>
                  <th>Per Month</th>
                  <td>{{ $offer->amount_per_month }}</td>
                </tr>
                <tr>
                  <th>Features</th>
                  <td>
                    <ul>
                    @foreach($offer->meta as $meta)
                      <li>{{ $meta->meta_key }}: {{ $meta->meta_value }}</li>
                    @endforeach
                    </ul>
                  </td>
                </tr>
                <tr>
                  <th>Offer Notes</th>
                  <td>{{ $offer->offer_notes }}</td>
                </tr>
                <tr>
                  <th>Featured</th>
                  <td>
                    @if($offer->is_featured)
                      <button class="btn btn-xs btn-success">Yes</button>
                      <ul>
                        <li>Featuring From: <span class="text-success">{{ (new Carbon\Carbon($offer->featured_from))->format('d/m/Y') }}</span></li>
                        <li>Featuring Till: <span class="text-danger">{{ (new Carbon\Carbon($offer->featured_to))->format('d/m/Y') }}</span></li>
                      </ul>
                    @else
                      <button class="btn btn-xs btn-danger">No</button>
                    @endif
                  </td>
                </tr>
                <tr>
                  <th>Status</th>
                  <td>
                    @if($offer->status == 'active')
                      <button class="btn btn-xs btn-success">Active</button>
                    @elseif($offer->status == 'pending')
                      <button class="btn btn-xs btn-warning">Pending</button>
                    @else
                      <button class="btn btn-xs btn-danger">Expired</button>
                    @endif
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    @if($offer->is_featured)
                      <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/un_feature/" . $offer->id)}}">
                        <button class="btn btn-warning"><i class="glyphicon glyphicon-star"></i> <strike>Feature</strike></button>
                      </a>
                    @else
                      <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/feature/" . $offer->id)}}">
                        <button class="btn btn-info"><i class="glyphicon glyphicon-star-empty"></i> Feature</button>
                      </a>
                    @endif
                    <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/edit/" . $offer->id)}}">
                      <button class="btn btn-primary"><i class="fa fa-edit"></i> Edit</button>
                    </a>
                    <a href="{{ base_url(_VENDOR_ROUTE_PREFIX_ . "/offers/remove/" . $offer->id)}}" class="removeThisNode">
                      <button class="btn btn-danger"><i class="fa fa-times"></i> Remove</button>
                    </a>
                  </td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      @endsection
@push('css')
  <!-- Custom -->
  <style type="text/css">
    button.add-btn-right
    {
      float: right;
    }
    button.add-btn-right a
    {
      color: #fff;
    }
  </style>
@endpush
@push('scripts')

@endpush
