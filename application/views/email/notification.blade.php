<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title><?=$subject ?? 'InstalmentPK Notification'?></title>
	<style type="text/css">
		table
		{
	/*		text-align: center;*/
			font-family: "Gill Sans", "Gill Sans MT", "Myriad Pro", "DejaVu Sans Condensed", Helvetica, Arial, "sans-serif";
		}
		.tbl_border 
		{
			border-top: #000 solid 1px;
		}
		.para td p
		{
			 text-indent: 80px;
			margin: 0;
		}
		.border_tbl td
		{
			border-top: #35bfae solid 2px;
		}
	</style>
</head>
<body>
	<table cellpadding="5" cellspacing="5" border="0" width="50%" align="center">
		<tr>
			<th style="text-align: left;">
				<img src="{{base_url('assets/images/logo.png')}}" alt="InstalmentPK">
			</th>
			<td style="text-align: right;">4B Commercial Area DHA Phase 1<br> 0323-0677776</td>
		</tr>
		<tr class="border_tbl">
			<td colspan="2"></td>
		</tr>
		<tr class="margin">
			<td colspan="2">Dear <?=$recepient_name ?? 'User'?>,</td>
		</tr>
		<tr class="para">
			<td colspan="2">
				<p><?=$content ?? ''?></p>
			</td>
		</tr>
		<tr>
			<td style="text-align: right;" colspan="2">Thank you!</td>
		</tr>
		<tr class="border_tbl">
			<td colspan="2"></td>
		</tr>
		<tr class="tbl_border">
			<td width="50%">Disclaimer: This E-mail is confidential. If you are not the addressee you may not copy, forward, disclose or use any part of it.</td>
			<td style="text-align: right;" width="50%">© Instalment.pk All rights reserved.</td>
		</tr>
	</table>
</body>
</html>
