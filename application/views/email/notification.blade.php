<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title><?=$subject ?? 'InstalmentPK Notification'?></title>
	<style type="text/css">
		table
		{
	/*		text-align: center;*/
			font-family: "Gill Sans", "Gill Sans MT", "Myriad Pro", "DejaVu Sans Condensed", Helvetica, Arial, "sans-serif";
		}
		.tbl_border 
		{
			border-top: #000 solid 1px;
		}
		.para td p
		{
			 text-indent: 80px;
			margin: 0;
		}
		.border_tbl td
		{
			border-top: #35bfae solid 2px;
		}
	</style>
</head>
<body>
	<table cellpadding="5" cellspacing="5" border="0" width="50%" align="center">
		<tr>
			<th style="text-align: left;">
				<img src=" data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQEAAABBCAMAAADmHeEXAAAC91BMVEUAAAAUTYgUTYgUTYgUTYgUTYgUTYgUTYgUU4oUTYgUTYgUTYgUTYgUTYgUTYgTTIgUTYgUTYgAkrMUTYgAkrMUTYgUTYgwta8UTYgUTYgUTYgUTYgUTYgUTYgUTYgUTokUTYgUTYgUTYgUTIcClLQ1vq0UTYg1vq0UTYg2v64UTYgUTYg2v602vrAUTYgUTogAkrM1vq00vq0US4cUS4cVT4o0vq01v60UTYg1vq0UTYgUTYgUTYgUTYg1vq02vbA1vq0UTYg1vq01vq01vq01vq0UTYg1vq0UTYg1vq0UTYgUTIc1vq0UTIgAkrM+urU1vq0UTYgUTYgUTYg1vq00v601vq01vq01vq0TTIg1vq0TTIgAkrM1vq02v645vbEAkrMAkrM1vq0AkrM1vq01vq01vq0UTYg1vq03wK01vq01vq0UTYg1vq01vq0AkrMSRIMUTYg1vq01vq0AkrMUTYg1v60UTog1vq01vq0UTYg1vq0ptK8UTYgAkrMUTYgAkrM1vq01vq0AlLSI1eoUTYgAkrMHl7c1vq2E0+kJOIAAkrMPP4AAk7QAkLNwxcUAkrNpt8uV3PAAnrsAkrMUTYgAkrMAkLOF0usAmbcAkrMUTYgUTYg1vq0AkrM1vq01v60Dm7p7zeGD0edoyq0Ah62Y3/KP3fB94L4Aia8NMXgAlrQAM3eE0ugAkrOD0OqG0uoAlLZszLRkws195MCD0ec82LcSQIMAn7wINH8AkrMXQoKa4/UYPn9Tz7CD0eeb7vyD0ed1yuAEOnp0zuSI0vIYQYEw0rOd7/1+17yU5fZAyK1oy6x3zrkDOHkHPX081bYPPII927cCM36S2O0ApL4/3LcqvawNNoBzzbif7f+D0ecFO3sAImmG0u5vzbgAMnV738AEmbBW1bh95b0AmLcAobw6eagUTYgAkrM1vq2D0edtzLc1v64UTomH1OwLQoISSoaL2+8Ai7GT5PV10ro8x64uvq1ry7QAm7kAlrYKOHl43L3FboLjAAAA6HRSTlMA+CgTUkOA8wIayeT9l2INcN/8rhqlnwS0NhUHj9ceC7KpfGcw++fjokI8LBQOuDApJSFXTyfdrpSKiod3NCod9u+mTfDn3NK8oZxrUiMeCOzsz8Kdk4V4cFpKR0Q2Mi3t49fSy7y4g31zZlZLRjoM/suzq4tfWjnEjnQ+CvrzxcKXgVdFQD4jGtCFgXxzaycSDP3u2NHLt7OtoI6NbGVhXl0cEv7k3b++sJ+Uk4R7bGVKRz79893b18/OycO6kpGQcVBHLfLx6ejf3Nzc29rZxcS3t7W1tbOvqaGhkoBwb2xcS0NAPjwu1pLuMAAADMxJREFUeNrs2FmMS1Ecx/Ff0XaKVktUq7ooaWrKVKyTzjQyLzKpNIJ4EMKjSDyQWBIhHhCxhBBreLDvW6yR2PedWCL2NfGvy9h3D+7tuT3nurcdaouEz4NEx6S3354VELIyMfyoTlYmhSLYqqxZ/hL8UYusTA9ww4lJ4kd1JCaEIgSIMf3hAlFiKsFVExPHj6pHTFMUwdaQshr84QJ2Ypz/C4Ab/88X6ENM7J8tEKijqKRJ/2wBVbj/v14A+F/gh3mJseMrByf+jQUqiOkHzmtmgkDQzHizryealnmi4TR0+ldEQ2Uh9+BSZMXMZgsx1easzlBMnCKdQrOAI2G3L3LGwMTEe2kLNFdfTgNo5HR7PO4eQXCdw1FPWctyRyMYTMo+SzTRyQcubWZsAHwj7U3kX+2Xq1xqNoeIsZuz6gJwib0gRkwDoNN4UjWJQ2iecFFO9xZJACHSc0O2X5Kk12Pv1hDjr7BB1kS8l7ZAP2LKUZKooqxIJZiO1aSqSgWgFUh0p5xItBSqRXyY+yoixLjCyDKR3lDNqbgOUIcfb1uQRitx/q2ir6TSaEp65QAWSLJnmzt/evFcNkx+1TpGcyStoy0gFpKOvrnEtcwWl5sJJgeESt3HsatjZDAx3oCLhPo2yBqQnhlAfWOBPh7itKuGkwzilaR3kwWQnZi1bmo86Av023DuRQ2FvlkgVZ80yoDOEcr/JAhWk54rCYU6Jxs27UVaESWQ6zsLGI2GYigZBcKkU7MBByTm2WYAs2ZBdvh8DVUjUUsBo1awkl6pGsBFeXhFAaPeP1WgVwlkPKpgR7U+wPPps15LqtVYPfvly5erDhwBNhA5nEUVsPYmAxcUJd0pn3F1RQGjHgAZdCpcIBJdbBdP4IBYrxq0cDrCqezPTD7Us1haEjPXIpt+69EeiVu2QB0N14Gp1NBfRIEC6kHmJs5fXZ+4ProCfcoXtzTxrzEIp8WSW/bKLFnpggUqgpDxSV4NoKX6lkF1V5T/PgaKpFgCFRNfStzG5WqBQ/ce4lINfbOAv4c33b+cNLo7R6dLxVwLARhNqoYJL4A6FjFdtQW694cswHe1jtqFsiNUBQqMBOPWjC91eYwiZ2jTknxnwimSsANT1AVhxfZmJc+/WaCpWM9VIbDK4/hOLS5zkRiYpFX8awsP4ENWSe6H5YYzYeECTaFqZBL7Zpk6mOK1n4qXSVoTJ0qyBTjz5P0aTP1WgTKo+JIzXH+WM9VF89zXYkNOJ1LVFQXU5xRN6hdRQHzI4WJQ8K05laytwA1Jawp2SNJR7Hv/5MmbmXj+jQKd9Z+X31jFsyX53AxD8PO9wqKrKQ56/u8vYPIhp6VYCp3E9bZMKlhg45SXr1+LpWD5QenZ8q3vn8jWYHftBfzIGZkbyeBcfMtTn6mhtxHnW8x3UIuuj3gr1/cX8INrIQo0s5LG8Iq4sQAzc+a2Lav3rJo9+9kzSdq//NltrHii2I7ptRcoF4dP43XTw8cnXwasQhUxg3kBB3LqmoouMB5crq0jz4moiTd/ARzuH0O7bkcObdl4edXsiVuWyQEUn2eZX9RaIGyY125wIT5TXFRYghcY+TMF6ucdA0BH/c2iRf4CNOwT1q9cO2Jp68mDti3D1jVn37x5oiwE8ee1FrAYJlYKnIcXiFBhFWIM/I4CCOjvQS3zFuhOx7Awk8m8evVq5VpgyMC2g7ZtvXPt5MzRxRZw/20FgNKoSX+7NBYYT8OwPsMMmHG/77suV/uOatPmQTPHi58vEIOfCkv9zgIqWyuPlbje+QrY5QITWIBNyKzF26dPn7579+44ptLPF0jmDiZV7iYGnn6/s4BQt9JPKqtPPG4UKie9MONDRjYP8zKvenZ9qnh8Abt/SQG3WgD5fG+BHj9VQHtj6Jxn2UpSzV5ckQM0xjT5zxkd3ioB3nYIDvsFBbziv5R+psCYHylgKfOBCxMT1x9ofAEgQsOaj5UHQevJGdlKnFYKXMQ6+iUFSkz8aitE+xdXwA1FoG4xBQLy47ZCTqm4MsVI8wka9Y4APZRBsPTjNDTOyF61XyIH2AnbsF9TACnSHyDM9cmU/o4C4pszlQLwWhd9d4FW6tG7d2W8GWRp9dcikFn55jjY3it7ha9SJtrSyXMyWfPx9vHOdthV84sK2L6QazatTURRGH7TtDZR05E2WDvRpMlElEFraxItLSqpkCLVaFu/SrowLlqEYMVFE6V+QGkL0oqQutCNe3euLBT8D27EP6BkZhEo7lw5M/cm95qbtBMIpJBnM8PJDOfknZO5c96MuaWWhNrWpj4fsEpx21Ggg7k780FDCMl2D7CJ9VD/8dEeZhIZBAUfJ1Uo7ESwlv+rmfwJf/sB/CxOvmqIAmaPVSMRsKFAwCE81NlTIOsvVMUJgxgXIMktR3Ad+PVhK5/Pb30BpO/FQnaiIQpQ01kkHthbAVRYuw67CmRk0Zxla8qwoIv1W92OgiK9eVIszMPTGAXEtqNH+/ZUQLTM67gPKOLT6AE2tvAck0paf/2sRKOR9W0HWX9ON0wBhEQ7dwawo4B/skKBE9x/RuJ07GEKQGIPQqR4K6XQBZ10qe0gU+wOnaY7AARZLn/paBebhUeE6TgovueQBUEd+L+bPTIsRljFFJlG4iD4uXZ+vwJMOwgxIEZ3p1FmhIborJnyTB0uGQQhHzjU/hdkcRjmvKtgnJY3QM73dBJigLvTYZFwAVGaJAQCi3hQpoeGmEG1cjRRckwPjpazhljFFGecRNiFVWjjT1kXS5KdFi7ARfZkCWUCMgmxb+vui3YpEdWPStzqjNJ+yQce6Vxm4kh7nwyCTyJwuZxmmCYJgCJGWJ294LihRhSlvc+N2hWbqWSSij8x1aVkYmhN1sJocWa11xu/0coMagZetDBhzSCJ/cL1u6jOTezG0J1F2KR30AB4t7o8mywr0O0FsGR8EG76TeGUTsp0gefehcVb42dQg6VH47p+HjZJagarGy/NzccFTgFv2gwto8nk9Psw8LnA80wfe6gP1WiO27quX3yMuhR4oBHSSapAEguWAG/RbMb0MVThKnClRlPn9LlPZ2Efb7fGsdlL7gPA5v4QAE9PoirXcpdrtIA+B6BuBdL/qrG71rShOAzgz+U+wCCfIIRAAo0XkdxE0QQRhHrhGzK19aX4ggoqpcNSqoWuMDp6MVY26MuuB4Pd7hv5fIrlJNF27TpaRi37IXo4McJ55J9/Ti43jn6KwXs/ge3h5+A57ssbE3+SIhlDYJrGbTtUKk9OYOsMnoYY4Z1IQAwXE6xTuoilzdEJAK0WVAEw3xnt4jfFOKpMr0IqA5CsaK6rw5Mj+/DY+VePTuD0pg8Oh4vQe6yTTQVLb5kEkmRCLK8ecehJ4kYMo6wqteQwL5mm/9d74tFSt48Lypr4GRe3SKtBvns/gbDnbYu2gLUnUC2LVTl+/xps+glEYdCMi2VbLml1dmjCk6mgU8GohzJJ2ZEgdHcsTiHRLVoJmWQHuGIeQEvBysCSRFht9JMarXDpk8nGMgExCMvgBxZLb7Aee4Snxwywa0oAdDrIKYDbBCzmg7r3vwKbOBHD/mynGdcgNDOQ20DRBlSHsz48St0voAGWmor/noM7A2dBF9wSTS9M4Bi+LRGGtFjawnpE2UyqvQRjQCsXzqAQL9TSVMtOyq5mOmm3qapzmuWsXE4x3Z/G7MyYOgTlqizPVLW6mzMpK29jar805hxAh2zb+4BulHOUS2qmxesSlV0WIHz3r3VhAl8gXIopvBbvh6cLf3otIlkKLQDxNoQeMSAp86+a8FzUZa6kYhYDEgAtTjKbqsd5hwbhw8JzuuyGlwDeiNEnbAT7gkPxcYy1yPvXsBMArIcNDSjVlwnEzZa/ilaR8WKTha5Jj5IWJ2TEKEjQjXaBWkpMJKoQ7D0GnPRujpStBKNXWTkJ39BbYmPoJyAcTs6D2g/uCYc4W18dbDrjSFWH0GK0P+ikWIBn381WaoYdEYc0o2IDhoZNA0CtasfEidDNcYEDFJmoGQgYlQhWksVZ2ipldACxigYYgK5h6eho2Qu8V2hysy+4XFcd6HucIpSkTwmb4gX+IhM1oE0BmgaYBk5K6M7xdCKBg0m4Mfh2a3cc1MFXPK/ri7ZDE6/ac6OVBNCmJ6HXimonSxcPUkfkHnwW6XAARFlwWcZd+fZj7ohen33ePtg+99vfRqPR+HkmjnxqND6e43ldZ0klBonM04En0utFgGqKpBvBQwrk2xpCc7IEwB71q7kY7kiS7Uck8AZ4NZTwBxKeW0XdBzAtTLu936ZLXR0P6pVxi6ZBkOoj3GOMmXlUAi8uQgv/SCMjuG+A/yOBCpV/T1HFk4VPSH7g5Z3oeBmvtw4ODo7xoF9AJPO8fHpzDAAAAABJRU5ErkJggg==" alt="InstalmentPK">
			</th>
			<td style="text-align: right;">4B Commercial Area DHA Phase 1<br> 0323-0677776</td>
		</tr>
		<tr class="border_tbl">
			<td colspan="2"></td>
		</tr>
		<tr class="margin">
			<td colspan="2">Dear <?=$recepient_name ?? 'User'?>,</td>
		</tr>
		<tr class="para">
			<td colspan="2">
				<p><?=$content ?? ''?></p>
			</td>
		</tr>
		<tr>
			<td style="text-align: right;" colspan="2">Thank you!</td>
		</tr>
		<tr class="border_tbl">
			<td colspan="2"></td>
		</tr>
		<tr class="tbl_border">
			<td width="50%">Disclaimer: This E-mail is confidential. If you are not the addressee you may not copy, forward, disclose or use any part of it.</td>
			<td style="text-align: right;" width="50%">© Instalment.pk All rights reserved.</td>
		</tr>
	</table>
</body>
</html>
