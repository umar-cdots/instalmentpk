<?php
use \Application\Models\Users;
use \Application\Models\LoginHistory;
use \Carbon\Carbon;
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-09 17:41:52
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-07-19 19:51:47
 */
class LoginInfo
{
	private $User;
	public 	$LoginHistory;
	private $LoginTime;
	private $SessionExpirationTime;
	private $LastActionTime;
	private $RememberMe 				= false;
	private $IsLogin					= false;
	private $Roles;
	private $StatusMessages;
	private static $Instance;
	private $WhoShouldI; 

	public function __construct()
	{
		$this->User 					= new Users();
		$this->LoginHistory 			= new LoginHistory();
		$this->LoginTime				= Carbon::now();
		$this->LastActionTime			= Carbon::now();
		$this->SessionExpirationTime 	= Carbon::now(+1);
		$this->RememberMe 				= false;
		$this->IsLogin 					= false;
		$this->Roles 			= array(
									'super_admin' 			=> "Super Admin",
									'admin' 	  			=> "Admin",
									'verification_officer' 	=> "Verification Officer",
									'vendor'	  			=> "Vendor",
									'customer'				=> "Customer"
								);
		$this->StatusMessages 	= array(
									Users::_STATUS_DEACTIVE_ => "Your Account is Not Active. Please Contact Administrator.",
									Users::_STATUS_EMAIL_VERIFICATION_PENDING_ 	=> "Your Account is Not Active. Email Verification is Pending.",
									Users::_STATUS_SMS_VERIFICATION_PEDNING_ 	=> "Your Account is Not Active. SMS Verification is Pending.",
								);
		self::$Instance 				= null;
		$this->WhoShouldI 				= null;
	}

	public function __destruct()
	{
		if(isset(get_instance()->session->loginInfo))
		{
			get_instance()->session->set_userdata(array('loginInfo' => serialize(self::$Instance)));
		}
	}

	public function __wakeup()
	{
		$this->LastActionTime			= Carbon::now();
		if($this->LoginHistory instanceof LoginHistory)
		{
			$this->LoginHistory->last_activity 	= $this->LastActionTime;
			$this->LoginHistory->save();
		}
		$this->updateUser();
		self::setInstance($this);
	}

	private function updateUser()
	{
		if($this->User != null && $this->User instanceof Users)
		{
			$this->User->refresh();	
		}
	}

	public function getUser()
	{
		return ($this->User instanceof Users ? $this->User : new Users);
	}

	public function getRole()
	{
		if($this->User instanceof Users)
		{
			return $this->User->user_type;
		}
		return false;
	}

	public function getReadableRole()
	{
		if($this->User instanceof Users)
		{
			return isset($this->Roles[$this->User->user_type]) ? $this->Roles[$this->User->user_type] : $this->User->user_type;
		}
		return false;
	}

	public function getID()
	{
		return $this->User->id;
	}

	public function getUserTypeID()
	{
		return $this->getRole() == 'vendor' ? $this->User->vendor_id : ($this->getRole() == 'customer' ? $this->User->customer_id : $this->getID());
	}

	public function getName()
	{
		return $this->User->first_name .' '. $this->User->last_name;
	}

	public function getAccessLevelBits()
	{
		return $this->User->access_level_bits;
	}

	public function isLogin()
	{
		if(!(self::$Instance->getAccessLevelBits() & self::$Instance->getWhoShouldI()))
		{
			return false;
		}
		return $this->IsLogin;
	}

	public function hasAccess($required_bits = 0)
	{
		return ($required_bits & $this->User->access_level_bits);
	}

	public function attemptLogin($email = '', $password = '', $remember_me = false, $access_bits_required = null)
	{
		$this->User 	= Users::where('email_address', '=', $email)->first();

		if($this->User !== null)
		{
			if(!is_null($access_bits_required) && !$this->hasAccess($access_bits_required))
			{
				get_instance()->session->set_flashdata('status', "error");
				get_instance()->session->set_flashdata('message', "You don't have enough rights.");
				return false;
			}
			elseif($this->User->status != Users::_STATUS_ACTIVE_)
			{
				get_instance()->session->set_flashdata('status', "error");
				get_instance()->session->set_flashdata('message', $this->StatusMessages[$this->User->status]);
				return false;
			}

			$dyc_password 					= decrypt_me($this->User->password);
			if($dyc_password === $password)
			{
				$this->RememberMe 	= $remember_me;
				$this->createLoginHistory();
				$this->iniateSession();
				return $this->User;
			}
		}
		get_instance()->session->set_flashdata('status', "error");
		get_instance()->session->set_flashdata('message', "Invalid Email or Password!");
		return false;
	}

	private function createLoginHistory()
	{
		if($this->User instanceof Users)
		{
			$this->LoginHistory->user_id 		= $this->User->id;
			$this->LoginHistory->login 			= $this->LoginTime;
			$this->LoginHistory->ip_address 	= get_instance()->input->ip_address();;
			$this->LoginHistory->user_agent 	= $_SERVER['HTTP_USER_AGENT'];
			if(isset(get_instance()->session->user_location_full))
			{
				$this->LoginHistory->location 		= get_instance()->session->user_location_full;
				$this->LoginHistory->lat 			= get_instance()->session->location_lat;
				$this->LoginHistory->lon 			= get_instance()->session->location_lon;
			}
			$this->LoginHistory->last_activity 	= $this->LastActionTime;
			$this->LoginHistory->save();
		}
	}

	private function iniateSession()
	{
		if(isset(get_instance()->session))
		{
			$this->IsLogin 		= true;
			get_instance()->session->set_userdata(array('loginInfo' => serialize(self::$Instance)));
		}
		else
		{
			show_500();
		}
	}

	public function destroySession()
	{
		if($this->IsLogin)
		{
			$this->IsLogin 					= false;
			$this->LoginHistory->logout 	= Carbon::now();
			$this->LoginHistory->save();
		}
		get_instance()->session->sess_destroy();
		return true;
	}
	/**
     * Call this method to get singleton
     *
     * @return UserFactory
     */
    public static function getInstance()
    {
        if (self::$Instance === null) 
        {
            self::$Instance = new LoginInfo();
            if(isset(get_instance()->session->loginInfo))
            {
            	unserialize(get_instance()->session->loginInfo);
            }
        }
        return self::$Instance;
    }

    private static function setInstance($instance)
    {
    	self::$Instance = $instance;
    }

    public function setWhoShouldI($who)
    {
    	$this->WhoShouldI 	= $who;
    }

    public function getWhoShouldI()
    {
    	return $this->WhoShouldI;
    }
}