<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-07-13 14:52:32
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-25 11:44:43
 */
abstract class viewComposer
{
	static public function boot(&$container)
	{
		get_instance()->config->load('view_composers', TRUE);
		$composers 	= get_instance()->config->item('composers', 'view_composers');

		foreach($composers as $view_name => $callback)
		{
			$container->composer($view_name, $callback);
		}
	}
}
/*
 * Please add this to File vendor\jenssegers\blade\src\Blade.php on Line # 103
 if($method == 'make' && class_exists('viewComposer'))
 {
     \viewComposer::boot($this->container['view']);
 }
 *
 */