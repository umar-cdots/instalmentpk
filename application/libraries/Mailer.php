<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-09-25 16:05:11
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-25 16:17:01
 */
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mailer extends PHPMailer
{
	function __construct($debug = 0)
	{
	    //Server settings
	    $this->SMTPDebug 	= $debug;                   // Enable verbose debug output
	    $this->isSMTP();                                // Set mailer to use SMTP
	    $this->Host 		= 'mail.instalment.pk';  	// Specify main and backup SMTP servers
	    $this->SMTPAuth 	= true;                     // Enable SMTP authentication
	    $this->Username 	= 'no-reply@instalment.pk'; // SMTP username
	    $this->Password 	= 'omi@work321';            // SMTP password
	    $this->SMTPSecure 	= 'tls';                    // Enable TLS encryption, `ssl` also accepted
	    $this->Port 		= 465;                      // TCP port to connect to

	    $this->setFrom('no-reply@instalment.pk', 'InstalmentPK');
	    
	}
}