<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] 	= 'Welcome/index';
$route['404_override'] 			= '';
$route['translate_uri_dashes'] 	= FALSE;

/** 
 * Admin Routes
 */
$route[_ADMIN_ROUTE_PREFIX_] 					       			    = 'admin/A_DashboardController/index';
$route[_ADMIN_ROUTE_PREFIX_ . '/dashboard'] 	      				= 'admin/A_DashboardController/index';
$route[_ADMIN_ROUTE_PREFIX_ . '/login'] 		   					= 'admin/A_AuthController/login';
$route[_ADMIN_ROUTE_PREFIX_ . '/try'] 			   					= 'admin/A_AuthController/tryLogin';
$route[_ADMIN_ROUTE_PREFIX_ . '/logout'] 		  		 			= 'admin/A_AuthController/logout';
$route[_ADMIN_ROUTE_PREFIX_ . '/profile']['get'] 		  		 	= 'admin/A_AuthController/profile';
$route[_ADMIN_ROUTE_PREFIX_ . '/profile']['post'] 		  		 	= 'admin/A_AuthController/updateProfile';
$route[_ADMIN_ROUTE_PREFIX_ . '/products'] 	   	   					= 'admin/A_ProductsController/index';
$route[_ADMIN_ROUTE_PREFIX_ . '/products/create'] 					= 'admin/A_ProductsController/create';
$route[_ADMIN_ROUTE_PREFIX_ . '/products/store']['post'] 			= 'admin/A_ProductsController/store';
$route[_ADMIN_ROUTE_PREFIX_ . '/products/(:num)'] 					= 'admin/A_ProductsController/show/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/products/edit/(:num)'] 				= 'admin/A_ProductsController/edit/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/products/update/(:num)']['post'] 	= 'admin/A_ProductsController/update/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/products/remove/(:num)'] 			= 'admin/A_ProductsController/destroy/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/products/showOffers/(:num)'] 	    = 'admin/A_ProductsController/showOffers/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/category'] 	   	   					= 'admin/A_CategoryController/index';
$route[_ADMIN_ROUTE_PREFIX_ . '/category/create'] 					= 'admin/A_CategoryController/create';
$route[_ADMIN_ROUTE_PREFIX_ . '/category/store']['post'] 			= 'admin/A_CategoryController/store';
$route[_ADMIN_ROUTE_PREFIX_ . '/category/(:num)'] 					= 'admin/A_CategoryController/show/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/category/edit/(:num)'] 				= 'admin/A_CategoryController/edit/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/category/update/(:num)']['post'] 	= 'admin/A_CategoryController/update/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/category/remove/(:num)'] 			= 'admin/A_CategoryController/destroy/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/brand'] 	   	   					= 'admin/A_BrandsController/index';
$route[_ADMIN_ROUTE_PREFIX_ . '/brand/create'] 						= 'admin/A_BrandsController/create';
$route[_ADMIN_ROUTE_PREFIX_ . '/brand/store']['post'] 				= 'admin/A_BrandsController/store';
$route[_ADMIN_ROUTE_PREFIX_ . '/brand/(:num)'] 						= 'admin/A_BrandsController/show/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/brand/edit/(:num)'] 				= 'admin/A_BrandsController/edit/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/brand/update/(:num)']['post'] 		= 'admin/A_BrandsController/update/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/brand/remove/(:num)'] 				= 'admin/A_BrandsController/destroy/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/offers'] 	   	   					= 'admin/A_OffersController/index';
$route[_ADMIN_ROUTE_PREFIX_ . '/offers/create'] 					= 'admin/A_OffersController/create';
$route[_ADMIN_ROUTE_PREFIX_ . '/offers/store']['post'] 				= 'admin/A_OffersController/store';
$route[_ADMIN_ROUTE_PREFIX_ . '/offers/(:num)'] 					= 'admin/A_OffersController/show/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/offers/edit/(:num)'] 				= 'admin/A_OffersController/edit/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/offers/update/(:num)']['post'] 		= 'admin/A_OffersController/update/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/offers/remove/(:num)'] 				= 'admin/A_OffersController/destroy/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/offers/feature/(:num)'] 			= 'admin/A_OffersController/feature/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/offers/un_feature/(:num)'] 			= 'admin/A_OffersController/unFeature/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/subscriptions']						= 'admin/A_SubscriptionsController/index';

$route[_ADMIN_ROUTE_PREFIX_ . '/cms/about-us'] 						= 'admin/A_CmsController/about';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/about-us/save']['post'] 		= 'admin/A_CmsController/aboutUsSave';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/faq'] 						    = 'admin/A_CmsController/faq';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/faq/save']['post'] 		        = 'admin/A_CmsController/faqSave';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/terms-conditions'] 				= 'admin/A_CmsController/terms';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/terms-conditions/save']['post'] = 'admin/A_CmsController/termsconditionsSave';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/privacy-policy'] 				= 'admin/A_CmsController/policy';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/privacy-policy/save']['post']   = 'admin/A_CmsController/privacyPolicy';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/corporate-installments'] 		= 'admin/A_CmsController/corporate';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/corporate-installments/save']['post']   = 'admin/A_CmsController/corporateInstallments';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/Why-buy-from-us'] 		        = 'admin/A_CmsController/buy';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/Why-buy-from-us/save']['post']   = 'admin/A_CmsController/whyBuy';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/islamic-financing'] 		     = 'admin/A_CmsController/islamicFinancing';
$route[_ADMIN_ROUTE_PREFIX_ . '/cms/islamic-financing/save']['post'] = 'admin/A_CmsController/Financing';

/**
 * AJAX
 */
$route[_ADMIN_ROUTE_PREFIX_ . '/ajax/unsubscribe/(:num)'] 			= 'admin/A_AjaxController/unsubscribe/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/ajax/subscribe'] 					= 'admin/A_AjaxController/subscribe';
$route[_ADMIN_ROUTE_PREFIX_ . '/ajax/upload'] 						= 'admin/A_AjaxController/uploadImages';
$route[_ADMIN_ROUTE_PREFIX_ . '/ajax/get_location']['post']			= 'admin/A_AjaxController/getLocation';

$route[_ADMIN_ROUTE_PREFIX_ . '/ajax/update_vendor_status/(:num)']['post'] = 'admin/A_AjaxController/updateVendorStatus/$1';

$route[_ADMIN_ROUTE_PREFIX_ . '/ajax/get_key_features']['get']		= 'admin/A_AjaxController/getKeyFeatures';

/** 
 * ledger Routes
 */
$route[_ADMIN_ROUTE_PREFIX_ . '/ledger'] 							= 'admin/A_LedgerController/index';
$route[_ADMIN_ROUTE_PREFIX_ . '/ledger/create'] 					= 'admin/A_LedgerController/create';
$route[_ADMIN_ROUTE_PREFIX_ . '/ledger/view/(:num)'] 				= 'admin/A_LedgerController/show/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/ledger/store']['post'] 				= 'admin/A_LedgerController/store';
$route[_ADMIN_ROUTE_PREFIX_ . '/ledger/edit/(:num)'] 				= 'admin/A_LedgerController/edit/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/ledger/update/(:num)']['post']		= 'admin/A_LedgerController/update/$1';

/** 
 * Orders Routes
 */
$route[_ADMIN_ROUTE_PREFIX_ . '/orders'] 							= 'admin/A_OrdersController/index';
$route[_ADMIN_ROUTE_PREFIX_ . '/orders/free'] 						= 'admin/A_OrdersController/index/free';
$route[_ADMIN_ROUTE_PREFIX_ . '/orders/phone_verification'] 		= 'admin/A_OrdersController/index/phone_verification';
$route[_ADMIN_ROUTE_PREFIX_ . '/orders/physical_verification'] 		= 'admin/A_OrdersController/index/physical_verification';
$route[_ADMIN_ROUTE_PREFIX_ . '/orders/(:num)'] 					= 'admin/A_OrdersController/show/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/orders/edit/(:num)'] 				= 'admin/A_OrdersController/edit/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/orders/update/(:num)'] 				= 'admin/A_OrdersController/update/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/orders/phone'] 				        = 'admin/A_OrdersController/phoneVerification';
$route[_ADMIN_ROUTE_PREFIX_ . '/orders/verification/(:num)'] 		= 'admin/A_OrdersController/verificationInfo/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/orders/verify/(:num)'] 				= 'admin/A_OrdersController/verifyOrder/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/orders/save_verification/(:num)'] 	= 'admin/A_OrdersController/saveOrderVerification/$1';


$route[_ADMIN_ROUTE_PREFIX_ . '/orders/phone_verification/form/(:any)']		= 'admin/A_OrdersController/phone_verification_form';
$route[_ADMIN_ROUTE_PREFIX_ . '/orders/physical_verification/form/(:any)']	= 'admin/A_OrdersController/physical_verification_form';

$route[_ADMIN_ROUTE_PREFIX_ . '/orders/phone_verification/add/(:any)']		= 'admin/A_OrdersController/phone_verification_add';
$route[_ADMIN_ROUTE_PREFIX_ . '/orders/physical_verification/add/(:any)']	= 'admin/A_OrdersController/physical_verification_add';

/** 
 * Admin Vendors Routes
 */

$route[_ADMIN_ROUTE_PREFIX_ . '/seller'] 							= 'admin/A_VendorsController/index';
$route[_ADMIN_ROUTE_PREFIX_ . '/seller/(:num)'] 					= 'admin/A_VendorsController/show/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/seller/remove/(:num)'] 				= 'admin/A_VendorsController/destroy/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/seller/edit/(:num)'] 				= 'admin/A_VendorsController/edit/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/seller/update/(:num)'] 				= 'admin/A_VendorsController/update/$1';

$route[_ADMIN_ROUTE_PREFIX_ . '/customers'] 						= 'admin/A_CustomerController/index';
$route[_ADMIN_ROUTE_PREFIX_ . '/customers/(:num)'] 					= 'admin/A_CustomerController/show/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/customers/remove/(:num)'] 			= 'admin/A_CustomerController/destroy/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/customers/edit/(:num)'] 			= 'admin/A_CustomerController/edit/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/customers/update/(:num)'] 			= 'admin/A_CustomerController/update/$1';

/**
 * Key Feature Routes
 */
$route[_ADMIN_ROUTE_PREFIX_ . '/key_feature'] 	   	   					= 'admin/A_KeyFeatureController/index';
$route[_ADMIN_ROUTE_PREFIX_ . '/key_feature/create'] 					= 'admin/A_KeyFeatureController/create';
$route[_ADMIN_ROUTE_PREFIX_ . '/key_feature/store']['post'] 			= 'admin/A_KeyFeatureController/store';
$route[_ADMIN_ROUTE_PREFIX_ . '/key_feature/(:num)'] 					= 'admin/A_KeyFeatureController/show/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/key_feature/edit/(:num)'] 				= 'admin/A_KeyFeatureController/edit/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/key_feature/update/(:num)']['post'] 	= 'admin/A_KeyFeatureController/update/$1';
$route[_ADMIN_ROUTE_PREFIX_ . '/key_feature/remove/(:num)'] 			= 'admin/A_KeyFeatureController/destroy/$1';


/** 
 * Vendor Routes
 */
$route[_VENDOR_ROUTE_PREFIX_] 										= 'vendor/V_DashboardController/index';
$route[_VENDOR_ROUTE_PREFIX_ . '/dashboard'] 						= 'vendor/V_DashboardController/index';
$route[_VENDOR_ROUTE_PREFIX_ . '/login'] 							= 'vendor/V_AuthController/login';
$route[_VENDOR_ROUTE_PREFIX_ . '/try'] 		    					= 'vendor/V_AuthController/tryLogin';
$route[_VENDOR_ROUTE_PREFIX_ . '/logout'] 							= 'vendor/V_AuthController/logout';
$route[_VENDOR_ROUTE_PREFIX_ . '/register'] 						= 'vendor/V_AuthController/register';
$route[_VENDOR_ROUTE_PREFIX_ . '/forget'] 						    = 'vendor/V_AuthController/forget';
$route[_VENDOR_ROUTE_PREFIX_ . '/profile']['get'] 		  		 	= 'vendor/V_AuthController/profile';
$route[_VENDOR_ROUTE_PREFIX_ . '/profile']['post'] 		  		 	= 'vendor/V_AuthController/updateProfile';
$route[_VENDOR_ROUTE_PREFIX_ . '/save'] 							= 'vendor/V_AuthController/saveRegisteration';
$route[_VENDOR_ROUTE_PREFIX_ . '/recover'] 							= 'vendor/V_AuthController/Recoverpassword';
$route[_VENDOR_ROUTE_PREFIX_ . '/offers'] 	   	   					= 'vendor/V_OffersController/index';
$route[_VENDOR_ROUTE_PREFIX_ . '/offers/create'] 					= 'vendor/V_OffersController/create';
$route[_VENDOR_ROUTE_PREFIX_ . '/offers/store']['post'] 			= 'vendor/V_OffersController/store';
$route[_VENDOR_ROUTE_PREFIX_ . '/offers/(:num)'] 					= 'vendor/V_OffersController/show/$1';
$route[_VENDOR_ROUTE_PREFIX_ . '/offers/edit/(:num)'] 				= 'vendor/V_OffersController/edit/$1';
$route[_VENDOR_ROUTE_PREFIX_ . '/offers/update/(:num)']['post'] 	= 'vendor/V_OffersController/update/$1';
$route[_VENDOR_ROUTE_PREFIX_ . '/offers/remove/(:num)'] 			= 'vendor/V_OffersController/destroy/$1';
$route[_VENDOR_ROUTE_PREFIX_ . '/offers/phone/(:num)']['post']		= 'vendor/V_OffersController/phone/$1';
$route[_VENDOR_ROUTE_PREFIX_ . '/offers/feature/(:num)'] 			= 'vendor/V_OffersController/feature/$1';
$route[_VENDOR_ROUTE_PREFIX_ . '/offers/un_feature/(:num)'] 		= 'vendor/V_OffersController/unFeature/$1';
$route[_VENDOR_ROUTE_PREFIX_ . '/packages']							= 'vendor/V_PackageController/lists';
$route[_VENDOR_ROUTE_PREFIX_ . '/thanks']							= 'vendor/V_PackageController/thanks';
//subscription
$route[_VENDOR_ROUTE_PREFIX_ . '/subscription']						= 'vendor/V_SubscriptionController/index';

//vendors orders
$route[_VENDOR_ROUTE_PREFIX_ . '/orders']							= 'vendor/V_OrdersController/index';
$route[_VENDOR_ROUTE_PREFIX_ . '/orders/free']						= 'vendor/V_OrdersController/customlist/free';

//$route[_VENDOR_ROUTE_PREFIX_ . '/orders/phone_verification']		= 'vendor/V_OrdersController/customlist/phone_verification';
//$route[_VENDOR_ROUTE_PREFIX_ . '/orders/physical_verification']		= 'vendor/V_OrdersController/customlist/physical_verification';

$route[_VENDOR_ROUTE_PREFIX_ . '/orders/phone_verification/form']		= 'vendor/V_OrdersController/phone_verification_form';
$route[_VENDOR_ROUTE_PREFIX_ . '/orders/physical_verification/form']	= 'vendor/V_OrdersController/physical_verification_form';

$route[_VENDOR_ROUTE_PREFIX_ . '/orders/phone_verification/add']		= 'vendor/V_OrdersController/phone_verification_add';
$route[_VENDOR_ROUTE_PREFIX_ . '/orders/physical_verification/add']		= 'vendor/V_OrdersController/physical_verification_add';

$route[_VENDOR_ROUTE_PREFIX_ . '/orders/view/(:num)'] 				= 'vendor/V_OrdersController/show/$1';
$route[_VENDOR_ROUTE_PREFIX_ . '/orders/edit/(:num)'] 				= 'vendor/V_OrdersController/edit/$1';
$route[_VENDOR_ROUTE_PREFIX_ . '/orders/update/(:num)']				= 'vendor/V_OrdersController/update/$1';


$route[_VENDOR_ROUTE_PREFIX_ . '/orders/verification/(:num)'] 		= 'vendor/V_OrdersController/verificationInfo/$1';


/**
 * AJAX
 */
$route[_VENDOR_ROUTE_PREFIX_ . '/ajax/product_meta/(:num)'] 		= 'vendor/V_AjaxController/getProductMeta/$1';
$route[_VENDOR_ROUTE_PREFIX_ . '/ajax/unsubscribe/(:num)'] 			= 'vendor/V_AjaxController/unsubscribe/$1';


/**
 * Customer Routes
 */
$route['sms'] 		                    = 'customer/HomepageController/sendVendorSMS';
$route['home'] 						    = 'customer/HomepageController/index';
$route['signup'] 						= 'customer/AuthController/signup';
$route['login'] 						= 'customer/AuthController/login';
$route['register']['post'] 				= 'customer/AuthController/register';
$route['forget'] 				        = 'customer/AuthController/forget';
$route['recover'] 				        = 'customer/AuthController/Recoverpassword';
$route['register']['get'] 				= 'customer/AuthController/simpleRedirect';
$route['tryLogin']['post']				= 'customer/AuthController/tryLogin';
$route['tryLogin']['get']				= 'customer/AuthController/simpleRedirect';
$route['logout'] 						= 'customer/AuthController/logout';
$route['offer/thanks']					= 'customer/OffersController/thanks';
$route['offer/(:any)']['get']			= 'customer/OffersController/show/$1';
$route['offer/(:any)']['post']			= 'customer/OffersController/showWithFilters/$1';
$route['wishlist']						= 'customer/OffersController/wishList';
$route['product/(:any)']['get']			= 'customer/ProductsController/show/$1';
$route['product/(:any)']['post']		= 'customer/ProductsController/showWithFilters/$1';
$route['product-offers/(:any)']			= 'customer/ProductsController/showOffers/$1';
$route['brand/(:any)']					= 'customer/BrandsController/show/$1';
$route['seller/(:any)']					= 'customer/VendorsController/show/$1';
$route['category/(:any)']				= 'customer/CategoryController/show/$1';
$route['search/(:any)']['get']			= 'customer/SearchController/show/$1';
$route['search/(:any)']['post']			= 'customer/SearchController/showWithFilters/$1';


//My Accounts pages
$route['my-account']					= 'customer/MyAccount/index';
$route['my-account/orders']				= 'customer/MyAccount/orders';
$route['my-account/edit-account']		= 'customer/MyAccount/edit_account';


/**
 * AJAX
 */
$route['ajax/set_location']['post']		= 'customer/AjaxController/setLocation';
$route['ajax/search_location']['get']	= 'customer/AjaxController/searchLocations';
$route['ajax/get_location']['post']		= 'customer/AjaxController/getLocation';
$route['ajax/update_wishlist']['post']	= 'customer/AjaxController/updateWishlist';
$route['ajax/suggestions']['get']		= 'customer/AjaxController/searchSuggestions';


/** 
 * CMS Routes
 */
$route['about-us'] 						= 'customer/CmsController/about';
$route['product-request'] 				= 'customer/CmsController/request';
$route['contact-us'] 					= 'customer/CmsController/contact';
$route['faq'] 					        = 'customer/CmsController/faq';
$route['terms-condition'] 			    = 'customer/CmsController/termsCondition';
$route['privacy-policy'] 			    = 'customer/CmsController/privacyPolicy';
$route['corporate-installments'] 	    = 'customer/CmsController/CorporateInstallments';
$route['why-buy-from-us'] 	            = 'customer/CmsController/WhybuyfromUs';
$route['islamic-financing'] 	        = 'customer/CmsController/financingIslamic';

$route['cms/contact-us/save']['post'] 	   = 'customer/CmsController/contactUsSave';
$route['cms/product-request/save']['post'] = 'customer/CmsController/requestSave';





