<?php
$config['email_templates']	= array(
	'upcoming_subscription_expiry'	=> array(
		'subject' 	=> "Upcoming %s Expiration - InstalmentPK",
		'body'		=> "We hope you’re well. This is just to remind you that your subscription is set to expire in %d days on %s %d, %d. We’re sure that you’re busy, but we’d appreciate if you could take a moment to renew your subscription and avoid further inconvenience."),
	'tomorrow_subscription_expiry'	=> array(
		'subject' 	=> "Upcoming %s Expiration - InstalmentPK",
		'body'		=> "We hope you’re well. As previously notified to you, your subscription is expiring tomorrow on %s %d, %d. We would appreciate if you could take out some time and renew your subscription within 24 hours to avoid your active offers being frozen."),
	'subscription_expired'			=> array(
		'subject' 	=> "%s Expired - InstalmentPK",
		'body'		=> "We hope you’re well. This is just to inform you that due to non-renewal of your packages, we have frozen all your active packages. You will no longer be able to sign in to your account or use our services until the packages are re-subscribed. You can click here → <a href=\"%s\">%s</a> to renew your subscriptions and start using our services again."),
	'password_request' 				=> array(
		'subject' 	=> "Password Recovery Request - InstalmentPK",
		'body'		=> "We hope you’re well. This is your password <i><strong>%s</strong></i>. You can <a href=\"%s\">%s</a> to login.")
	);