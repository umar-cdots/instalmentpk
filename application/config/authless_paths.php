<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-05-09 16:19:04
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-05-14 12:42:34
 */

$config['authless'] = array(
	array(
		'module'	 => "admin",
		'controller' => "A_AuthController",
		'method' 	 => "login",
	),
	array(
		'module'	 => "admin",
		'controller' => "A_AuthController",
		'method' 	 => "tryLogin",
	),
	array(
		'module'	 => "vendor",
		'controller' => "V_AuthController",
		'method' 	 => "login",
	),
	array(
		'module'	 => "vendor",
		'controller' => "V_AuthController",
		'method' 	 => "tryLogin",
	),
	array(
		'module'	 => "vendor",
		'controller' => "V_AuthController",
		'method' 	 => "register",
	),
	array(
		'module'	 => "vendor",
		'controller' => "V_AuthController",
		'method' 	 => "saveRegisteration",
	),
	array(
		'module'	 => "customer",
		'controller' => "AuthController",
		'method' 	 => "register",
	),
);