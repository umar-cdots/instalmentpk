<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-07-13 14:45:43
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-10-04 19:10:24
 */

$config['composers'] = array(
	//view => Function
	'customer.layouts.header'		=> 		function($view){
		$wishlist 	= get_instance()->session->wishlist ?? [];
		
		$view->with(['wishlist' => $wishlist]);
	},
	'customer.category.show'		=> 		function($view){
		$categories 	= Application\Models\ProductCategories::where('parent_id', Application\Models\ProductCategories::where('slug', getCategorySlugBySegment())->first()->id)
																	->orderBy('sort', 'ASC')
																		->get();
		$view->with(['categories' => $categories]);
	},
	'customer.layouts.navigation'	=> 		function($view){
		
		$categories 	= Application\Models\ProductCategories::where('show_at_homepage', 1)
																	->orderBy('sort', 'ASC')
																		->get();

		$location 		= array('user_location' => "Lahore", 'user_location_full' => "Lahore", 'location_lat' => "31.4826352", 'location_lon' => "74.0712738");

		if(isset(get_instance()->session->user_location))
		{
			$location 		= ['user_location' => get_instance()->session->user_location, 'user_location_full' => get_instance()->session->user_location_full, 'location_lat' => get_instance()->session->location_lat, 'location_lon' => get_instance()->session->location_lon];
		}

		$view->with(['categories' => $categories, 'location' => $location]);
	},
	'customer.layouts.featured'		=> 		function($view){

		if(isBrandPage())
		{
			$featuredOffers 	= Application\Models\ProductOffers::ambiguityFreeSelection()
																	->nearestOffers()
			  														    ->activeOffers()
			  														    	->where('products.brand_id', getBrandIdBySlug())
																			->where('is_featured', '1')
																				->get();
			// die("<h1>Here".getBrandIdBySlug()."</h1>");
		}
		elseif(isCategoryPage())
		{
			$featuredOffers 	= Application\Models\ProductOffers::ambiguityFreeSelection()
																	->nearestOffers()
			  														    ->activeOffers()
			  														    	->categoryOffers(getCategoryIdBySlug())
																			->where('is_featured', '1')
																				->get();
			// die("<h1>Here".getCategoryIdBySlug()."</h1>");
		}
		else
		{
			$featuredOffers 	= Application\Models\ProductOffers::ambiguityFreeSelection()
																	->nearestOffers()
			  														    ->activeOffers()
																			->where('is_featured', '1')
																				->get();
		}	

		if(empty($featuredOffers) || $featuredOffers->count() == 0)
		{
			$featuredOffers 	= Application\Models\ProductOffers::ambiguityFreeSelection()
																	->nearestOffers()
			  														    ->activeOffers()
																			->where('is_featured', '1')
																				->get();
		}				

		$wishlist 			= get_instance()->session->wishlist ?? [];
		
		$view->with(['featuredOffers' => $featuredOffers, 'wishlist' => $wishlist]);
		
	},
	'customer.layouts.top-items'	=> 		function($view){
		$topProducts 	= Application\Models\Products::topProducts()
															->nearestProducts()
																->limit(10)
																	->get();
		$topBrands 		= Application\Models\Brands::topBrands()
														->nearestBrands()
															->limit(10)
																->get();
		$topVendors 	= Application\Models\Vendors::topVendors()
															->nearestVendors()
																->activeVendor()
																	->limit(5)
																		->get();
		$wishlist 		= get_instance()->session->wishlist ?? [];
		
		$view->with(['topProducts' => $topProducts, 'topBrands' => $topBrands, 'topVendors' => $topVendors, 'wishlist' => $wishlist]);
	},
	'customer.offer.show'			=> function($view){
		$wishlist 	= get_instance()->session->wishlist ?? [];
		
		$view->with(['wishlist' => $wishlist]);
	},
	'customer.layouts.more-offers' => function($view){
		$more_offers 	= Application\Models\ProductOffers::singleCheapest()
																->ambiguityFreeSelection()
																	->nearestOffers()
																		->activeOffers()
																			->orderBy('product_offers.id', 'DESC')
																				->limit(4)
																					->get();

		$view->with(['more_offers' => $more_offers]);
	},
	'customer.search'				=> 		function($view){
		$wishlist 			= get_instance()->session->wishlist ?? [];
		
		$view->with(['wishlist' => $wishlist]);
	},
	'customer.layouts.best-offers'	=> function($view){
		$best_offers	= Application\Models\ProductOffers::singleCheapest()
																->ambiguityFreeSelection()
																	->nearestOffers()
																		->featuredOffers()
																			->activeOffers()
																				->limit(6)
																					->get();
		if(empty($best_offers) || $best_offers->count() < 3)
		{
			$best_offers	= Application\Models\ProductOffers::singleCheapest()
																->ambiguityFreeSelection()
																->nearestOffers()
																	->activeOffers()
																		->orderBy('is_featured', 'DESC')
																			->limit(6)
																				->get();
		}
		$wishlist 			= get_instance()->session->wishlist ?? [];

		$view->with(['best_offers' => $best_offers, 'wishlist' => $wishlist]);
	},
		'customer.layouts.links'				=> 		function($view){
			$topBrands 		= Application\Models\Brands::topBrands()
														->nearestBrands()
															->limit(5)
																->get();
			$categories 	= Application\Models\ProductCategories::where('show_at_homepage', 1)
																		->orderBy('sort', 'ASC')
																			->limit(5)
																				->get();
			$topVendors 	= Application\Models\Vendors::topVendors()
															->nearestVendors()
																->activeVendor()
																	->limit(5)
																		->get();	
			$view->with(['topBrands' => $topBrands, 'categories'=> $categories,'topVendors'=>$topVendors]);
		},
		'admin.layouts.left-nav' => function($view){
			$new_orders_count                   = Application\Models\Orders::where('is_admin_view', false)->count();
			$new_seller_count                   = Application\Models\Vendors::where('is_admin_view', false)->count();
			$new_customer_count                 = Application\Models\Customers::where('is_admin_view', false)->count();

			$view->with(['new_orders_count' => $new_orders_count, 'new_seller_count' => $new_seller_count,'new_customer_count'=> $new_customer_count]);
		}
);
