<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-07-13 14:45:43
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-10-04 19:10:24
 */

$config['composers'] = array(
	//view => Function
	'customer.layouts.header'		=> 		function($view){
		$wishlist 	= get_instance()->session->wishlist ?? [];
		
		$view->with(['wishlist' => $wishlist]);
	},
	'customer.layouts.navigation'	=> 		function($view){
		$categories 	= Application\Models\ProductCategories::where('show_at_homepage', 1)
																	->orderBy('sort', 'ASC')
																		->get();
		$location 		= array('user_location' => "Lahore", 'user_location_full' => "Lahore", 'location_lat' => "31.4826352", 'location_lon' => "74.0712738");

		if(isset(get_instance()->session->user_location))
		{
			$location 		= ['user_location' => get_instance()->session->user_location, 'user_location_full' => get_instance()->session->user_location_full, 'location_lat' => get_instance()->session->location_lat, 'location_lon' => get_instance()->session->location_lon];
		}

		$view->with(['categories' => $categories, 'location' => $location]);
	},
	'customer.layouts.featured'		=> 		function($view){
		$featuredOffers 	= Application\Models\ProductOffers::ambiguityFreeSelection()
																	->nearestOffers()
																		->activeOffers()
																			->where('is_featured', '1')
																				->get();
		$wishlist 			= get_instance()->session->wishlist ?? [];
		
		$view->with(['featuredOffers' => $featuredOffers, 'wishlist' => $wishlist]);
	},
	'customer.layouts.top-items'	=> 		function($view){
		$topProducts 	= Application\Models\Products::topProducts()
															->limit(5)
																->get();
		$topBrands 		= Application\Models\Brands::topBrands()
															->limit(5)
																->get();
		$topVendors 	= Application\Models\Vendors::topVendors()
															->activeVendor()
																->limit(5)
																	->get();
		$wishlist 		= get_instance()->session->wishlist ?? [];
		
		$view->with(['topProducts' => $topProducts, 'topBrands' => $topBrands, 'topVendors' => $topVendors, 'wishlist' => $wishlist]);
	},
	'customer.offer.show'			=> function($view){
		$wishlist 	= get_instance()->session->wishlist ?? [];
		
		$view->with(['wishlist' => $wishlist]);
	},
	'customer.layouts.more-offers' => function($view){
		$more_offers 	= Application\Models\ProductOffers::singleCheapest()
																->ambiguityFreeSelection()
																->nearestOffers()
																	->activeOffers()
																		->orderBy('product_offers.id', 'DESC')
																			->limit(4)
																				->get();

		$view->with(['more_offers' => $more_offers]);
	},
	'customer.search'				=> 		function($view){
		$wishlist 			= get_instance()->session->wishlist ?? [];
		
		$view->with(['wishlist' => $wishlist]);
	},
	'customer.layouts.best-offers'	=> function($view){
		$best_offers	= Application\Models\ProductOffers::singleCheapest()
																->ambiguityFreeSelection()
																->nearestOffers()
																	->featuredOffers()
																		->activeOffers()
																			->limit(6)
																				->get();
		if(empty($best_offers) || $best_offers->count() < 3)
		{
			$best_offers	= Application\Models\ProductOffers::singleCheapest()
																->ambiguityFreeSelection()
																->nearestOffers()
																	->activeOffers()
																		->orderBy('is_featured', 'DESC')
																			->limit(6)
																				->get();
		}
		$wishlist 			= get_instance()->session->wishlist ?? [];

		$view->with(['best_offers' => $best_offers, 'wishlist' => $wishlist]);
	},
		'customer.layouts.links'				=> 		function($view){
			$topBrands 		= Application\Models\Brands::topBrands()
															->limit(5)
																->get();
			$categories 	= Application\Models\ProductCategories::where('show_at_homepage', 1)
																	->orderBy('sort', 'ASC')
																	->limit(5)
																		->get();
			$view->with(['topBrands' => $topBrands, 'categories'=> $categories]);
		}
);
