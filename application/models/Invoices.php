<?php
namespace Application\Models;
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-07-23 17:46:40
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-07-23 17:48:10
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Invoices extends Eloquent 
{
	use SoftDeletes;

    protected $table = "invoices";

    public function user()
    {
        return $this->belongsTo('Application\Models\Users', 'user_id');
    }
}