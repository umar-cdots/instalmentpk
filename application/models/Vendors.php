<?php
namespace Application\Models;
if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Query\Expression as Raw;
use \Illuminate\Database\Query\Builder;

class Vendors extends Eloquent 
{
    use SoftDeletes;

    protected $table = "vendors";

    public function __construct()
    {
        parent::__construct();
        $this->registerTopVendorsMacro();
        $this->registerActiveVendorMacro();
        $this->registerNearestVendorsMacro();
    }

    public function user()
    {
        return $this->hasOne('Application\Models\Users', 'vendor_id');
    }

    public function subscriptions()
    {
    	return $this->belongsTo('Application\Models\SubscriptionHistory', 'current_subscription_id');
    }

    public function currentSubscription()
    {
    	return $this->hasOne('Application\Models\SubscriptionHistory', 'vendor_id')->where('subscription_status', 'subscribed')->limit(1);
    }

    public function lastSubscription()
    {
        return $this->hasOne('Application\Models\SubscriptionHistory', 'vendor_id')->orderBy('id', 'desc')->limit(1);
    }

    public function productOffers()
    {
        return $this->hasMany('Application\Models\ProductOffers', 'vendor_id')->where('status', 'active');
    }

    public function freezedProductOffers()
    {
        return $this->hasMany('Application\Models\ProductOffers', 'vendor_id')->where('status', 'freezed');
    }

    public function orders()
    {
        return $this->hasMany('Application\Models\Orders', 'vendor_id');
    }

    public function fullName()
    {
        return $this->user->first_name . ' ' . $this->user->last_name;
    }

    public function businessName()
    {
        return $this->business_name;
    }
    public function businessPhone()
    {
        return $this->business_phone;
    }
    public function businessEmail()
    {
        return $this->business_email;
    }
    public function businessArea()
    {
        return $this->business_address;
    }
    public function logo()
    {
        return $this->user->logo;
    }

    // public static function topAndNearVendors($take = 5)
    // {
    //     $customerLat    =  get_instance()->session->location_lat;
    //     $customerLon    =  get_instance()->session->location_lon;

    //     return  (new static)->select('vendors.*')
    //                         ->join('product_offers', 'vendors.id', '=', 'product_offers.vendor_id')
    //                         ->join('products', 'product_offers.product_id', '=', 'products.id')
    //                         ->join('orders', 'products.id', '=', 'orders.product_id')
    //                         ->groupBy('orders.product_id')
    //                         ->where('product_offers.status', 'active')
    //                         ->whereRaw("DISTANCEE({$customerLat}, {$customerLon}, `vendors`.`vendor_lat`, `vendors`.`vendor_lon`) <= `vendors`.`operational_area`")
    //                         ->orderBy(new Raw("COUNT(`orders`.`id`)"), 'DESC')
    //                         ->take($take)->get();
    // }

    private function registerTopVendorsMacro()
    {
        Builder::macro("topVendors", function (){
            return $this->selectRaw(new Raw("*, vendors.id, vendors.slug, (SELECT COALESCE(count(temp.id), 0) FROM orders temp WHERE temp.product_offer_id = product_offers.id) as totalSoldOffer"))
                            ->join('product_offers', 'product_offers.vendor_id', '=', 'vendors.id')
                                ->groupBy('vendors.id')
                                    ->orderBy('totalSoldOffer', 'DESC');
        });
    }  

    private function registerNearestVendorsMacro()
    {
        $customerLat    =  get_instance()->session->location_lat ?? _DEFAULT_LAT_;
        $customerLon    =  get_instance()->session->location_lon ?? _DEFAULT_LON_;

        Builder::macro("nearestVendors", function () use ($customerLat, $customerLon) {
            return $this//->selectRaw(new Raw("*, product_offers.slug, product_offers.id"))
                            // ->join('vendors', 'vendors.id', '=', 'product_offers.vendor_id')
                                ->whereRaw("DISTANCEE({$customerLat}, {$customerLon}, `vendors`.`vendor_lat`, `vendors`.`vendor_lon`) <= `vendors`.`operational_area`");
        });
    }

    private function registerActiveVendorMacro()
    {
        Builder::macro("activeVendor", function (){
            return $this->where('vendors.status', 'active');
        });
    }

    public function freezeOffers()
    {
        foreach($this->productOffers as $offer)
        {
            $offer->status_before_freezing  = $offer->status;
            $offer->status                  = 'freezed';
            $offer->save();
        }
    }

    public function unFreezeOffers()
    {
        foreach($this->freezedProductOffers as $offer)
        {
            $offer->status                  = $offer->status_before_freezing;
            $offer->status_before_freezing  = null;
            $offer->save();
        }
    }

    public function delete()
    {
        $vendor_id = $this->id;

        if(parent::delete())
        {
            \Application\Models\ProductOffers::where('vendor_id', $vendor_id)->delete();
            return true;
        }
        return false;
    }

    public function save(array $options = [])
    {
        if(parent::save($options))
        {
            if($this->status == "deactive")
            {
                $this->freezeOffers();
            }
            elseif($this->status == "active")
            {
                $this->unFreezeOffers();
            }
            return true;
        }
        return false;
    }
}