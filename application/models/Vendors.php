<?php
namespace Application\Models;
if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Query\Expression as Raw;
use \Illuminate\Database\Query\Builder;

class Vendors extends Eloquent 
{
    use SoftDeletes;

    protected $table = "vendors";

    public function __construct()
    {
        parent::__construct();
        $this->registerTopVendorsMacro();
        $this->registerActiveVendorMacro();
    }

    public function user()
    {
        return $this->hasOne('Application\Models\Users', 'vendor_id');
    }

    public function subscriptions()
    {
    	return $this->belongsTo('Application\Models\SubscriptionHistory', 'current_subscription_id');
    }

    public function currentSubscription()
    {
    	return $this->hasOne('Application\Models\SubscriptionHistory', 'vendor_id')->where('subscription_status', 'subscribed')->limit(1);
    }

    public function lastSubscription()
    {
        return $this->hasOne('Application\Models\SubscriptionHistory', 'vendor_id')->orderBy('id', 'desc')->limit(1);
    }

    public function productOffers()
    {
        return $this->hasMany('Application\Models\ProductOffers', 'vendor_id')->where('status', 'active');
    }

    public function freezedProductOffers()
    {
        return $this->hasMany('Application\Models\ProductOffers', 'vendor_id')->where('status', 'freezed');
    }

    public function orders()
    {
        return $this->hasMany('Application\Models\Orders', 'vendor_id');
    }

    public function fullName()
    {
        return $this->user->first_name . ' ' . $this->user->last_name;
    }

    public function businessName()
    {
        return $this->business_name;
    }

    public function logo()
    {
        return $this->user->logo;
    }

    // public static function topAndNearVendors($take = 5)
    // {
    //     $customerLat    =  get_instance()->session->location_lat;
    //     $customerLon    =  get_instance()->session->location_lon;

    //     return  (new static)->select('vendors.*')
    //                         ->join('product_offers', 'vendors.id', '=', 'product_offers.vendor_id')
    //                         ->join('products', 'product_offers.product_id', '=', 'products.id')
    //                         ->join('orders', 'products.id', '=', 'orders.product_id')
    //                         ->groupBy('orders.product_id')
    //                         ->where('product_offers.status', 'active')
    //                         ->whereRaw("DISTANCEE({$customerLat}, {$customerLon}, `vendors`.`vendor_lat`, `vendors`.`vendor_lon`) <= `vendors`.`operational_area`")
    //                         ->orderBy(new Raw("COUNT(`orders`.`id`)"), 'DESC')
    //                         ->take($take)->get();
    // }

    private function registerTopVendorsMacro()
    {
        Builder::macro("topVendors", function (){
            return $this->selectRaw(new Raw("*, vendors.slug, (SELECT COALESCE(count(temp.id), 0) FROM orders temp WHERE temp.product_offer_id = product_offers.id) as totalSoldOffer"))
                            ->join('product_offers', 'product_offers.vendor_id', '=', 'vendors.id')
                                ->groupBy('vendors.id')
                                    ->orderBy('totalSoldOffer', 'DESC');
        });
    }

    private function registerActiveVendorMacro()
    {
        Builder::macro("activeVendor", function (){
            return $this->where('vendors.status', 'active');
        });
    }

    public function freezeOffers()
    {
        foreach($this->productOffers as $offer)
        {
            $offer->status_before_freezing  = $offer->status;
            $offer->status                  = 'freezed';
            $offer->save();
        }
    }

    public function unFreezeOffers()
    {
        foreach($this->freezedProductOffers as $offer)
        {
            $offer->status                  = $offer->status_before_freezing;
            $offer->status_before_freezing  = null;
            $offer->save();
        }
    }
}