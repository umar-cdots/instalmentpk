<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class ProductsMeta extends Eloquent 
{
	use SoftDeletes;
	
    protected $table = "products_meta";

    protected $fillable	= ['meta_key', 'meta_value'];

    function product()
    {
    	return $this->belongsTo('Application\Models\ProductsMeta', 'product_id');
    }
}