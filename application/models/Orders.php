<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Query\Expression as Raw;

class Orders extends Eloquent 
{
	use SoftDeletes;

    protected $table = "orders";

    public function customer()
    {
    	return $this->belongsTo('Application\Models\Customers', 'customer_id');
    }
	
	public function vendor()
    {
    	return $this->belongsTo('Application\Models\Vendors', 'vendor_id')->withTrashed();
    }
	
	public function product()
    {
    	return $this->belongsTo('Application\Models\Products', 'product_id')->withTrashed();
    }
	
	public function productOffer()
    {
    	return $this->belongsTo('Application\Models\ProductOffers', 'product_offer_id')->withTrashed();
    }

	public function phoneVerification()
    {
    	return $this->hasOne('Application\Models\PhoneVerifications', 'order_id');
    }

    public function physicalVerification()
    {
        return $this->hasOne('Application\Models\PhysicalVerifications', 'order_id');
    }

    public function guarantors()
    {
        return $this->hasMany('Application\Models\Guarantors', 'order_id');
    }
}