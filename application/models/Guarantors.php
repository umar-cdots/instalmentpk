<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Guarantors extends Eloquent 
{
	use SoftDeletes;

    protected $table = "guarantors";

    public function customer()
    {
    	return $this->belongsTo('Customers', 'customer_id');
    }
}