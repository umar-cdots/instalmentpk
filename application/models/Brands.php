<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Query\Expression as Raw;
use \Illuminate\Database\Query\Builder;

class Brands extends Eloquent 
{
	use SoftDeletes;

    protected $table = "brands";

    public function __construct()
    {
        parent::__construct();
        $this->registerTopBrandsMacro();
    }

    public function products()
    {
        return $this->hasMany('Application\Models\Products', 'brand_id');
    }

    public function logo()
    {
    	return $this->belongsTo('Application\Models\Uploads', 'upload_id');
    }

    // public static function topAndNearBrands($take = 5)
    // {
    //     $customerLat    =  get_instance()->session->location_lat;
    //     $customerLon    =  get_instance()->session->location_lon;

    //     return  (new static)->select('brands.*')
    //     					->join('products', 'brands.id', '=', 'products.brand_id')
    //                         ->join('orders', 'products.id', '=', 'orders.product_id')
    //                         ->join('product_offers', 'products.id', '=', 'product_offers.product_id')
    //                         ->join('vendors', 'product_offers.vendor_id', '=', 'vendors.id')
    //                         ->groupBy('orders.product_id')
    //                         ->where('product_offers.status', 'active')
    //                         ->whereRaw("DISTANCEE({$customerLat}, {$customerLon}, `vendors`.`vendor_lat`, `vendors`.`vendor_lon`) <= `vendors`.`operational_area`")
    //                         ->orderBy(new Raw("COUNT(`orders`.`id`)"), 'DESC')
    //                         ->take($take)->get();
    // }

    private function registerTopBrandsMacro()
    {
        Builder::macro("topBrands", function (){
            return $this->selectRaw(new Raw("*, brands.slug, (SELECT COALESCE(count(temp.id), 0) FROM orders temp WHERE temp.product_offer_id = product_offers.id) as totalSoldOffer"))
                            ->join('products', 'products.brand_id', '=', 'brands.id')
                                ->join('product_offers', 'product_offers.product_id', '=', 'products.id')
                                    ->groupBy('brands.id')
                                        ->orderBy('totalSoldOffer', 'DESC');
        });
    }
}