<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptionHistory extends Eloquent 
{
	use SoftDeletes;

    protected $table = "subscription_history";

    public function subscriptionPackage()
    {
    	return $this->belongsTo('Application\Models\SubscriptionPackages', 'subscription_package_id');
    }

    public function vendor()
    {
    	return $this->belongsTo('Application\Models\Vendors', 'vendor_id');
    }

    public function daysLeft()
    {
        return \Carbon\Carbon::parse($this->subscription_exp_date)->diffInDays(\Carbon\Carbon::now());
    }

    public function expireMe()
    {
        $this->subscription_status = 'expired';
        $this->save();
    }
}