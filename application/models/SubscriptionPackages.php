<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptionPackages extends Eloquent 
{
	use SoftDeletes;
	
    protected $table = "subscription_packages";

    public function subscriptionHistory()
    {
    	$this->hasMany('Application\Models\Subscription_History', 'subscription_package_id');
    }
}