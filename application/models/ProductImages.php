<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class ProductImages extends Eloquent 
{
    protected $table = "product_images";

    public function product()
    {
    	return $this->belongsTo('Application\Models\Products', 'product_id');
    }
}