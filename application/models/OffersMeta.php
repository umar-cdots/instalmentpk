<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class OffersMeta extends Eloquent 
{
	use SoftDeletes;
	
    protected $table = "offers_meta";

    protected $fillable	= ['meta_key', 'meta_value'];

    function offer()
    {
    	return $this->belongsTo('Application\Models\OffersMeta', 'product_offer_id');
    }
}