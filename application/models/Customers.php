<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customers extends Eloquent 
{
    use SoftDeletes;
    
    protected $table = "customers";

    public function user()
    {
		
    	return $this->hasOne('Application\Models\Users', 'customer_id');
    }

    public function guarantors()
    {
    	return $this->hasMany('Application\Models\Guarantors', 'customer_id');
    }
}