<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class CMS extends Eloquent 
{
    protected $table = "cms";

    protected $fillable = ['page_title', 'page_content'];
}