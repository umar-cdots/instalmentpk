<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class SeoMeta extends Eloquent 
{
    protected $table = "seo_meta";

    public function type()
    {
    	if($this->type == 'brand')
    		return $this->belongsTo('Application\Models\Brands');
    	elseif($this->type == 'category')
    		return $this->belongsTo('Application\Models\ProductCategories');
    	elseif($this->type == 'cms')
    		return $this->belongsTo('Application\Models\CMS');
    	elseif($this->type == 'product')
    		return $this->belongsTo('Application\Models\Products');
    	elseif($this->type == 'vendor')
    		return $this->belongsTo('Application\Models\Vendors');
    }
}