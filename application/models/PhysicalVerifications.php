<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class PhysicalVerifications extends Eloquent 
{
	use SoftDeletes;

    protected $table = "physical_verifications";
}