<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Query\Expression as Raw;
use \Illuminate\Database\Query\Builder;

class ProductOffers extends Eloquent 
{
	use SoftDeletes;

    protected $table = "product_offers";

    public function __construct()
    {
        parent::__construct();
        $this->registerNearestOffersMacro();
        $this->registerActiveOffersMacro();
        $this->registerTopOffersMacro();
        $this->registerSearchMacro();
        $this->registerSingleCheapestMacro();
        $this->registerSingleCheapestByVendorMacro();
        $this->registerOrderByCheapestMacro();
        $this->registerOrderByHighestMacro();
        $this->registerAmbiguityFreeSelectionMacro();
        $this->registerFeaturedOffersMacro();
        $this->registerCategoryRelivantOffersMacro();
    }

    public function product()
    {
    	return $this->belongsTo('Application\Models\Products', 'product_id')->withTrashed();
    }

    public function vendor()
    {
    	return $this->belongsTo('Application\Models\Vendors', 'vendor_id');
    }

    public function orders()
    {
        return $this->hasMany('Application\Models\Orders', 'product_offer_id');
    }

    public function meta()
    {
        return $this->hasMany('Application\Models\OffersMeta', 'product_offer_id');
    }

    // public static function nearCustomer()
    // {
    //     $customerLat    =  get_instance()->session->location_lat;
    //     $customerLon    =  get_instance()->session->location_lon;

    //     return  (new static)->select('product_offers.*')
    //                             ->join('vendors', 'vendors.id', '=', 'product_offers.vendor_id')
    //                                 ->where('product_offers.status', 'active')
    //                                     ->whereRaw("DISTANCEE({$customerLat}, {$customerLon}, `vendors`.`vendor_lat`, `vendors`.`vendor_lon`) <= `vendors`.`operational_area`");
    // }

    private function registerNearestOffersMacro()
    {
        $customerLat    =  get_instance()->session->location_lat ?? _DEFAULT_LAT_;
        $customerLon    =  get_instance()->session->location_lon ?? _DEFAULT_LON_;

        Builder::macro("nearestOffers", function () use ($customerLat, $customerLon) {
            return $this//->selectRaw(new Raw("*, product_offers.slug, product_offers.id"))
                            ->join('vendors', 'vendors.id', '=', 'product_offers.vendor_id')
                                ->whereRaw("DISTANCEE({$customerLat}, {$customerLon}, `vendors`.`vendor_lat`, `vendors`.`vendor_lon`) <= `vendors`.`operational_area`");
        });
    }

    private function registerActiveOffersMacro()
    {
        Builder::macro("activeOffers", function (){
            return $this->join('products', 'product_offers.product_id', '=', 'products.id')
                            ->where('product_offers.status', 'active')
                                ->where('products.deleted_at', null);
        });
    }

    private function registerCategoryRelivantOffersMacro()
    {
        Builder::macro("categoryOffers", function ($category_id){
            if(is_array($category_id))
                return $this->join('product_to_category', 'product_offers.product_id', '=', 'product_to_category.product_id')
                            ->whereIn('product_to_category.category_id', $category_id);

            return $this->join('product_to_category', 'product_offers.product_id', '=', 'product_to_category.product_id')
                            ->where('product_to_category.category_id', $category_id);
        });
    }

    private function registerTopOffersMacro()
    {
        Builder::macro("topOffers", function (){
            return $this->selectRaw(new Raw("(SELECT COALESCE(count(temp.id), 0) FROM orders temp WHERE temp.product_offer_id = product_offers.id) as totalSoldOffer"))
                            ->orderBy('totalSoldOffer', 'DESC');
        });
    }

    private function registerSearchMacro()
    {
        Builder::macro("search", function ($keyword){
            return $this->where('tp.title', 'LIKE', '%' . $keyword . '%')
                            ->join('products AS tp', 'product_offers.product_id', '=', 'tp.id');
        });
    }

    private function registerSingleCheapestMacro()
    {
        Builder::macro("singleCheapest", function (){
            $product_offer_ids = collect(\Illuminate\Database\Capsule\Manager::select("SELECT t1.`id` FROM `product_offers` t1 LEFT JOIN `product_offers` t2 ON t1.`product_id` = t2.`product_id` AND t1.`amount_per_month` > t2.`amount_per_month` WHERE t2.`amount_per_month` IS NULL GROUP BY t1.`product_id`"))->pluck('id')->all();

            return $this->whereIn('product_offers.id', $product_offer_ids)
                            ->groupBy('product_offers.product_id')
                                ->orderBy('amount_per_month')
                                    ->orderBy('down_payment')
                                        ->orderBy('total_installments', 'DESC');
        });
    }

    private function registerSingleCheapestByVendorMacro()
    {
        Builder::macro("singleCheapestByVendor", function ($vendor_id){
            $product_offer_ids = collect(\Illuminate\Database\Capsule\Manager::select("SELECT t1.`id` FROM `product_offers` t1 LEFT JOIN `product_offers` t2 ON t1.`product_id` = t2.`product_id` AND t1.`amount_per_month` > t2.`amount_per_month` WHERE t1.`vendor_id` = $vendor_id GROUP BY t1.`product_id`"))->pluck('id')->all();

            return $this->whereIn('product_offers.id', $product_offer_ids)
                                ->orderBy('amount_per_month')
                                    ->orderBy('down_payment')
                                        ->orderBy('total_installments', 'DESC');
        });
    }

    private function registerOrderByCheapestMacro()
    {
        Builder::macro("orderByCheapest", function (){
            return $this->orderBy('amount_per_month')
                            ->orderBy('down_payment')
                                ->orderBy('total_installments', 'DESC');
        });
    }

    private function registerOrderByHighestMacro()
    {
        Builder::macro("orderByHighest", function (){
            return $this->orderBy('amount_per_month', 'DESC')
                            ->orderBy('down_payment', 'DESC')
                                ->orderBy('total_installments');
        });
    }

    private function registerAmbiguityFreeSelectionMacro()
    {
        Builder::macro("ambiguityFreeSelection", function (){
            return $this->selectRaw(new Raw("*, product_offers.slug, product_offers.id"));
        });
    }

    private function registerFeaturedOffersMacro()
    {
        Builder::macro("featuredOffers", function (){
            return $this->where('product_offers.is_featured', '1');
        });
    }
}