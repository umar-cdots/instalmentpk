<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Query\Expression as Raw;
use \Illuminate\Database\Query\Builder;

class Products extends Eloquent 
{
	use SoftDeletes;

    protected $table = "products";

    public function __construct()
    {
        parent::__construct();
        $this->registerTopProductsMacro();
        $this->registerSearchMacro();
    }

    public function productOffers()
    {
    	return $this->hasMany('Application\Models\ProductOffers', 'product_id');
    }

    public function latestProductOffers()
    {
        return $this->hasMany('Application\Models\ProductOffers', 'product_id')
                        ->orderBy('id', 'DESC');
    }

    public function productCategories()
    {
    	return $this->belongsToMany('Application\Models\ProductCategories', 'product_to_category', 'product_id', 'category_id');
    }

    public function brand()
    {
        return $this->belongsTo('Application\Models\Brands', 'brand_id');
    }

    public function productImages()
    {
        return $this->hasMany('Application\Models\ProductImages', 'product_id');
    }

    public function orders()
    {
        return $this->hasMany('Application\Models\Orders', 'product_id');
    }

    public function meta()
    {
        return $this->hasMany('Application\Models\ProductsMeta', 'product_id');
    }

    // public static function topAndNearProducts($take = 5)
    // {
    //     $customerLat    =  get_instance()->session->location_lat;
    //     $customerLon    =  get_instance()->session->location_lon;

    //     return  (new static)->select('products.*')
    //                         ->join('orders', 'products.id', '=', 'orders.product_id')
    //                         ->join('product_offers', 'products.id', '=', 'product_offers.product_id')
    //                         ->join('vendors', 'product_offers.vendor_id', '=', 'vendors.id')
    //                         ->groupBy('orders.product_id')
    //                         ->whereRaw("DISTANCEE({$customerLat}, {$customerLon}, `vendors`.`vendor_lat`, `vendors`.`vendor_lon`) <= `vendors`.`operational_area`")
    //                         ->orderBy(new Raw("COUNT(`orders`.`id`)"), 'DESC')
    //                         ->take($take)->get();
    // }

    private function registerTopProductsMacro()
    {
        Builder::macro("topProducts", function (){
            return $this->selectRaw(new Raw("*, products.id, products.slug, (SELECT COALESCE(count(temp.id), 0) FROM orders temp WHERE temp.product_offer_id = product_offers.id) as totalSoldOffer"))
                            ->join('product_offers', 'product_offers.product_id', '=', 'products.id')
                                ->groupBy('products.id')
                                    ->orderBy('totalSoldOffer', 'DESC');
        });
    }

    private function registerSearchMacro()
    {
        Builder::macro("search", function ($keyword){
            return $this->where(new Raw('LOWER(products.title)'), 'LIKE', '%' . strtolower($keyword) . '%');
        });
    }
}