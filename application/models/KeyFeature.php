<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class KeyFeature extends Eloquent 
{
    protected $table = "key_features";

    public function productCategory()
    {
    	return $this->belongsTo('Application\Models\ProductCategories', 'product_category_id');
    }

    // public function products()
    // {
    // 	return $this->hasManyThrough('Application\Models\Products', 'Application\Models\ProductToCategory', 'category_id', 'id', 'product_category_id', 'product_idd');
    // }
}