<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Query\Expression as Raw;
use \Illuminate\Database\Query\Builder;

class ProductCategories extends Eloquent 
{
	use SoftDeletes;

    protected $table = "product_categories";

    public function __construct()
    {
         parent::__construct();
        // $this->registerTopCategoriesMacro();
        // $this->registerNearestCategoriesMacro();
    }

    public function parentCategory()
    {
        return $this->belongsTo('Application\Models\ProductCategories', 'parent_id');
    }

    public function childCategories()
    {
        return $this->hasMany('Application\Models\ProductCategories', 'parent_id');
    }

    // private function registerTopCategoriesMacro()
    // {
    //     Builder::macro("topCategories", function (){
    //         return $this->selectRaw(new Raw("*, brands.slug, (SELECT COALESCE(count(temp.id), 0) FROM orders temp WHERE temp.product_offer_id = product_offers.id) as totalSoldOffer"))
    //                         ->join('product_offers', 'product_offers.id', '=', 'temp.product_offer_id')
    //                             ->join('products', 'products.id', '=', 'product_offers.product_id')
    //                                 // ->groupBy('brands.id')
    //                                 ->whereRaw("products.id in ()")
    //                                     ->orderBy('totalSoldOffer', 'DESC');
    //     });
    // }   

    // private function registerNearestCategoriesMacro()
    // {
    //     $customerLat    =  get_instance()->session->location_lat;
    //     $customerLon    =  get_instance()->session->location_lon;

    //     Builder::macro("nearestCategories", function () use ($customerLat, $customerLon) {
    //         return $this//->selectRaw(new Raw("*, product_offers.slug, product_offers.id"))
    //                         ->join('vendors', 'vendors.id', '=', 'product_offers.vendor_id')
    //                             ->whereRaw("DISTANCEE({$customerLat}, {$customerLon}, `vendors`.`vendor_lat`, `vendors`.`vendor_lon`) <= `vendors`.`operational_area`");
    //     });
    // }


    public function getAllParentCategories()
    {
        return $this->selectRaw(new Raw("id, category_title, slug, parent_id"))
            ->from(new Raw("(SELECT id, category_title, slug, parent_id,
            CASE WHEN id = {$this->id} THEN @id := parent_id
                 WHEN id = @id THEN @id := parent_id
            END as checkId FROM {$this->table} WHERE deleted_at IS NULL ORDER BY id DESC)T "))
                ->whereRaw("checkId IS NOT NULL")
                    ->withTrashed();
    }


    public function getAllChildCategories()
    {
        return $this->selectRaw(new Raw("id, category_title, slug, parent_id"))
            ->from(new Raw("(SELECT id, category_title, slug, parent_id,
            CASE WHEN id = {$this->id} THEN @idlist := CONCAT(id)
            WHEN FIND_IN_SET(parent_id, @idlist) THEN @idlist := CONCAT(@idlist, ',', id)
            END as checkId FROM {$this->table} WHERE deleted_at IS NULL ORDER BY id ASC)T "))
                ->whereRaw("checkId IS NOT NULL")
                    ->withTrashed();
    }

    public function seoMeta()
    {
        return $this->hasOne('Application\Models\SeoMeta', 'type_id')
                        ->where('type', 'category');
    }
}