<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategories extends Eloquent 
{
	use SoftDeletes;

    protected $table = "product_categories";

    public function parentCategory()
    {
        return $this->belongsTo('Application\Models\ProductCategories', 'parent_id');
    }

    public function childCategories()
    {
        return $this->hasMany('Application\Models\ProductCategories', 'parent_id');
    }
}