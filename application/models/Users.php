<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Eloquent 
{
    use SoftDeletes;

	const _STATUS_ACTIVE_ 						= "active";
	const _STATUS_DEACTIVE_ 					= "deactive";
	const _STATUS_EMAIL_VERIFICATION_PENDING_ 	= "email_verification_pending";
	const _STATUS_SMS_VERIFICATION_PEDNING_ 	= "sms_verification_pending";

	const _USER_TYPE_SUPER_ADMIN_				= "super_admin";
	const _USER_TYPE_ADMIN_						= "admin";
	const _USER_TYPE_VERIFICATION_OFFICER_		= "verification_officer";
	const _USER_TYPE_VENDOR_					= "vendor";
	const _USER_TYPE_CUSTOMER_					= "customer";

    protected $table 	= "users";

    public function customer()
    {
    	return $this->belongsTo('Application\Models\Customers', 'customer_id');
    }

    public function vendor()
    {
    	return $this->belongsTo('Application\Models\Vendors', 'vendor_id');
    }

    public function loginHistory()
    {
    	return $this->hasMany('Application\Models\LoginHistory', 'user_id');
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function businessName()
    {
        return $this->vendor->businessName();
    }

    public function logo()
    {
        return $this->belongsTo('Application\Models\Uploads', 'upload_id');
    }
}