<?php
namespace Application\Models;
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-07-23 17:46:40
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-09-25 18:36:24
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Ledger extends Eloquent 
{
	use SoftDeletes;

    protected $table = "ledger";

    public function user()
    {
        return $this->belongsTo('Application\Models\Users', 'user_id')->withTrashed();
    }
    
    public function productOffer()
    {
        return $this->belongsTo(ProductOffers::class, 'product_id', 'id')->withTrashed();
    }
    
    public function subscriptionHistory()
    {
        return $this->belongsTo(SubscriptionHistory::class, 'product_id', 'id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Products::class, 'product_id')->withTrashed();
    }
}