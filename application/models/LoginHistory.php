<?php
namespace Application\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class LoginHistory extends Eloquent 
{
    protected $table = "login_history";

    public function user()
    {
    	return $this->belongsTo('Application\Models\Users', 'user_id');
    }
}